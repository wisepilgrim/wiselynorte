/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { Navigation } from 'react-native-navigation';
import { Component } from 'react';
import { Platform } from 'react-native';

import { registerScreens } from './Scenes';
import { Colors, Images } from './Themes';

registerScreens();

const navigatorStyle = {
  drawUnderNavBar: false,
  navBarTranslucent: false,
  navBarHidden: false,
  navBarTextFontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
  navBarTextFontSize: 22,
  topBarElevationShadowEnabled: true,
  navBarLeftButtonColor: Colors.hamburger,
  navBarTextColor: Colors.hamburger,
  navBarBackgroundColor: Colors.snow,
  navBarButtonColor: Colors.hamburger,
  statusBarTextColorScheme: 'light',
  statusBarTextColorSchemeSingleScreen: 'dark'
};

const navigatorButtons = {
  leftButtons: [
    {
      id: 'sideMenu',
      buttonColor: Colors.hamburger,
      icon: Images.menu
    }
  ]
};

class App extends Component {
  constructor(props) {
    super(props);
    this.startApp();
    console.ignoredYellowBox = ['Setting a timer'];
  }

  startApp() {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'home',
        //titleImage: require('./Scenes/assets/icons/drawerHeaderIcon.png'),
        // TODO: Rename Page
        title: 'The Camino DEL NORTE',
        navigatorStyle: navigatorStyle,
        navigatorButtons: navigatorButtons
      },
      drawer: {
        left: {
          screen: 'drawer',
          passProps: {}
        }
      },
      type: 'TheSideBar',
      animationType: 'fade'
    });
  }
}

export default App;
