nodes = [{
    "node": {
        "title": "Citadel",
        "latitude": "43.162840",
        "longitude": "-1.234971",
        "nid": "8517",
        "nodetype": "POI",
        "body": "To get a unique perspective over the plan of St. Jean, and to warm up your legs for the hike ahead of you, make a visit to the fortifications that loom over the village.\n\nBegun in the early 1600's, and improved steadily since, the Citadel is actually connected to the fortified walls surrounding the village. It is now a school, and sadly not open to the public. That said, plenty of the grounds that surround it are public areas and a visit is encouraged.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Castle.png"
    }
}, {
    "node": {
        "title": "Rue de la Citadelle",
        "latitude": "43.162321",
        "longitude": "-1.237260",
        "nid": "8516",
        "nodetype": "POI",
        "body": "The Rue de la Citadelle is an 18th. c. creation and many of the original houses that lined this cobbled street can still be enjoyed.\u00a0It is the historical center of town and the location of both the pilgrims office, many pilgrim albergues, and the Iglesia de Santa Maria.\n\nThe Rue ends at the Porte d'Espagne and a bridge over the River Nive. To the West of the river lies the newer part of town.\n\nWhen walking along this cobbled road try to look beyond the modern signs doing their best to capture your attention and wallet, and search for the old carvings in the stone that once upon a time served a similar purpose.\n\n",
        "image": "8516-67159.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "\u00c9glise de Notre Dame du Bout du Pont",
        "latitude": "43.162491",
        "longitude": "-1.237096",
        "nid": "8518",
        "nodetype": "POI",
        "body": "More commonly referred to as the Iglesia de Santa Maria, this 14th. c. Gothic structure sits at the end of the Rue de la Citadelle. \u00a0\nThe original was built in the early 13th. c. by Sancho the Strong of Navarre. Sancho the Strong (and the Prudent and the Retired) was not only a man of many nicknames but also a giant. Standing at over 2.2m it is no wonder that he managed to command an army capable of holding back a moorish invasion. It is exactly such a battle, that of Tolosa in Andalucia that this church was built in celebration of.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Casa Sabina for Dinner",
        "latitude": "43.009357",
        "longitude": "-1.320700",
        "nid": "8222",
        "nodetype": "POI",
        "body": "Casa Sabina, the bar nearest the albergue, has long been the place to break with ice with your fellow pilgrims. Drop in when you arrive to put your name on the dinner list, which is 9\u20ac.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Real Colegiata de Roncesvalles & Museum",
        "latitude": "43.009715",
        "longitude": "-1.319818",
        "nid": "7409",
        "nodetype": "POI",
        "body": "Sometimes referred to as the Colegiata de Santa Mar\u00eda.\n\nBuilt in 1219 and restored in the early 40s. The cloister is from the 17th. c. and replaces the Gothic version which was destroyed by the weather. It houses the tomb of it\u2019s creater, the Navarran King Sancho VII el Fuerte (the Strong). The adjoined museum is home to some notable piece, including several reliquaries that contain bits and pieces of several saints, thorns from the Crown of Thorns, and Oliphant, the carved ivory battle horn used in vain by Roland to call Charlemagne.\n\nThe church is also the final resting place of Sancho, who is buried in the chapel of Saint Augustine.\n\n",
        "image": "7409-134834.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Capilla de Santiago",
        "latitude": "43.008730",
        "longitude": "-1.319561",
        "nid": "8096",
        "nodetype": "POI",
        "body": "Also called the Silo of Charlemagne, the jury is out on whether it contains the remains of pilgrims or warriers or both. It dates from the 12th c. with modern additions.\n\n",
        "image": "8096-60996.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "San Nicolas de Bari",
        "latitude": "42.989032",
        "longitude": "-1.335815",
        "nid": "7410",
        "nodetype": "POI",
        "image": "7410-60997.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "San Bartolome",
        "latitude": "42.979062",
        "longitude": "-1.369182",
        "nid": "8097",
        "nodetype": "POI",
        "image": "8097-60999.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "San Pedro",
        "latitude": "42.967212",
        "longitude": "-1.417440",
        "nid": "7411",
        "nodetype": "POI",
        "body": "13th c. The church appears to have been closed when last visited. Please leave a note in the comments if you discover otherwise.\n\n",
        "image": "7411-61000.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Puente de la Rabia",
        "latitude": "42.929851",
        "longitude": "-1.503389",
        "nid": "7412",
        "nodetype": "POI",
        "body": "Tradition holds that this bridge across the river Arga on the way into Zubiri contains a relic of Santa Quiteria, whose strength lies in it's ability to cure rabies in the animals who circle the central pillar 3 times.\n\n",
        "image": "7412-134853.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Bridge.png"
    }
}, {
    "node": {
        "title": "Ermita de San Nicol\u00e1s",
        "latitude": "42.901688",
        "longitude": "-1.541205",
        "nid": "7545",
        "nodetype": "POI",
        "image": "7545-61011.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Esteban y Stephan",
        "latitude": "42.901688",
        "longitude": "-1.541205",
        "nid": "7546",
        "nodetype": "POI",
        "body": "13th. c Romanesque church. Very much worth a visit\/exploration. To receive a well informed tour of the locked church, ring the bell of the adjoined building. One of the four Sisters that live there will unlock the door with a smile, and can easily provide the tour in English. One of the churches two bells is the oldest in Navarra. It also houses the second Santiago Peregrino to be found on the Franc\u00e9s.\n\n",
        "image": "7546-61012.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "La Ciudadela",
        "latitude": "42.812149",
        "longitude": "-1.649258",
        "nid": "8533",
        "nodetype": "POI",
        "body": "Like the one in St. Jean Pied de Port, the Citadel of Pamplona was constructed to the design of the French military engineer S\u00e9bastien Le Prestre de Vauban. The French evidently new more about it than it's own defenders, and in 1808 Napoleon added Pamplona to his growing list of conquered cities.\n\nToday the citadel is surrounded by Pamplona's largest park and is a common place to find pilgrims and locals alike enjoying a bit of nature. The camino passes by the citadel but doesn't get close enough. For the full experience be sure to walk its full circumference.\n\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Castle.png"
    }
}, {
    "node": {
        "title": "Ayuntamiento - Town Hall",
        "latitude": "42.818311",
        "longitude": "-1.644680",
        "nid": "7418",
        "nodetype": "POI",
        "body": "Town halls are not usually part of a cultural agenda, but the Baroque facade of Pamplona\u2019s is the subject of countless sketchbooks and worthy of a pause.\n\n",
        "image": "7418-61066.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Camara de Comptos Reales",
        "latitude": "42.817623",
        "longitude": "-1.645559",
        "nid": "7420",
        "nodetype": "POI",
        "body": "Literally the \"Counting House,\" the Camara de Comptos Reales is notable for having been the coin mint for the region of Navarra during the 14th. c. It was created by Carlos II of Navarra to control the finances of the region. It survived as the treasury of Navarra until a Royal decree made it obsolete in 1836, due in large part to the annexation of Navarra by neighboring Castille.\n\nIn 1980 it resumed, albeit in a more advisory manner, many of it's old duties.\n\nIt remains as the only civil gothic structure in Pamplona.\n\nYou can visit the interior patio for free Monday to Friday between 8:00 and 15:00. Guided visits are also available.\n\n",
        "image": "7420-134846.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Diocesan Museum",
        "latitude": "42.819293",
        "longitude": "-1.641324",
        "nid": "7417",
        "nodetype": "POI",
        "body": "14th. c. part of the Cathedral of Pamplona which was the kitchen of the dining hall. Artifacts include several reliquaries and processional crosses, as well as an impressive collection of Virgin Mary statues. Admission is included with the admission to the cathedral.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Parque de la Taconera",
        "latitude": "42.816825",
        "longitude": "-1.651715",
        "nid": "8534",
        "nodetype": "POI",
        "body": "The Parque de la Taconera is along the camino when you leave the old town. It preserves many of the old city gates, has a small zoo, and is generally a good place to stroll among the locals.\n\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Saturnino",
        "latitude": "42.817788",
        "longitude": "-1.644886",
        "nid": "7419",
        "nodetype": "POI",
        "body": "With an interior that is far more exciting than it's exterior the mostly 13th c. Gothic. church is worth a visit. A Santiago Peregrino makes it all worthwhile. From the outside the church has elements of a fortress. These are additions made during the period when Pamplona was at war with itself.\n\n",
        "image": "7419-61203.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Cathedral of Pamplona",
        "latitude": "42.819573",
        "longitude": "-1.641464",
        "nid": "7416",
        "nodetype": "POI",
        "body": "14th. c. Gothic structure which replaced its Romanesque predecessor after it collapsed a decade earlier. The towers have been outfitted as part of a museum and contains interactive screens and the original \u201cCaldera\u201d sculptures which adorned the facade. It also affords a rare glimpse of the interstitial between the side nave and the roof, with a demonstration of the tools used to build the cathedral. Admission is 3\u20acfor pilgrims, and includes the Diocesan Museum.\u00a0\n",
        "image": "7416-61068.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Museo de Navarra",
        "latitude": "42.819429",
        "longitude": "-1.646005",
        "nid": "7421",
        "nodetype": "POI",
        "body": "Formerly housed in the C\u00e1mara de Comptos the museum found is current home, inside the former Hospital of Our Lady of Mercy, in 1956. The collection is exactly as the name would indicate, culturally and historically significant artifacts and artwork from Navarra.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Miguel",
        "latitude": "42.788710",
        "longitude": "-1.674447",
        "nid": "7422",
        "nodetype": "POI",
        "body": "13th. c. and heavily fortified, it was part of the nearby Monasterio de San Juan de Jerusalem, a military order often referred to as the Order of Malta.\n\nSaint Michael the Archangel is commonly represented as a protector, and churches named after him are frequently found to be more fortified than the average church.\n\n",
        "image": "7422-134845.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de La Asunci\u00f3n",
        "latitude": "42.708430",
        "longitude": "-1.760849",
        "nid": "7423",
        "nodetype": "POI",
        "body": "16-18th. c. Santiago Peregrino\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Esteban",
        "latitude": "42.689559",
        "longitude": "-1.770655",
        "nid": "7424",
        "nodetype": "POI",
        "body": "14-17th. C\n",
        "image": "7424-61202.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Juan Bautista de Obanos",
        "latitude": "42.679541",
        "longitude": "-1.786212",
        "nid": "8100",
        "nodetype": "POI",
        "image": "8100-61058.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Puente",
        "latitude": "42.673037",
        "longitude": "-1.810724",
        "nid": "7428",
        "nodetype": "POI",
        "body": "You can't miss the bridge as it is your way out of town. It is also the namesake, having been built by the queen in the 11th c. to ensure the safety of pilgrims who would otherwise have to brave the river and the bandits that stalked it banks.\n\nFor a better view of the full span, go to the modern bridge that runs parallel. When the water is at the right level, the reflections of the arches form perfect circles.\n\n",
        "image": "7428-61041.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Bridge.png"
    }
}, {
    "node": {
        "title": "Iglesia del Crucifijo \/ Monasterio de los Padres Reparadores",
        "latitude": "42.673037",
        "longitude": "-1.810724",
        "nid": "7427",
        "nodetype": "POI",
        "body": "Since the 1950s these two structures have been joined by the porch which serves as a sort of gate to the city center and the Calle Mayor. Now two combined structures, the Iglesia del Crucifijo is the older of the two and dates from the 12th. c. It\u2019s history is long and colorful. The monastery dates from the 18th. c. and is run by the Padres Reparadores, the same Germans who offer year round hospitality in the nearby Albergue de Los Padres Reparadores.\n\nPreviously called Santa Maria de las Huertas, the current name is attributed to the rather unusual crucifix inside.\u00a0\n",
        "image": "7427-61048.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santiago",
        "latitude": "42.672242",
        "longitude": "-1.814439",
        "nid": "8099",
        "nodetype": "POI",
        "body": "The evident mixture of styles reflect the various additions the church which dates from the 12th. c. The majority of the church dates from the 16th. c. Known for it's sculpture of Black Santiago.\n\n",
        "image": "8099-61053.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Pedro",
        "latitude": "42.669722",
        "longitude": "-1.862822",
        "nid": "8536",
        "nodetype": "POI",
        "body": "Late 18th. c.\n\n",
        "image": "8536-67952.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Catalina de Alejandr\u00eda",
        "latitude": "42.676701",
        "longitude": "-1.891301",
        "nid": "7562",
        "nodetype": "POI",
        "body": "13th. c. \u00a0\u00a0\u00a0\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Rom\u00e1n",
        "latitude": "42.675796",
        "longitude": "-1.891802",
        "nid": "7561",
        "nodetype": "POI",
        "body": "12th. c. \u00a0\u00a0\u00a0Well attended by the village which offers tours in various languages. A milario which dates from more than 2000 years is preserved here.\n\n",
        "image": "7561-133874.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Salvador",
        "latitude": "42.673261",
        "longitude": "-1.944414",
        "nid": "8538",
        "nodetype": "POI",
        "body": "Late 12th. c.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de la Asunci\u00f3n Villatuerta",
        "latitude": "42.657874",
        "longitude": "-1.994276",
        "nid": "7563",
        "nodetype": "POI",
        "image": "7563-61022.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Palacio de los Reyes de Navarra",
        "latitude": "42.669196",
        "longitude": "-2.029590",
        "nid": "7567",
        "nodetype": "POI",
        "body": "12th. c.\n\n",
        "image": "7567-61077.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia del Santo Sepulcro",
        "latitude": "42.669616",
        "longitude": "-2.024145",
        "nid": "8101",
        "nodetype": "POI",
        "body": "The Santo Sepulcro church is the first church that you will see when entering Estella. Unfortunately it is closed to the public, but the 14th. c. Gothic facade offers plenty of curiosities to hold your attention.\n\n",
        "image": "8101-67990.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Juan",
        "latitude": "42.671991",
        "longitude": "-2.031307",
        "nid": "8542",
        "nodetype": "POI",
        "image": "8542-133881.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Pedro de la R\u00faa",
        "latitude": "42.668765",
        "longitude": "-2.029145",
        "nid": "7566",
        "nodetype": "POI",
        "body": "12th. c. Guided tours are available, and visits are in the evenings, ask in the tourist office opposite. [In the Palacio de los Reyes de Navarra]\n",
        "image": "7566-134861.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Bas\u00edlica Nuestra Se\u00f1ora del Puy",
        "latitude": "42.675510",
        "longitude": "-2.030094",
        "nid": "8541",
        "nodetype": "POI",
        "body": "Star shaped basilica built on a ridge above the city where legends tells of the discovery of the image of the Virgin.\n\n",
        "image": "8541-134864.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Miguel Arc\u00e1ngel",
        "latitude": "42.670872",
        "longitude": "-2.028415",
        "nid": "7568",
        "nodetype": "POI",
        "body": "12th. c. \u00a0The church has a formidable presence, and side which faces the river looks more like a fortress than a place of worship. The Northern facade is considered the example of the Romanesque style in Navarra.\n\nHours\nMonday to Saturday: 11:00 to 13:00 and 17:00 to 20:00\nSundays and Holidays: 17:00 to 20:00\n",
        "image": "7568-61080.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia Santa Mar\u00eda Jus del Castillo",
        "latitude": "42.668667",
        "longitude": "-2.027007",
        "nid": "8543",
        "nodetype": "POI",
        "body": "Perched on the hill behind the albergue at the entrance to the town. To get there, take the stairs up to the main road and uphill just a short distance\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Castillo de Monjardin \/ Ermita de San Esteban",
        "latitude": "42.633830",
        "longitude": "-2.106307",
        "nid": "8556",
        "nodetype": "POI",
        "body": "Perched high on the mountain to the North is the Castillo de Monjardin, or rather the Ermita de San Esteban that now occupies the site.\n\nIt is accessible by road and is worth the climb if a vista is what you seek.\u00a0\n",
        "image": "8556-134865.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Castle.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Andres",
        "latitude": "42.628865",
        "longitude": "-2.103619",
        "nid": "8557",
        "nodetype": "POI",
        "body": "Open all day, with mass at 1pm on Sundays and 9:30 the rest of the week. It is also holds the Cross of Monjardin, which is a 12th. c. silver cross with an interesting fable attached. A shepherd in the area came upon a goat which would not move. Using his sling to frighten away vultures, he inadvertently struck the silver cross with one of his stones. The mistake was an honest one, as it was he that discovered the very existence of the cross. Nonetheless, the damage done to it was enough to cause him concern, and he swore that he would rather lose the loss of his own arm than to have damaged the cross. The next part of the tale is predictable enough and a reminder to everyone around that higher powers controlled our destiny.\n\n",
        "image": "8557-68456.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "The Convento de Concepcionistas",
        "latitude": "42.570955",
        "longitude": "-2.193272",
        "nid": "8571",
        "nodetype": "POI",
        "body": "This 17th. c. Church originally belonged to the Capuchinos. It is rather understated and mostly overlooked. It should not be neglected altogether though and I include it here for that reason.\n\n",
        "image": "8571-134869.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ermita de San Bias",
        "latitude": "42.567265",
        "longitude": "-2.198719",
        "nid": "8568",
        "nodetype": "POI",
        "body": "12th. c. Romanesque. It is not possible to visit the inside as it has been incorporated into what looks like a private residence.\u00a0\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda",
        "latitude": "42.568960",
        "longitude": "-2.192470",
        "nid": "7571",
        "nodetype": "POI",
        "body": "12-18th. c. Santiago Peregrino\n",
        "image": "7571-61092.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ermita del Calvario",
        "latitude": "42.571069",
        "longitude": "-2.202340",
        "nid": "8570",
        "nodetype": "POI",
        "body": "17th. c. chapel with Baroque retablo.\n\nIt is not in town, but rather on a hillside to the West of town. If you wish to visit, leave town via the camino and turn Right on the first road after the Ermita de San Blas. It is about 500m down the road on your left.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Arco de Felipe V",
        "latitude": "42.568983",
        "longitude": "-2.193032",
        "nid": "8569",
        "nodetype": "POI",
        "body": "The last remaining arch of what was once an impressive defensive system. It was originally constructed in the early 1700's.\n\nOf the original wall quite a bit still remains, although in most places it is incorporated into private homes.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Zoilo",
        "latitude": "42.568983",
        "longitude": "-2.193032",
        "nid": "8572",
        "nodetype": "POI",
        "body": "18th. c. Boroque, with a painted dome. Turn right as you enter town as the church sits on the West edge of the town overlooking Torres del Rio.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Andr\u00e9s (Torres del R\u00edo)",
        "latitude": "42.551243",
        "longitude": "-2.271875",
        "nid": "8573",
        "nodetype": "POI",
        "body": "Adding more confusion to the possible origin of the nearby Santo Sepulcro, the 16th. c. church of San Andr\u00e9s also contains crosses indicating that it once belonged to the Knights Templar.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Santo Sepulcro",
        "latitude": "42.552104",
        "longitude": "-2.271423",
        "nid": "7572",
        "nodetype": "POI",
        "body": "This 12th. c. church shares the same \"ambiguity of origin\" as Eunate near Puente la Reina. Many believe that it belonged to the Knights Templar, and like Eunate many others believe it to be a funeral chapel.\n\nWhatever its true origin, the church is worth a quick visit to get a glimpse of the octagonal dome. If you are carrying a sketchbook, this church is an excellent opportunity to practice your perception of scale; the width of the dome being equal to the height of the walls, among many other well thought out geometries.\n\n",
        "image": "7572-61098.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ruins of the Iglesia de San Pedro",
        "latitude": "42.514772",
        "longitude": "-2.373680",
        "nid": "8103",
        "nodetype": "POI",
        "body": "The 13th. c. ruins of San Pedro are located on the western edge of town next to the municipal albergue. Although it is in ruins (which came about shortly after the Carlist wars) the church is still worth a visit for a glimpse into how things are built. On hot Summer days it also offers a bit of shade for anyone in need.\n\n",
        "image": "8103-61110.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ruin.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda de la Asunci\u00f3n (Viana)",
        "latitude": "42.515147",
        "longitude": "-2.371641",
        "nid": "8102",
        "nodetype": "POI",
        "body": "13th. c. and 16th. c.\n\n",
        "image": "8102-61109.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Puente de Piedras",
        "latitude": "42.470560",
        "longitude": "-2.443911",
        "nid": "8579",
        "nodetype": "POI",
        "body": "The \"Stone Bridge\" which spans the R\u00edo Ebro, it was first constructed by San Juan de Ortega (a Saint who dedicated himself to improving the roads to Santiago).\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Bridge.png"
    }
}, {
    "node": {
        "title": "Concatedral de Santa Mar\u00eda de la Redonda",
        "latitude": "42.466610",
        "longitude": "-2.445703",
        "nid": "7574",
        "nodetype": "POI",
        "body": "16th - 18th c. Baroque. There is a painting of the crucifixion in the cathedral, which although small and in a less than prominent spot, is an original by Michelangelo.\n\n",
        "image": "7574-61114.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Mercado de San Blas",
        "latitude": "42.465659",
        "longitude": "-2.446733",
        "nid": "8532",
        "nodetype": "POI",
        "body": "Logro\u00f1o's market doesn't get quite the coverage it deserves. Spice up your albergue dinner with staples from this fresh market.\n\nIf you can't find what you are looking for at the market, the nearby shops will surely have what you need.\n\nLike most things, it is closed on Sundays.\n\n",
        "image": "8532-67834.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santiago el Real",
        "latitude": "42.467736",
        "longitude": "-2.447698",
        "nid": "7573",
        "nodetype": "POI",
        "body": "Located along the camino and difficult to miss, the church features a massive flemish rendition of Santiago Matamorosand Santiago Peregrino over the entrance.\n\n",
        "image": "7573-69151.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Cemetary",
        "latitude": "42.427412",
        "longitude": "-2.573584",
        "nid": "8582",
        "nodetype": "POI",
        "body": "The original entrance to the Hospital de Peregrinos which you passed on the way into Navarrete is now located here.\u00a0Take a moment to enjoy the entrance, which for it's age and mobility manages to preserve several great narratives.\n\n",
        "image": "8582-69157.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de La Asuncion",
        "latitude": "42.429639",
        "longitude": "-2.561375",
        "nid": "8580",
        "nodetype": "POI",
        "body": "Arguably one of the most amazing retablo's in all of Spain, do not miss this church if you can help it.\n\nIt is only open from 9:00-13:00, and again from 17:00-20:00.\u00a0\n",
        "image": "8580-69156.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ruins of Hospital de Peregrinos Saint Juan de Acre",
        "latitude": "42.434842",
        "longitude": "-2.551011",
        "nid": "7575",
        "nodetype": "POI",
        "body": "On the way into Navarrete you pass the remains of a pilgrims hospital. While there is not much left, the footprint remains well enough preserved to spark the imagination. The original entrance to the hospital has been reincarnated as the entrance to the town cemetery which is passed on your way out of Navarrete.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ruin.png"
    }
}, {
    "node": {
        "title": "Monasterio de Santa Mar\u00eda La Real",
        "latitude": "42.415891",
        "longitude": "-2.735397",
        "nid": "7577",
        "nodetype": "POI",
        "body": "The Monasterio de Santa Mar\u00eda la Real is quite possibly one of the most important monasteries on the whole of the Camino Franc\u00e9s. As such it should come as little surprise that it, like nearly everything in this part of the world, was the subject of much fighting and political maneuvering.\n\nTo understand it better, a short history lesson is in order. In 1044 King Garcia III of Navarra discovered a small carving of the Virgin Mary in a cave on the hillside while hunting. It was this Virgin, he determined, that caused the defeat of the Muslims and the Christian's triumphant return to power. He ordered the construction of a chapel, which subsequently grew into a church and then a monastery. The monastery rose to prominence and was eventually consecrated as a Cathedral.\n\nWhat followed was something out of the pages of a Ken Follett novel. First, in 1076 La Rioja (Logro\u00f1o) was captured by Castilla. King Alfonso VI (the Castillan King) gave the Monastery to the Benedictine monks of Cluny against the wishes of the Bishop who did not take the matter lightly. In retaliation, the Bishop moved the bishopric to Calahorra(Southeast of N\u00e1jera along the Ebro) where to this day it is shared with Logro\u00f1o. What he did not do though was give up his claim to the Monastery, and that claim in turn passed to a succession of new Bishops.\n\nNearly 100 years later the Bishop of Calahorra took action, first by pleading to the Pope, then by making charges against the prior of Santa Maria. When these measures failed to persuade the Pope, he took up arms, invading the Monastery and liberating it of it's valuables. This, however, got the attention of the Pope who responded with the excommunication of the Bishop.\n\nSubsequent battles over Rioja brought both prosperity and famine to the Monastery, which would eventually see all of its gold siezed to pay for the costly wars.\n\nIt has managed to survive, for better of for worse, and is today a popular tourist destination. Among it's highlights are the cave, the Royal Pantheon of Kings (with the tombs of the Kings which reigned here), the Pantheon de los Infantes (with the tombs of those that did not become kings), the Pantheon of the Dukes of N\u00e1jera, the Cloister of the Knights, and a Monastic Choir.\n\n",
        "image": "7577-133895.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Catedral de Santo Domingo de La Calzada",
        "latitude": "42.441000",
        "longitude": "-2.953520",
        "nid": "7578",
        "nodetype": "POI",
        "body": "2.5\u20ac Admission to the Cathedral and Cloister for pilgrims.\n\nMonday to Friday: 9:00-20:20\nSaturday: 9:00-19:30\nSunday: 9:00-12:20 and 14:00-19:10\n",
        "image": "7578-61212.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Iglesia Parroquial Nuestra Se\u00f1ora de la Calle ",
        "latitude": "42.438356",
        "longitude": "-3.065033",
        "nid": "8583",
        "nodetype": "POI",
        "body": "Notable primarily for it's 12th. century baptismal font. If the townspeople are to believed, it was used for baptisms up until recent times.\n\n",
        "image": "8583-134907.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Pedro (Belorado)",
        "latitude": "42.420411",
        "longitude": "-3.189759",
        "nid": "8587",
        "nodetype": "POI",
        "body": "Located in the plaza mayor and home to the relics of the local martyr San Vitores.\n\n",
        "image": "8587-134910.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda (Belorado)",
        "latitude": "42.420636",
        "longitude": "-3.188449",
        "nid": "8586",
        "nodetype": "POI",
        "body": "Contains both a Santiago Matamoros and a Santiago Peregrino. It was built to the East of the R\u00edo Verdeancho (which is actually neither green nor wide in this part of town) and was originally part of the castle.\n\nIt is now home to Belorado's parochial albergue.\n\n",
        "image": "8586-134913.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Convento de Santa Clara (Belorado)",
        "latitude": "42.419463",
        "longitude": "-3.198599",
        "nid": "8588",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ermita de la Virgen de la Pe\u00f1a",
        "latitude": "42.415088",
        "longitude": "-3.244137",
        "nid": "7579",
        "nodetype": "POI",
        "body": "Built into the side of the cliff, visits are organized by the albergue. The image of the child Jesus, for whom the ermita was built as a means of protection, spends it's time both here and in the church below.\n\n",
        "image": "7579-133899.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Iglesia San Esteban",
        "latitude": "42.404786",
        "longitude": "-3.261207",
        "nid": "8589",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia parroquial de Santiago",
        "latitude": "42.388219",
        "longitude": "-3.309438",
        "nid": "7580",
        "nodetype": "POI",
        "body": "Late 18th. c.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Juan de Ortega",
        "latitude": "42.375855",
        "longitude": "-3.436307",
        "nid": "8590",
        "nodetype": "POI",
        "body": "As you emerge from the trees into the field outside of San Juan de Ortega, this church is the first building you will see.\n\nSome of the oldest parts of the church were built while San Juan was still alive, possibly by himself. The remainder is from the 15th. c. and was constructed by the same master builder that oversaw the building of the Burgos Cathedral.\n\nThe church is the final resting place of San Juan, and if you are here on March 20 or September 22 be sure to visit the church. On those days (the equinoxes, when the length of day is equal to the length of night) the sun shines directly upon the depiction of the Annunciation.\n\n",
        "image": "8590-134916.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Puente",
        "latitude": "42.371654",
        "longitude": "-3.485488",
        "nid": "8592",
        "nodetype": "POI",
        "body": "Not all of the bridges constructed by Juan and Domingo were on the same scale. Here, between Ag\u00e9s and Atapuerca, is an original single arch bridge built by San Juan.\n\n",
        "image": "8592-134920.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Bridge.png"
    }
}, {
    "node": {
        "title": "Museum of Human Evolution",
        "latitude": "42.339435",
        "longitude": "-3.698285",
        "nid": "9723",
        "nodetype": "POI",
        "image": "9723-134937.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Monasterio de Las Huelgas",
        "latitude": "42.336753",
        "longitude": "-3.720567",
        "nid": "7582",
        "nodetype": "POI",
        "body": "Formerly a royal palace, it was converted for use by the Cistercian Order by Alfonso VIII. To visit now will require a formal guided tour; worth the admission if you enjoy a few treasures. The Monastery boasts jewels, tombs, and a Santiago Matamoros that was used to dub knights into the order of Santiago.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Arco de Santa Mar\u00eda",
        "latitude": "42.339684",
        "longitude": "-3.703769",
        "nid": "9720",
        "nodetype": "POI",
        "body": "Located across from the Cathedral the Aroco de Santa Mar\u00eda was the front door to Burgos. Not heavily advertised, it's interior is also an exhibition hall worth visiting.\n\n",
        "image": "9720-134951.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Cartuja de Santa Mar\u00eda de Miraflores ",
        "latitude": "42.337979",
        "longitude": "-3.657149",
        "nid": "7584",
        "nodetype": "POI",
        "body": "This Carthusian Monastery is still in use, but is not the easiest to get to. If you are in the city center, follow the path on the south side of the river back to the East. However, if you arrived in Burgos on the river trail from Casta\u00f1ares the Monastery is on your way into town, just south of the beach and the campsite Fuentes Blancas.\n\nThe Carthusians obey a strict order, living hermit like in separate quarters; meeting only to eat and work.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Gil",
        "latitude": "42.343723",
        "longitude": "-3.702233",
        "nid": "9722",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Burgos Cathedral",
        "latitude": "42.340528",
        "longitude": "-3.704410",
        "nid": "7581",
        "nodetype": "POI",
        "body": "The Burgos cathedral is one of the \"not to be missed\" treasures of the camino. Recent restaurations (ongoing) have brought back to life this impressive structure. Admission is not free, and if you have any trouble coming to terms with paying for the ability to enter a church you can console yourself with the fact that a quality museum is included in the price.\n\nWhatever you do, take your time; both from inside and out this cathedral has a lot to offer. Construction began at the start of the 13th. c. and was not wrapped up until the 18th. c. That being said, it is important to note that an astounding (almost impossible) amount of the labor that went into it's construction was completed within the first 22 years of construction. It's design is heavily influenced by French Gothic and tour guides are happy to boast that it's western facade is similar to that of Notre Dame\u2026 except finished.\u00a0\n",
        "image": "7581-69264.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Museo del Retablo and the Iglesia de San Esteban",
        "latitude": "42.341921",
        "longitude": "-3.705259",
        "nid": "9719",
        "nodetype": "POI",
        "body": "The museum housed inside the Iglesia de San Esteban is worth visiting if you are the type that likes to get close to the artifacts. A collection of retablos can be enjoyed quite close.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Hospital del Rey",
        "latitude": "42.340830",
        "longitude": "-3.728378",
        "nid": "7583",
        "nodetype": "POI",
        "body": "You will pass the Hospital del Rey on the road out of Burgos. It is now part of the university of Burgos.\n\nPilgrims of the 15th and 16th centuries were cared for here by the order, and the grounds surrounding the Hospital have turned up several pilgrims for whom the excellent care given was not quite enough.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Statue of El Cid",
        "latitude": "42.340681",
        "longitude": "-3.699605",
        "nid": "9721",
        "nodetype": "POI",
        "body": "El Cid as a warrior sits atop Babieca.\n\n",
        "image": "9721-135020.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Castillo de Burgos",
        "latitude": "42.342362",
        "longitude": "-3.707034",
        "nid": "9718",
        "nodetype": "POI",
        "body": "The castle sits atop a hillside adjacent to Burgos and is well worth a visit for the vistas alone. Occupants of the castle range from Kings held as prisoners (as was the case of King Garcia) to Isabelle and Ferdinand. It was the French occupation though that brought about it's destruction from the inside out, preferring to die at their own hands than surrender.\u00a0\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Castle.png"
    }
}, {
    "node": {
        "title": "Santo Domingo",
        "latitude": "42.288667",
        "longitude": "-4.135554",
        "nid": "7587",
        "nodetype": "POI",
        "image": "7587-61254.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "El Bazar del Peregrino",
        "latitude": "42.288603",
        "longitude": "-4.140141",
        "nid": "8223",
        "nodetype": "POI",
        "body": "It might be a stretch to call this place a place to explore, but it is certainly worth knowing about if you find yourself in need of just about anything under the sun. A hat for example, to protect you from the hot meseta sun?The owner is a friendly man, the last of his kind and working hard to preserve a store which is a dying breed in Spain.\n\nIt is located in the square (rectangle) near the municipal albergue.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de Nuestra Se\u00f1ora del Manzano",
        "latitude": "42.292672",
        "longitude": "-4.128028",
        "nid": "8149",
        "nodetype": "POI",
        "image": "8149-61252.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Juan",
        "latitude": "42.289720",
        "longitude": "-4.143519",
        "nid": "7588",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Rollo de Justicia",
        "latitude": "42.258482",
        "longitude": "-4.346783",
        "nid": "7590",
        "nodetype": "POI",
        "body": "The Gothic spire that rises from the plaza opposite the Iglesia Santa Mari\u00e1 is from the 15th. c. It was built shortly after the Catholic Monarchs (Ferdinand and Isabella) granted Boadilla jurisdictional autonomy from the lords of nearby Melgar and Castrojeriz.\u00a0It was to this rollo that wrongdoers were chained and publicly humiliated, prior to being tried.\n\n",
        "image": "7590-61235.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda",
        "latitude": "42.258425",
        "longitude": "-4.346965",
        "nid": "7589",
        "nodetype": "POI",
        "body": "Ask in the albergue about times and keys.\n\n",
        "image": "7589-61234.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Pedro",
        "latitude": "42.268229",
        "longitude": "-4.405224",
        "nid": "9725",
        "nodetype": "POI",
        "body": "16th century Gothic. Also home to a museum. It is not along the camino passing through Fr\u00f3mista and mostly overlooked by modern pilgrims, though at their loss. Do yourself a favor and devote some time to exploring the details.\n\n",
        "image": "9725-134962.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Mart\u00edn",
        "latitude": "42.266754",
        "longitude": "-4.406765",
        "nid": "9724",
        "nodetype": "POI",
        "body": "Located in the main square, the Iglesia de San Mart\u00edn is an 11th century church restored. Small binoculars or a long lens will help you to make out the details.\n\n",
        "image": "9724-134961.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda del Castillo",
        "latitude": "42.267760",
        "longitude": "-4.402461",
        "nid": "9726",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda la Blanca",
        "latitude": "42.316765",
        "longitude": "-4.542772",
        "nid": "7595",
        "nodetype": "POI",
        "body": "Construction began in the end of the 12th. cand was completed in the early 13th. Gothic, and Massive, and a National Monument since 1919 when rehabilitation began. The church had close ties with the Knights Templar. Absolutely worth a visit.\n\n",
        "image": "7595-134981.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mari\u00e1 del Camino",
        "latitude": "42.339573",
        "longitude": "-4.605562",
        "nid": "7596",
        "nodetype": "POI",
        "body": "12th c. , built to commemorate the defeat of the Moors.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Monasterio de San Zoilo",
        "latitude": "42.340549",
        "longitude": "-4.610599",
        "nid": "7598",
        "nodetype": "POI",
        "body": "Arguably one of the more important sites along the camino, the Monastery (originally dedicated to St. John the Babtist) was renamed to reflect the relics of San Zoilo in the 11th c.\n\nThe skull of Santiago himself was stored here for safekeeping while en route from Jerusalem to Santiago.\n\n",
        "image": "7598-134993.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de santiago ",
        "latitude": "42.338266",
        "longitude": "-4.603268",
        "nid": "7597",
        "nodetype": "POI",
        "body": "12th. c. , reconstructed in the late 19th century.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Monasterio de Santa Clara",
        "latitude": "42.336996",
        "longitude": "-4.598653",
        "nid": "7599",
        "nodetype": "POI",
        "body": "Originally from the 13th. c. , rebuilt in the 17th.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Convento de las Madres Benedictinas",
        "latitude": "42.370712",
        "longitude": "-5.033397",
        "nid": "9731",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Monasterio de San Facundo",
        "latitude": "42.370980",
        "longitude": "-5.033381",
        "nid": "9728",
        "nodetype": "POI",
        "body": "Once the most powerful Monastery in all of the kingdom's of Spain, the Monastery has all but vanished. Its roots date back to the construction of tombs over Facundo and Primitivo.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Tirso",
        "latitude": "42.370854",
        "longitude": "-5.032372",
        "nid": "9730",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ermita de la Virgen del Puente",
        "latitude": "42.370803",
        "longitude": "-4.999966",
        "nid": "9727",
        "nodetype": "POI",
        "body": "Formerly a pilgrim hospice and burial ground, the Ermita stands to the North of the camino just before entering Sahag\u00fan.\u00a0\n",
        "image": "9727-134997.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Arch of San Benito",
        "latitude": "42.370982",
        "longitude": "-5.033568",
        "nid": "9729",
        "nodetype": "POI",
        "body": "Once the facade of the Monasterio de San Facundo's church.\n\n",
        "image": "9729-134995.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Fortified Walls",
        "latitude": "42.500504",
        "longitude": "-5.419377",
        "nid": "9732",
        "nodetype": "POI",
        "body": "The majority of the walls that surrounded Mansilla in the 12th. century remain. You will pass through one gate when entering town, but for the best view of the wall cross the bridge on the west side of town and look back on the town.\u00a0\n",
        "image": "9732-135041.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Le\u00f3n Cathedral",
        "latitude": "42.599281",
        "longitude": "-5.567107",
        "nid": "7606",
        "nodetype": "POI",
        "body": "The Cathedral of Le\u00f3n, dedicated to Santa Mar\u00eda de la Regla, is a monument to behold and a masterpiece of Gothic architecture.\n\nIt was built between the 13th and 15th centuries, a relatively short time for such a building, and preserves nearly all of the original stained glass for which it is know.\n\nThere is as well a Cathedral museum with an impressive collection of artifacts and artwork.\n\n",
        "image": "7606-134546_0.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Casa de los Botines",
        "latitude": "42.598309",
        "longitude": "-5.570724",
        "nid": "9733",
        "nodetype": "POI",
        "body": "Built in 1892 and designed by Gaudi, whose statue stands (or rather sits) casually in front.\n\nIt was originally commissioned to serve as the home and commercial offices of a fabric company. The upper floors housed rental properties.\n\nSince 1929 it has served as a bank and more recently as a museum and tourist destination. Over the years it has seen very little modifications to the original design.\n\nAmong the details to note are the sculpture of Saint George slaying a dragon and a diabolical wrought iron fence.\n\n",
        "image": "9733-134545_0.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "San Marcos",
        "latitude": "42.601840",
        "longitude": "-5.581613",
        "nid": "7609",
        "nodetype": "POI",
        "body": "Now a luxurious Parador, the building served as a monastery since the 16th century under the Order of Santiago. The facade is the masterpiece, but you can visit both the church and the lobby of the Parador. The church has a museum as well.\n\n",
        "image": "7609-133903_0.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Basilica de San Isidoro",
        "latitude": "42.600601",
        "longitude": "-5.570884",
        "nid": "7608",
        "nodetype": "POI",
        "body": "Perhaps even more so than the Cathedral of Le\u00f3n, the Basilica de San Isidoro is not to be missed. It houses the Basilica proper, the Royal Pantheon, and a Museum.\n\nOf the three it is the Royal Pantheon that impresses as quite possibly the single best surviving example of Romanesque art in Spain. It is the funeral chapel of the Kings of Le\u00f3n, 11 of which are interred here along with their queens.\n\nThe Basilica is one of only two in Spain (the other is in Lugo) that is permitted to display the Sacrament at all times.\n\n",
        "image": "7608-135047_0.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Barrio H\u00famedo",
        "latitude": "42.596889",
        "longitude": "-5.567693",
        "nid": "9734",
        "nodetype": "POI",
        "body": "Located in the historical center of town, and centered around the Plaza de San Mart\u00edn, the Barrio H\u00famedo is home to the largest concentration of restaurants and bars (over 100) in L\u00e9on. It is here, between the hours of 9:30 and early morning, that the locals converge for celebration.\n\n",
        "image": "9734-135045_0.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Puente de \u00d3rbigo",
        "latitude": "42.463981",
        "longitude": "-5.877890",
        "nid": "9735",
        "nodetype": "POI",
        "body": "The extraordinary bridge that connects Puente de \u00d3rbigo (on the East bank) with Hospital de \u00d3rbigo (on the west bank) is the site of one of the camino's more charming tales.\n\nIt was here, in 1434, that a Leonese knight by the name of Suero de Qui\u00f5nes held a tournament of honor. It was all for the sake of a love scorned, and to prove himself to his lady he challenged all comers to a tournament of jousting. With the kings permission a notice was sent out across Europe. It turned out to be a large affair, and in July of that year the tournament began. Although he nearly lost his life in his efforts, Suero emerged triumphant; 300 broken lances as proof of his love.\n\nHe later became a pilgrim, and in Santiago he left a jeweled bracelet that can be seen around the neck of the image of Santiago Alfeo in the cathedral museum.\n\n",
        "image": "9735-135054.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Bridge.png"
    }
}, {
    "node": {
        "title": "Palacio Episcopal",
        "latitude": "42.457680",
        "longitude": "-6.056015",
        "nid": "7610",
        "nodetype": "POI",
        "body": "Started, but not finished, by Gaudi. The bishop for whom it was commission passed away and construction stopped.\n\nThe inside, which houses a Museum dedicated to Pilgrimage, bears little resemblance to the exterior. There is a small fee for admission, but on a hot summer day the cool interior alone is worth the price.\n\n",
        "image": "7610-133906.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Chocolate Museum",
        "latitude": "42.456049",
        "longitude": "-6.053344",
        "nid": "9736",
        "nodetype": "POI",
        "body": "Chocolate is big business in these parts, and Astorga has a Museum to celebrate it.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Astorga Cathedral and Museum",
        "latitude": "42.457676",
        "longitude": "-6.057158",
        "nid": "7611",
        "nodetype": "POI",
        "body": "The current building dates from the late 15th century and was built upon a earlier church that was erected in the 11th.\n\n",
        "image": "7611-61267.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Ayuntamiento",
        "latitude": "42.454320",
        "longitude": "-6.052990",
        "nid": "9737",
        "nodetype": "POI",
        "body": "The facade is Baroque in style, but it is the clock which captures the attention of Pilgrims. Maragato figures emerge to ring in the hour, and have done so for centuries. Check your watch and if you are near the Plaza Espa\u00f1a at the appointed time keep your eyes on the clock.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Cowboy Bar",
        "latitude": "42.462803",
        "longitude": "-6.207072",
        "nid": "8224",
        "nodetype": "POI",
        "body": "Somewhat notorious in the guidebooks for it's Cowboy American schtick, the cowboy bar does a pretty good business. It is not alone in town however, and the bar next door might just provide a bit more of what you are looking for.\n\n",
        "image": "8224-133839.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Monasterio de San Salvador de Monte Irago",
        "latitude": "42.482166",
        "longitude": "-6.284890",
        "nid": "7615",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ermita del Bendito Cristo de la Vera Cruz",
        "latitude": "42.480077",
        "longitude": "-6.278295",
        "nid": "7612",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda (Rabanal)",
        "latitude": "42.482302",
        "longitude": "-6.284754",
        "nid": "7614",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Capilla de San Jose",
        "latitude": "42.481964",
        "longitude": "-6.282672",
        "nid": "7613",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda de la Encina",
        "latitude": "42.544701",
        "longitude": "-6.592283",
        "nid": "9742",
        "nodetype": "POI",
        "image": "9742-135074.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Castillo de los Templarios",
        "latitude": "42.543552",
        "longitude": "-6.593449",
        "nid": "7616",
        "nodetype": "POI",
        "body": "The Knights Templar inherited the original fort that stood here, and during the first half of the 13th c. built it into something much larger indeed. Shortly thereafter their order was dissolved and the castle was in turn given to the count of Lemos. It has survived since, though over the centuries remained the focal point of several battles.\n\nA tour of the castle is worth the effort.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Castle.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Francisco",
        "latitude": "42.605732",
        "longitude": "-6.808960",
        "nid": "7620",
        "nodetype": "POI",
        "body": "It is rumored that the Saint himself founded this church en route to Santiago.\u00a0\n",
        "image": "7620-135095.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santiago (Villafranca del Bierzo)",
        "latitude": "42.604099",
        "longitude": "-6.807752",
        "nid": "7619",
        "nodetype": "POI",
        "body": "Located adjacent to the Albergue Ave Fenix.\n\nIt is one of the few churches along the way which, like the Cathedral in Santiago, has a Puerta del Perd\u00f3n. It is through this door in both churches that ones sins would be pardoned upon passing through. This one was reserved for pilgrims too ill to complete the distance to Santiago.\n\n",
        "image": "7619-135145.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Colegiata de Santa Mar\u00eda (Villafranca del Bierzo)",
        "latitude": "42.609453",
        "longitude": "-6.809056",
        "nid": "7621",
        "nodetype": "POI",
        "image": "7621-135146.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Castle of Sarrac\u00edn",
        "latitude": "42.661706",
        "longitude": "-6.953408",
        "nid": "9743",
        "nodetype": "POI",
        "body": "It is an hour of walking there and back, and through beautiful chestnut forests. It is a short detour but uphill and certainly worth the climb.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Castle.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda (O Cebreiro)",
        "latitude": "42.707900",
        "longitude": "-7.043036",
        "nid": "7622",
        "nodetype": "POI",
        "image": "7622-135119.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Monasterio de Samos",
        "latitude": "42.732337",
        "longitude": "-7.326250",
        "nid": "7623",
        "nodetype": "POI",
        "body": "Guided visits by, and vespers with, the few monks that remain. For times and prices check either with the albergue or the ticket office.\n\n",
        "image": "7623-135150.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": " Capilla del Cipr\u00e9s",
        "latitude": "42.733467",
        "longitude": "-7.324689",
        "nid": "7624",
        "nodetype": "POI",
        "image": "7624-135127.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Convento de la Magdalena",
        "latitude": "42.778807",
        "longitude": "-7.421343",
        "nid": "9745",
        "nodetype": "POI",
        "body": "Founded in the 12th century.\n\nThere is now an albergue here\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Marina",
        "latitude": "42.777354",
        "longitude": "-7.414443",
        "nid": "9744",
        "nodetype": "POI",
        "body": "Modern and of little note, apart from the painted mural which makes for a decent backdrop to pilgrim portraits.\n\nBehind the church is a nice shaded grove of trees with benches.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Pedro",
        "latitude": "42.809355",
        "longitude": "-7.614755",
        "nid": "9748",
        "nodetype": "POI",
        "body": "Also brought up from the old town, but only the facade. It dates from 1182.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "La Virgen de las Nieves",
        "latitude": "42.805174",
        "longitude": "-7.616970",
        "nid": "9750",
        "nodetype": "POI",
        "body": "A small chapel dedicated to the protection from drowning it was meant to provide the towns people. Of the relocated buildings, it traveled the shortest distance, from the end of the old bridge to the end of the new bridge.\n\n",
        "image": "9750-134282.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cruceiro.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Juan",
        "latitude": "42.807663",
        "longitude": "-7.615651",
        "nid": "9747",
        "nodetype": "POI",
        "body": "\nThis is the church that was moved from its original foundation. From the 13th c. , it doubled as a fortress.\n\n",
        "image": "9747-134283.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Pazo de la Marquesa de B\u00f3veda",
        "latitude": "42.807604",
        "longitude": "-7.616027",
        "nid": "9749",
        "nodetype": "POI",
        "body": "Also brought up from the old town, the 17th c. palace of the Berbetoro \/ Montenegros families.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Old Portomar\u00edn",
        "latitude": "42.802606",
        "longitude": "-7.617673",
        "nid": "9746",
        "nodetype": "POI",
        "body": "When crossing over the bridge into Portomar\u00edn, take a peek below for remnants of the old town of Portomar\u00edn. Some houses, a few roads, and a bridge which parallels the on you are on remain in place. They are only visible when the water is low.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Pilgrim Cemetary",
        "latitude": "42.859501",
        "longitude": "-7.780654",
        "nid": "9752",
        "nodetype": "POI",
        "body": "Remains and artifacts from pilgrims have been located by archeologists here.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Cruceiro de Ligonde",
        "latitude": "42.855193",
        "longitude": "-7.777355",
        "nid": "9753",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cruceiro.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda (Leboreiro)",
        "latitude": "42.888108",
        "longitude": "-7.965655",
        "nid": "7647",
        "nodetype": "POI",
        "body": "The founding of the church is credited to the miraculous virgin. Seeing a bright light emanating from a nearby fountain, the villagers investigated and found an image of the Virgin. It was moved to the church but by the following morning it had vanished, only to be found back on the fountain. The villagers returned it to the church, and again the Virgin retreated to the fountain. This went on until the church was rededicated to her, at which time she stayed put.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Juan",
        "latitude": "42.887216",
        "longitude": "-7.964938",
        "nid": "7648",
        "nodetype": "POI",
        "body": "Tours are offered for an optional donativo. The church here has a crucifix that is only one of a few which depicts Christ with one are removed from the cross. Rather it is reaching downward, to pilgrims?\n\n",
        "image": "7648-133391.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Roque",
        "latitude": "42.913684",
        "longitude": "-8.012922",
        "nid": "7650",
        "nodetype": "POI",
        "image": "7650-61628.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Capela de San Antonio",
        "latitude": "42.915156",
        "longitude": "-8.016555",
        "nid": "8161",
        "nodetype": "POI",
        "body": "17th. c. \n",
        "image": "8161-61625.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda (Melide)",
        "latitude": "42.911436",
        "longitude": "-8.025373",
        "nid": "7649",
        "nodetype": "POI",
        "body": "About 600m west of the municipal albergue along the camino.\n\n",
        "image": "7649-61629.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Sancti Spiritus",
        "latitude": "42.915281",
        "longitude": "-8.016458",
        "nid": "7651",
        "nodetype": "POI",
        "image": "7651-61623.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Pulperia A Garnacha",
        "latitude": "42.913171",
        "longitude": "-8.011445",
        "nid": "8160",
        "nodetype": "POI",
        "body": "Although it has been on the menu as far back as O Cebreiro, Pulpo (octopus) doesn't really seem to garnish much attention until Melide. Served with oil and spices, it is often accompanied by local wines from unlabeled bottles. One of the better places to try it is Pulper\u00eda a Garnacha, the last door on your left before you get to the main road in Melide.\n\n",
        "image": "8160-61620.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santiago (Boente)",
        "latitude": "42.916194",
        "longitude": "-8.077741",
        "nid": "7652",
        "nodetype": "POI",
        "body": "It is hard not to visit this church, on the main road and one of the only places where you will find the parish priest actively calling you in for a visit. You can get a stamp, a prayer card, and it is one of the few churches where you can get a peek at the sacristy.\n\n",
        "image": "7652-61630.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santiago",
        "latitude": "42.926302",
        "longitude": "-8.163319",
        "nid": "9754",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de La Magdalena",
        "latitude": "42.926220",
        "longitude": "-8.162804",
        "nid": "9755",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "The River",
        "latitude": "42.898374",
        "longitude": "-8.444224",
        "nid": "9757",
        "nodetype": "POI",
        "body": "Not much more than a stream, but the traditional bathing place for medieval pilgrims not know for their attention to hygiene. It was here that they would wash, sometimes for the first time since beginning their pilgrimage.\u00a0\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "Iglesia and Cross of Benaval",
        "latitude": "42.899758",
        "longitude": "-8.446550",
        "nid": "9756",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Ermita San Marcos del Monte do Gozo",
        "latitude": "42.889527",
        "longitude": "-8.494310",
        "nid": "8226",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "The Palace of the Archbishop",
        "latitude": "42.881006",
        "longitude": "-8.545274",
        "nid": "9760",
        "nodetype": "POI",
        "body": "You will pass under a portion of the palace when entering the Cathedral's Plaza Obradoiro from the North. The corridor here is famous for always having a bag piper on duty to welcome you.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Monastery of San Agust\u00edn",
        "latitude": "42.880637",
        "longitude": "-8.541319",
        "nid": "9768",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia and Hosptal of San Roque",
        "latitude": "42.883446",
        "longitude": "-8.541837",
        "nid": "9765",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Monasterio de San Mart\u00edn Pinario",
        "latitude": "42.881354",
        "longitude": "-8.544665",
        "nid": "9762",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de la Compa\u00f1\u00eda",
        "latitude": "42.878773",
        "longitude": "-8.542345",
        "nid": "9770",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Casa G\u00f3tica and the Pilgrimage Museum",
        "latitude": "42.882115",
        "longitude": "-8.542813",
        "nid": "9759",
        "nodetype": "POI",
        "body": "Much more interesting than the pilgrim museum in Astorga, this one is a great deal more practical now that you are in Santiago. Start here to get a full picture of what you have accomplished and of what the city has to offer. If you have not gone into the Cathedral yet, try to go here first.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Museum.png"
    }
}, {
    "node": {
        "title": "Convent of Santo Domingo of Bonaval",
        "latitude": "42.882596",
        "longitude": "-8.539102",
        "nid": "9767",
        "nodetype": "POI",
        "body": "Located very near the Puerta del Camino, the city gate you entered along the camino, on your right as you approach the city.\n\nThis convent, and the massive park behind it are certainly worth visiting. It houses the Museum of the Galician Pueblo and if you have seen a photo of an amazing spiral staircase on a brochure, this is where it is located. It is technically a triple helix and is quite impressive.\n\nThe oldest parts date back to the early 13th century when it was founded by St. Dominic de Guzman, and additions such as the facade have been added in subsequent centuries.\n\n\u00a0\n\n\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Las \u00c1nimas",
        "latitude": "42.881569",
        "longitude": "-8.541857",
        "nid": "9764",
        "nodetype": "POI",
        "image": "9764-134575.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Hostal de los Reyes Cat\u00f3licos (Parador)",
        "latitude": "42.881030",
        "longitude": "-8.545843",
        "nid": "9761",
        "nodetype": "POI",
        "body": "Once a pilgrim hostal, now a luxury hotel and one of the fanciest places to stay in all of Spain. In its former role each of the for corners of the complex housed a particular category of pilgrim, divided in half first by gender and then again by level of health.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Monument.png"
    }
}, {
    "node": {
        "title": "Iglesia de San Fiz",
        "latitude": "42.879410",
        "longitude": "-8.541719",
        "nid": "9769",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Cathedral of Santiago de Compostela",
        "latitude": "42.880571",
        "longitude": "-8.544896",
        "nid": "9758",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cathedral.png"
    }
}, {
    "node": {
        "title": "Monastery of San Paio de Antealtares",
        "latitude": "42.880833",
        "longitude": "-8.543203",
        "nid": "9766",
        "nodetype": "POI",
        "body": "This is the large, almost prison like structure located opposite the Cathedral in the Plaza Quintana.\n\nIn fact it is a monastery, and it's founding dates back to before that of the Cathedral itself. It was here that the relics of Saint James were kept during the construction of the Cathedral.\n\nIt is free to visit the church, which contains a fairly unique representation of Mary expecting.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Monasterio de San Francisco de Valdedi\u00f3s",
        "latitude": "42.883154",
        "longitude": "-8.545126",
        "nid": "9763",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Iglesia de Santa Mar\u00eda Salom\u00e9",
        "latitude": "42.878018",
        "longitude": "-8.543363",
        "nid": "9771",
        "nodetype": "POI",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "El Acebo",
        "latitude": "42.498381",
        "longitude": "-6.456399",
        "nid": "931",
        "nodetype": "City",
        "body": "Acebo is a typical mountain village, with rickety wooden balconies suspended over the road. All stone construction and slate roofs are common, the slate coming from a quarry not far off.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "931-133843.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/el-acebo-de-san-miguel.html?aid=1499405",
        "tosantiago": "220.3",
        "marker": "City.png",
        "tohere": "6.80",
        "barrest": "Yes",
        "road": "You will first make sight of Acebo (from holly, as in the hardy evergreen shrub) as you descend from above. It is a single road town and typical of the area.\n\n"
    }
}, {
    "node": {
        "title": "Castromayor",
        "latitude": "42.831738",
        "longitude": "-7.708705",
        "nid": "1003",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "82.9",
        "marker": "City.png",
        "tohere": "1.30",
        "barrest": "Yes",
        "road": "Shortly beyond the village, a mere 50m off the camino to your left, are the remains of the Iron Age castro that gives the town its name. It is seldom visited (there are no signs) but is worth an exploration. It can be found before the point where you meet the main road.\n\n"
    }
}, {
    "node": {
        "title": "Alto del Portillo",
        "latitude": "42.575680",
        "longitude": "-5.539041",
        "nid": "3039",
        "nodetype": "Alto",
        "body": "It is from the Alto del Portillo that you get your first glimpse of Le\u00f3n. It is customary at such a height to search for the spires of Le\u00f3n's magnificent cathedral.\n\n",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Villamayor del R\u00edo",
        "latitude": "42.427704",
        "longitude": "-3.135910",
        "nid": "865",
        "nodetype": "City",
        "body": "If you wish to stay in the albergue here, be advised that it is not in the village proper but rather set off from the main road a bit to the north.\n\nDo not let the grandiose name of the village fool you, neither the town nor the river is remarkable.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "538.5",
        "marker": "City.png",
        "tohere": "3.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Camponaraya",
        "latitude": "42.580831",
        "longitude": "-6.670375",
        "nid": "939",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/camponaraya.html?aid=1499405",
        "tosantiago": "194.8",
        "marker": "City.png",
        "tohere": "2.60",
        "barrest": "Yes",
        "road": "The way through town is along its busiest road. At the end of town the camino crosses over the motorway and continues through the countryside, back among the vines.\n\n"
    }
}, {
    "node": {
        "title": "Lestedo",
        "latitude": "42.872190",
        "longitude": "-7.813811",
        "nid": "1011",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "72.3",
        "marker": "City.png",
        "tohere": "0.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villada",
        "latitude": "42.251012",
        "longitude": "-4.967194",
        "nid": "5527",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Off Camino by 800m.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "marker": "City.png",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Detour to Santo Domingo de Silos",
        "latitude": "42.340508",
        "longitude": "-3.714409",
        "nid": "7585",
        "nodetype": "POI",
        "body": "One of the longer detours on the Camino Franc\u00e9s, this one is perhaps better reached by the buses which leave the terminal in the evening, and return the following morning. Inquire at the tourist information office about accommodation there, which should be arranged before you go.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Atapuerca",
        "latitude": "42.376807",
        "longitude": "-3.508055",
        "nid": "873",
        "nodetype": "City",
        "body": "For the better part of the last 3 decades excavations have been underway to learn more about our prehistory. In the caves outside of town, the remains of early humans have been discovered; some as old as 800,000 years. Most of the discoveries have been transferred to the Museum of Human Evolution in Burgos.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/atapuerca.html?aid=1499405",
        "tosantiago": "503.8",
        "marker": "City.png",
        "tohere": "2.40",
        "barrest": "Yes",
        "road": "From Atapuerca, the camino climbs upward into the Atapuerca mountains before descending again into Carde\u00f1uela.\n\n"
    }
}, {
    "node": {
        "title": "Vega de Valcarce",
        "latitude": "42.664103",
        "longitude": "-6.946020",
        "nid": "947",
        "nodetype": "City",
        "atm": "Yes, the last one till Triacastela",
        "body": "The nearby truck stop of the same name has a full restaurant, as well as a co-op boutique selling local delights like wine, honey, and chocolate. There is also a fruit stand.\n\nThe ruins of the Castle of Sarrac\u00edn are located high on the hill on the other side of the river.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/vega-de-valcarce.html?aid=1499405",
        "tosantiago": "164.4",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ponte Campa\u00f1a",
        "latitude": "42.878383",
        "longitude": "-7.914399",
        "nid": "1020",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "62.7",
        "marker": "City.png",
        "tohere": "1.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Arroyo San Bol",
        "latitude": "42.323691",
        "longitude": "-3.990483",
        "nid": "883",
        "nodetype": "City",
        "body": "The spring that gives this arroyo (creek) its name, also cures your feet. The operations of the albergue here were passed to a nearby municipality, who electrified and modernized this former hippy hideaway. \nThe small deviation from the camino that takes you there is worth the shade and rejuvenating spring it provides.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "883-133784.webp",
        "tosantiago": "458.3",
        "marker": "City.png",
        "tohere": "5.30",
        "barrest": "",
        "road": "Non lasciatevi intimidire dalle sconfinate distese di grano. Hontanas non sar\u00e0 visibile finch\u00e9 non vi sarete abbastanza vicini; si nasconde in una piccola depressione in un paesaggio altrimenti del tutto piatto."
    }
}, {
    "node": {
        "title": "Ga\u00f1ecoleta",
        "latitude": "43.067100",
        "longitude": "-1.306600",
        "nid": "12202",
        "nodetype": "City",
        "body": "From here the way to the pass is quite steep, particularly the last few kilometers.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "747.5",
        "marker": "City.png",
        "tohere": "3.20",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Hospital de la Condesa",
        "latitude": "42.704402",
        "longitude": "-7.099464",
        "nid": "955",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "955-160.webp",
        "tosantiago": "147.2",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "A Peroxa",
        "latitude": "42.915515",
        "longitude": "-8.073664",
        "nid": "1028",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "47.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Roncesvalles [Orreaga]",
        "latitude": "43.009410",
        "longitude": "-1.319796",
        "nid": "816",
        "nodetype": "City",
        "body": "The popular dinner spot is Casa Sabina, and you\u2019ll want to put your name on the list (at their bar) in advance. The dining room sees pilgrims from the world over sat down at random to fill the tables. Half are about to start their walk, beginning here, the other half just came off the mountain. The length of their stride tells them apart.\n\nIt doesn\u2019t take much to see the whole town, but nearly every building is a treasure of some kind.\n\nNearest the albergue is the Iglesia de Santa Maria, where a pilgrim Mass and benediction is held daily at 8pm (6pm on weekends). The Real Colegiata de Santa Mar\u00eda de Roncesvalles is also not to be missed, but the relics and other notable items are part of the Museum (small fee). \nThe small Capilla de Santiago o de los Peregrinos and the Silo de Carlomagno (also known as the Capilla de Sancti Spiritus) sit side by side along the road.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "816-133772.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/roncesvallhtml?aid=1499405",
        "tosantiago": "742.7",
        "marker": "City.png",
        "tohere": "3.90",
        "barrest": "Yes",
        "road": "The camino leaves along a footpath to the west of the main road (your right hand side), pressing deeper into the forest before returning to the road as it approaches Burguete.\n\n"
    }
}, {
    "node": {
        "title": "Poblaci\u00f3n de Campos",
        "latitude": "42.269910",
        "longitude": "-4.447145",
        "nid": "891",
        "nodetype": "City",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/poblacion-de-campos.html?aid=1499405",
        "tosantiago": "415.7",
        "marker": "City.png",
        "tohere": "3.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Cristovo do Real",
        "latitude": "42.740198",
        "longitude": "-7.273442",
        "nid": "963",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "135",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "A Peroxa",
        "latitude": "42.926681",
        "longitude": "-8.195025",
        "nid": "1037",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "35.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Eunate",
        "latitude": "42.672402",
        "longitude": "-1.761600",
        "nid": "7426",
        "nodetype": "POI",
        "body": "\u200cDetour to visit the Church at eunate +3km\nOctagonal 12th century Iglesia de Eunate.\n\nThe history of this ermita is still up for debate, and like most things in Spain are divided into two camps. Half believe it is of Knights Templar origin, and the rest who believe it was a funeral chapel. Separate yourself from that intellectual debate and you will find a countryside setting with a small gem of architecture for you to contemplate.\n\nNovember through February - Closed\nMarch 1 to April 9\nTUE - FRI 10:30-13:30 (closed mondays)\nSAT\/SUN 10:30-13:30\nApril 10 to June 30\n10:30-13:00 & 16:30-18:00\nClosed Mondays\nJuly through August\n10:30-13:00 & 17:30-20:00\nSeptember through October\nTUE - FRI 10:30-13:00 (closed mondays)\nSAT\/SUN 10:30-13:300\n",
        "image": "7426-52721.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Valtuille de Arriba",
        "latitude": "42.615843",
        "longitude": "-6.764939",
        "nid": "7618",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "185.3",
        "marker": "City.png",
        "tohere": "2.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Larrasoa\u00f1a",
        "latitude": "42.901716",
        "longitude": "-1.540918",
        "nid": "824",
        "nodetype": "City",
        "body": "The grocery has minimal hours and stock but does open early for breakfast. \nLike Zubiri, Larrasoa\u00f1a has a bridge which is well known, though for less beneficial reasons. Instead of healing, it was a frequent point of banditry; the name of the bridge is the Puente de los Bandidos . This is an important concept to keep in mind if you find yourself trying to imagine the life of a medieval pilgrim. All across Spain the stories of banditry appear, and when they do there is most often a river involved.\n\nModern pilgrims can hardly complain about the state of river crossings, but the medieval pilgrim would frequently find himself (depending on the century) with a more difficult situation. The rivers themselves grow in size as you continue westward, and some of them look quite daunting indeed. They were a natural choice for a more nefarious crowd looking to exploit pilgrims. \nA bridge, whatever it\u2019s design or age, is a wonderful thing indeed.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "824-61201.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/larrasoana.html?aid=1499405",
        "tosantiago": "715.6",
        "marker": "City.png",
        "tohere": "2.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Moratinos",
        "latitude": "42.360510",
        "longitude": "-4.926810",
        "nid": "899",
        "nodetype": "City",
        "body": "Moratinos has seen its share of pilgrims over the years and is currently undergoing what can be described as a slow renaissance. The hillside is chock full of hobbit holes, specially built to store large barrels of wine.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "899-133764.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/moratinos.html?aid=1499405",
        "tosantiago": "370.9",
        "marker": "City.png",
        "tohere": "3.20",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pasc\u00e1is",
        "latitude": "42.731868",
        "longitude": "-7.344725",
        "nid": "971",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "125.5",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Santa Irene",
        "latitude": "42.917473",
        "longitude": "-8.330383",
        "nid": "1046",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "1046-134740.webp",
        "tosantiago": "22.4",
        "marker": "City.png",
        "tohere": "1",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "SPLIT: Scenic Route or Road Route",
        "latitude": "42.577754",
        "longitude": "-5.645060",
        "nid": "7466",
        "nodetype": "POI",
        "body": "When you get to the Basilica de la Virgen del Camino you will cross the busy road and continue on a small access road. In 300m you are presented with the choice between two routes. The painting of yellow arrows here is confusing, and there is zero official signage to indicate your options. Distances are indicated from La Virgen del Camino to Puente de \u00d3rbigo where they converge.\n\n\u200cThe SCENIC ROUTE - 27 (green below)\nThis variation is called the Scenic Route, and it adds 2.4km to your journey. It is considerably more pleasant and there are now plenty of services and albergues along the way. To follow this route turn left onto a dirt track.\n\n\u200cThe ROAD ROUTE - 24.6 (red below)\nThe official camino route is often called the Road Route due to its proximity to the road. It is the shorter of the two and offers little to no peace from the traffic. To take this route continue straight.\n\n",
        "image": "7466-135057.webp",
        "tosantiago": "298.1",
        "marker": "POI.png",
        "tohere": "0.60",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Gorolfe",
        "latitude": "42.740277",
        "longitude": "-7.355229",
        "nid": "7626",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "123.7",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Burlada",
        "latitude": "42.825939",
        "longitude": "-1.615677",
        "nid": "832",
        "nodetype": "City",
        "body": "The border between Burlada and Pamplona is the R\u00edo Arga, which is spanned by the Puente de la Magdalena. Since Trinidad de Arre the terrain has been flat. From the bridge here you get your first glimpse of Pamplona\u2019s defenses, both natural and man-made.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "703.5",
        "marker": "City.png",
        "tohere": "0.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villamoros de Mansilla",
        "latitude": "42.534172",
        "longitude": "-5.444155",
        "nid": "907",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "319.7",
        "marker": "City.png",
        "tohere": "4.80",
        "barrest": "",
        "road": "If you want to escape the busy road for a bit, bear left at the start of town and follow the arrows through the village. Doing so doesn\u2019t add any distance to your journey but spares you 500m of the road.\n\nWhen entering Puente de Villarente it is now the bridge, not the river; that poses the greatest threat to pilgrims. Be sure to follow the arrows before the road bridge, they will direct you towards a smaller pedestrian crossing which is a great deal safer.\n\n"
    }
}, {
    "node": {
        "title": "Sarria",
        "latitude": "42.777186",
        "longitude": "-7.413900",
        "nid": "979",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Sarria now holds the record for the most albergues in one town. Don\u2019t be alarmed by the swell of pilgrims that appear overnight once you reach this point; the closest city to the minimum 100km point set by the church to be eligible to receive the Compostela. The effect can be dramatic during the high season and if you have been on the road for a few weeks it can be a challenge to adapt to the change.\n\nIf you have arrived early and plan to stay the night, consider the local pool as a place to pamper your feet a bit.\n\nThere are plenty of bars and restaurants along the R\u00faa Maior, where the bulk of the albergues are centered. To get to the grocery shopping though, you have to make your way to the main road where options abound.\n\nThe R\u00faa Maior evolved as a market street during the Middle Ages, due primarily to the pilgrim traffic; and this hasn\u2019t changed. As you walk through town take a moment to admire the well-preserved coats of arms on several of the houses that line the street.\n\nThe Iglesia de Santa Maria is an unspectacular example of modern church building, but it does sit atop it\u2019s 12th century predecessor. The Iglesia de San Salvador is recently restored and is located at the top of the R\u00faa Maior. Beyond it are the Convento de la Magdalena and the remains of the old Castle. The convent has roots in the 12th century and currently operates as a hospice and a primary school. The Castle is more recent, from the 15th century, and like most castles in Galicia, it is in poor shape. Only one tower remains, the rest was destroyed during the Irmandi\u00f1a uprisings of 1467 (see inset). It was rebuilt, but those efforts also fell into ruin. The remnants have been re-used to pave several of Sarria\u2019s sidewalks.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/sarria.html?aid=1499405",
        "tosantiago": "113.6",
        "marker": "City.png",
        "tohere": "4",
        "barrest": "Yes",
        "road": "The camino exits town along the R\u00faa Maior in the old town and passes the Convento de la Magdalena. Take note that the camino actually turns left BEFORE arriving at the convent. It goes steeply downhill to the road, turns right, and soon crosses the R\u00edo Celeiro on the Ponte \u00c1spera. It follows along the river, and in the shadow of a super bridge before crossing the train tracks. The first climb of the day (excluding the stairs in Sarria) is ahead and passes through an ancient forest full of gnarly oaks and chestnut trees. \n"
    }
}, {
    "node": {
        "title": "Vilamaior",
        "latitude": "42.892190",
        "longitude": "-8.450460",
        "nid": "1055",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "9",
        "marker": "City.png",
        "tohere": "1.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Various detours from Triacastela to Sarria",
        "latitude": "42.755840",
        "longitude": "-7.241739",
        "nid": "7475",
        "nodetype": "POI",
        "body": "At the end of town when leaving Triacastela you arrive at a fork in the road, presenting two distinct ways to Sarria. Distances shown are from Triacastela, through Aguiada to Sarria.\n\nBeyond Samos, the camino once again splits, allowing you to rejoin the Calvor and San Xil route in Aguiada. This is the preferred route as it spends the least amount of time along the roadside. Alternatively, you can stay on the road from Samos all the way to Sarria.\n\n\u200c\u200cVia the Monastery at Samos - 25.1\nAt the end of town, turn left to follow the road to the Monastery at Samos (all services). The way is generally through small hamlets along country lanes.\n\n\u200cVia Calvor and San Xil - 18.2\nAt the end of town turn right. Cross the main road to follow a smaller road. The way is generally through small hamlets along country lanes.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Fontearcuda",
        "latitude": "42.768503",
        "longitude": "-7.309219",
        "nid": "7637",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "123.7",
        "marker": "City.png",
        "tohere": "1",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ma\u00f1eru",
        "latitude": "42.669815",
        "longitude": "-1.862783",
        "nid": "841",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Ma\u00f1eru is a small village whose streets rival in confusion the best of labyrinths. Like the next village Cirauqui, it boasts a large number of impressive coats of arms carved into the large homes.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "671.6",
        "marker": "City.png",
        "tohere": "5",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Valverde de la Virgen",
        "latitude": "42.569091",
        "longitude": "-5.682764",
        "nid": "915",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "915-133767.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/valverde-de-la-virgen.html?aid=1499405",
        "tosantiago": "294.3",
        "marker": "City.png",
        "tohere": "3.80",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Lavandeira",
        "latitude": "42.777297",
        "longitude": "-7.503533",
        "nid": "987",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "103.3",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Castilla y Le\u00f3n \/ Le\u00f3n",
        "latitude": "42.367340",
        "longitude": "-5.000113",
        "nid": "1449",
        "nodetype": "Province Border",
        "body": "The unceremonious crossing into the province of Le\u00f3n is by way of the bridge over the R\u00edo Valderaduey.  \nLe\u00f3n, tucked into the far northwest corner of Castilla and Le\u00f3n, was formed from the old Kingdom of the same name.  The Meseta begins to wind down, and soon the mountain ranges that form its border with Asturias and Galicia will come into sharp focus.\n\nPerhaps the most celebrated aspects of modern Le\u00f3n are its culinary achievements. The city of Le\u00f3n is a city that turns out to the streets to celebrate food and drink. Sure, they have adopted a passion for the morcilla (blood sausage) from neighboring Burgos, but they perfected their own cecina (a salted, smoked, beef hind leg) to send back over the border. For drink, they turn to the local Bierzo wine (the heartland of which you will walk through soon), and with increasing frequency craft beers whose hops are grown up-river from Hospital de \u00d3rbigo. Given the landscape, it should not surprise that the regional favorite is a wheat beer known as Zerep. A puff pastry known as hojaldre, and a sweet bread called mantecadas, as well as a booming chocolate trade can all be found in Astorga. \nThe Galician mountains are creeping closer now, and by the time you depart Astorga you will begin the ascent.\n\n",
        "image": "1449-639.png",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Saint Jean Pied de Port ",
        "latitude": "43.162462",
        "longitude": "-1.238236",
        "nid": "7495",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Saint Jean Pied de Port is the nexus of several routes that spread out across mainland Europe, and it was from here (primarily but not exclusively) that pilgrims most frequently made the crossing into the Iberian peninsula. \nThe tradition remains and the small town is often saturated with pilgrims, and pilgrim related tourism, during most of the year. With so many newly minted pilgrims roaming the streets, it has an air of excitement mixed with apprehension and a touch of dread.\n\nYou are advised to have a reservation before arriving, and the same holds true for the very few accommodation options that exist between here and Roncesvalles. If reservations are not part of your plans, march on with the philosophy that the camino provides, as it so often does in the most unexpected of ways. \nLandmarks to explore include the Citadelle at the top of town, the Porte de San Jacques, and the Notre Dame du Bout du Pont. Look to the cobbled streets for the well-trodden plaques that point the way to Santiago de Compostela.\n\nBefore you set off for the Pyrenees and the border crossing into Spain be certain to pick up a pilgrim\u2019s credencial at the pilgrims office (Rue de la Citadelle). This accordion-fold booklet is your passport to the camino and will become your most cherished souvenir of the trip. It is a required document in most pilgrim-specific accommodation, earns you pilgrim prices in many museums, and will serve as proof of your journey when presented to the Pilgrims Office in Santiago if you are planning on getting your Compostela Certificate. \n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "1 Rue de la Poste, 64220, Mon-Sat: 0900-1200 & Mon-Fri: 1400-1700",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/fr\/saint-jean-pied-de-port.html?aid=1499405",
        "tosantiago": "767",
        "marker": "City.png",
        "barrest": "Yes",
        "road": "The distance between here and Roncesvalles is the most physically difficult stretch of road you are likely to encounter on the whole of your camino. From the \u201cFoot of the Pass,\u201d it is a steady climb for the majority of the day, with a rather steep descent into Roncesvalles for the last hours. \n\u200cThe Valcarlos Route - 23.5\nThe Valcarlos Route follows close to the road and is the advised route during poor weather. \nIt passes through two towns (with services) located directly on the road between here and Roncesvalles, and two villages that are not on the road and which have no services.\n\n\u200cThe Napoleon Route - 24.3\nThe Napoleon Route is the longer of the two and covers small mountain roads and trails. It was popularized as a way to bypass the banditry that was all too common in the valley below. You will soon see that up in the mountains, there are few places to hide. \nDuring times of poor weather, this route will be closed. When the skies are clear you can see for miles, otherwise you are not likely to see past the horses and sheep that pasture here.\n\n"
    }
}, {
    "node": {
        "title": "Los Arcos",
        "latitude": "42.570876",
        "longitude": "-2.193189",
        "nid": "849",
        "nodetype": "City",
        "atm": "Yes",
        "body": "For an early breakfast or coffee look to the main road (southeast of the church) where you will find a few bars that cater to early-to-rise pilgrims and early-to-work truck drivers.\n\nThe Iglesia de Santa Mar\u00eda de la Asunci\u00f3n, located in the main square, is a radiant sight.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "849-133885.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/los-arcos-html?aid=1499405",
        "tosantiago": "633.8",
        "marker": "City.png",
        "tohere": "11.70",
        "barrest": "Yes",
        "road": "Exiting the town, you pass through the Arco de Felipe V and cross over the R\u00edo Odr\u00f3n. A few of the albergues in town are located on the west side of the river.\n\n"
    }
}, {
    "node": {
        "title": "San Justo de la Vega",
        "latitude": "42.454155",
        "longitude": "-6.014800",
        "nid": "923",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "923-133904.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/san-justo-de-la-vega.html?aid=1499405",
        "tosantiago": "260.9",
        "marker": "City.png",
        "tohere": "7.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Moimentos",
        "latitude": "42.787158",
        "longitude": "-7.563792",
        "nid": "995",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "97.3",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Alto de Mezqu\u00edriz",
        "latitude": "42.979880",
        "longitude": "-1.386750",
        "nid": "3031",
        "nodetype": "Alto",
        "tosantiago": "734.5",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Azofra",
        "latitude": "42.424217",
        "longitude": "-2.800913",
        "nid": "857",
        "nodetype": "City",
        "body": "A somewhat sleepy town, it is well maintained and a suitable place for breakfast if you set off from Najera in the morning. The municipal albergue here is well appointed, and all rooms are double rooms.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "857-133397.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/azofra.html?aid=1499405",
        "tosantiago": "571.4",
        "marker": "City.png",
        "tohere": "5.60",
        "barrest": "Yes",
        "road": "The camino between Azafra and Cirue\u00f1a has been modified in recent years to navigate around a large and largely unused golf course and neighborhood, and in the process the village of Ciri\u00f1uela was bypassed entirely.\n\n"
    }
}, {
    "node": {
        "title": "Alto de la Pedraja",
        "latitude": "42.372955",
        "longitude": "-3.341796",
        "nid": "3036",
        "nodetype": "Alto",
        "body": "The path here intersects with several ATV trails. It is well marked but still requires some attention. Like the river crossings in Navarra, the low scrubland that surrounds you here was  a popular place for bandits to lay in wait. \nA lovely new oasis has sprung up here at the hand of a pilgrim and artist who encourages you to add your own personal touch to the small forest rest stop. The hammocks are a welcome break too.\n\n",
        "image": "3036-133805.webp",
        "tosantiago": "518.4",
        "marker": "Alto.png",
        "barrest": "",
        "road": "Il cammino qui si interseca con diversi sentieri ATV. \u00c8 ben segnalato, ma state comunque attenti. Come gli attraversamenti fluviali in Navarra, la bassa vegetazione di questi posti costituiva un ottimo nascondiglio per gli agguati dei banditi.\n\n\nUna nuova oasi \u00e8 sorta qui per mano di un pellegrino artista, che ti incoraggia ad aggiungere il tuo tocco personale al piccolo punto di sosta nel bosco. Siete i benvenuti ad approfittare delle amache per una piccola pausa."
    }
}, {
    "node": {
        "title": "Alto do Poio",
        "latitude": "42.712462",
        "longitude": "-7.126114",
        "nid": "8159",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "8159-133854.webp",
        "tosantiago": "144.3",
        "marker": "City.png",
        "tohere": "0.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Redecilla del Camino",
        "latitude": "42.438122",
        "longitude": "-3.065100",
        "nid": "862",
        "nodetype": "City",
        "body": "Redecilla is the first town in Castilla y Le\u00f3n along the Camino Franc\u00e9s and was an important stop along the way during the 12th century, when two pilgrim hospices cared for pilgrims.  One of them, San L\u00e1zaro, lends its name to the municipal albergue.\n\nThe gem of Redecilla is the Romanesque baptismal font in the Iglesia de Nuestra Se\u00f1ora de la Calle.\n\nThere is a tourist information office here, remarkable given the size of the town but easily explained by its location.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "862-133781.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/redecilla-del-camino.html?aid=1499405",
        "tosantiago": "545.6",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Compostilla",
        "latitude": "42.564732",
        "longitude": "-6.596475",
        "nid": "936",
        "nodetype": "City",
        "body": "Compostilla is a curious place. It was largely built by a private mining and energy company and its rectilinear streets, complete with football pitches and schools, is reminiscent of something out of an upper class North American neighborhood. \nOn your way through you will pass Nuestra Se\u00f1ora del Refugio, an indication that Compostilla had a place for pilgrims long ago.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "936-133844.webp",
        "tosantiago": "201.9",
        "marker": "City.png",
        "tohere": "3.20",
        "barrest": "Yes",
        "road": "You will pass underneath the highway, and past the Ermita de San Blas and San Roque before arriving at Columbrianos where a large zebra crossing takes you over the busy road.\n\n"
    }
}, {
    "node": {
        "title": "Ligonde",
        "latitude": "42.857721",
        "longitude": "-7.778964",
        "nid": "1008",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "75.9",
        "marker": "City.png",
        "tohere": "3",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Calzadilla de los Hermanillos",
        "latitude": "42.434100",
        "longitude": "-5.157394",
        "nid": "3125",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "348.2",
        "marker": "City.png",
        "tohere": "8.50",
        "barrest": "Yes",
        "road": "The camino from Calzidalla once went directly to Mansilla de la Mulas, bypassing Reliegos. The route proved to be less desirable than going first to Reliegos which is close enough. The split occurs 1km before arriving at Reliegos and there may still exist conflicting arrows: to get to Reliegos keep straight at a four-way intersection or to go to Mansilla turn right. Both routes are the same distance to Mansilla.\n\n"
    }
}, {
    "node": {
        "title": "Villafranca Montes de Oca",
        "latitude": "42.389457",
        "longitude": "-3.308086",
        "nid": "870",
        "nodetype": "City",
        "body": "To the right of the main road when entering are a pair of bars that serve food to a mix of pilgrims and long road truck drivers, the Meson Alba being the most authentic dive of the two.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villafranca-montes-de-oca.html?aid=1499405",
        "tosantiago": "522",
        "marker": "City.png",
        "tohere": "3.40",
        "barrest": "Yes",
        "road": "Villafranca Montes de Oca, named for the nettle-filled mountains to the west, sits at the foot of the Pedraja pass. If you have become accustomed to the gently rolling hills behind you, brace yourself for a climb. Between here and San Juan de Ortega lay 12km of fountain-less and featureless hiking. There are no shops in San Juan, but a bar there does serve food. Pack accordingly. To follow the camino, turn right uphill towards the church.\n\n"
    }
}, {
    "node": {
        "title": "Trabadelo",
        "latitude": "42.649427",
        "longitude": "-6.882162",
        "nid": "944",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "944-133850.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/trabadelo.html?aid=1499405",
        "tosantiago": "171.2",
        "marker": "City.png",
        "tohere": "4.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Palas de Rei",
        "latitude": "42.872688",
        "longitude": "-7.868437",
        "nid": "1017",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Palas is bisected by a pair of large roads which twist and turn through town. After coming down the staircase, and with the municipal albergue on your left, the road to the right leads to a grocery and back to Portomarin. Straight on to bars and restaurants and albergues and the road out of town.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "1017-536.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/palamos.html?aid=1499405",
        "tosantiago": "67.4",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": "Yes",
        "road": "You have now entered eucalyptus territory, and although there are patches of old oak forests to be found this non-indigenous species has taken over the landscape and has become a symbol of Galicia.\n\n"
    }
}, {
    "node": {
        "title": "\u200cWARNING: Finding the river route can be tricky",
        "latitude": "42.343653",
        "longitude": "-3.627419",
        "nid": "7407",
        "nodetype": "POI",
        "body": "From Casta\u00f1ares there are again two routes into Burgos, the Road Route and the River Route.   The latter can be difficult to find because businesses repaint the yellow arrows on an almost daily basis. The most important part to remember is that the River Route does not leave Casta\u00f1ares along the busy road, see map. \nTo find it, cross the main road in Casta\u00f1ares near the bar La Peregrina. A short ways on you will find a small plaza and children\u2019s play area with a fountain. Stay on that road, the Lugar de Barrio Casta\u00f1ares, towards a cement factory at its end. \nFrom there turn right to pick up the trail again. It crosses a small bridge, parallels the A1 for a short distance (well separated from the road) before passing underneath it. At this point you can follow the trail, keeping the river on your right-hand side all the way into the center of Burgos. You will pass the circular Plaza de Toros and several bridges across the river. If you cross the river at the Puente de San Pablo you will be near the center of town and not far from the Municipal albergue.\n\n",
        "image": "7407-133759.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Tardajos",
        "latitude": "42.349063",
        "longitude": "-3.815478",
        "nid": "880",
        "nodetype": "City",
        "atm": "Yes",
        "body": "There are a few bar\/bakeries and a pharmacy on your right as you enter the town, the former serve up Torta de Aceite, a type of flatbread that is both delicious and unique to the area.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "880-303.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/tardajos.html?aid=1499405",
        "tosantiago": "473.2",
        "marker": "City.png",
        "tohere": "11.20",
        "barrest": "Yes",
        "road": "The camino departs from the main road in Tardajos and from here you won\u2019t return to anything resembling a busy road until Fr\u00f3mista, do not be tempted to keep on the main road but rather cross the street and pass through the village.\n\n"
    }
}, {
    "node": {
        "title": "Arn\u00e9guy",
        "latitude": "43.108808",
        "longitude": "-1.282246",
        "nid": "9907",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "9907-134826.webp",
        "tosantiago": "757.9",
        "marker": "City.png",
        "tohere": "8.30",
        "barrest": "Yes",
        "road": "In Arn\u00e9guy you will cross the main road (D933) and the bridge. The way will lead you to a country road that is lower in the valley and which will bring you to Valcarlos from the lower part of town.\n\n"
    }
}, {
    "node": {
        "title": "Laguna de Castilla",
        "latitude": "42.701204",
        "longitude": "-7.021165",
        "nid": "952",
        "nodetype": "City",
        "body": "Laguna sits about 1km from the Galician border, making it a rather remote frontier town.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "155",
        "marker": "City.png",
        "tohere": "2.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Furelos",
        "latitude": "42.908825",
        "longitude": "-7.998852",
        "nid": "1025",
        "nodetype": "City",
        "body": "In addition to the Iglesia de San Juan, with its uncommon crucifix, Furelos has a bridge and a bar. Both are impressive, the former for dating back to the Romans, and the latter for the tortillas.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "1025-134735.webp",
        "tosantiago": "54.2",
        "marker": "City.png",
        "tohere": "3.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Magdalena Bridge - Turn left for Casa Paderborn",
        "latitude": "42.820995",
        "longitude": "-1.638063",
        "nid": "7415",
        "nodetype": "POI",
        "body": "14th century bridge, restored in the 1960s. The albergue Casa Paderborn is along this river. Turn left after crossing if you plan to stay there.\n\n",
        "image": "7415-52720.webp",
        "tosantiago": "701.4",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Itero de la Vega",
        "latitude": "42.287215",
        "longitude": "-4.259906",
        "nid": "888",
        "nodetype": "City",
        "body": "The Iglesia parroquial de San Pedro Ap\u00f3stol (at the entrance to town) has an image of Santiago Peregrino. \nWe can learn a bit about the name of the town, the name of the river, and the word for the stones that mark the camino. They all derive from hito (Fitero, Itero, and hito), which describes a stone that has been driven into the ground, and which is used to indicate a frontier or boundary.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "432.6",
        "marker": "City.png",
        "tohere": "1.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pasantes",
        "latitude": "42.751519",
        "longitude": "-7.221580",
        "nid": "960",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "133.8",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ribadiso da Carretera",
        "latitude": "42.930618",
        "longitude": "-8.136296",
        "nid": "1034",
        "nodetype": "City",
        "body": "There is now an albergue in the upper part of Ribadiso. It is located at the top of the climb and before you get to Arzua. The albergue here is bookable online.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "40.8",
        "marker": "City.png",
        "tohere": "0.80",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Zubiri [Esteribar]",
        "latitude": "42.930411",
        "longitude": "-1.504655",
        "nid": "821",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Zubiri, like Larrosoa\u00f1a ahead, is adjacent to the camino and not strictly on it. You do not need to cross the bridge if you don\u2019t plan on stopping here. If you do go into town, retrace your steps and turn right once back over the bridge.\n\nTwo restaurants offer pilgrim menus. They are on opposite ends of town, the first near the main square and the other down the road. The shop here offers everything you need to put together a meal.\n\nOn extremely busy days, the polideportivo (sports complex) that is adjacent to the municipal albergue is used to accommodate pilgrims.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/zubiri.html?aid=1499405",
        "tosantiago": "721",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Calzadilla de la Cueza",
        "latitude": "42.329108",
        "longitude": "-4.803686",
        "nid": "896",
        "nodetype": "City",
        "body": "Calzadilla\u2019s Iglesia de San Mart\u00edn, possibly the least visited church along the camino, has a generous display of crosses of Santiago. \n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "383.1",
        "marker": "City.png",
        "tohere": "17",
        "barrest": "Yes",
        "road": "The camino between here and Ledigos, a distance of 6.2km, has several detours. A large stone marker at the edge of town indicates the possible routes ahead. These variations will take you further into the hillside and away from the road. Most merge shortly before entering Ledigos, but the one that keeps fartest to the left (known as the Palomares route because of the number of dovecotes along the way) bypasses both Ledigo and Teradilllos and is the shortest route to Moratinos.\n\n"
    }
}, {
    "node": {
        "title": "Samos",
        "latitude": "42.731990",
        "longitude": "-7.326293",
        "nid": "968",
        "nodetype": "City",
        "atm": "Yes",
        "body": "It is difficult to separate Samos from the monastery that dominates this small village. The monastery albergue here is reminiscent of the old albergue in Roncesvalles; one long vaulted space full of hobbling pilgrims, snoring, and laughter. I cannot recommend the experience enough.\n\nOpposite the gas station is the R\u00faa do Salvador. At the end of this road are a wonderful shaded park and one of the oldest surviving buildings on the camino, the 11th century Capilla del Cipr\u00e9s.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "968-133866.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/samos.html?aid=1499405",
        "tosantiago": "128.8",
        "marker": "City.png",
        "tohere": "4.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Salceda",
        "latitude": "42.926211",
        "longitude": "-8.277801",
        "nid": "1043",
        "nodetype": "City",
        "body": "There are a pair of bars in Salceda, and a restaurant (La Esquipa) that is thick with pilgrims every day but Monday when it is closed.",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/salceda-es-1.html?aid=1499405",
        "tosantiago": "27.6",
        "marker": "City.png",
        "tohere": "1.80",
        "barrest": "Yes",
        "road": "The camino leaves town to the right of a wedge shaped park next to La Esquipa, not along the road.\n\n"
    }
}, {
    "node": {
        "title": "Alternate Route to Los Arcos, skipping Estella",
        "latitude": "42.658285",
        "longitude": "-1.994426",
        "nid": "7431",
        "nodetype": "POI",
        "body": "From Villatuerta there is an alternate route which passes to the south of Estella and continues to Los Arcos via the towns of Noveleta, Zaraputz, and the Monastery of Irache. From there you can either continue along the camino to Villamayor de Monjardin or choose another alternate route through Luqu\u00edn and on to Los Arcos. This was the route favored by pilgrims prior to the development of Estella in the 12th. c.\n\nTo bypass Estella and go directly to the Monastery of Irache, turn Left at the church in Villatuerta. In years past there was a bright orange house here. You want it on your Right, and the streetside parking of the Church on your Left.\n\nCarry on straight. The road will turn to a track as soon as you leave town.\n\nTurn Left and go under the NA-132 (which leads to Estella). The houses of Noveleta will be on your right. Stay on this track, go under the A-12, and turn Right. When you get to the river the track will turn Left. Cross the bridge on your right, and turn Right again to follow the river a short distance. Very quickly the track turns Left up a steep hill and past the ruins of a Hospital de Peregrinos. From here the camino parallels a tree-lined asphalt road. Cross the first intersection and turn Left towards a pedestrian foot bridge over the NA-122.\n\nCarry on the gravel path until it joins a larger track and keep to your right. The trail twists and turns a bit but will turn into an asphalt road when it widens with cemetary ahead on your right. DO NOT walk as far as the cemetary, but rather turn Left onto a small path as soon as you get to the asphalt. The trail will turn the the right at a solitary tree. A short distance on, turn Left to arrive at an asphalt road. Turn Right here, and continue until it is joined by another well used track from your left. In 130m, turn Left into the the trees.\n\nIn 750 meters you will arrive at a junction. From here you will have two options.\u20281. Continue straight to Luqu\u00edn (bar, pool) and from there Los Arcos.\u20282. Turn right towards the Monastery of Irache, and the Fountain of Wine.\n\nIf you want to skip Irache, but continue on the camino via Villayamor de Monjardin, go under the highway, but turn left 300m later to rejoin the camino coming from Estella.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Arleta",
        "latitude": "42.847118",
        "longitude": "-1.586838",
        "nid": "829",
        "nodetype": "City",
        "body": "The pair of buildings here are the Palace of the Lords and the Iglesia de Santa Marina. Neither are open to the public but there is place enough for a rest or a picnic before tackling the suburbs of Pamplona.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "707.1",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "",
        "road": "The camino passes through a tunnel beneath the highway and then climbs up and around the Monte Miravalles before nearing Trinidad de Arre.\n\n"
    }
}, {
    "node": {
        "title": "El Burgo Ranero",
        "latitude": "42.422950",
        "longitude": "-5.221553",
        "nid": "904",
        "nodetype": "City",
        "body": "As far as the eye can tell the only thing keeping El Burgo Ranero on the map is the business brought in by the camino; the wheat and wool business that once sustained it has been replaced by mechanization.\n\nAlthough the church in town was robbed of its treasures long ago (some are preserved in the Le\u00f3n Cathedral Museum) the bells still ring, and an ever aging congregation turns up daily.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "904-133833.webp",
        "tosantiago": "343.1",
        "marker": "City.png",
        "tohere": "7.50",
        "barrest": "Yes",
        "road": "The camino between El Burgo and Reliegos is along a manicured path lined with trees. Those trees, unfortunately, are on the wrong side of the path, so on sunny summer days you are exposed: bring ample amounts of water.\n\n"
    }
}, {
    "node": {
        "title": "Carballal",
        "latitude": "42.776105",
        "longitude": "-7.388281",
        "nid": "976",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "116.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Cimadevila",
        "latitude": "42.905645",
        "longitude": "-8.398061",
        "nid": "1052",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "16",
        "marker": "City.png",
        "tohere": "0.40",
        "barrest": ""
    }
}, {
    "node": {
        "title": "\u200cALTERNATE ROUTE  to TRABADELO, via PRADELA +1.7km ",
        "latitude": "42.608988",
        "longitude": "-6.811234",
        "nid": "7472",
        "nodetype": "POI",
        "body": "Leave town by way of the bridge over the R\u00edo Burbia. At the end of the bridge, the camino splits. Most pilgrims keep along the road which winds along the valley floor following the old National VI road, in the shadow of its replacement which soars above. The camino here is on paved asphalt, the old road surface, but is separated from the road by a barrier wall. There are numerous places where it crosses the road, and you are encouraged to watch out for traffic here; it is infrequent but speedy.\n\nHowever, if you fancy a climb and a not too long detour, look for a narrow path on your right immediately after crossing the bridge. It looks like a hastily poured concrete drive and starts off quite steep. The way offers much better views with little to no traffic and it passes through a chestnut forest. There is one town along the way, Pradela, which has a bar and a small albergue.\n\nThe route via Pradela adds less than two kilometers of distance to the day, but a not insignificant amount of elevation gain\/loss. The two routes rejoin in Trabadelo.\n\n",
        "tosantiago": "180.5",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "O Pacio de Fafi\u00e1n",
        "latitude": "42.758286",
        "longitude": "-7.404056",
        "nid": "7632",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Muruzabal",
        "latitude": "42.689691",
        "longitude": "-1.770945",
        "nid": "838",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/www.booking.com\/searchresults.html?city=-393504&aid=1499405",
        "tosantiago": "680.9",
        "marker": "City.png",
        "tohere": "2.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Le\u00f3n",
        "latitude": "42.599409",
        "longitude": "-5.566987",
        "nid": "912",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Le\u00f3n is now a fully modernized city home to more than half of the population in the province and like Burgos it is worthy of more time than the average pilgrim spends here. To explore in full would take many days, to cover the basics at least two. If you are keen to keep your feet moving every day, consider spending an extra afternoon in Le\u00f3n and setting out after lunch for a half day of walking. Between here and Astorga there are two routes to choose from and plenty of albergues.\n\nThe Catedral de Santa Mar\u00eda de la Regla is a magnificent example of Gothic architecture whose multicolored windows are not to be missed, particularly on a sunny day when the interior is filled with light. The Virgen Blanca on the main portico is a reproduction, the original is in a chapel behind the main altar. You\u2019ll also notice that quite a few sculptures have been removed for restoration. \nThe Iglesia de San Isidoro is the other not to be missed church; it houses the relics of San Isidoro and is the site of the Pante\u00f3n de los Reyes where 23 of the Leonese monarchy are resting. It also preserves some of the best, and most intriguing, 13th century Romanesque frescoes as well as the easternmost Holy Door on the Camino Franc\u00e9s. \nFans of Gaudi will not want to miss the Casa de Los Botines, just a short stroll from the Cathedral, and perhaps capture a selfie with a bronze statue of the architect seated on a bench in front.\n\nThe best food and the liveliest crowds are in the Barrio Humedo, a twisted knot of narrow streets near the Plaza Mayor whose well-earned name implies that the drinks are overflowing. The region of Le\u00f3n is unique in Spain as the only one that grows hops for the production of beer. A few new breweries have appeared on the scene in recent years (Four Lions is recommended) and most produce a local wheat beer known as Zerep.\n\nAs you walk around town you will notice several types of plaques embedded in the pavement. One is the impression of the left foot of a Roman soldier (their marches begain with the left foot) and marks the boundary of the original Roman settlement and wall. The other, seen closer to the Plaza Mayor, is a symbol of the peninsula and bears the Hebrew word \u05e1\u05e4\u05e8\u05d3 which translates as Sefarad, the Jewish name for Spain. It was that Jewish population that was expelled from Spain under the Alhambra Decree in 1492. The marker indicates the start of what was once the original Jewish barrio of Le\u00f3n.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Jard\u00edn de San Francisco, 24004, Mon-Fri: 0830-2030 & Sat: 0930-1300, 987 876 081",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "912-417_0.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/leon.html?aid=1499405",
        "tosantiago": "305.7",
        "marker": "City.png",
        "tohere": "3",
        "barrest": "Yes",
        "road": "Perhaps the best place to get your bearings is the Cathedral, and the familiar yellow arrows are visible directly opposite the principle facade. The journey through town is short, passing first the Church of San Isidoro and later the Parador (once hospital) of San Marcos before crossing over the R\u00edo Bernesga and leaving the city limits. That said, it is several more kilometers through the suburbs of Trobajo and La Virgen.\n\n"
    }
}, {
    "node": {
        "title": "Leim\u00e1n",
        "latitude": "42.780193",
        "longitude": "-7.483270",
        "nid": "984",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "105.5",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "La Rioja",
        "latitude": "42.489635",
        "longitude": "-2.414423",
        "nid": "1446",
        "nodetype": "Province Border",
        "body": "You have now left Navarra behind you, and the basque \u2018feeling\u2019 which has been fading since Roncesvalles will now disappear almost entirely. The name La Rioja, which first appears in a charter from 1099, is fairly new; prior to that, it was referred to as the province of Logro\u00f1o (the city you enter next and the capital of the province). From a legal perspective, the name La Rioja was not legitimized until after the 1975 death of Francisco Franco when Spain transitioned into a democratic monarchy.\n\nLa Rioja is the second smallest autonomous community in Spain and the smallest along the Camino Franc\u00e9s. The camino will pass through several of it\u2019s larger cities, including Logro\u00f1o, Santo Domingo de la Calzada (which is next to the \u201cR\u00edo Oja\u201d, and N\u00e1jera. It will also pass through many smaller villages, whose populations seldom exceed 200.\n\nIt\u2019s much fought over history and diminutive size aside, La Rioja has managed to make its mark on the world as one of Spain\u2019s largest producers of wine. Enjoy it while it lasts, which is only about 60km worth of walking along the camino. If that strikes you as too little, consider a detour to the Monasterios de Yuso y Suso; just enough to prolong your stay and explore some of the finest Monasteries around.\n\nTo the south of Logro\u00f1o lies the town and castle of Castillo de Clavijo. It was there in 844 during a battle between Ramiro I of Asturias and Abd ar-Rahman II of C\u00f3rdoba that the legend of Santiago Matamoros was born. \nThe legend tells of the return of Santiago (long since deceased remember) on a white horse and dressed for battle. Such a sight energized the Christian fighters (who had been on the loosing side of the Moorish invasion) who went on to defeat Abd ar-Rahman. The scene is depicted in countless places along the camino, with Santiago riding high and the moors trampled underfoot.\n\n",
        "image": "1446-136926.webp",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Collado de Bentartea",
        "latitude": "43.047400",
        "longitude": "-1.265500",
        "nid": "7492",
        "nodetype": "Alto",
        "body": "Also known as Napoleon\u2019s Pass. There is often a small food truck which sells drinks and snacks, and which offers the last stamp in France.\n\n",
        "image": "7492-134836.webp",
        "tosantiago": "750.8",
        "marker": "Alto.png",
        "tohere": "5",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ayegui",
        "latitude": "42.656687",
        "longitude": "-2.038436",
        "nid": "846",
        "nodetype": "City",
        "body": "This town has grown to connect with Estella.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "652.9",
        "marker": "City.png",
        "tohere": "2",
        "barrest": "Yes",
        "road": "On the way out of Estella, look for the arrows at the second roundabout which direct you away from the road and onto a track which runs up and to the right, taking you behind the commercial shopping centers and into Ayegui.\n\nAt the end of town, BEFORE you get to a small park, blue signs with arrows point you downhill and to your left. Follow them, cross over the main road, and resume the camino towards to the Fuente de Vino and the Monasterio de Irache.\n\n"
    }
}, {
    "node": {
        "title": "Hospital de Orbigo",
        "latitude": "42.463475",
        "longitude": "-5.884486",
        "nid": "920",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "920-133366.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/hospital-de-orbigo.html?aid=1499405",
        "tosantiago": "273.5",
        "marker": "City.png",
        "tohere": "0.60",
        "barrest": "Yes",
        "road": "\u200cMinor Detour via Villares de \u00d3rbigo\nWhen leaving town, the camino once again splits. The preferred (though unofficial) path is to the right, passing through nearby Villares de \u00d3rbigo and scenic countryside. Both routes now rejoin in Santiba\u00f1ez, though if you take the road route it is also possible to stay on the road all the way to San Justo de la Vega. Unless it has been particularly rainy the route through Santiba\u00f1ez is the recommended and most scenic route. For either option, take plenty of water when departing Santiba\u00f1ez.\n\n"
    }
}, {
    "node": {
        "title": "Mirallos",
        "latitude": "42.783797",
        "longitude": "-7.536523",
        "nid": "992",
        "nodetype": "City",
        "body": "At the bottom of the hill from Ferreiros, there is a bar and a church\/cemetery.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "992-134718.webp",
        "tosantiago": "100.1",
        "marker": "City.png",
        "tohere": "0.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Oncina de la Valdoncina",
        "latitude": "42.551689",
        "longitude": "-5.660877",
        "nid": "2634",
        "nodetype": "City",
        "body": "Manuel runs a small donativo and geodesic kiosk in town. Drinks and snacks available, as well as some souvenir stuff.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "297.3",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Navarrete",
        "latitude": "42.430742",
        "longitude": "-2.564793",
        "nid": "854",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Navarrete, much fought over and mostly destroyed, no longer has any of its medieval history to share. It does, however, have a few covered arcades which give you a sense of its past. Like is common on an otherwise flat land, the town was developed around a hill; you will see plenty more of this strategy before you get back into the mountains.\n\nThe Iglesia de Santa Maria de la Asuncion deserves an extra mention here in case you were thinking to skip it. Don\u2019t. It maintains hours which can be difficult for pilgrims but do try to make a visit.\n\nIf you are staying in Navarrete for the night, you might consider a small climb the \u2018Tede\u00f3n\u2019 to where the castle once stood. There is a nice picnic park there and a good view of the surrounding land.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "854-133893.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/navarrete.html?aid=1499405",
        "tosantiago": "593.2",
        "marker": "City.png",
        "tohere": "12.50",
        "barrest": "Yes",
        "road": "On the way into Navarrete you pass the remains of 12th century San Juan de Acre. While there is not much left, the footprint remains well enough preserved to spark the imagination. The original entrance to the former pilgrim hospital has been reincarnated as the entrance to the town cemetery which is passed on your way out of Navarrete.\n\n"
    }
}, {
    "node": {
        "title": "Rabanal del Camino",
        "latitude": "42.481830",
        "longitude": "-6.282635",
        "nid": "928",
        "nodetype": "City",
        "body": "Now home to 4 albergues and many pensions, Rabanal is a common (and wise) stopping point along the way. Ahead is a high mountain pass that demands a bit of rest.\n\nVespers are sung by the monks every night at 7pm at the small parish Iglesia de Santa Mar\u00eda and a benediction for pilgrims is held afterward. \nThe town has always enjoyed a level of prosperity because of the camino, and in recent years has developed a slightly bohemian feel.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "928-455.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/rabanal-del-camino.html?aid=1499405",
        "tosantiago": "236.9",
        "marker": "City.png",
        "tohere": "6.60",
        "barrest": "Yes",
        "road": "The camino leaves town along the same cobbled path that you found at the start of town, which leads past the church. It does NOT leave along the road.\n\n"
    }
}, {
    "node": {
        "title": "Portomar\u00edn",
        "latitude": "42.807681",
        "longitude": "-7.615585",
        "nid": "1000",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Portomar\u00edn holds the distinction of being the newest oldest town along the camino. The Portomar\u00edn we see today is a transplanted version of the original town that originally settled in the valley below. Most of the town is new construction, but the church and a few smaller buildings were relocated stone by stone. Close inspection of the Iglesia de San Juan shows that the stones were numbered to avoid head scratching later.\n\nThe Embalse de Belasar sits beneath the bridge, a swollen version of the recently (\u201856) dammed R\u00edo Mi\u00f1o.\n\nThe Iglesia of San Juan (also known as the Iglesia of San Nicol\u00e1s) is an imposing fortress in the center of town. It is the largest single-nave Romanesque church in Galicia. The Ayuntamiento building in the main square was once the Casa del Conde from the 16th century. The Iglesia de Santa Mar\u00eda (also known as La Virgen de las Nieves) is the chapel that you passed under at the top of the stairs going into town; the local people believe that it will protect them from drowning.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "1000-532.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/portomarin.html?aid=1499405",
        "tosantiago": "91.7",
        "marker": "City.png",
        "tohere": "2",
        "barrest": "Yes",
        "road": "From the square simply head downhill along the colonnaded street and stick to it until you arrive at the main road. DO NOT keep going straight. Rather turn left and head back in the direction of the bridge into town. Before you get there arrows will direct you onto a different bridge over a small river that feeds the reservoir. At the end of the bridge are two options: TURN RIGHT. From here it is a steadily uphill march all the way to Gonzar, passing Toxibo with its h\u00f3rreo along the way. After passing through a stretch of forest the camino returns to the main road and parallels it on a gravel track. This track crosses back and forth over the main road on several occasions. Be mindful of traffic here, particularly during the morning hours when the area can be thick with fog.\n\n"
    }
}, {
    "node": {
        "title": "Cirue\u00f1a",
        "latitude": "42.410720",
        "longitude": "-2.896013",
        "nid": "859",
        "nodetype": "City",
        "body": "If you are interested in a coffee or breakfast in Cirue\u00f1a, turn Left on the main road and walk for about 200m.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/ciruena.html?aid=1499405",
        "tosantiago": "562.2",
        "marker": "City.png",
        "tohere": "9.20",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Molinaseca",
        "latitude": "42.537690",
        "longitude": "-6.519201",
        "nid": "933",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Molinaseca is a popular weekend destination for ponferradinos, who make the short drive to swim in the dammed up R\u00edo Maruelo. The riverfront makes for an excellent place to swim, cool down, and mingle.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "933-135077.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/molinaseca.html?aid=1499405",
        "tosantiago": "212.4",
        "marker": "City.png",
        "tohere": "4.50",
        "barrest": "Yes",
        "road": "Between Molinaseca and Ponferrada you will be almost exclusively on the sidewalk adjacent to the road. Three kilometers from the center of Molinaseca the camino splits. The camino technically turns left at the curve in the road and passes through Campo on the way to Ponferrada. Turning right to follow the road will get you to Ponferrada just the same and is a shorter path to the Municipal albergue.\n\n"
    }
}, {
    "node": {
        "title": "Ventas de Nar\u00f3n",
        "latitude": "42.844250",
        "longitude": "-7.748880",
        "nid": "1005",
        "nodetype": "City",
        "body": "The bar O Cruceiro opens at 6 am, and there is a wonderful fountain in the picnic area. The small chapel here has a stamp, which is given by its blind caretaker with a bit of help from you.\n\nThe land in this area is poor for farming as the soil is rich with quartz and feldspar. The ridge that you are walking towards is the watershed between the R\u00edo Mi\u00f1o (behind you, which empties into the Atlantic near Guarda) and the R\u00edo Ulla (ahead of you, and which empties into the Atlantic near Padron). The area is covered thickly with broom.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "78.9",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Alto de Cerezales",
        "latitude": "42.489093",
        "longitude": "-6.419964",
        "nid": "3041",
        "nodetype": "Alto",
        "tosantiago": "223.9",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Tosantos",
        "latitude": "42.413825",
        "longitude": "-3.243585",
        "nid": "867",
        "nodetype": "City",
        "body": "In recent years, the diminutive Tosantos has grown up a bit and now has a bar. The albergue parroquial is somewhat unique and maintains a tradition of communal meals and singalongs. \nThe nearby Ermita de la Pe\u00f1a, built into the cliffs, is a must see. A statue of Jesus as a child was hidden there for protection during the 9th century. It now spends half the year in the Ermita and the other half in the Iglesia de San Esteban. Ask there for the key. If you stay in the albergue here, the hospitalero will likely offer a guided visit.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "529",
        "marker": "City.png",
        "tohere": "4.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pieros",
        "latitude": "42.605758",
        "longitude": "-6.749296",
        "nid": "941",
        "nodetype": "City",
        "body": "The Iglesia de San Mart\u00edn de Tours still bears the inscription indicating that it was commissioned by the bishop of Astorga. San Mart\u00edn of Tours was, prior to the popularization of Santiago (and his battle at Clavijo), the patron saint of most of the peninsula.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "187.7",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Mamurria",
        "latitude": "42.874908",
        "longitude": "-7.829160",
        "nid": "1013",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "70.8",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Casta\u00f1ares",
        "latitude": "42.343836",
        "longitude": "-3.627312",
        "nid": "7028",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "7028-134928.webp",
        "tosantiago": "490.9",
        "marker": "City.png",
        "tohere": "4.80",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Carde\u00f1uela R\u00edopico",
        "latitude": "42.359971",
        "longitude": "-3.557768",
        "nid": "875",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "498",
        "marker": "City.png",
        "tohere": "1.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Las Herrer\u00edas",
        "latitude": "42.670932",
        "longitude": "-6.979709",
        "nid": "949",
        "nodetype": "City",
        "body": "Las Herrerias, and the conjoined hamlet of Hospital, are at the base of the mountain that has been lurking on the horizon for a few days. \nAt the end of town, just past Casa Polin, is a small picnic area and the Fuente de Qui\u00f1ones (named after the famous jouster of Hospital de \u00d3rbigo).\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "949-135128.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/las-herrerias.html?aid=1499405",
        "tosantiago": "160.9",
        "marker": "City.png",
        "tohere": "1.40",
        "barrest": "Yes",
        "road": "The climb begins as a gradual ascent along the paved road. The camino will eventually leave this road in favor of a footpath which descends to your left. Arrows at the turn-off indicate that the camino for cyclists remains on the road and if you are on a bike this is the preferred route. When it trail begins to climb again the path is rocky and steep and slick with mud and horse shit. Tread carefully and think happy thougths. \n"
    }
}, {
    "node": {
        "title": "Campanilla",
        "latitude": "42.881558",
        "longitude": "-7.952168",
        "nid": "1022",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "59.5",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Ant\u00f3n - The Ruins",
        "latitude": "42.292890",
        "longitude": "-4.098887",
        "nid": "885",
        "nodetype": "City",
        "body": "The ruins of the monastery and pilgrim hospital of San Ant\u00f3n is a delightful space that seems to embrace the camino, its archway spanning the road itself. Once a sprawling complex there is little that remains.\n\nThe albergue here has never seen better days, and the efforts of a dedicated team of volunteer hospitaleros keep the place as traditional as can be expected.\n\nThe niches in the archway, previously used by monks to deliver food to late arriving pilgrims, are now a popular spot to leave written messages.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "885-325.webp",
        "tosantiago": "447.2",
        "marker": "City.png",
        "tohere": "5.70",
        "barrest": ""
    }
}, {
    "node": {
        "title": "A54 Junction",
        "latitude": "42.914484",
        "longitude": "-8.414068",
        "nid": "12501",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "13.8",
        "marker": "City.png",
        "tohere": "26.90",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Fonfr\u00eda",
        "latitude": "42.731000",
        "longitude": "-7.156305",
        "nid": "957",
        "nodetype": "City",
        "body": "Small shop inside the albergue. The village is quite small, only 40 people. Three of the families make their living producing Queixo de Cebreiro, which can be sampled at the bar in the albergue.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/fonfria-html?aid=1499405",
        "tosantiago": "141.2",
        "marker": "City.png",
        "tohere": "3.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Casta\u00f1eda",
        "latitude": "42.926066",
        "longitude": "-8.104160",
        "nid": "1031",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "44.1",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": "Yes",
        "road": "It is uphill once more, followed by a steady descent into the valley below, steep at points.\n\n"
    }
}, {
    "node": {
        "title": "Espinal [Aurizberri]",
        "latitude": "42.979284",
        "longitude": "-1.367798",
        "nid": "818",
        "nodetype": "City",
        "body": "An alternative to spending the night in Roncesvalles for pilgrims arriving there too late in the evening to walk a full day, as the stretch between the two is 5km along mostly flat ground.\n\nBetween here and Pamplona, you will notice many villages perched above the valley floor. This was to maximize both farmland (shared and more fertile near the river) and security.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "image": "818-134835.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/espinal-auzperri.html?aid=1499405",
        "tosantiago": "736.4",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villarmentero de Campos",
        "latitude": "42.297807",
        "longitude": "-4.499447",
        "nid": "893",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villarmentero-de-campos.html?aid=1499405",
        "tosantiago": "410.1",
        "marker": "City.png",
        "tohere": "1.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Lastres",
        "latitude": "42.740419",
        "longitude": "-7.295051",
        "nid": "965",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "130",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Calle - O Outeiro",
        "latitude": "42.917922",
        "longitude": "-8.242369",
        "nid": "1040",
        "nodetype": "City",
        "body": "Calle is a town forsaken by modernization. Apart from a pair of bars, it does not appear that much has changed in recent centuries. The camino twists and turns through it, crossing a small creek next to a large wash basin.",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "30.8",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Zuriain",
        "latitude": "42.878567",
        "longitude": "-1.566131",
        "nid": "826",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "711.9",
        "marker": "City.png",
        "tohere": "3.10",
        "barrest": "Yes",
        "road": "The camino joins the main road here but not for more than 700m before turning left onto the first road. Along the road you are likely to find herds of spandexed cyclists. This stretch of asphalt was where Spain\u2019s most famous cyclist Miguel Indurain trained to win his five Tour de France titles.\n\n"
    }
}, {
    "node": {
        "title": "Sahag\u00fan",
        "latitude": "42.371988",
        "longitude": "-5.030494",
        "nid": "901",
        "nodetype": "City",
        "atm": "Yes",
        "body": "If you choose to spend the night in Sahag\u00fan do your best to explore the town, it is not the prettiest of places but there are a few gems hiding about that give you a glimpse of its illustrious past.  At one time it rivaled Burgos and Le\u00f3n in importance and grandeur. \nFor reasons unknown the town hall has drawn the camino path through town in a way that skips nearly every building worth seeing, and which bypasses the plaza mayor entirely.  If you want to explore you will have to leave the marked path.\n\n",
        "bus": "Yes",
        "correos": "Regina Franco 20, 24320, Mon-Fri: 0830-1430 & Sat: 0930-1300, 987 780 207",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "901-135007.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/sahagun.html?aid=1499405",
        "tosantiago": "360.7",
        "marker": "City.png",
        "tohere": "7.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Perros",
        "latitude": "42.771188",
        "longitude": "-7.356269",
        "nid": "973",
        "nodetype": "City",
        "body": "From here you climb gradually uphill 100 meters and pass under the road that leads to Samos. Two bar signs indicated opposite directions (climb to the road or stay on the path). Both will get you to a bar and back to the camino without adding any distance.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "973-135126.webp",
        "tosantiago": "0.8",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "O Pedrouzo [Arca do Pino]",
        "latitude": "42.904648",
        "longitude": "-8.362431",
        "nid": "1049",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Unfortunately, there is little to say about this modernized town. When it comes to charm, or monuments, or outrageous legends, it comes up short. During the busier periods along the camino the town feels overrun with pilgrims; most of whom are excited to have finished their penultimate day of walking.",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/o-pedrouzo.html?aid=1499405",
        "tosantiago": "19.4",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes",
        "road": "If you spent the night in Arca, it is important to find your way back to the camino proper which runs through the forest to the north. To get to it, find the intersection of the main road and Calle de Condello (where the Casa do Concello is located). Continue uphill (north from here) and in a few hundred meters the camino presents itself. Turn left and continue through the forest to Amenal. \nThe camino between here and Santiago is a mixture of rural and urban settings, some forests and some sprawl. The up and downs that you have been experiencing continue: the elevation gain\/loss is +308\/-339m, a not insignificant amount. \n"
    }
}, {
    "node": {
        "title": "\u25b3 on Camino via Campo or \u25b7 to Ponferrada by road",
        "latitude": "42.545209",
        "longitude": "-6.550169",
        "nid": "7469",
        "nodetype": "POI",
        "body": "Between Molinaseca and Ponferrada you will be almost exclusively on the sidewalk adjacent to the road. Three kilometers from the center of Molinaseca the camino splits. The camino technically turns left at the curve in the road and passes through Campo on the way to Ponferrada. Turning right to follow the road will get you to Ponferrada just the same and is a shorter path to the Municipal albergue.\n\n",
        "tosantiago": "209.5",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Teibilide",
        "latitude": "42.734918",
        "longitude": "-7.370560",
        "nid": "7629",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Cizur Menor [Zizur Txikia]",
        "latitude": "42.787748",
        "longitude": "-1.677089",
        "nid": "834",
        "nodetype": "City",
        "body": "One restaurant, Asador El Tremendo, does a pilgrim menu for about 10 euro. The grocery store in Cizur MAYOR is a short walk to your west, though a smaller shop with basic staples is available in town.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "695.6",
        "marker": "City.png",
        "tohere": "4.80",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Arcahueja",
        "latitude": "42.566230",
        "longitude": "-5.498228",
        "nid": "909",
        "nodetype": "City",
        "body": "The bar and albergue are in the same building, turn left at the playground.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/arcahueja.html?aid=1499405",
        "tosantiago": "313.9",
        "marker": "City.png",
        "tohere": "4.10",
        "barrest": "Yes",
        "road": "The way continues straight past the small playground and climbs upwards to the Alto de Portillo. \n"
    }
}, {
    "node": {
        "title": "Barbadelo",
        "latitude": "42.766194",
        "longitude": "-7.449996",
        "nid": "981",
        "nodetype": "City",
        "body": "Barbadelo, small as it is, once housed a monastery in 874. The current church dates from the 12th century and has several quality sculptures, including the animals carved into the north portal. It is 50m off the camino to your left, signed.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "109.6",
        "marker": "City.png",
        "tohere": "0.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Monte del Gozo",
        "latitude": "42.887406",
        "longitude": "-8.498172",
        "nid": "1057",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Monte de Gozo, or Mount Joy, was once the first place that pilgrims could get a glimpse of the Cathedral spires. A new stand of trees blocks the view now. It is a large gathering place for pilgrims, who flock to the over-sized monument commemorating the pilgrimage that Pope John Paul II made here in 1993. The modest Capilla de San Marcos has the last stamp and a small kiosk selling cold drinks.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "1057-133863.webp",
        "tosantiago": "5",
        "marker": "City.png",
        "tohere": "0.30",
        "barrest": "Yes",
        "road": "You do not need to enter the complex but for the sake of curiosity, carrying on down the road will take you where you are heading.\n\nPass the outdoor gallery of a local (and gifted) sculptor of stone and cross the bridge over the highway. It is midway over this bridge that you enter the city of Santiago de Compostela but to keep pilgrims from crossing the road half way across the bridge, the sign indicating such has been moved further into the city.\n\n"
    }
}, {
    "node": {
        "title": "\u25c1 into Pedrouzo or \u25b3 along camino",
        "latitude": "42.912176",
        "longitude": "-8.357366",
        "nid": "7477",
        "nodetype": "POI",
        "body": "After passing through A Rua the camino meets up with the N-547.\n\nTurn left here if you intend to stay in Pedrouzo or use any of it's services. From the center of town the camino will turn right to eventually rejoin the track that continues straight across the road towards a sports complex and at least one bar.\n\n",
        "tosantiago": "20.3",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "San Xil de Carballo",
        "latitude": "42.767067",
        "longitude": "-7.271579",
        "nid": "7639",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "128.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Lorca",
        "latitude": "42.672635",
        "longitude": "-1.945212",
        "nid": "843",
        "nodetype": "City",
        "body": "The two bars in town are at the far end, near the main plaza\/fronton court.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "843-133876.webp",
        "tosantiago": "663.4",
        "marker": "City.png",
        "tohere": "5.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villadangos del P\u00e1ramo",
        "latitude": "42.517568",
        "longitude": "-5.766492",
        "nid": "917",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "917-427.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villadangos-del-paramo.html?aid=1499405",
        "tosantiago": "285.4",
        "marker": "City.png",
        "tohere": "7.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Brea",
        "latitude": "42.779594",
        "longitude": "-7.515689",
        "nid": "989",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "102.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "La Coru\u00f1a",
        "latitude": "42.882442",
        "longitude": "-7.954402",
        "nid": "1451",
        "nodetype": "Province Border",
        "image": "1451-134728.webp",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Torres del R\u00edo",
        "latitude": "42.551160",
        "longitude": "-2.270329",
        "nid": "851",
        "nodetype": "City",
        "body": "Descending from the heights of Sansol into Torres del R\u00edo, one wonders why anyone would build a village in such an awkward to defend location. The only logical reason is the R\u00edo Linares which gives the town its name and easy access to water.\n\nThe town is curious for the fact that nearly all of the buildings (at least their exteriors) have been restored, giving the impression that there are more (and more to do) people living here than there actually are.\n\nPay a visit to the octagonal Iglesia de Santo Sepulcro, which like the one in Eunate cannot escape references to a Templar connection.\n\n",
        "bus": "Yes",
        "supermarket": "Yes, though small",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "851-133889.webp",
        "tosantiago": "625.9",
        "marker": "City.png",
        "tohere": "0.60",
        "barrest": "Yes",
        "road": "Il tratto tra qui e Viana viene soprannominato \u2018Il Distruttore di Ginocchia\u2019, a causa dei suoi continui saliscendi. L\u2019unico posto per fermarsi a riposare all\u2019ombra \u00e8 presso la Cappella della Virgen del Poyo, dove troverete anche un chiosco che vende alimenti di stagione."
    }
}, {
    "node": {
        "title": "Murias de Rechivaldo",
        "latitude": "42.460352",
        "longitude": "-6.104236",
        "nid": "925",
        "nodetype": "City",
        "body": "The town is one of a dozen that is considered to be a part of the Maragater\u00eda, whose inhabitants are characterized by their red hair, their hard work ethic, and their honest reputation.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/murias-de-rechivaldo.html?aid=1499405",
        "tosantiago": "252.4",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Moutras",
        "latitude": "42.789911",
        "longitude": "-7.571038",
        "nid": "997",
        "nodetype": "City",
        "body": "The small shop\/cafe here wins the award for flair. It is run by an artist who hand paints camino shells and other souvenir items. Rock and roll sello.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "997-134721.webp",
        "tosantiago": "96.7",
        "marker": "City.png",
        "tohere": "0.10",
        "barrest": "Yes",
        "road": "You will pass through Parrocha, no services.\n\n"
    }
}, {
    "node": {
        "title": "Alto del Perd\u00f3n",
        "latitude": "42.735758",
        "longitude": "-1.742443",
        "nid": "3033",
        "nodetype": "Alto",
        "body": "At 752m the Sierras del Perd\u00f3n are the highest point for the next 170km. A food truck named La Kontxa is parked at the top and offers a large selection of food and drinks, right down to their vegetarian burgers (and regular burgers). They are open from April through October. Between going up, and going back down, it is the latter which requires more care and a sturdy foot.\n\n",
        "image": "3033-133869.webp",
        "tosantiago": "687.1",
        "marker": "Alto.png",
        "tohere": "2.40",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Manjar\u00edn",
        "latitude": "42.490327",
        "longitude": "-6.386627",
        "nid": "930",
        "nodetype": "City",
        "body": "Manjar\u00edn wins the prize for the least inhabited inhabited town on the whole of the camino. Exactly one person lives here year round, joined occasionally by friends and pilgrims.\n\nHis name is T\u00f3mas, and he holds himself to be the last of the Knights Templar. No services beyond a cup of cowboy coffee from T\u00f3mas and his animal pen turned refugio for pilgrims.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "930-133842.webp",
        "tosantiago": "227.1",
        "marker": "City.png",
        "tohere": "2.30",
        "barrest": "",
        "road": "There are a few stretches of road ahead, just before arriving in Acebo, that are rugged and slippery in wet weather.\n\n"
    }
}, {
    "node": {
        "title": "Gonzar",
        "latitude": "42.825278",
        "longitude": "-7.696180",
        "nid": "1002",
        "nodetype": "City",
        "body": "Gonzar is notable for the number of Bovines kept there, far more than the human population.\n\nThe bar along the road can be miserably swamped, but if you go a short way into town you can enjoy a coffee at Casa Garcia (next to the church) with its beautiful courtyard and quiet atmosphere and its bathroom without a queue.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "84.2",
        "marker": "City.png",
        "tohere": "7.50",
        "barrest": "Yes",
        "road": "The camino leaves along the road but departs from it quite quickly. You will find that it does this often; the arrows have been placed in a manner which keeps you as far from heavy traffic as possible.\n\n"
    }
}, {
    "node": {
        "title": "Alto de Mostelares",
        "latitude": "42.287278",
        "longitude": "-4.179246",
        "nid": "3038",
        "nodetype": "Alto",
        "body": "The Alto de Mostelares is the highest of the peaks you will encounter along the meseta, but are not as difficult as they appear from Castrojeriz. From the top the view is more remarkable looking back towards where you came from.\n\nFrom the top the camino follows along a newly resurfaced stretch of the camino. Always downward, the descent is easy going after the first stretch.\n\nBetween the Alto and the Ermita is a shaded picnic area.\n\n",
        "tosantiago": "440.1",
        "marker": "Alto.png",
        "tohere": "3.60",
        "barrest": "",
        "road": "Da qui il cammino prosegue lungo un segmento recentemente risistemato. La discesa \u00e8 tranquilla dopo il primo tratto.\n\n\nTra l\u2019Alto e l\u2019eremo c\u2019\u00e8 un\u2019ombreggiata area picnic."
    }
}, {
    "node": {
        "title": "Detour to San Mill\u00e1n de la Cogolla",
        "latitude": "42.424509",
        "longitude": "-2.802533",
        "nid": "7576",
        "nodetype": "POI",
        "body": "From the albergue in Azofra it is possible to follow an alternative route to Ca\u00f1as and\/or San Mill\u00e1n, which in turn returns you to the camino prior to Santo Domingo. There are two monasteries in San Millan, Yuso (lower) and Suso (upper).\n\nIf all of this sounds confusing don't let it deter you from going. The best way to see them all is by first getting to Azofra.\n\nFrom there you can:\n1. Walk to the Monastery in Ca\u00f1as, adding only a few km to the journey to Santo Domingo. To do so, turn Left at the end of Azofra and follow the LR-206. It is main road. The road will take you first to Alesanco (1.7km, bar, fountain) and then on to Ca\u00f1as (5.2km, bar and Monastery). From Ca\u00f1as it is possible to take the road to Cirue\u00f1a (5.5km) and rejoin the camino.\n\n2. Walk to the Monastery in Ca\u00f1as, and continue onwards to the Monasteries of Yuso and Suso. To do so, keep on the road that brought you to Ca\u00f1as. It is another 7km to Berco (birthplace of San Millan) and another 1.7km beyond that to San Millan. All tours are organized from here, and a coach will take you to Suso. Accommodation (no albergue) is available here and if you fancy a splurge stay at the Monastery as part of it has been converted into a hotel. To return to the camino, retrace your steps as far as the first roundabout and turn Left towards Villar de Torre. From there continue straight through town and keep to the road, eventually arriving in Cirue\u00f1a and rejoining the camino.\n\n",
        "image": "7576-62568.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Viloria de Rioja",
        "latitude": "42.426308",
        "longitude": "-3.100333",
        "nid": "864",
        "nodetype": "City",
        "body": "This sleepy agricultural town is best known for being the birthplace (cu\u00f1a) of Santo Domingo de la Calzada. History did not see fit to preserve his house but the font in which he was baptized remains in the church.\n\nSandwiches and beverages are available at the Parada Viloria albergue.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "864-134935.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/viloria-de-rioja.html?aid=1499405",
        "tosantiago": "542",
        "marker": "City.png",
        "tohere": "1.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Fuentes Nuevas",
        "latitude": "42.575174",
        "longitude": "-6.640377",
        "nid": "938",
        "nodetype": "City",
        "body": "The road here begins to enter vine country, and soon the source of El Bierzo\u2019s delicious products become evident. Resist the urge to sample the grapes; they are frequently sprayed with insecticides. So too are the cherry trees, but that doesn\u2019t stop most pilgrims.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "197.4",
        "marker": "City.png",
        "tohere": "2.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Portos",
        "latitude": "42.873548",
        "longitude": "-7.807056",
        "nid": "1010",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "72.8",
        "marker": "City.png",
        "tohere": "2",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pint\u00edn",
        "latitude": "42.773790",
        "longitude": "-7.339243",
        "nid": "3127",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "120.2",
        "marker": "City.png",
        "tohere": "1.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ag\u00e9s",
        "latitude": "42.370276",
        "longitude": "-3.479962",
        "nid": "872",
        "nodetype": "City",
        "body": "A pleasant but small town with a quirky mix of albergues. Bar Alquimista is a friendly stop if you are passing through, and they do a decent dinner too.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "872-134933.webp",
        "tosantiago": "506.2",
        "marker": "City.png",
        "tohere": "3.90",
        "barrest": "Yes",
        "road": "Just outside of town, on your left-hand side and likely obscured by tall grass, sits one of San Juan de Ortega\u2019s smaller engineering feats. It only takes one span to cross the diminutive R\u00edo Vena.\n\n"
    }
}, {
    "node": {
        "title": "Ambasmestas",
        "latitude": "42.664829",
        "longitude": "-6.931086",
        "nid": "946",
        "nodetype": "City",
        "body": "Ambasmestas is named for the confluence of the two valley rivers, the Valc\u00e1rcel (\u201cvalley of the prison,\u201d so named because of its width) and the Balboa. Soaring above you is the A-6 highway connecting A Coru\u00f1a with Madrid.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/ambasmestas.html?aid=1499405",
        "tosantiago": "166",
        "marker": "City.png",
        "tohere": "0.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Xulian",
        "latitude": "42.874436",
        "longitude": "-7.903214",
        "nid": "1019",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "63.8",
        "marker": "City.png",
        "tohere": "2",
        "barrest": "Yes",
        "road": "Near San Xulain the camino is undergoing a rerouting to accommodate the new highway connecting Santiago with Lugo. Most recently this meant walking across the unfinished highway. Changes are frequent and it is best to follow the detour signage.\n\n"
    }
}, {
    "node": {
        "title": "Alternate to Los Arcos via Luquin",
        "latitude": "42.646324",
        "longitude": "-2.047950",
        "nid": "8555",
        "nodetype": "POI",
        "body": "500 meters beyond the Monasterio de Irache, the camino splits. The camino turns right, but the natural inclination is to continue going straight along the alternate route via Luquin.\n\n\u200cVia Luquin - 17.4\nIf you continue straight you will be rewarded with a more diverse hike with better views. The drawback is a lack of dependable services along this stretch of trail at the base of the Montejurra mountain. Luquin is the only town with a bar, and it is open irregularly. If you plan to go this way be sure to include extra food and drink. This lesser traveled route is the more scenic and peaceful option. Outside of Luquin it passes back under the A-12 and rejoins the first option during it\u2019s zigging and zagging.\n\n\u200cVia Villamayor de Monjard\u00edn - 18.1\nFollowing the camino to the right will take you past the campgrounds of Irache, Azqueta, and Villamayor de Monjardin, before zig-zagging through the broad countryside that surrounds Los Arcos.\n\n",
        "image": "8555-133886.webp",
        "tosantiago": "651.6",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Hornillos del Camino",
        "latitude": "42.338515",
        "longitude": "-3.924479",
        "nid": "882",
        "nodetype": "City",
        "body": "Hornillos is one of the best examples of a \u201cRoyal\u201d camino road: in this case one long street (the aptly named calle real) stretched out along the camino.  This layout allowed every building to face the camino, a trait which reminds us that the current heavy development along the camino is anything but a new phenomenon. The town once belonged to the monastery of St. Denis and was home to a Benedictine community. \nFolklore places Charlemagne here too, baking bread for his troops along the river.\n\nRuins and medieval bridges are the only visible remains.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "882-133900.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/hornillos-del-camino.html?aid=1499405",
        "tosantiago": "463.6",
        "marker": "City.png",
        "tohere": "7.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "El Lugar de Tabernavella",
        "latitude": "42.922690",
        "longitude": "-8.186182",
        "nid": "11944",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "33.4",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Li\u00f1ares",
        "latitude": "42.698283",
        "longitude": "-7.074423",
        "nid": "954",
        "nodetype": "City",
        "body": "Li\u00f1ares lacks the charm of O Cebreiro but offers the distinct advantage of having a new albergue. The municipal albergue in O Cebreiro has a tendency to become very crowded.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "149.5",
        "marker": "City.png",
        "tohere": "3.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ra\u00eddo",
        "latitude": "42.916593",
        "longitude": "-8.048938",
        "nid": "1027",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "49.4",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Fr\u00f3mista",
        "latitude": "42.267433",
        "longitude": "-4.406848",
        "nid": "890",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Fr\u00f3mista is the southernmost city along the camino franc\u00e9s and the epicenter of wheat growing in Spain. Its rises and falls over the centuries were finally steadied with the arrival of the canals in the late 18th century; they brought proper irrigation and an abundant source of power for mills.\n\nIn the center of town sits the Iglesia de San Mart\u00edn, which underwent a rather unfortunate renovation in the 19th century. In the process, many of the more artistic elements were removed for reasons of decency. Quite a few decorated capitals make for an interesting, if not amusing, visit. Two other churches, San Pedro and Santa Mar\u00eda del Castillo, are generally open to the public.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "890-133901.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/fromista.html?aid=1499405",
        "tosantiago": "419",
        "marker": "City.png",
        "tohere": "5.40",
        "barrest": "Yes",
        "road": "Leaving Fr\u00f3mista is accomplished by retracing a few steps to the main intersection, do not blindly make your way through town and if you plan to leave before the sun rises do yourself the favor of scouting for arrows in advance. The trail here follows a gravel path punctuated by frequent \u201ctwin hitos\u201d that are placed to discourage off-road vehicles from taking over the path.\n\n"
    }
}, {
    "node": {
        "title": "Triacastela",
        "latitude": "42.756655",
        "longitude": "-7.240419",
        "nid": "962",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Triacastela is full of choices: where to eat, where to shop, where to sleep, and even how to leave town. Congratulate yourself on a job well done, the pass into Galicia has been overcome and from here on out the mountains soften in severity. Read the next note regarding the choice of options ahead.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/triacastela.html?aid=1499405",
        "tosantiago": "131.8",
        "marker": "City.png",
        "tohere": "2",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pregonto\u00f1o",
        "latitude": "42.922690",
        "longitude": "-8.186182",
        "nid": "1036",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "36.2",
        "marker": "City.png",
        "tohere": "2.30",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Detour to Church of Eunate +3km",
        "latitude": "42.688220",
        "longitude": "-1.770612",
        "nid": "7425",
        "nodetype": "POI",
        "body": "\u200cDetour to visit the Church at eunate +3km\nOctagonal 12th century Iglesia de Eunate.\n\nThe history of this ermita is still up for debate, and like most things in Spain are divided into two camps. Half believe it is of Knights Templar origin, and the rest who believe it was a funeral chapel. Separate yourself from that intellectual debate and you will find a countryside setting with a small gem of architecture for you to contemplate.\n\n",
        "tosantiago": "681",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Detour to Monasterio de Carracedo",
        "latitude": "42.598531",
        "longitude": "-6.725864",
        "nid": "7617",
        "nodetype": "POI",
        "body": "A 6.5km detour (3+ each way) south of Cacabelos can be taken to the Cistercian (originally Benedictine) Monastery of Carracedo. It is in a state of partial repair, after being sacked by Almanzor in 997.\n\nTo get there, head south from the Plaza Vendimia along the Carretera de Villadepalos Elias Iglesias passing through San Juan (2km). To return, retrace your steps back to Cacabelos.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Ezkirotz",
        "latitude": "42.907892",
        "longitude": "-1.524888",
        "nid": "823",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "717.5",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Terradillos de los Templarios",
        "latitude": "42.362983",
        "longitude": "-4.890890",
        "nid": "898",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "374.1",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": "Yes",
        "road": "Between here and Sahag\u00fan the camino follows along a dirt track which frequently parallels the N-120. The N-120 has numerous blind spots for motorists and you are advised to keep off it whether by bike or by foot.\n\n"
    }
}, {
    "node": {
        "title": "Teigu\u00edn",
        "latitude": "42.724600",
        "longitude": "-7.341285",
        "nid": "970",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "123.8",
        "marker": "City.png",
        "tohere": "0.60",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Empalme",
        "latitude": "42.915389",
        "longitude": "-8.323517",
        "nid": "1045",
        "nodetype": "City",
        "body": "Several roadside bars that cater to trucker and pilgrim alike.",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "23.4",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes",
        "road": "The camino crosses the main road at the highest point in the road, there is no marked crosswalk and the arrows on the other side of the road are often obscured by parked cars. You may see pilgrims continuing along the road but are advised against following them as the camino returns to the trail when you turn off the road. \nHalf way down the hill it splits and arrows indicate that you should either turn left to go under the road or continue straight. Unless you have reason to visit Santa Irene you can keep on straight and avoid the hassle of crossing back over the road. If you continue straight you will arrive at the important part of Santa Irene (the part with the bar).\n\n"
    }
}, {
    "node": {
        "title": "\u200cMinor detour to Ventosa +800m",
        "latitude": "42.412075",
        "longitude": "-2.614102",
        "nid": "7433",
        "nodetype": "POI",
        "body": "With changing times Ventosa has found itself separated from the camino by the rearrangement of yellow arrows for the sake of shaving 800m from the camino. There is at least one bar and albergue.\n\n",
        "tosantiago": "587.7",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "\u25b3 on along Road to Samos or \u25b7 to rejoin camino in Perros",
        "latitude": "42.726830",
        "longitude": "-7.344682",
        "nid": "7625",
        "nodetype": "POI",
        "body": "It is possible to stay on the road all the way to Sarria, but the connection to Aguiada is recommended. Along the road to Sarria is 8.8km, via Aguiada 12.6km.\n\n",
        "tosantiago": "126.2",
        "marker": "POI.png",
        "tohere": "0.50",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Villava",
        "latitude": "42.830943",
        "longitude": "-1.608853",
        "nid": "831",
        "nodetype": "City",
        "atm": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "704.4",
        "marker": "City.png",
        "tohere": "0.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Mansilla de las Mulas",
        "latitude": "42.497510",
        "longitude": "-5.416474",
        "nid": "906",
        "nodetype": "City",
        "atm": "Yes",
        "body": "There is a nice place for a swim in the river in the northwest corner of Mansilla. Cross over the small pedestrian bridge towards the camping place (not the road bridge that the camino crosses) located in the northwest corner of town. Getting there will take you through several of Mansilla\u2019s small plazas.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "906-407.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/mansilla-de-las-mulas.html?aid=1499405",
        "tosantiago": "324.5",
        "marker": "City.png",
        "tohere": "5.90",
        "barrest": "Yes",
        "road": "Mansilla still has large sections of its defensive walls, turn back when leaving town to get the best view of them. \nMuch of the way between here and Puente de Villarente is adjacent to the road.\n\n"
    }
}, {
    "node": {
        "title": "Vigo de Sarria",
        "latitude": "42.775739",
        "longitude": "-7.404713",
        "nid": "978",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "114.7",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Lavacolla",
        "latitude": "42.900144",
        "longitude": "-8.449945",
        "nid": "1054",
        "nodetype": "City",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/lavacolla.html?aid=1499405",
        "tosantiago": "10.3",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes",
        "road": "If you walked down the stairs to visit either of the bars at the bottom, turn and walk up the steps towards the Iglesia de Benaval. The camino continues around to the right-hand side and down to cross the road. At the road, cross at the crosswalk and continue along the road and over the famous river (see inset below).\n\nThe last hill is ahead, and if you are a stickler for doing things according to tradition you should start running now. It is said that the first of your group to arrive in Monte de Gozo is entitled to be called King. Be advised that there is no prize.\n\n"
    }
}, {
    "node": {
        "title": "Turn downward for walkers, or up road for cyclists",
        "latitude": "42.677074",
        "longitude": "-7.000244",
        "nid": "7474",
        "nodetype": "POI",
        "body": "The climb begins as a gradual ascent along the paved road. The camino will eventually leave this road in favor of a footpath which descends to your left. Arrows at the turn-off indicate that the camino for cyclists remains on the road and if you are on a bike this is the preferred route. When it trail begins to climb again the path is rocky and steep and slick with mud and horse shit. Tread carefully and think happy thoughts.\n\n",
        "tosantiago": "158.9",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Furela",
        "latitude": "42.773815",
        "longitude": "-7.326046",
        "nid": "7636",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "121.5",
        "marker": "City.png",
        "tohere": "2.20",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Puente la Reina [Gares]",
        "latitude": "42.671771",
        "longitude": "-1.815791",
        "nid": "840",
        "nodetype": "City",
        "atm": "Yes",
        "body": "There are a pair of churches in town that are worthy of a peek. They are the Iglesia del Crucifijo (with a rather distinct crucifix) and the Iglesia de Santiago (with its black Santiago).  The best view of the old bridge is from the new bridge.\n\n",
        "bus": "Yes",
        "correos": "Cerco Viejo 9, 31100,Mon-Fri: 0830-1430 & Sat: 0930-1300,948 340 620",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "840-200.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/puente-la-reina.html?aid=1499405",
        "tosantiago": "676.6",
        "marker": "City.png",
        "tohere": "2.50",
        "barrest": "Yes",
        "road": "The camino leaves Puente la Reina by passing over its namesake bridge. From there it turns left, crosses the main road, and follows a footpath. Shortly, it begins a short but steep ascent on slippery-when-wet terrain. It is not uncommon to find cyclists pushing their gear upwards. \n"
    }
}, {
    "node": {
        "title": "La Virgen del Camino",
        "latitude": "42.580736",
        "longitude": "-5.640020",
        "nid": "914",
        "nodetype": "City",
        "atm": "Yes",
        "body": "La Virgen del Camino is named for the miraculous sighting of the Virgen by a local shepherd. His vision has been translated into sculptures several times by artists. One is here in the modern looking church, and another is in the Le\u00f3n Cathedral (in the Chapel where the daily mass is held).\n\nThe sculptures are stunning and bear a resemblance to La Piedad by Michelangelo, with the notable exception that Jesus is facing downwards.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "914-135056.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/la-virgen-del-camino.html?aid=1499405",
        "tosantiago": "298.7",
        "marker": "City.png",
        "tohere": "3.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Corti\u00f1as",
        "latitude": "42.777844",
        "longitude": "-7.500543",
        "nid": "986",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "103.7",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Castilla y Le\u00f3n \/ Palencia",
        "latitude": "42.278307",
        "longitude": "-4.245690",
        "nid": "1448",
        "nodetype": "Province Border",
        "body": "Crossing the Puente Fitero brings you into the province of Palencia. The bridge here has survived the years but others to the south have long since disappeared, taking with them the pilgrims whose commerce helped to build several uncharacteristically large churches along the way.\n\nIf you are up for a detour those of Santoyo (Iglesia de San Juan, with beautiful organ) and T\u00e1mara de Campos (Iglesia de San Hipolito) are among the best. To get there, follow the P-432 west from Boadilla, and turn left onto the P-431 (which comes south from Fr\u00f3mista). The trip there is about 12km (visiting both locations), and 7km straight back to Fr\u00f3mista. \nPalencia also marks the start of a region known as Tierra de Campos, and the wide expanses of the meseta seem to magnify here.\n\n",
        "image": "1448-638.png",
        "tosantiago": "434.5",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Honto [Huntto]",
        "latitude": "43.123947",
        "longitude": "-1.244888",
        "nid": "7494",
        "nodetype": "City",
        "body": "First bar stop and first accommodation option. Reservation is recommended.",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/fr\/saint-michel-fr.html?aid=1499405",
        "tosantiago": "761.8",
        "marker": "City.png",
        "tohere": "5.20",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Villamayor de Monjard\u00edn",
        "latitude": "42.628812",
        "longitude": "-2.104826",
        "nid": "848",
        "nodetype": "City",
        "body": "The Monjard\u00edn is the peak that overlooks the town, and remnants of the 10th century Castillo de San Esteban that once protected it remain. The key to the gate can be found at Bar Ilarria in the plaza.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "848-133776.webp",
        "tosantiago": "645.5",
        "marker": "City.png",
        "tohere": "1.90",
        "barrest": "Yes",
        "road": "The distance between here and Los Arcos has a tendency to pass very slowly, and there are little opportunities for shade. Stock up well with water.\n\n"
    }
}, {
    "node": {
        "title": "Santib\u00e1\u00f1ez de Valdeiglesias",
        "latitude": "42.458072",
        "longitude": "-5.930257",
        "nid": "922",
        "nodetype": "City",
        "body": "The church is often locked up, but if you ask around for the key the locals are always happy to unlock it for pilgrims.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "922-136755.webp",
        "tosantiago": "268.6",
        "marker": "City.png",
        "tohere": "4.90",
        "barrest": "Yes",
        "road": "Between here and San Justo de la Vega there are no facilities, but a small kiosk run by pilgrims operates during the busier seasons. Just before the steep descent into San Justo de la Vega is the Cruceiro de Santo Toribio overlooking Astorga in the distance. From this vantage point, you can see the Cathedral of Astorga and the string of towns that dot the camino as it crosses the mountains ahead.\n\n"
    }
}, {
    "node": {
        "title": "Rozas",
        "latitude": "42.783971",
        "longitude": "-7.549897",
        "nid": "994",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "994-133860.webp",
        "tosantiago": "98.7",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Villavante",
        "latitude": "42.461428",
        "longitude": "-5.835886",
        "nid": "2636",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "2636-135060.webp",
        "tosantiago": "277.7",
        "marker": "City.png",
        "tohere": "9.80",
        "barrest": "Yes",
        "road": "The camino turns right and into Villavante, though some old yellow arrows continue straight ahead (painted on the elevated irrigation canal). The reason for the detour around town is to avoid a surface crossing of the dangerous rail lines at the edge of town. The distance added is worth the elevated crossing, as the trains pass deceptively fast.\n\n"
    }
}, {
    "node": {
        "title": "N\u00e1jera",
        "latitude": "42.416075",
        "longitude": "-2.734394",
        "nid": "856",
        "nodetype": "City",
        "atm": "Yes",
        "body": "The eastern entrance to N\u00e1jera is not easy on the eyes. This new part of the town grew up on the east bank of the R\u00edo Najerilla (the cliffs prohibit expansion to the west) and much of the city\u2019s industry is located here. Leaving town is notably better as you transition quickly back to the rural roadways that connect many of the smaller villages.\n\nThe Monasterio de Santa Mar\u00eda la Real is one of the finest cloistered buildings in all of La Rioja and merits a visit. It is built into the entrance of a cave where the image of the Virgin was discovered by Sancho\u2019s son Garc\u00eda IV.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Paseo Jan Julian 14, 26300, Mon-Fri: 0830-1430 & Sat: 0930-1300,941 360 051",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "856-133896.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/najera.html?aid=1499405",
        "tosantiago": "577",
        "marker": "City.png",
        "tohere": "16.20",
        "barrest": "Yes",
        "road": "On the way to Azofra, take notice of the modern aqueducts which help to irrigate the land. They are gravity fed and are a real-world example of hydraulics in action, with sections that pass beneath the road rather than passing above it.\n\n"
    }
}, {
    "node": {
        "title": "Valdeviejas",
        "latitude": "42.461680",
        "longitude": "-6.079264",
        "nid": "8150",
        "nodetype": "City",
        "body": "Valdeviejas sits adjacent to the camino and most pilgrims never go into town. There is not much cause to do so unless you plan to stay at the municipal albergue here.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "254.5",
        "marker": "City.png",
        "tohere": "2.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Gra\u00f1\u00f3n",
        "latitude": "42.450410",
        "longitude": "-3.026819",
        "nid": "861",
        "nodetype": "City",
        "body": "Of the two monasteries which once cared for pilgrims in Gra\u00f1\u00f3n, only a single altarpiece remains. It is in the Iglesia de San Juan, which also houses one of the towns albergues. \nThe care given to pilgrims in the San Juan albergue remains top notch and distinct, and the recent opening of an albergue in Carrasquedo (1.5km south and in a forest) keeps Gra\u00f1\u00f3n on the list of pilgrim favorites.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "861-133898.webp",
        "tosantiago": "549.3",
        "marker": "City.png",
        "tohere": "7",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ponferrada",
        "latitude": "42.547179",
        "longitude": "-6.589462",
        "nid": "935",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Ponferrada\u2019s Templar castle is one of the best-preserved examples of its kind and offers a glimpse into what castle life must have been like in the 12th and 13th centuries. \nThe Iglesia de Santa Mar\u00eda de la Encina is named for a Templar legend according to which her image was found in the trunk of an Oak tree. It is located in the oldest part of town, near the castle.\n\n",
        "bus": "Yes",
        "correos": "Ave General Vives, 24401, Mon-Fri: 0830-2030 & Sat: 0930-1300, 987 403 187",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "12203-481.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/ponferrada.html?aid=1499405",
        "tosantiago": "205.1",
        "marker": "City.png",
        "tohere": "3.20",
        "barrest": "Yes",
        "road": "Exiting Ponferrada takes you past the castle and briefly through the old town before crossing over the R\u00edo Sil. At the end of that bridge turn right. The way curves left and at the next large intersection turn right. Keep straight through the first roundabout, and turn right at the second. From here the camino follows temporary signage through construction.\n\n"
    }
}, {
    "node": {
        "title": "Lameiros",
        "latitude": "42.854441",
        "longitude": "-7.775167",
        "nid": "1007",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "76.5",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Alto de Ligonde",
        "latitude": "42.848031",
        "longitude": "-7.758236",
        "nid": "3044",
        "nodetype": "Alto",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Espinosa del Camino",
        "latitude": "42.406411",
        "longitude": "-3.281093",
        "nid": "869",
        "nodetype": "City",
        "body": "Espinosa has seen better days. Even with centuries of passing pilgrims it struggles to stay on the map. In recent years, a few buildings have been rehabilitated into an albergue and a bar.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "525.4",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pereje",
        "latitude": "42.626255",
        "longitude": "-6.843710",
        "nid": "943",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "943-133849.webp",
        "tosantiago": "175.6",
        "marker": "City.png",
        "tohere": "5.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Rosario",
        "latitude": "42.874621",
        "longitude": "-7.851727",
        "nid": "1016",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "68.8",
        "marker": "City.png",
        "barrest": "",
        "road": "Along the descent from here you will pass Os Chacotes,  a recreational area with a log-cabin style hotel and sports facilities. At the entrance to Sarria is the Iglesia de San Tirso, where a stamp is available. From there it is down a set of stairs into town.\n\n"
    }
}, {
    "node": {
        "title": "Left to Casta\u00f1ares or Straight to Vilafria",
        "latitude": "42.362222",
        "longitude": "-3.596263",
        "nid": "7406",
        "nodetype": "POI",
        "body": "Less than 1km after leaving Orbaneja R\u00edopico the camino splits, offering two different ways to get to Burgos.  Of the two, the River Route is the strong favorite but the yellow arrows come and go as local business vandalize the official markers in an attempt to divert traffic.\n\n\u200cRiver Route, via Casta\u00f1ares - 11.9\nAlthough the trail through Casta\u00f1ares is considered by some to be an alternative route, it offers a much more pleasent and fully signed way to the center of Burgos. There are a few tricks, since from Casta\u00f1ares there are also two trails, one along the road and one along the river. To begin your way to Casta\u00f1ares, follow these directions:\nWhen leaving Orbaneja you will cross a bridge over the AP-1 (see map on previous page). A short distance afterward, at the junction leading to a housing development on your left hand side, turn left and follow the trail to Casta\u00f1ares. From Casta\u00f1ares you will have another choice between the road and a trail (see map below).\n\nIf you have any doubts about how you begin the River Route, or \u201cCamino Fluvial\u201d to Burgos, inquire in the Bar Peregrina T. Owners Cesar and Paloma will point you in the right direction. They speak English and are quite helpful.\n\n\u200cVia VilLafr\u00eda - 11.8\nWhen leaving Orbaneja you will cross a bridge over the AP-1. A short distance afterward, at the junction leading to a housing development on your left hand side, continue straight for the road via Vilafr\u00eda.  This route will take you around the airport and is city walking along the sidewalk to the center of Burgos.\n\n",
        "tosantiago": "494.6",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Burgos",
        "latitude": "42.340878",
        "longitude": "-3.699775",
        "nid": "878",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Burgos is a city that brings history to life. For many pilgrims it is the first place on the Camino Franc\u00e9s that beckons a two-night stay; and rightly so, Burgos has a tremendous amount of museums, churches, one cathedral, a few monasteries, and a healthy (or not) number of bars and restaurants to keep you busy.\n\nOn the not to miss list are the Catedral y Museo de Burgos in the very center of the city, and the Castillo de Burgos perched on the hillside overlooking the town. \nThe Museum of Human Evolution houses the artifacts from Atapuerca, and the Arco de Santa Mar\u00eda (free visit) houses an exhibition hall.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Plaza Conde de Castro 1, 09002, Mon-Fri: 0830-2030 & Sat: 0930-1300, 947 256 597",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "878-133782.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/burgos.html?aid=1499405",
        "tosantiago": "483.8",
        "marker": "City.png",
        "tohere": "7.10",
        "barrest": "Yes",
        "road": "The road out of Burgos is, thankfully, much shorter than the road in. The camino from here enters the meseta, with its endless plains of wheat. The landscape, while seemingly unremarkable, offers a wealth of flora and fauna and opportunities for peaceful contemplation.\n\n"
    }
}, {
    "node": {
        "title": "Valcarlos",
        "latitude": "43.093922",
        "longitude": "-1.299455",
        "nid": "9906",
        "nodetype": "City",
        "atm": "Yes",
        "body": "To get to most services in town you will have to follow the road a short distance back in the direction of Arn\u00e9guy.\n\nThe church here is of a modern design and is often locked up. This is frequently the case, particularly in the rural corners of Spain. If you are comfortable asking around, usually at the nearest bar, somebody will know somebody with a key. They will almost always open up for pilgrims.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "9906-134825.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/valcarlos.html?aid=1499405",
        "tosantiago": "754.8",
        "marker": "City.png",
        "tohere": "3.10",
        "barrest": "Yes",
        "road": "You will leave town along the main road, eventually abandoning it for a smaller road on your left. Ga\u00f1ecoleta is less than 1km from the departure from the main road.\n\n"
    }
}, {
    "node": {
        "title": "La Faba",
        "latitude": "42.685495",
        "longitude": "-7.009363",
        "nid": "951",
        "nodetype": "City",
        "body": "A small hamlet on the way up to O\u2019Cebreiro, there is little more than a collection of houses, a parochial albergue (run by a German confraternity) and the adjacent church which has a daily evening Mass. Two other bars are in the center of the village, one distinctly Gallego in feel and the other distinctly not. The latter also functions as a small albergue.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "951-133368.webp",
        "tosantiago": "157.4",
        "marker": "City.png",
        "tohere": "2.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Leboreiro",
        "latitude": "42.887177",
        "longitude": "-7.964925",
        "nid": "1024",
        "nodetype": "City",
        "body": "Leboreiro is one of the more charming hamlets along this stretch of road. It is sparsely populated but well maintained. The small Romanesque Iglesia de Santa Mar\u00eda de las Nieves has a nice tympanum. Also of note is the first cabaceiro, a small woven structure with a thatched roof that serves the same function as an h\u00f3rreo.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "58.1",
        "marker": "City.png",
        "tohere": "0.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "SPLIT: Alternate Route via Huarte or follow the camino",
        "latitude": "42.852659",
        "longitude": "-1.583721",
        "nid": "7414",
        "nodetype": "POI",
        "body": "At the picnic area adjacent to the road you can choose your own adventure. The majority of walkers continue onward and upward. Optionally you can follow the river, keeping it on your right, on an alternate route into Huarte (albergue municipal, all services) and on to Pamplona. This route is marginally longer than the other, but does bypass the very obvious ascent from here.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Straight along the Camino Real or turn Right for the Via Trajana",
        "latitude": "42.382477",
        "longitude": "-5.076649",
        "nid": "7602",
        "nodetype": "POI",
        "body": "Leaving Sahag\u00fan cross over the R\u00edo Cea. The camino becomes something of a confusion just short of Calzada de Coto, which lies to the north of the A-231. You will find yourself standing at a large highway interchange (4.4km from Sahag\u00fan) with a brick bus-stop and very little signage to point the way. Distances indicated below are from Sahag\u00fan to Reliegos, though the Via Trajana can also bypass Rel\u00edegos and go directly to Mansilla de las Mulas without adding any distance The two options rejoin in Reliegos, or Mansilla de las Mulas.\n\n\u200cThe Camino Real - 30.3\nCarry on straight here to follow the route via Bercianos and El Burgo Ranero. \n\u200cThe Via Trajana - 30.9\nTurn right here (north) and cross over the bridge to follow the old Roman Road through Calzada de Coto and Calzadilla de los Hermanillos.\n\n",
        "image": "7602-135000.webp",
        "tosantiago": "356.3",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Ermita de San Nicol\u00e1s",
        "latitude": "42.278769",
        "longitude": "-4.243298",
        "nid": "887",
        "nodetype": "City",
        "body": "The Ermita of San Nicol\u00e1s, like the Ruins of San Anton before Castrojeriz, has a long history of taking care of pilgrims. Both stand on their own far from towns and as a result, they don\u2019t necessarily get the amount of traffic they deserve. Perhaps that is for the best as pilgrims often claim them to be the most memorable albergues along the way.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "434.5",
        "marker": "City.png",
        "tohere": "5.60",
        "barrest": "",
        "road": "Just beyond the Ermita is the Puente Fitero which takes you over the river of the same name, and into Palencia.\n\n"
    }
}, {
    "node": {
        "title": "Filloval",
        "latitude": "42.744041",
        "longitude": "-7.204723",
        "nid": "959",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "135.5",
        "marker": "City.png",
        "tohere": "2.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ribadiso da Riba",
        "latitude": "42.930751",
        "longitude": "-8.133312",
        "nid": "1033",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "1033-546.webp",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Linzoain",
        "latitude": "42.962784",
        "longitude": "-1.437655",
        "nid": "820",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "820-134838.webp",
        "tosantiago": "729.3",
        "marker": "City.png",
        "tohere": "1.90",
        "barrest": "Yes, but irregular hours",
        "road": "When you get to this town, be sure to follow the arrows that direct you to the right and uphill. If you get to the main road (the N-135) you have gone wrong. The climb is fairly steep up to the Alto de Erro.\n\n"
    }
}, {
    "node": {
        "title": "Carri\u00f3n de los Condes",
        "latitude": "42.337316",
        "longitude": "-4.601898",
        "nid": "895",
        "nodetype": "City",
        "atm": "Yes",
        "body": "The Caf\u00e9 Bar Espa\u00f1a serves as the local bus stop, should you find yourself in need of motorized transport inquire at the bar for schedules.\n\n",
        "bus": "Yes",
        "correos": "Jos\u00e9 Antonio Giron 18, 34120, Mon-Fri: 0830-1430 & Sat: 0930-1300, 979 880 345",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "895-133831.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/carrion-de-los-condhtml?aid=1499405",
        "tosantiago": "400.1",
        "marker": "City.png",
        "tohere": "6",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Marti\u00f1o do Real",
        "latitude": "42.737116",
        "longitude": "-7.315993",
        "nid": "967",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "127.5",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "O Castro",
        "latitude": "42.926091",
        "longitude": "-8.274273",
        "nid": "1042",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Monasterio de Irache",
        "latitude": "42.650915",
        "longitude": "-2.043607",
        "nid": "7430",
        "nodetype": "POI",
        "body": "The paper trail for the monastery leads back to 958, and it was the site of Navarra\u2019s first pilgrim hospital. It is a large complex set in a landscape of vineyards. Like many other Benedictine monasteries across the peninsula, it has served as both a barrack to Napoleon\u2019s troops, and as a hospital during the Carlist wars.\n\nFree admission, but opens a bit late (10am) for pilgrims departing from Estella. The park opposite has picnic tables and nice shade trees.\n\n",
        "image": "7430-52723.webp",
        "tosantiago": "652",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Church.png"
    }
}, {
    "node": {
        "title": "Zabaldika",
        "latitude": "42.856242",
        "longitude": "-1.583061",
        "nid": "828",
        "nodetype": "City",
        "body": "The somewhat confused signs through Zabaldika are the result of local politics interfering with tradition. In this case, the ayuntamiento has managed to one up the culturally significant Iglesia de San Esteban and the original Camino Franc\u00e9s. To follow the original way, turn right in town and look out for the signs which take you across the main road and up a trail to the church in the upper half of Zabaldika. There is no need to retrace your steps, as the other camino does the very same some 100m down the road.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "828-133357.webp",
        "tosantiago": "708.7",
        "marker": "City.png",
        "tohere": "1",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Bercianos del Real Camino",
        "latitude": "42.387522",
        "longitude": "-5.144477",
        "nid": "903",
        "nodetype": "City",
        "body": "The name of the town tells us that the original inhabitants came from the Bierzo area, and that it is situated along the Camino Real, or Royal Camino. This designation has nothing at all to do with royalty but is rather a term given to primary roads that met a certain metric for size; much like versions of our modern day National highways.\n\nThe Iglesia de San Salvador contains a sculpture of St. John the Baptist. The original church tower very nearly lasted into the 21st c., but not quite. The replacement is an uninspiring steel bell tower.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "350.6",
        "marker": "City.png",
        "tohere": "10.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Mamed do Cami\u00f1o",
        "latitude": "42.773331",
        "longitude": "-7.371805",
        "nid": "975",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "117.6",
        "marker": "City.png",
        "tohere": "0.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Amenal",
        "latitude": "42.905991",
        "longitude": "-8.393683",
        "nid": "1051",
        "nodetype": "City",
        "body": "Amenal is little more than a camino pit-stop, and the large outdoor patio of the only bar is often filled to capacity with pilgrims doing their best to make the last day last.",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/amenal.html?aid=1499405",
        "tosantiago": "16.4",
        "marker": "City.png",
        "tohere": "2.30",
        "barrest": "Yes, with cafeteria.",
        "road": "Leaving the bar behind you climb steeply uphill a short distance. The path soon levels out on a comfortable trail surrounded by eucalyptus trees. The Santiago Airport is very near, and the camino follows a path around the runway.\n\n"
    }
}, {
    "node": {
        "title": "\u200cVARIATION through Valtuille de Arriba ",
        "latitude": "42.606169",
        "longitude": "-6.755433",
        "nid": "7471",
        "nodetype": "POI",
        "body": "Shortly after leaving Pieros the camino splits near a set of power lines. The detour through Valtuille, favored for its safety and scenery, is to the right. The road route continues straight for several kilometers before turning off onto a dirt track. This track will rejoin the Valtuille route.\n\nNote that the distances shown are for the route, as it is now considered the primary route.\n\n",
        "tosantiago": "186.8",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Rosende da Pena",
        "latitude": "42.740430",
        "longitude": "-7.385741",
        "nid": "7631",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Uterga",
        "latitude": "42.710002",
        "longitude": "-1.759529",
        "nid": "837",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "837-133871.webp",
        "tosantiago": "683.6",
        "marker": "City.png",
        "tohere": "3.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Puente de Castro",
        "latitude": "42.580420",
        "longitude": "-5.552216",
        "nid": "911",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Puente Castro, notable for its bridge, has now grown large enough to merge with one of Le\u00f3n\u2019s outer barrios (Santa Ana). There are plenty of places to take a rest and enjoy a beverage; there are still a few kilometers to go before arriving at the center of Le\u00f3n.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "308.7",
        "marker": "City.png",
        "tohere": "3.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Mercado da Serra",
        "latitude": "42.770881",
        "longitude": "-7.466820",
        "nid": "983",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "107.7",
        "marker": "City.png",
        "tohere": "1.90",
        "barrest": "Yes",
        "road": "The camino passes through the hamlets of Leim\u00e1n, Peruscallo, Corti\u00f1as, Lavandeira, Casal, and Brea. None have any services. The way is more gently rolling hills.\n\nBetween here and Morgade you may find, behind a fence on your right-hand side, a rather curious and grumpy ostrich. Ostrich meat is a growing industry in Spain, but this particular fella is the family pet. \n"
    }
}, {
    "node": {
        "title": "Spain and Navarra",
        "latitude": "43.045203",
        "longitude": "-1.265317",
        "nid": "1445",
        "nodetype": "Province Border",
        "body": "Navarra is the first of the Spanish Autonomous Communities along the Camino Franc\u00e9s. It extends from here, in the heights of the Pyrenees to the river plains of the Ebro on the outskirts of Logro\u00f1o. \nThe language in these parts is officially Spanish and Basque. It should be noted that although Navarra is divided between Basques and Spaniards, the majority of the population here refers to this part of the world as Basqueland (pais vasco) rather than Spain.\n\nThe signage here reflects that sentiment. Most towns have an alternative name whose spelling is strictly monitored and maintained by spray can wielding nationalists. \nYou will as well see that the street names have dropped the castillian \u2018Calle\u2019 for the basque \u2018Kalea.\u2019\n",
        "image": "7781-635.png",
        "tosantiago": "750.3",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Collado de Lepoeder",
        "latitude": "43.026000",
        "longitude": "-1.295807",
        "nid": "7491",
        "nodetype": "Alto",
        "tosantiago": "746.6",
        "marker": "Alto.png",
        "tohere": "4.20",
        "barrest": ""
    }
}, {
    "node": {
        "title": "A Ferrer\u00eda",
        "latitude": "42.766801",
        "longitude": "-7.252843",
        "nid": "7641",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "130.1",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Estella [Lizarra]",
        "latitude": "42.670383",
        "longitude": "-2.030840",
        "nid": "845",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Estella, in spite of not being a very big town, has a lot to offer. The camino enters along the river and keeps to the west bank. To see many of the churches, or to stay in some of the albergues, you will need to cross the R\u00edo Ega.\n\nThe town boasts an impressive number of historically significant buildings, so many that to visit them all would take the better part of a weekend. The two not to be missed are the Palacio de los Reyes de Navarra (never actually used by the King of Navarra) and the Iglesia de San Pedro de la Rua. The others include the Iglesia de Santo Sepulcro (as you enter town on your left, closed since 1881), the Iglesia de San Miguel, and the Bas\u00edlica del Puy which requires a climb to the top of town. Be sure to spend enough time wandering the streets to take it all in.\n\nIf you find yourself in Estella for a day of rest and exploration, there are some excellent walking trails nearby. Ask for maps in the tourist information office next to the Palacio.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Estella 5, 31200,Mon-Fri: 0830-2030 & Sat: 0930-1300,948 551 792",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "845-133775.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/estella.html?aid=1499405",
        "tosantiago": "654.9",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes",
        "road": "If you stayed in one of the albergues on the new side of the town, cross back over the river to rejoin the camino out of town.\n\n"
    }
}, {
    "node": {
        "title": "Puente de Orbigo",
        "latitude": "42.463961",
        "longitude": "-5.876999",
        "nid": "919",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "274.1",
        "marker": "City.png",
        "tohere": "6.80",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ferreiros",
        "latitude": "42.783564",
        "longitude": "-7.533220",
        "nid": "991",
        "nodetype": "City",
        "body": "As the name implies, Ferreiros was once home to a notable blacksmithing trade. More recently it was another center for small dairy production, and that industry has given way to supporting pilgrims.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "991-133858.webp",
        "tosantiago": "100.2",
        "marker": "City.png",
        "tohere": "1.40",
        "barrest": "Yes",
        "road": "Beyond Ferreiros you will encounter the 100km marker. It is the single most vandalized object on the camino, changing from day to day as pilgrims leave their mark. From here on to Santiago the terrain has smoothed out, but do not be deceived. Although the height of the peaks has been reduced, the frequency with which you have to climb the smaller ones has increased.\n\n"
    }
}, {
    "node": {
        "title": "Fresno del Camino",
        "latitude": "42.563954",
        "longitude": "-5.654526",
        "nid": "2633",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "298.9",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Logro\u00f1o",
        "latitude": "42.466616",
        "longitude": "-2.445690",
        "nid": "853",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Once you have crossed the R\u00edo Ebro, you have entered Logro\u00f1o proper. \nWorth visiting are the Concatedral de Santa Mar\u00eda de La Redonda (it shares the title of Cathedral with Santo Domingo, and it has a semi-hidden painting of the crucifixion by Michelangelo), the Iglesia de San Bartolom\u00e9, and the Iglesia de Santiago del Real. Of these, only the last one is located along the camino route, though all are easy to find and centrally located.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Once de Junio 1, 26001, Mon-Fri: 0830-2030 & Sat: 930-1300, 9412 86 802",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "853-133804.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/logrono.html?aid=1499405",
        "tosantiago": "605.7",
        "marker": "City.png",
        "tohere": "9.70",
        "barrest": "Yes",
        "road": "Leaving Logro\u00f1o is not unlike departing most large cities in that it winds its way across town in a way that mixes landmarks with practicalities. There are plenty of arrows to mark the way, but most are painted low on curbs and can be difficult to see if you are a pre-dawn walker. If in doubt, make your way to the Cathedral and with it to your back head straight ahead along the Calle de Marqu\u00e9s de San Nicol\u00e1s. You will pass straight through two roundabouts turn left on the third.\n\n"
    }
}, {
    "node": {
        "title": "El Ganso",
        "latitude": "42.462916",
        "longitude": "-6.209679",
        "nid": "927",
        "nodetype": "City",
        "body": "Water fountain adjacent to the church. Surrounding the town are several low stone walls that used to hold cattle and sheep, you will be forgiven for thinking that the village was falling down around you. \nHome of the Cowboy bar, and that other bar.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "243.5",
        "marker": "City.png",
        "tohere": "4.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Vilach\u00e1",
        "latitude": "42.796030",
        "longitude": "-7.602453",
        "nid": "999",
        "nodetype": "City",
        "body": "There are no shops of any kind in Vilach\u00e1, the last town before Portomarin. However, Casa Banderas sells drinks and snacks at the front gate.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "93.7",
        "marker": "City.png",
        "tohere": "3",
        "barrest": "",
        "road": "The camino will soon bring you within sight of Portomar\u00edn. You will also pass through the town of Vilach\u00e1 before beginning the rather steep descent into the valley of the R\u00edo Mi\u00f1o and across the modern bridge to Portomar\u00edn.\n\nThe camino into town passes over the new bridge. The old bridge spends most of its time submerged beneath the river below but by the end of summer it emerges as the water level drops. Up the stairs and through the small chapel, you are almost into town. It is a bit more uphill still.\n\n"
    }
}, {
    "node": {
        "title": "Alto de San Ant\u00f3n",
        "latitude": "42.411984",
        "longitude": "-2.642974",
        "nid": "3035",
        "nodetype": "Alto",
        "image": "3035-133396.webp",
        "tosantiago": "585.2",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ciri\u00f1uela",
        "latitude": "42.421682",
        "longitude": "-2.893181",
        "nid": "858",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "563.6",
        "marker": "City.png",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Riego de Ambr\u00f3s",
        "latitude": "42.522281",
        "longitude": "-6.480088",
        "nid": "932",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/riego-de-ambros.html?aid=1499405",
        "tosantiago": "216.9",
        "marker": "City.png",
        "tohere": "3.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Hospital de la Cruz",
        "latitude": "42.840777",
        "longitude": "-7.734090",
        "nid": "1004",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "80.5",
        "marker": "City.png",
        "tohere": "2.40",
        "barrest": "Yes",
        "road": "When leaving Hospital de la Cruz it is important to pay close attention to the traffic. The camino crosses a large roundabout, goes over the highway, and then turns left. At this point, you are actually walking along the highway on-ramp, though only for a short distance. It turns right and continues the upward march towards Ventas de Naron.\n\n"
    }
}, {
    "node": {
        "title": "Cruz de Ferro",
        "latitude": "42.488951",
        "longitude": "-6.361706",
        "nid": "3040",
        "nodetype": "Alto",
        "body": "The Iron Cross marks the highest part of the mountain pass and has become the place where pilgrims the world over leave behind a stone brought with them from home or elsewhere on the camino.\n\n",
        "image": "3040-52996.webp",
        "tosantiago": "229.4",
        "marker": "Alto.png",
        "tohere": "2.30",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Belorado",
        "latitude": "42.420542",
        "longitude": "-3.189855",
        "nid": "866",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Belorado manages to maintain some of its advantages, but hard times have certainly taken their toll. A walk around the perimeter streets will reveal a quickly crumbling infrastructure. On the hillside sit the remains of a castle.\n\nStill, several churches remain and the hospices have been replaced by many modern albergues. The main square is a nice place to hide from the sun if you fancy a rest.\n\nDo like the locals and avoid the restaurants in the main square; one of the best options for dinner is at the Cuatro Cantones albergue restaurant. \nThe Iglesia de Santa Mar\u00eda has an altarpiece which depicts Santiago both as a Pilgrim and as a Moorslayer.\n\n",
        "bus": "Yes",
        "correos": "Ave Cerezo de Rio Tiron 6, 09250, Mon-Fri: 1000-1330 & Sat: 1000-11:30",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/belorado.html?aid=1499405",
        "tosantiago": "533.7",
        "marker": "City.png",
        "tohere": "4.80",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Cacabelos",
        "latitude": "42.599819",
        "longitude": "-6.725693",
        "nid": "940",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Cacabelos is best known for the production of wine, you\u2019ll notice a modern building at the start of town that was built to promote the region. \nTake the time to wander the lateral streets and you will be rewarded with several out of the way restaurants. The municipal albergue here has a curious design; horseshoe in shape it wraps around the church and enjoys semi-private two person rooms.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "940-133847.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/cacabelos.html?aid=1499405",
        "tosantiago": "189.4",
        "marker": "City.png",
        "tohere": "5.40",
        "barrest": "Yes",
        "road": "The camino leaves town along the road over the R\u00edo C\u00faa and past the church and albergue.\n\n"
    }
}, {
    "node": {
        "title": "Os Valos",
        "latitude": "42.873042",
        "longitude": "-7.822531",
        "nid": "1012",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "71.4",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Castrillo de los Polvazares",
        "latitude": "42.464832",
        "longitude": "-6.126037",
        "nid": "6988",
        "nodetype": "City",
        "body": "Castrillo has been declared a Historic-Artistic site and the restored village is off limits to non-resident vehicles.  The main street has a cross at the beginning and at the end of town, and is lined with large houses. The village dates back to the 16th century, and is a replacement for the old town of Castrillo that was destroyed by a flood.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "6988-133835.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/castrillo-de-los-polvazanhtml?aid=1499405",
        "marker": "City.png",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Fountain and picnic area",
        "latitude": "42.340970",
        "longitude": "-3.865042",
        "nid": "7586",
        "nodetype": "POI",
        "body": "There is a fountain and a picnic area 2.4km outside of Rab\u00e9 de las Calzadas.\n\n",
        "tosantiago": "468.7",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Fountain.png"
    }
}, {
    "node": {
        "title": "Villalval",
        "latitude": "42.369056",
        "longitude": "-3.551888",
        "nid": "874",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "499.4",
        "marker": "City.png",
        "tohere": "4.40",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ruitel\u00e1n",
        "latitude": "42.673191",
        "longitude": "-6.965976",
        "nid": "948",
        "nodetype": "City",
        "body": "San Froilan lived as a hermit in a nearby cave during the 9th century. At the behest of the people of Le\u00f3n, King Alfonso III named him as Bishop, a position which he held for only 5 years.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "162.3",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Casanova",
        "latitude": "42.878733",
        "longitude": "-7.928282",
        "nid": "1021",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "61.6",
        "marker": "City.png",
        "tohere": "1.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Hontanas",
        "latitude": "42.312180",
        "longitude": "-4.043398",
        "nid": "884",
        "nodetype": "City",
        "body": "The town of Hontanas derives its name from the numerous natural springs, fontanas, that are to be found here. \nIf you spend enough time here you are bound to bump into a retired sheep farmer named Victorino who rather horrifically entertains pilgrims by drinking wine poured onto his forehead from the spout of a glass porr\u00f3n (a glass wine jug with a long tapered spout). \nWhile you could imagine it running down his face, over his nose, and more or less managing to find a path to his mouth, nothing prepares you for the actual spectacle of it.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "884-133787.webp",
        "tosantiago": "452.9",
        "marker": "City.png",
        "tohere": "5.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Barrio San L\u00e1zaro",
        "latitude": "42.886376",
        "longitude": "-8.514517",
        "nid": "12273",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "marker": "City.png",
        "barrest": "Yes",
        "road": "Der Weg nach Santiago hinein f\u00fchrt durch die urbanisierte Zone, die um die Altstadt herum entstanden ist. Nachdem Sie die Autobahnbr\u00fccke passiert haben, ist der erste Teil von Santiago durch den Sie gehen das Barrio San Lazaro; Die Kirche hier soll die Grenze f\u00fcr PilgerInnen mit Lepra gewesen sein. Es gibt eine ziemlich gro\u00dfe Kreuzung, wo die Pfeile verschwinden und durch blaue und gelbe Schilder auf Pfosten ersetzt werden. Endlich f\u00fchrt der Camino in die Altstadt, durch die Porta do Cami\u00f1o, windet sich sanft durch die gepflasterten Gassen, \u00fcber die Plaza Cervantes, unter der Bischofsresidenz und auf die Plaza de Obradoiro. Herzlichen Gl\u00fcckwunsch und willkommen in Santiago de Compostela!\n"
    }
}, {
    "node": {
        "title": "Padornelo",
        "latitude": "42.711336",
        "longitude": "-7.121909",
        "nid": "956",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "956-133908.webp",
        "tosantiago": "144.8",
        "marker": "City.png",
        "tohere": "2.40",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Boente",
        "latitude": "42.916081",
        "longitude": "-8.078899",
        "nid": "1029",
        "nodetype": "City",
        "body": "The first building on your left in Boente is the Bar de los Alemanes, and this is the best bar in town. Further down the hill, the camino crosses the main road (caution) where two albergues and a few bars are located.\n\nYou can get a sello in the Iglesia de Santiago de Boente, and curiously enough the entrance that most pilgrims use leads directly to the sacristy. A collection of hundreds of prayer cards from around the world adorn the walls, and if the priest is not there to stamp your credencial he leaves it out for you to take matters into your own hands.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "46.9",
        "marker": "City.png",
        "tohere": "5.80",
        "barrest": "Yes",
        "road": "Walking past the church the camino turns right and descends more. The hills ahead are steep in both directions, take it easy.\n\n"
    }
}, {
    "node": {
        "title": "Burguete [Auritz]",
        "latitude": "42.991889",
        "longitude": "-1.332135",
        "nid": "817",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Immortalized by and little changed since Ernest Hemmingway lodged here 1924. He was on a fishing trip, no doubt catching some of the trout that swim these rivers and which luckily for us end up on more than a few dinner tables.\n\nThe architecture here is typically Basque; massive multi-generation family homes with a name or a crest above the door.\n\nSeveral good caf\u00e9s and proof that the first cup of coffee you come to in town may not be the best. There is also a bakery\/caf\u00e9 beyond the turn mentioned below.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "817-133773.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/burguete-navarra.html?aid=1499405",
        "tosantiago": "740.1",
        "marker": "City.png",
        "tohere": "2.60",
        "barrest": "Yes",
        "road": "Keep your wits about you when walking through town because the camino takes a sharp right turn halfway through. There are no less than twelve yellow arrows marking the turn, but it can still be overlooked by those admiring the charm of Burguete.\n\n"
    }
}, {
    "node": {
        "title": "Revenga de Campos",
        "latitude": "42.284072",
        "longitude": "-4.482551",
        "nid": "892",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "412",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Renche",
        "latitude": "42.740299",
        "longitude": "-7.290673",
        "nid": "964",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "133.3",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": "Yes",
        "road": "Pass several villages with no services: Lastres, Freituxe, and San Marti\u00f1o do Real.\n\n"
    }
}, {
    "node": {
        "title": "Calzada",
        "latitude": "42.925069",
        "longitude": "-8.225069",
        "nid": "1039",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "32.5",
        "marker": "City.png",
        "tohere": "0.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Aquerreta [Akerreta]",
        "latitude": "42.896507",
        "longitude": "-1.543720",
        "nid": "825",
        "nodetype": "City",
        "body": "A small Basque hamlet, notable only for the large rural hotel. Leave the paved road for a dirt track here.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "715",
        "marker": "City.png",
        "tohere": "0.60",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Information board with optional routes to Burgos",
        "latitude": "42.376234",
        "longitude": "-3.441060",
        "nid": "8591",
        "nodetype": "POI",
        "body": "Ignore the information board when leaving San Juan de Ortega. It is incorrect and may trick you into taking the wrong road to Casta\u00f1ares. Continue straight along the camino that heads back into the forest.\n\n",
        "tosantiago": "509.5",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "POI.png"
    }
}, {
    "node": {
        "title": "San Nicol\u00e1s del Real Camino",
        "latitude": "42.363459",
        "longitude": "-4.951830",
        "nid": "900",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "368.3",
        "marker": "City.png",
        "tohere": "2.60",
        "barrest": "Yes",
        "road": "More than halfway between San Nicol\u00e1s and Sahag\u00fan the camino departs from the dirt track and crosses the highway. It heads towards the Ermita de la Virgen del Puente. Just beyond the Ermita stands a monument which marks the geographical center of the camino, the halfway point. Naturally the math doesn\u2019t quite add up, but you can be certain that you have crossed that half way point sometime between Moratinos and Sahag\u00fan.\n\n"
    }
}, {
    "node": {
        "title": "Sivil",
        "latitude": "42.762904",
        "longitude": "-7.350222",
        "nid": "972",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "126.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "A R\u00faa [O Pino]",
        "latitude": "42.914672",
        "longitude": "-8.350098",
        "nid": "1047",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "21",
        "marker": "City.png",
        "tohere": "1.40",
        "barrest": "Yes",
        "road": "Where the camino returns to the road at the start of O Pedrouzo you will find an abundance of arrows and a large map which is nearly worthless. Arrows and dozens of signs advertising various hostels and hotels point in every direction. If you have a reservation, review the map to find the best path, otherwise turn left up the road. If you are not staying the night in Pedrouzo, cross the road here and continue along the camino.\n\n"
    }
}, {
    "node": {
        "title": "\u200cDetour from Murias to Maragato village of Castrillo de los Polvazares",
        "latitude": "42.459671",
        "longitude": "-6.104965",
        "nid": "7468",
        "nodetype": "POI",
        "body": "With the recent addition of albergues in Castrillo this deviation has become popular for more than just its cultural itinerary. It is a fully restored Maragato village well worth a visit and perhaps a meal. Napoleon is reputed to have spent the night there, where a battle was fought and where the mayor and priest were captured and subsequently rescued.\n\nTo get there follow the LE-142 which brought you into Murias. It runs adjacent to the main camino road where a few bars and restaurants are located. To rejoin the camino, follow the road at the far end of Castrillo.  The detour adds 700m.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "\u25c1 leave the road at the LU-641",
        "latitude": "42.735017",
        "longitude": "-7.363672",
        "nid": "7628",
        "nodetype": "POI",
        "body": "The camino follows the road for 2km past the turn off to Perros. Where this road meets the LU-641 the camino turns Left onto a track. From here it proceeds through or near several small hamlets. The names and distances are listed here, but none offer any services and might even be mistaken for just a farm.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Pamplona [Iru\u00f1a]",
        "latitude": "42.817975",
        "longitude": "-1.644258",
        "nid": "833",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Modern Pamplona is the most populous city along the Camino Franc\u00e9s. It offers just about every service you might need along the way. Its main square, the Plaza del Castillo was renovated in 2003 and is the easiest point from which to get your bearings. \nHemingway helped put the city back on the map and into the minds of many. He visited frequently and wrote kindly of the place. Some of his old haunts are still around (Bar Txoko, Hotel La Perla, and Caf\u00e9 Iru\u00f1a). \nAs is usual in every Spanish town with a large tourist population, the best food is found off the beaten path. Luckily, that is a quick stroll down Calle San Nicol\u00e1s on the west side of the Plaza. \nPamplona is worthy of a rest day to see the sights, or perhaps a half day. Near the entrance to town is the Neoclassical Cathedral de Santa Mar\u00eda el Real, your credential gets you a discount on the Cathedral and the attached Museo Diocesano. Another church of note is the fortified Iglesia of San Saturnino.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Paseo Pablo Sarasate 9, 64220, Mon-Fri: 0830-2030 & Sat: 0930-1300, 948 207 217",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "833-133795.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/pamplona.html?aid=1499405",
        "tosantiago": "700.4",
        "marker": "City.png",
        "tohere": "3.10",
        "barrest": "Yes",
        "road": "The camino leaves town via the star-shaped Citadel that rose up during the 17th century (much like the citadel in Saint Jean Pied de Port, as both were based off the designs of the same French military engineer). If you are lost, find your way to the park which surrounds the citadel. The camino is well marked from there."
    }
}, {
    "node": {
        "title": "Puente de Villarente",
        "latitude": "42.545872",
        "longitude": "-5.457373",
        "nid": "908",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villarente.html?aid=1499405",
        "tosantiago": "318",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": "Yes",
        "road": "As you are leaving Puente behind you, keep an eye out for arrows pointing you along a trail to your right-hand side, and up a small hill towards Arcahueja. It is fairly well marked but is along a service road that is often lined with parked trucks and the way might be obscured.\n\n"
    }
}, {
    "node": {
        "title": "Vilei",
        "latitude": "42.768658",
        "longitude": "-7.444299",
        "nid": "980",
        "nodetype": "City",
        "body": "Vilei is often mismarked as Barbadelo on the map and in guidebooks. They are distinct hamlets, less than 1km apart. It has grown into quite a pilgrim stop from the abandoned hamlet it once was. It now counts two albergues and a trinket shop and is a pleasant place to stay if you find Sarria to be overcrowded. It is also the first coffee since Sarria.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "980-133856.webp",
        "tosantiago": "110",
        "marker": "City.png",
        "tohere": "3.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Marcos",
        "latitude": "42.891624",
        "longitude": "-8.490458",
        "nid": "1056",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/san-marcos.html?aid=1499405",
        "tosantiago": "5.3",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Detour to Vilar de Donas and La Iglesia de El Salvador",
        "latitude": "42.873700",
        "longitude": "-7.809091",
        "nid": "7476",
        "nodetype": "POI",
        "body": "\u200cDetour to Vilar de Donas and La Iglesia de El Salvador\nAfter passing the bar of Portos, turn right along a gravel road. There is a fork at 500m, keep left and continue strait over the N-547. To the church and back is 4.5km.\n\nGuided tours are offered by a local, and in some cases even when the church is otherwise closed. The official hours are:\nWinter (November through March): 12-17:30  CLOSED MONDAYS\nSummer (April through October): 11-14 & 16-20  CLOSED MONDAYS.\n\nThe church was given to the Order of Santiago under the pretext that Galician members of the order be buried there. The frescoes in the apse depict the parable of the 10 virgins and is in remarkable condition given that it dates from the early 15th century.\n\n",
        "image": "7476-61613.webp",
        "tosantiago": "72.7",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Mont\u00e1n",
        "latitude": "42.769184",
        "longitude": "-7.299653",
        "nid": "7638",
        "nodetype": "City",
        "body": "You will not enter the village of Mont\u00e1n, but rather pass along the edge. While there is no fixed bar, there is a small picnic area with an on-again-off-again vending machine.\n\nYou will pass through Fontearcuda and Furela, no services.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "124.7",
        "marker": "City.png",
        "tohere": "5",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Cirauqui",
        "latitude": "42.675715",
        "longitude": "-1.891580",
        "nid": "842",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Cirauqui sits perched atop a small hill and it seems that everywhere you go is uphill.  At the entrance to town is a medieval cross and at the top of town stands the Iglesia de San Rom\u00e1n, the plaza, and an albergue. \nDon\u2019t forget to stamp your credentials under the archway.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "842-133873.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/cirauqui.html?aid=1499405",
        "tosantiago": "668.8",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Miguel del Camino",
        "latitude": "42.562216",
        "longitude": "-5.696626",
        "nid": "916",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "293",
        "marker": "City.png",
        "tohere": "1.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Casal",
        "latitude": "42.777204",
        "longitude": "-7.505486",
        "nid": "988",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "103.2",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Galicia \/ Lugo",
        "latitude": "42.706071",
        "longitude": "-7.031864",
        "nid": "1450",
        "nodetype": "Province Border",
        "body": "Welcome to Galicia, where the legend of Santiago was born. This northwesternmost corner of the Spanish Peninsula bears little resemblance to the type of landscape that typically comes to mind when thinking about Spain.  It is wet, and very green, and the mountains that surround it have for centuries kept it isolated from the rest of the country.\n\nThe language here is Gallego and although it has been distilled into one teachable form, you are more likely to get an earful of a more rustic, ancient, and totally incomprehensible version as you pass from one village to another.  Village is perhaps too generous of a word, hamlet would be better suited... or perhaps just \u2018place\u2019 as so many collections of more than one building are often called here.\n\nFor centuries the land here has been fought over by invaders, but the Galicians did not defend it with quite the same gusto as their Basque counterparts and as a result, they have spent most of recorded history an occupied nation. Perhaps this is the reason that Galicians have a reputation for being introverted, or guarded, or skeptical, and above all non-committal.  Ask a Galician on a staircase the saying goes, and he will be unable to tell you which direction he is heading. \nYou are likely to find Celtic symbols carved into the stone of a home, or a church.  Common also are witches, both the good kind and the bad kind.  Hearty soups, a darker bread with a heavier crust, and a strong liqueur (the by-product of the local wine production) are a part of every meal.\n\nThe terrain will change as you get closer to Santiago. Mountains shrink as you head west, but they become more frequent and by the time you reach the Apostle you will find precious little flat ground.\n\n",
        "image": "1450-133851.png",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Zubiri to Larrosoa\u00f1a",
        "latitude": "42.929651",
        "longitude": "-1.505084",
        "nid": "7544",
        "nodetype": "POI",
        "body": "You can take the road or the trail out of Zubiri. The trail, which passes a belching magnesium plant, can get quite soggy during wet weather.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Sansol",
        "latitude": "42.553761",
        "longitude": "-2.265759",
        "nid": "850",
        "nodetype": "City",
        "body": "The yellow arrows have a tendency to keep to the road as they take you through this small village. To see anything of importance, including the church and many of Sansol\u2019s several old baroque homes, turn right at the large ceramic tile sign indicating \u201cSansol\u201d on the side of one of the first buildings.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "626.5",
        "marker": "City.png",
        "tohere": "7.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Astorga",
        "latitude": "42.455391",
        "longitude": "-6.053317",
        "nid": "924",
        "nodetype": "City",
        "atm": "Yes",
        "body": "In Astorga you may notice a sudden increase in the number of pilgrims, new faces will appear amongst the regular crowd you have grown accustomed to. They are likely Spanish, as this is a common starting point for Spaniards, or they have come from Sevilla along the Via de la Plata. A quick peek at their boots will tell the two apart.\n\nHere you will take first notice of the Maragato culture, the ancient tribe of red-headed Iberians that served as the muleteers between the coast and the interior of Spain. They maintain several curious customs by modern standards, doing most thing the exact opposite as we are accustomed. One of these is their cocido, which is served meat first and greens last. It is a hearty meal and one not to be tackled alone. Grab a group of pilgrims and head off to one of the restaurants serving \u201cCocido Maragato\u201d for a taste. Be warned, this is not a vegetarian-friendly option. \nLater on down the road you have the choice of passing through a well preserved Maragato village where only recently have the locals abandoned their old way of locking their doors: it was their custom to leave the key in the outside door when they left town as a sign to their neighbors that they are gone and as an invitation to the same neighbors to let themselves in if they need anything.\n\nThree buildings of note: 1. The Ayuntamiento (whose animatronic sculptures ring in the hours), 2. The Palacio Episcopal, which was never occupied by the Bishop and which was designed by Gaudi, and 3. the Catedral de Astorga. The last two can be visited on a joint ticket for 5 euros but the opening hours are not always clear.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Correos 3, 24700, Mon-Fri: 0830-1430 & Sat: 0930-1300, 987 615 442",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "924-133905.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/astorga.html?aid=1499405",
        "tosantiago": "256.9",
        "marker": "City.png",
        "tohere": "4",
        "hospital": "Yes",
        "barrest": "Yes",
        "road": "The camino here leaves the last of the plains behind and begins, slowly at first, to climb. The ascent is slow and gradual until Rabanal, where it begins a more steep ascent to the alto and an equally quick descent into Molinaseca. On the way up it passes through several small but equipped towns (no ATM\u2019s but plenty to eat).\n\n"
    }
}, {
    "node": {
        "title": "Mercadoiro",
        "latitude": "42.788732",
        "longitude": "-7.568662",
        "nid": "996",
        "nodetype": "City",
        "body": "Another bar-and-albergue-only kind of town, run by some fellows from Valencia and the most promising place to find paella on the menu.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "96.8",
        "marker": "City.png",
        "tohere": "2.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Alto de Erro",
        "latitude": "42.941431",
        "longitude": "-1.470714",
        "nid": "3032",
        "nodetype": "Alto",
        "body": "There is a food truck which provides year-round service to pilgrims, and which has a nice multicolored stamp.\n\nThe basket of underwear there has an explanation: The owner originally put out a basket to collect unwanted items from pilgrims which could be donated to charity. One day a particular peregrina came along with uncomfortable undergarments and she thought it a good idea to leave them behind. She subsequently fell in love and got married on the camino, prompting the owner to put up the additional notice that \u201cMagic Happens.\u201d\n",
        "image": "3032-134840.webp",
        "tosantiago": "724.7",
        "marker": "Alto.png",
        "tohere": "4.60",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Toxibo",
        "latitude": "42.812911",
        "longitude": "-7.664065",
        "nid": "1001",
        "nodetype": "City",
        "body": "H\u00f3rreo: If you pronounce this word like OREO you are pretty close. H\u00f3rreos are everywhere in northern Spain, from the smallest of hamlets to the largest of private estates. They vary in design from region to region and in Galicia they tend to stretch out lengthwise. They function as corn-cribs, with slatted walls that allow for circulation and a foundation designed to prevent rodents from climbing up. They range in length from 2m to 35m.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "87.4",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Sierra de Atapuerca",
        "latitude": "42.379800",
        "longitude": "-3.533400",
        "nid": "3037",
        "nodetype": "Alto",
        "image": "3037-133758.webp",
        "tosantiago": "501.5",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Castildelgado",
        "latitude": "42.437204",
        "longitude": "-3.084497",
        "nid": "863",
        "nodetype": "City",
        "body": "Small town with little more than a church, an ermita, and a truck stop restaurant with a reputation for serving up some decent hot chocolate.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "543.9",
        "marker": "City.png",
        "tohere": "1.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Columbrianos",
        "latitude": "42.572188",
        "longitude": "-6.610851",
        "nid": "937",
        "nodetype": "City",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "937-487.webp",
        "tosantiago": "200.1",
        "marker": "City.png",
        "tohere": "1.80",
        "barrest": "Yes",
        "road": "You will pass through Columbrianos quickly and continue along a quiet country road, bypassing much of the industry around Ponferrada. Along the way, you will pass along the outskirts of Fuentes Nuevas and you will get your first good glimpse of the mountains ahead.\n\n"
    }
}, {
    "node": {
        "title": "Eirexe [Airexe]",
        "latitude": "42.865405",
        "longitude": "-7.787265",
        "nid": "1009",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "74.8",
        "marker": "City.png",
        "tohere": "1.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Calvor",
        "latitude": "42.773779",
        "longitude": "-7.355540",
        "nid": "3126",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "118.8",
        "marker": "City.png",
        "tohere": "1.40",
        "barrest": "",
        "road": "The camino does not go through Calvor, but rather passes near its Iglesia de San Estevo y San Pablo. To get to the church, turn left for a short detour once you leave Pint\u00edn. The church itself is nothing spectacular, but the site has been home to one building or another since two monks from Samos founded a church here in the 8th century. Also the view to Sarria is worth the detour.\n\n"
    }
}, {
    "node": {
        "title": "San Juan de Ortega",
        "latitude": "42.375856",
        "longitude": "-3.436661",
        "nid": "871",
        "nodetype": "City",
        "body": "The monastery has been partially rehabilitated into an albergue for pilgrims, and it is here that pilgrims were served garlic soup by the monks; a tradition that continued up to recent times. If you have never spent the night in a room full of pilgrims that have eaten garlic soup for dinner, you have not lived.\n\nThe church has been restored and is a popular place for weddings. Twice a year (the 20th of March and the 23rd of September, both equinoxes) at around 5pm, light shines through one of the windows to illuminate one of the capitals, moving from Anunciation to the birth of Christ.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "871-133762.webp",
        "tosantiago": "510.1",
        "marker": "City.png",
        "tohere": "11.90",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "La Portela de Valcarce",
        "latitude": "42.660884",
        "longitude": "-6.920743",
        "nid": "945",
        "nodetype": "City",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/la-portela-de-valcarce.html?aid=1499405",
        "tosantiago": "166.9",
        "marker": "City.png",
        "tohere": "4.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Carballal",
        "latitude": "42.871319",
        "longitude": "-7.885201",
        "nid": "1018",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "65.8",
        "marker": "City.png",
        "tohere": "1.60",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Cruz de Peregrinos",
        "latitude": "43.003141",
        "longitude": "-1.320376",
        "nid": "7408",
        "nodetype": "POI",
        "body": "Croce gotica dei pellegrini (replica) del XIV secolo.\n\n",
        "image": "7408-61017.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Cruceiro.png"
    }
}, {
    "node": {
        "title": "\u200cALTERNATE ROUTE to Villalc\u00e1zar via Villovieco",
        "latitude": "42.269148",
        "longitude": "-4.449313",
        "nid": "7594",
        "nodetype": "POI",
        "body": "The camino follows the road out of Poblaci\u00f3n, over the bridge and along the same type of path that brought you into town. An alternative route bypasses the next two towns (Revenga and Villarmentero) and follows a quieter trail alongside the river.\n\nTo follow this detour stay on to the right through Poblaci\u00f3n, keeping on the north side of the river and passing through Villovieco. In Villovieco you cross the bridge and turn to the right and follow the river (or carry on straight and rejoin the camino in Vilarmenteros, which has been paralleling this one along the road). If you turn right, the waymarking is hard to see but you can hardly get lost as the trail stays adjacent to the river until you get to a crossroad (P-981) and a weathered ermita. Turn left on the road and into Villac\u00e1zar de Sirga.\n\n",
        "tosantiago": "415.1",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Rab\u00e9 de las Calzadas",
        "latitude": "42.340212",
        "longitude": "-3.834271",
        "nid": "881",
        "nodetype": "City",
        "body": "The camino has now entered the central Meseta, and the landscape here has had a lasting effect on the character of the people, but its lack of naturally durable building materials has meant the opposite for the architecture. Many of the treasures that once stood here are all but lost, the oldest remaining building is the 17th century palace house and village church.\n\nThe broad expanses here have a certain effect on the pilgrim psyche and to skip over it is to miss out on an important stage of the journey. Do not expect to see many of the places along the way until the last moment, they are often built into the shallow valleys where shade and water collects.\n\n",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "881-305.webp",
        "tosantiago": "471.1",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes",
        "road": "There is a fountain and a picnic area 2.4 km outside of Rab\u00e9 de las Calzadas.\n\n"
    }
}, {
    "node": {
        "title": "Virgen de Biakorri",
        "latitude": "43.080483",
        "longitude": "-1.251682",
        "nid": "11634",
        "nodetype": "Alto",
        "body": "The Virgin stands at the Pic D'Orisson.\n\n",
        "image": "11634-134842.webp",
        "tosantiago": "755.8",
        "marker": "Alto.png",
        "tohere": "3.70",
        "barrest": ""
    }
}, {
    "node": {
        "title": "O Cebreiro",
        "latitude": "42.707823",
        "longitude": "-7.043561",
        "nid": "953",
        "nodetype": "City",
        "body": "O Cebreiro has grown from a small and ancient village of dairy farmers into a small and ancient village of large scale tourism. With luck you will arrive in a shroud of fog and leave with an abundance of sunshine; both suit this village well.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "953-509.webp",
        "tosantiago": "152.8",
        "marker": "City.png",
        "tohere": "2.20",
        "barrest": "Yes",
        "road": "From the top, it is mostly downhill, though there remain a few brief climbs, all of the way to Triacastela.\n\n"
    }
}, {
    "node": {
        "title": "Melide",
        "latitude": "42.913891",
        "longitude": "-8.014595",
        "nid": "1026",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Although it has been on the menu as far back as O Cebreiro, Pulpo (octopus) doesn\u2019t seem to garnish much attention until Melide. Despite its apparent disconnect with the sea, Melide\u2019s thriving Thursday market meant that shipping pulpo was a profitable enterprise. It is served today as it was then: on a wooden plate, garnished only with a healthy drizzle of olive oil and a shake of paprika. It is eaten with a rather crude looking toothpick, alongside heavy Galician bread and a bowl of the local Ribeiro wine.\n\nOne of the better places to try it is Pulper\u00eda a Garnacha, the last door on your left before you get to the main road in Melide.\n\nMelide, long the crossroads between territories, is also the meeting point of the various camino routes which come from the north, including the part of the Camino del Norte and the Camino Primitivo. Because of this, and the proximity to Santiago, the road become a great deal more congested.\n\nThe melindre is another of Melide\u2019s favorite  foods. It resembles a glazed donut and is sold from dozens of identical booths during festivals.\n\n",
        "bus": "Yes",
        "correos": "Obispo Varela 20, 15800, Mon-Fri: 0830-1430 & Sat: 0930-1300, 981 505 866",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/melide.html?aid=1499405",
        "tosantiago": "52.7",
        "marker": "City.png",
        "tohere": "1.50",
        "barrest": "Yes",
        "road": "The traffic through Melide can be dangerous, particularly on market days. The arrows through town can be difficult to spot. Simply follow along with the main road to the first roundabout. If you have not yet crossed the main road do so at the roundabout, and then cross the road that runs perpendicular to it. Arrows should point you towards a small side street through the old part of town that parallels the main road. \nThere are many other yellow arrows that direct you towards the many albergues in town, they are often attached to adverts or are painted alongside a simple \u2018A\u2019. These can be ignored.\n\n"
    }
}, {
    "node": {
        "title": "Boadilla del Camino",
        "latitude": "42.258476",
        "longitude": "-4.347281",
        "nid": "889",
        "nodetype": "City",
        "body": "The fields surrounding Boadilla are dotted with adobe palomares (dovecotes). Find your way towards one and explore the interior, which holds thousand of niches.\n\nAdjacent to the town square is the Romanesque Iglesia de la Asunci\u00f3n and a well preserved Rollo de Justicia, a 15th century stone column to which criminals were chained while their punishments were meted out.\n\nThe best albergue in town (also in the square) is worth a visit even if you don\u2019t plan to stay there. Behind the unpretentious walls is a garden oasis complete with a pool and art.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "889-133360.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/boadilla-del-camino.html?aid=1499405",
        "tosantiago": "424.4",
        "marker": "City.png",
        "tohere": "8.20",
        "barrest": "Yes, Albergue \"En El Camino\" ",
        "road": "The camino leaving Boadilla soon joins a path that runs alongside the Canal de Castilla. You will follow along the canal until the outskirts of Fr\u00f3mista, where you cross at one of the locks. This canal was used for both transportation and irrigation.\n\n"
    }
}, {
    "node": {
        "title": "Ramil",
        "latitude": "42.756435",
        "longitude": "-7.232094",
        "nid": "961",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "132.7",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Arz\u00faa",
        "latitude": "42.927143",
        "longitude": "-8.164129",
        "nid": "1035",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Arz\u00faa is a pleasant town with almost enough beds for pilgrims. If you find everything to be full and don\u2019t feel like splurging on one of the many hotels in the area, the Polideportivo (sports hall) is often used to house pilgrims.\n\n\nBetween here and O Pedrouzo lie a string of very small Galician hamlets of little note. The locals in these parts enjoy telling you, without the slightest tone of irony or sarcasm, that \u2018no hay vacas in Galicia\u2019 (there are no cows in Galicia). Hold that thought in your head while you slosh through a soggy trail on an otherwise sunny day.",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "1035-133394.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/arzua-html?aid=1499405",
        "tosantiago": "38.5",
        "marker": "City.png",
        "tohere": "2.30",
        "barrest": "Yes",
        "road": "The camino leaves Arz\u00faa along a footpath, NOT the road. If you arrived at the main square, walk past the church (with your back to the road) and turn right onto the side street. The terrain is pleasant, a blend of trails and paved roads through small towns and lots of forests. There are a few steep sections but none of any considerable length.\n\n"
    }
}, {
    "node": {
        "title": "Ilarratz",
        "latitude": "42.911077",
        "longitude": "-1.517158",
        "nid": "822",
        "nodetype": "City",
        "body": "Four hundred meters from Ilarratz on the right-hand side of the road lies La Abadia de Ilarratz (the abbey). The medieval building, rumored to have been a fort, was converted to a church in the early 16th century. Pilgrims have recently bought it and are currently busy with a project to save the buildings. Pilgrims are welcome to pop in and say hello. They are also welcome to rest in the shade of the garden.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "822-134404.webp",
        "tosantiago": "718.2",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ledigos",
        "latitude": "42.353755",
        "longitude": "-4.865699",
        "nid": "897",
        "nodetype": "City",
        "bus": "Yes",
        "supermarket": "Yes, small and in albergue",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "897-134987.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/ledigos.html?aid=1499405",
        "tosantiago": "376.9",
        "marker": "City.png",
        "tohere": "6.20",
        "barrest": "Yes, in albergue",
        "road": "Ledigos wins the award for most confusing camino signs. Fear not, both directions will take you to Terradillos de los Templarios. However, if you are planning to stay at the Los Templarios albergue (located along the road, before the pueblo) you are advised to take the path which follows the N-120, which is the one indicated by the right arrow. The left arrow takes a more direct path to Terradillos across the fields.\n\n"
    }
}, {
    "node": {
        "title": "Foxos",
        "latitude": "42.726782",
        "longitude": "-7.336784",
        "nid": "969",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "127.3",
        "marker": "City.png",
        "tohere": "1.50",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Brea",
        "latitude": "42.918878",
        "longitude": "-8.305836",
        "nid": "1044",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "25",
        "marker": "City.png",
        "tohere": "2.60",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Ermita de La Virgen de las Cuevas",
        "latitude": "42.497463",
        "longitude": "-2.388861",
        "nid": "7432",
        "nodetype": "POI",
        "body": "The 17th century ermita, and the river that it sits along, are often considered to be the border to La Rioja. The actual modern day border is closer to Logro\u00f1o but the paper factory that marks it is much less quaint than this ermita.\n\n",
        "image": "7432-69150.webp",
        "tosantiago": "612.4",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Ermita.png"
    }
}, {
    "node": {
        "title": "Trinidad de Arre",
        "latitude": "42.836212",
        "longitude": "-1.604288",
        "nid": "830",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Crossing over the R\u00edo Ulzama you arrive to the district of Arre. Trinidad de Arre marks the point where you begin a mostly urban walk into Pamplona.\n\nThe camino here is joined by another from Bayonne to the northeast. It is an uncommon route by most measures, but if you find yourself among a few new faces, you may just ask them if they came along the Camino Bazt\u00e1n.\n\nThe albergue which is at the far end of the bridge is historically significant. Not only is it one of the oldest continually running albergues on the camino but it is also one of the most complete medieval monasteries in Navarra.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "705",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes",
        "road": "The next 7km are through the suburbs into and out of Pamplona.\n\n"
    }
}, {
    "node": {
        "title": "Reliegos",
        "latitude": "42.472596",
        "longitude": "-5.352745",
        "nid": "905",
        "nodetype": "City",
        "body": "Bar Elvis, quite possibly the most famous bar on the whole of the camino, is located in the main square. The owner is quite a character and if you\u2019ve brought your sharpie marker get to scribbling,  this is one of the few places where graffiti is sanctioned.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "905-399.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/reliegos.html?aid=1499405",
        "tosantiago": "330.4",
        "marker": "City.png",
        "tohere": "12.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "San Pedro do Cami\u00f1o",
        "latitude": "42.776198",
        "longitude": "-7.396569",
        "nid": "977",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "San Paio - San Payo",
        "latitude": "42.908913",
        "longitude": "-8.426128",
        "nid": "1053",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "12.4",
        "marker": "City.png",
        "tohere": "4",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "\u25b7 and uphill to Pradela",
        "latitude": "42.609345",
        "longitude": "-6.811926",
        "nid": "7473",
        "nodetype": "POI",
        "body": "The alternate route via Pradela begins here. Look for a steep trail leading up to your right.\n\nIt is important to note that although this route goes near Pradela, it is not actually necessary to visit Pradela. Signs mark the way.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "A Acea de Abaixo",
        "latitude": "42.765422",
        "longitude": "-7.416211",
        "nid": "7634",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Obanos",
        "latitude": "42.679564",
        "longitude": "-1.785879",
        "nid": "839",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "839-133872.webp",
        "tosantiago": "679.1",
        "marker": "City.png",
        "tohere": "1.80",
        "barrest": "Yes",
        "road": "Outside of Obanos, on the road to Puente la Reina, the Camino Aragon\u00e9s joins the Camino Franc\u00e9s.\n\n"
    }
}, {
    "node": {
        "title": "Trobajo del Camino",
        "latitude": "42.596175",
        "longitude": "-5.608008",
        "nid": "913",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "302",
        "marker": "City.png",
        "tohere": "3.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Peruscallo",
        "latitude": "42.780535",
        "longitude": "-7.493483",
        "nid": "985",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "104.6",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Castilla y Le\u00f3n \/ Burgos",
        "latitude": "42.445000",
        "longitude": "-3.045712",
        "nid": "1447",
        "nodetype": "Province Border",
        "body": "Here you leave La Rioja behind and enter into Castilla y Le\u00f3n, the largest of Spain\u2019s autonomous communities. It is comprised of nine provinces, of which you have just entered Burgos. Along the Camino Franc\u00e9s, you will also pass through Palencia and Le\u00f3n before leaving Castilla y Le\u00f3n. The other 6 provinces (Salamanca, Zamora, Valladolid, Avila, Segovia, and Soria) are all part of one camino or another as they cross central Spain.\n\nJust like La Rioja, you will continue to pass through villages with less than a two hundred (often less than one hundred) residents. \nBurgos also marks the start of the Meseta, Spain\u2019s central plateau and the breadbasket of the peninsula. The average elevation here is close to 800m and the weather can vary drastically from hot days to bone-chilling nights.\n\nWhile the next 300km have a reputation for being flat and boring they are anything but.\n\nIt is here, at elevation and with wide open skies that the camino begins to work on your mind.\n\n",
        "image": "1447-637.png",
        "marker": "ProvinceBorder.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Orisson",
        "latitude": "43.108754",
        "longitude": "-1.239245",
        "nid": "7493",
        "nodetype": "City",
        "body": "Orisson is a common stopping point for pilgrims wishing to split the walk over the pass into more manageable sections.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "7493-133356.webp",
        "tosantiago": "759.5",
        "marker": "City.png",
        "tohere": "2.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Azqueta",
        "latitude": "42.635758",
        "longitude": "-2.086930",
        "nid": "847",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "647.4",
        "marker": "City.png",
        "tohere": "5.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villares de \u00d3rbigo",
        "latitude": "42.471116",
        "longitude": "-5.913177",
        "nid": "921",
        "nodetype": "City",
        "body": "The church here is dedicated to St. James. So too is the church in Santiba\u00f1es. Both are worth a peek for their representations of Santiago.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "tosantiago": "271.2",
        "marker": "City.png",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Pena",
        "latitude": "42.785413",
        "longitude": "-7.542627",
        "nid": "993",
        "nodetype": "City",
        "body": "Not much in town apart from the well-kept albergue\/bar Casa do Rego.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "993-133861.webp",
        "tosantiago": "99.4",
        "marker": "City.png",
        "tohere": "0.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villar de Mazarife",
        "latitude": "42.483048",
        "longitude": "-5.728598",
        "nid": "2635",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "2635-135055.webp",
        "tosantiago": "287.5",
        "marker": "City.png",
        "tohere": "4.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ventosa",
        "latitude": "42.403749",
        "longitude": "-2.626505",
        "nid": "855",
        "nodetype": "City",
        "body": "A causa dei cambiamenti degli ultimi tempi Ventosa \u00e8 finita fuori dalla rotta del cammino; le frecce gialle sono state infatti riposizionate per accorciare il percorso di 800m. A Ventosa ci sono almeno un bar e un albergue.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/ventosa-html?aid=1499405",
        "tosantiago": "587.1",
        "marker": "City.png",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Foncebad\u00f3n",
        "latitude": "42.490692",
        "longitude": "-6.339240",
        "nid": "929",
        "nodetype": "City",
        "body": "Various guidebooks (and a few novels) have given Foncebad\u00f3n a reputation for being the home of rabid dogs eager for a tasty pilgrim snack. While this may once have been true, it no longer is. \nIn fact the mountain outpost has experienced something of a renaissance. In between utterly ruinous piles of stone are a smattering of lovingly restored old homes, most of them converted into accommodation for pilgrims. \nThis elevation is typically shrouded in fog or buried in deep snow; both have their charm.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "929-133840.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/foncebadon.html?aid=1499405",
        "tosantiago": "231.7",
        "marker": "City.png",
        "tohere": "5.20",
        "barrest": "Yes",
        "road": "The Cruz de Fierro, or Iron Cross, is a steady climb ahead. If you stopped at the road-side bar be sure to get back to the camino and avoid following the cyclists as they take the road.\n\n"
    }
}, {
    "node": {
        "title": "Santo Domingo de la Calzada",
        "latitude": "42.441986",
        "longitude": "-2.952662",
        "nid": "860",
        "nodetype": "City",
        "atm": "Yes",
        "body": "The highlight of the town is the Catedral de Santo Domingo de la Calzada and its gothic henhouse.  Santo Domingo is buried there as well, fittingly, in the middle of the road. The only remaining tower (of the original three) is detached and is located across the street.  This is a rarity in Spain, particularly for a building of such importance, and the location was selected as the nearest place where a suitable foundation could be built. Tours are available.\n\n",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "860-133897.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/santo-domingo-de-la-calzada.html?aid=1499405",
        "tosantiago": "556.3",
        "marker": "City.png",
        "tohere": "5.90",
        "barrest": "Yes",
        "road": "Leaving town you cross over the mostly dried up R\u00edo Oja. The bridge is a rebuilt version of Domingo\u2019s original. \n"
    }
}, {
    "node": {
        "title": "Campo",
        "latitude": "42.537683",
        "longitude": "-6.561198",
        "nid": "934",
        "nodetype": "City",
        "body": "Campo was once the Jewish quarter of Ponferrada.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "208.3",
        "marker": "City.png",
        "tohere": "4.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "A Prebisa",
        "latitude": "42.852518",
        "longitude": "-7.770167",
        "nid": "1006",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "76.9",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Alto de San Roque",
        "latitude": "42.698523",
        "longitude": "-7.084229",
        "nid": "3042",
        "nodetype": "Alto",
        "tosantiago": "148.8",
        "marker": "Alto.png",
        "tohere": "0.70",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Villambistia",
        "latitude": "42.406347",
        "longitude": "-3.262167",
        "nid": "868",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "527",
        "marker": "City.png",
        "tohere": "2",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villafranca del Bierzo",
        "latitude": "42.607527",
        "longitude": "-6.807833",
        "nid": "942",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Villafranca del Bierzo is one of the prettiest towns along the camino in El Bierzo. It is full of narrow streets that twist and turn and dare you to not get lost.\n\nLocated along the Burbia and Valcarce rivers it can be difficult to navigate and steep climbs seem to be obligatory no matter the destination.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "942-133848.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villafranca-del-bierzo.html?aid=1499405",
        "tosantiago": "180.9",
        "marker": "City.png",
        "tohere": "4.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Brea",
        "latitude": "42.875814",
        "longitude": "-7.836133",
        "nid": "1014",
        "nodetype": "City",
        "body": "Brea is Gallego for road, and is a very common name for a small village.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "70.2",
        "marker": "City.png",
        "tohere": "2.10",
        "barrest": "Yes",
        "road": "From here the camino passes through more forests and nears the road at Rosario.\n\n"
    }
}, {
    "node": {
        "title": "Pradela (Bierzo)",
        "latitude": "42.659385",
        "longitude": "-6.865168",
        "nid": "7029",
        "nodetype": "City",
        "body": "The bar and albergue are both in the town of Pradela, which sits just off the camino before it descends towards Trabadelo. You do not need to go into town unless you are after a drink or a bed. Otherwise follow the arrows to keep along the camino.  The forests here are thick with chestnut trees. If you see piles of prickly looking spheres at your feet, look out from above.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "173.6",
        "marker": "City.png",
        "barrest": "Yes",
        "road": "Pradela sits on the hillside just above Trabadelo. The descent along the trail is quick and will deliver you into a side street half way into town; it is also possible to follow the road for a longer but less steep journey.\n\n"
    }
}, {
    "node": {
        "title": "Orbaneja R\u00edopico",
        "latitude": "42.360225",
        "longitude": "-3.583689",
        "nid": "876",
        "nodetype": "City",
        "body": "Neither of the two towns along the R\u00edo Pico is remarkable for much. Pay attention when leaving the latter; shortly outside of town you will be faced with the first decision regarding how to enter Burgos.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "495.7",
        "marker": "City.png",
        "tohere": "2.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Hospital (Ingl\u00e9s)",
        "latitude": "42.672718",
        "longitude": "-6.990566",
        "nid": "950",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "159.9",
        "marker": "City.png",
        "tohere": "1",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Coto",
        "latitude": "42.884864",
        "longitude": "-7.958865",
        "nid": "1023",
        "nodetype": "City",
        "body": "With several bars and one casa rural, Coto sits along a small stretch of the old road.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "1023-134733.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/o-coto.html?aid=1499405",
        "tosantiago": "58.7",
        "marker": "City.png",
        "tohere": "0.80",
        "barrest": "Yes, 2"
    }
}, {
    "node": {
        "title": "\u200cDetour: Over the bridge into Larrosoa\u00f1a or remain on the camino",
        "latitude": "42.900677",
        "longitude": "-1.540537",
        "nid": "7413",
        "nodetype": "POI",
        "body": "Going into Larrosoa\u00f1a is not strictly necessary unless you are staying at the albergue or dropping in for food. If you have arrived here via the road from Zubiri and are eager to return to the trail, just pop over the bridge and turn to your right.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "Castrojeriz",
        "latitude": "42.288294",
        "longitude": "-4.138927",
        "nid": "886",
        "nodetype": "City",
        "atm": "Yes",
        "body": "The camino-road layout of Castrojeriz is deceptively long and unlike some of the smaller examples has spread to encompass several parallel streets. All three of its churches are worth a visit, and a hike to the ruins of the castle provide a view of the surrounding land that is paralleled only by the climb out of town.\n\nMost pilgrim related services can be found along the Calle Oriente, but the pharmacy and the larger groceries are closer to the main road.\n\nAlso along the Oriente is the Hospital del Alma; part art gallery and part meditation space and exactly where you want to go if you seek a bit a peace and quite.\n\nIn the main square is a small pilgrim shop, one of the last of its kind. The owner is quite a character and has managed to keep up with the times, trading the usual small village wares for the type of high-tech gear that modern pilgrims are looking for.\n\nOn the way out of town is the Bar Lagar, named for the well-preserved grape press sitting in the center. Check it out for a glimpse of how things were once done.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "886-133788.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/castrogeriz.html?aid=1499405",
        "tosantiago": "443.7",
        "marker": "City.png",
        "tohere": "3.50",
        "barrest": "Yes",
        "road": "The road ahead climbs up and over the Alto de Mostelares. There is not much shade to be had between here and the crossing into Palencia.\n\n"
    }
}, {
    "node": {
        "title": "Melide on the Camino Primitivo",
        "latitude": "42.913891",
        "longitude": "-8.014595",
        "nid": "12692",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Although it has been on the menu as far back as Fonsagrada, Pulpo (octopus) doesn\u2019t seem to garnish much attention until Melide. Despite its apparent disconnect with the sea, Melide\u2019s thriving Thursday market meant that shipping pulpo was a profitable enterprise. It is served today as it was then: on a wooden plate, garnished only with a healthy drizzle of olive oil and a shake of paprika. It is eaten with a rather crude looking toothpick, alongside heavy Galician bread and a bowl of the local Ribeiro wine.\n\nThe melindre is another of Melide\u2019s favorite foods. It resembles a glazed donut and is sold from dozens of identical booths during festivals.\n\n\nMelide, long the crossroads between territories, is also the meeting point of the various camino routes which come from the north and the Camino Franc\u00e9s from the east. Because of this, and the proximity to Santiago (within the minimum 100km), the road becomes a great deal more congested.",
        "bus": "Yes",
        "correos": "Obispo Varela 20, 15800, Mon-Fri: 0830-1430 & Sat: 0930-1300, 981 505 866",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/melide.html?aid=1499405",
        "tosantiago": "52.7",
        "marker": "City.png",
        "tohere": "1.50",
        "barrest": "Yes",
        "road": "The traffic through Melide can be dangerous, particularly on market days when booths line the crowded streets and the arrows through town become difficult to spot. Arrows direct you towards a small side street through the old part of town that parallels the main road. There are many other yellow arrows that direct you towards the many albergues in town, they are often attached to adverts or are painted alongside a simple \u2018A\u2019. These can be ignored."
    }
}, {
    "node": {
        "title": "Biduedo",
        "latitude": "42.744201",
        "longitude": "-7.178307",
        "nid": "958",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "958-133855.webp",
        "tosantiago": "138.4",
        "marker": "City.png",
        "tohere": "2.80",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Ribadiso da Baixo",
        "latitude": "42.930683",
        "longitude": "-8.130634",
        "nid": "1032",
        "nodetype": "City",
        "body": "A bar near the river with a large patio for celebrating a successful walk. The river here is quite cold and the perfect place to dip your feet.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "41.6",
        "marker": "City.png",
        "tohere": "2.50",
        "barrest": "Yes",
        "road": "The camino to Arz\u00faa begins with a steady walk uphill. By the time you reach the road it levels out and the remaining kilometers are flat into town.\n\n"
    }
}, {
    "node": {
        "title": "Bizkarreta [Gerendiain]",
        "latitude": "42.967758",
        "longitude": "-1.417593",
        "nid": "819",
        "nodetype": "City",
        "body": "There is a bar at the start of town which provides decent service. Another, Bar Juan, is in the square. The supermarket is at the far end of town.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "819-134837.webp",
        "tosantiago": "731.2",
        "marker": "City.png",
        "tohere": "5.20",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Villalc\u00e1zar de Sirga",
        "latitude": "42.316861",
        "longitude": "-4.543319",
        "nid": "894",
        "nodetype": "City",
        "body": "The Iglesia de Santa Mar\u00eda la Blanca dominates the hillside of Vill\u00e1lcazar. Its appearance, that of a church\/fortress, can be attributed to having belonged to the Knights Templar. King Alfonso X claimed that the White Virgin here had cured pilgrims, she is still there if you are in need of a miracle.\n\n",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villalcazar-de-sirga.html?aid=1499405",
        "tosantiago": "406.1",
        "marker": "City.png",
        "tohere": "4",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Freituxe",
        "latitude": "42.742238",
        "longitude": "-7.307959",
        "nid": "966",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "128.6",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Boavista",
        "latitude": "42.922303",
        "longitude": "-8.258629",
        "nid": "1041",
        "nodetype": "City",
        "bus": "Yes",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "29.4",
        "marker": "City.png",
        "tohere": "1.40",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Fuente de Vino",
        "latitude": "42.650915",
        "longitude": "-2.043607",
        "nid": "7429",
        "nodetype": "POI",
        "body": "Sponsored by the nearby Bodegas Irache, there is a fountain here which pours both water and wine. \nThe plaque on the wall reads: \u201cIf you want to go to Santiago with strength and vitality, of this great wine have a drink and toast to happiness.\u201d Greatness is subjective.\n\nAnother sign reminds you not to abuse their generosity.\n\n",
        "image": "7429-133883.webp",
        "tosantiago": "652.1",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Fountain.png"
    }
}, {
    "node": {
        "title": "Irotz",
        "latitude": "42.861185",
        "longitude": "-1.571546",
        "nid": "827",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "709.7",
        "marker": "City.png",
        "tohere": "2.20",
        "barrest": "",
        "road": "Between here and Zabaldika the camino splits, and while one path is not technically the camino it can be misleading. The camino proper heads towards Zabaldika, while the \u201cRiver Walk\u201d turns left after the bridge and follows the river into Pamplona; while this sounds nice it is, in fact, a much longer path due to the very not-straight nature of the river.\n\n"
    }
}, {
    "node": {
        "title": "Calzada de Coto",
        "latitude": "42.387680",
        "longitude": "-5.080748",
        "nid": "902",
        "nodetype": "City",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "902-134996.webp",
        "tosantiago": "356.7",
        "marker": "City.png",
        "barrest": "Yes",
        "road": "The two stretches of road between here and Reliegos are without any services outside of those to be found in Calzadilla de los Hermanillos. Be sure to carry enough water.\n\n"
    }
}, {
    "node": {
        "title": "Aguiada",
        "latitude": "42.773651",
        "longitude": "-7.361924",
        "nid": "974",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "118.3",
        "marker": "City.png",
        "tohere": "0.50",
        "barrest": "Yes",
        "road": "The remainder of the camino to Sarria is along the footpath adjacent to the road.\n\n"
    }
}, {
    "node": {
        "title": "San Ant\u00f3n",
        "latitude": "42.906204",
        "longitude": "-8.371338",
        "nid": "1050",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "18.7",
        "marker": "City.png",
        "tohere": "0.70",
        "barrest": ""
    }
}, {
    "node": {
        "title": "How to get out of Ponferrada",
        "latitude": "42.543137",
        "longitude": "-6.593492",
        "nid": "7470",
        "nodetype": "POI",
        "body": "Leaving Ponferrada, the camino passes along side the Castle and up to the Plaza de la Encina. Arrows will take you out of the square and over the river that you will follow North out of town via the main boulevard that runs parallel to it.\u00a0From here onward you will pass though a series of towns which have grown into one another. The first of these is Compostilla, with American style suburbs.\n\n",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Notice.png"
    }
}, {
    "node": {
        "title": "San Xuli\u00e1n de Teibilide",
        "latitude": "42.734433",
        "longitude": "-7.375871",
        "nid": "7630",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Zariquiegui",
        "latitude": "42.748217",
        "longitude": "-1.722802",
        "nid": "836",
        "nodetype": "City",
        "body": "From here the climb is steady to the Alto de Perd\u00f3n. If you have watched Martin Sheen\u2019s \u201cThe Way\u201d you may recognize the front of the Iglesia de San Andres as the setting for one of the movie\u2019s earlier scenes.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "image": "836-134855.webp",
        "tosantiago": "689.5",
        "marker": "City.png",
        "tohere": "6.10",
        "barrest": "Yes, in albergue and open early"
    }
}, {
    "node": {
        "title": "Valdelafuente",
        "latitude": "42.569391",
        "longitude": "-5.513463",
        "nid": "910",
        "nodetype": "City",
        "body": "Valdelafuente is not directly on the camino, but rather a few hundred meters off.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "910-133765.webp",
        "tosantiago": "312.6",
        "marker": "City.png",
        "tohere": "1.30",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Rente",
        "latitude": "42.768050",
        "longitude": "-7.459010",
        "nid": "982",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "108.4",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Santiago de Compostela",
        "latitude": "42.880430",
        "longitude": "-8.546333",
        "nid": "1058",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Welcome to Santiago! There are a tremendous amount of things to see and things to do in Santiago de Compostela; you are encouraged to stay for at least one full day extra for exploring the web of streets, all of which seem to bring you back to the Cathedral.\n\n\nYour pilgrim related business is likely to start in front of the cathedral, kilometer zero. A shell and plaque mark the spot in the center of Plaza Obradoiro (see below).\n\n\nIf you are interested in receiving your Compostela, the certificate of completion, you will need to visit the Pilgrim\u2019s Office, which was relocated in 2016 to a bright new building. To get there from the Plaza Obradoiro, face the Parador (the hotel on your left if you are facing the Cathedral) and look for the road that goes downhill to the left. Halfway down you pass the public restroom, and at the next street turn right. The office is at the end of that road and is easy enough to find. Note that there are few arrows indicating the way.\n\n\nThe Cathedral is the single largest attraction to Santiago and for good reason. Both inside and out it presents countless treasures to investigate, too many to list in fact but below are the best.\n\n\nThe Cathedral - Plaza by plaza\n1. Plaza de Inmaculada, a.k.a. Azabache: As you enter the city, the first part of the Cathedral that you pass is the Puerta de la Azabacher\u00eda. This is the entrance that faces the Monastery of San Mart\u00edn Pi\u00f1ario.\n\n\n2: Obradoiro: From Azabache you pass under the Palace of the Bishop which is adjoined to the Cathedral and cannot possibly be the sort of palace that affords much peaceful sleeping; the sound of bagpipes welcoming you can be heard from dawn to dusk. The stairway leads directly to the Plaza de Obradoiro and kilometer zero for pilgrims. In the center of the plaza is the last scallop shell and you are likely to find pilgrims taking their shoes off for a photo with it, and the Obradoiro Facade behind them.\n\nThis facade is the most majestic and most photographed of the Cathedral and was part of the 18th century building projects that took place in Santiago. The baroque design will keep your eyes moving and the massive amounts of glass allow for the illumination of the P\u00f3rtico de la Gloria that lies behind it. That P\u00f3rtico was the original front to the church designed by Maestro Mateo 600 years before the new facade.\n\n\n3. Plaza Platerias: If you continue around the Cathedral you arrive at the Puerta de las Plater\u00edas (named for the silver craft that still exists in the shops below it). You will notice that some of the stonework stands out as a different material. These are replacement carvings, the originals were damaged and subsequently moved to the Cathedral Museum; and unfortunately the original composition was forgotten, leaving a somewhat nonsensical layout. In front of the doors are a set of stairs and the Plater\u00edas fountain, the usual meeting point for pilgrims commonly referred to as \u201cthe horse fountain\u201d.\n\n\n4: Plaza de Quintana: Continuing up the stairs and around the Cathedral we arrive in the large Plaza de Quintana and the Puerta de Perd\u00f3n. The actual Holy Door is behind this facade (which is not actually a structural part of the Cathedral, it is more like a highly decorated wall around the Holy Door itself). The carvings here are impressive and depict 24 Saints and prophets.\n\n\nIn medieval times it was common for pilgrims to spend the night in the Cathedral, sleeping on the stone floors and fighting (to the death on a few occasions) for the privilege of sleeping close to their chapel of choice.\n\n\nThe best time to visit is early in the morning before the crowds arrive, when paying a visit to the crypt and hugging the bust of Santiago can be done quietly and with a bit of contemplation.\n\n\nThe botafumeiro, quite possibly the largest thurible in the Catholic Church, is swung across the transept (from north to south) by a group of men called the tiraboleiros. It has only come loose from the ropes twice, and never in modern times. At the time that this book was printed, the tradition of swinging it during the Friday evening mass had been canceled. Inquire at the pilgrim\u2019s office for more information.\n\nThe Monastery and Museum of San Mart\u00edn Pi\u00f1ario\nThe enormity of this Monastery is difficult to comprehend, but if you pay close attention to this building as you walk around Santiago you will find that you are almost always standing next to it if you are on the north side of the Cathedral. There are three cloisters! The facade of the church often feels like it is somewhere else entirely and is quite curious for the fact that you must descend the staircase to get to the doors, rather than the other way around. The reason for this was a decree by the Archbishop that no building should exceed in elevation that of the Cathedral; the architects did not compromise by redesigning San Mart\u00edn to be less tall, they simply dug down and started at a lower point.\n\n\nSan Fiz de Solovio\nCompared to the two churches above, San Fiz feels like an almost minuscule affair. To find it, make your way to the Mercado de Abastos (Supply Market). San Pelayo (the hermit that rediscovered the bones of Santiago) was praying here when the lights called him. Grand and majestic it is not, but the oldest building site in Santiago it certainly is. The church that exists today is not the original, but excavations have revealed the foundations and necropolis dating to the 6th century.\n\n\nThe Supply Market (Mercado de Abastos)\nThe produce market is a great place to wander for lunch. Compared to other markets in Spain (like those in Madrid and Barcelona) the Santiago market is a fairly solemn affair. In fact, the architecture appears almost strictly utilitarian and is as Galician as it gets. The vendors make the experience, and even if your Spanish is not up to par, it is worth the visit for a glimpse into the way the locals go about their most ordinary business.\n\n\nThe buildings you see today date from the early 1940\u2019s but replace ones that stood for 300 years. In fact, many of the vendors are second, third, or fifth generation market operators.\n\n\nAlameda Park \nAlameda Park was once the sort of place where the people of Santiago would turn out for elaborate displays of personal wealth and stature; the various paths that cut through and around the park were only to be used by members of a certain class. Nowadays it is far more democratic. The park is the site of a Ferris wheel and feria during the Summer months, an ice skating rink during the Winter holidays, and a massive eucalyptus tree overlooking the Cathedral year round.\n\n\nCasa De La Troya \nThe Troya House is the inspiration for and setting of one of the most celebrated novels in Spanish literature; in which a young man from Madrid is forced by his father to finish his Law studies in Santiago\u2026 a tale of misery and eventually love. It was once a boarding house for students and the museum that exists there today is an exhibit of what the house would have looked like at the time the novel was written. The Tuna, those wonderful musicians that perform in the Plaza Obradoiro every night, would have lived and performed here as well.\n\n\nThe Hidden Pilgrim \nHiding in the shadows cast by the Cathedral, in the Plaza Quintana, is the hidden pilgrim. He is only visible at night and might take a while to discover.\n\n\nAnd lastly, there are the many other Monasteries, and while it would be a challenge to visit all of them it is important to realize their construction shaped the city that we see today. Taking the time to walk between them will reveal countless little treasures.",
        "bus": "Yes",
        "busterminal": "Yes",
        "correos": "Rua do Franco 4, 15702, Mon-Fri: 0830-0230 & Sat: 0930-1300, 981 581 252",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "train": "Yes",
        "image": "1058-556_0.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/santiago-de-compostela.html?aid=1499405",
        "marker": "City.png",
        "hospital": "Yes",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Puerto de Iba\u00f1eta",
        "latitude": "43.019800",
        "longitude": "-1.323800",
        "nid": "7490",
        "nodetype": "Alto",
        "body": "Commonly referred to as Roncesvalles Pass and home to the modern Ermita de San Salvador de Iba\u00f1eta. The pass has existed as an important crossing point since Roman times and was one of the principal crossings into the Iberian peninsula. From here it is all downhill into Roncesvalles, 1.6km.\n\n",
        "image": "7490-60988.webp",
        "tosantiago": "744.3",
        "marker": "Alto.png",
        "tohere": "7.30",
        "barrest": ""
    }
}, {
    "node": {
        "title": "A Balsa",
        "latitude": "42.768414",
        "longitude": "-7.256102",
        "nid": "7640",
        "nodetype": "City",
        "body": "You will notice very quickly, through both smell and sight, that dairy in these parts is the primary trade. The light brown color gives them their name, the Rubia Gallega; though you will also see an imported imposter, the Dutch Holstein.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "129.7",
        "marker": "City.png",
        "tohere": "0.40",
        "barrest": "",
        "road": "Between here and San Xil you will pass a large fountain with a large scallop shell with a lot of graffiti and algae. Beyond that by two more kilometers is the Alto de Riocabo.\n\nYou will also pass through San Xil de Carballo, with no services.\n\n"
    }
}, {
    "node": {
        "title": "Villatuerta",
        "latitude": "42.659401",
        "longitude": "-1.993976",
        "nid": "844",
        "nodetype": "City",
        "atm": "Yes",
        "body": "You will pass through a brief section of modern rural expansion before crossing the R\u00edo Iranzu by way of a double arched Romanesque bridge to enter the old town.\n\nDo pay a visit to the Iglesia de Nuestra Se\u00f1ora de la Asunci\u00f3n.\n\n",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "844-133395.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/villatuerta.html?aid=1499405",
        "tosantiago": "658.6",
        "marker": "City.png",
        "tohere": "4.80",
        "barrest": "Yes, in polideportivo and in town."
    }
}, {
    "node": {
        "title": "San Mart\u00edn del Camino",
        "latitude": "42.494884",
        "longitude": "-5.810823",
        "nid": "918",
        "nodetype": "City",
        "atm": "Yes",
        "bus": "Yes",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/san-martin-del-camino.html?aid=1499405",
        "tosantiago": "280.9",
        "marker": "City.png",
        "tohere": "4.50",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Morgade",
        "latitude": "42.782012",
        "longitude": "-7.521225",
        "nid": "990",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "101.6",
        "marker": "City.png",
        "tohere": "6.10",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Chozas de Abajo",
        "latitude": "42.507065",
        "longitude": "-5.686927",
        "nid": "2632",
        "nodetype": "City",
        "body": "One bar, located 50m off the camino to the left and well signed in both directions.\n\n",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "291.6",
        "marker": "City.png",
        "tohere": "5.70",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Roman Bridge",
        "latitude": "42.677251",
        "longitude": "-1.894981",
        "nid": "7653",
        "nodetype": "POI",
        "body": "13th. c.\n\n",
        "image": "7653-133875.webp",
        "marker": "POI.png",
        "barrest": "",
        "poimarker": "Bridge.png"
    }
}, {
    "node": {
        "title": "Viana",
        "latitude": "42.515132",
        "longitude": "-2.371588",
        "nid": "852",
        "nodetype": "City",
        "atm": "Yes",
        "body": "Viana is a thriving city that serves as a good stopping point. A pair of albergues, a few churches, the occasional closing of the city streets to run with the bulls, and a handful of good restaurants offer a break from walking.\n\nThere is a nice green park, small but a great place to rest, on the west end of the town near the albergue and church ruins. It also offers great views of the plains to the west and the mountains to the north.\n\nThe restaurant Armendariz has a sidreria in the basement, where you can serve your own cider.  To do so is a two person job whereby you position your glass near a bucket in the center of the room, and the person seated nearest the spigot in the wall lets flow an arc of cider on a trajectory towards said bucket.\n\n",
        "bus": "Yes",
        "correos": "Plaza del Coso 17,  31100, Mon-Fri: 0830-1430 & Sat: 0930-1300, 948 645 878",
        "supermarket": "Yes",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "pharmacy": "Yes",
        "image": "852-252.webp",
        "bookingURL": "http:\/\/m.booking.com\/city\/es\/viana.html?aid=1499405",
        "tosantiago": "615.4",
        "marker": "City.png",
        "tohere": "10.50",
        "barrest": "Yes",
        "road": "Not long after leaving Viana you will have left Navarra behind and entered into La Rioja. The way into Logro\u00f1o, its capital, is without much heavy industry.\n\n"
    }
}, {
    "node": {
        "title": "Santa Catalina de Somoza",
        "latitude": "42.454621",
        "longitude": "-6.158738",
        "nid": "926",
        "nodetype": "City",
        "body": "The town is the only one on the whole of the Camino Franc\u00e9s to be named after a female Saint (St. Catherine of Alexandria). A relic of St. Blaise is kept in the parish church, and the town was known to have a pilgrim hospital. The \u2018Somoza\u2019 part of the name is a simple description of its geographical position at the foot of the hill. The hill, of course, lies ahead.\n\n",
        "hasshared": "\n        \n  \n  \n      \n        \n      \n          yes    \n    \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "247.8",
        "marker": "City.png",
        "tohere": "4.60",
        "barrest": "Yes"
    }
}, {
    "node": {
        "title": "Parrocha",
        "latitude": "42.793930",
        "longitude": "-7.587825",
        "nid": "998",
        "nodetype": "City",
        "hasshared": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hasprivate": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "hascamping": "\n        \n  \n  \n  \n  \n  \n  \n  \n  \n",
        "tosantiago": "95",
        "marker": "City.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Alto de la Grajera",
        "latitude": "42.439967",
        "longitude": "-2.524436",
        "nid": "3034",
        "nodetype": "Alto",
        "body": "The Alto de la Grajera is part of the park and embalse (reservoir) on the outskirts of Logro\u00f1o. It is an excellent place to walk through slowly, perhaps to stop in and enjoy some nature.\n\n\nThe dam here, and the resulting ecosystem, was constructed in 1883 as a way to store water for irrigation. Since then the park has grown in size and importance and is one of the few wetlands that exist in La Rioja. Anyone with a keen interest in bird watching will especially appreciate the protected areas which are home to several species of birds.\n\n\nBe advised that the embalse is strictly off limits, so no pilgrim bathing should be attempted.",
        "image": "3034-133890.webp",
        "tosantiago": "596.7",
        "marker": "Alto.png",
        "barrest": ""
    }
}, {
    "node": {
        "title": "Hotel des Pyr\u00e9n\u00e9es",
        "latitude": "43.163246",
        "longitude": "-1.237579",
        "nid": "11652",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/les-pyrenehtml?aid=1499405",
        "marker": "Hotel.png",
        "image": "11652-135290.webp",
        "email": "hotel.pyrenees@wanadoo.fr",
        "street": "Charles de Gaulle, 19",
        "phone": "0033 0559 370 101"
    }
}, {
    "node": {
        "title": "Refuge Esponda",
        "latitude": "43.164238",
        "longitude": "-1.236262",
        "nid": "7558",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "14 \u20ac",
        "beds": "14",
        "reserve": "Yes",
        "marker": "Private.png",
        "email": "jeanmarie.mailharro@wanadoo.fr",
        "street": "Trinquet, 9",
        "phone": "0033 679 075 252"
    }
}, {
    "node": {
        "title": "Albergue Accueil Pelerin",
        "latitude": "43.163254",
        "longitude": "-1.236215",
        "nid": "7533",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "13*\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "7533-60975.webp",
        "email": "contact@aubergedupelerin.com",
        "street": "Citadelle, 25",
        "phone": "0033 559 49 10 86 , 0033 689 31 96 10"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos de Saint Jean Pied de Port",
        "latitude": "43.163450",
        "longitude": "-1.235934",
        "nid": "12214",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "8\u20ac",
        "beds": "32",
        "bike": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "street": "Citadelle, 55",
        "phone": "00 33 05 59 37 05 09"
    }
}, {
    "node": {
        "title": "Hotel des Remparts",
        "latitude": "43.162504",
        "longitude": "-1.239277",
        "nid": "11649",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/des-remparts-saint-jean-pied-de-port.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11649-135293.webp",
        "email": "remparts.hotel@wanadoo.fr",
        "street": "Floquet, 16",
        "phone": "0033 0559 371 379"
    }
}, {
    "node": {
        "title": "Albergue Le Chemin vers L Etoile",
        "latitude": "43.161693",
        "longitude": "-1.237569",
        "nid": "7535",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "17*\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "14:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "7535-60972.webp",
        "email": "eric.voitte@gmail.com",
        "street": "Espagne, 21",
        "phone": "0033 559 372 071"
    }
}, {
    "node": {
        "title": "Hotel Ramuntcho",
        "latitude": "43.163174",
        "longitude": "-1.236512",
        "nid": "11651",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/logis-ramuntcho.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11651-135289.webp",
        "street": "France, 1",
        "phone": "0033 0559 370 391"
    }
}, {
    "node": {
        "title": "Albergue Azkorria",
        "latitude": "43.163555",
        "longitude": "-1.235200",
        "nid": "7538",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "29\u20ac",
        "beds": "8",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/gite-azkorria.html?aid=1499405",
        "marker": "Private.png",
        "image": "7538-135292.webp",
        "street": "Citadelle, 50",
        "phone": "0033 559 370 053"
    }
}, {
    "node": {
        "title": "La Villa Esponda",
        "latitude": "43.164016",
        "longitude": "-1.236311",
        "nid": "11653",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/la-villa-esponda.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11653-135291.webp",
        "street": "Trinquet, 9"
    }
}, {
    "node": {
        "title": "Camping Municipal Plaza Berri",
        "latitude": "43.161328",
        "longitude": "-1.235848",
        "nid": "7559",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "price": "10 \/ tent\u20ac",
        "beds": "50",
        "reserve": "Yes",
        "marker": "Camping.png",
        "email": "mairie.stjeanpieddeport@wanadoo.fr",
        "street": "Fronton",
        "phone": "0033 559 370 092"
    }
}, {
    "node": {
        "title": "Albergue Beilari",
        "latitude": "43.163307",
        "longitude": "-1.236048",
        "nid": "7534",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "30**\u20ac",
        "beds": "18",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - SEPT",
        "open": "6:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "7534-134821.webp",
        "email": "info@beilari.info",
        "street": "Citadelle, 40",
        "phone": "0033 559 372 468",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Zuharpeta",
        "latitude": "43.160942",
        "longitude": "-1.238680",
        "nid": "12215",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "13\u20ac",
        "beds": "22",
        "wifi": "Yes",
        "cal": "½ MAR - ½ OCT",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "gitezuharpeta@laposte.net",
        "street": "Zuharpeta, 5",
        "phone": "00 33 05 59 37 35 88"
    }
}, {
    "node": {
        "title": "Maison d\u2019H\u00f4tel Itzalpea",
        "latitude": "43.163931",
        "longitude": "-1.236783",
        "nid": "11650",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/itzalpea.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11650-135288.webp",
        "street": "Trinquet, 5",
        "phone": "0033 0559 370 366"
    }
}, {
    "node": {
        "title": "Gite Ultreia",
        "latitude": "43.162744",
        "longitude": "-1.237046",
        "nid": "7536",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "16\/17\u20ac",
        "beds": "15",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - ½ OCT",
        "open": "16:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "7536-60973.webp",
        "email": "dodo.ultreia@gmail.com",
        "street": "Rue de la Citadelle, 8",
        "phone": "0033 680 884 622"
    }
}, {
    "node": {
        "title": "Refuge Orisson",
        "latitude": "43.108785",
        "longitude": "-1.239175",
        "nid": "7539",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "35**\u20ac",
        "beds": "18",
        "barrest": "Yes, as well as sandwiches to take away",
        "bike": "Yes",
        "close": "None",
        "cal": "APR - OCT",
        "open": "12:00",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "7539-60984.webp",
        "email": "refuge.orisson@wanadoo.fr",
        "street": "Uhart-Cize",
        "phone": "0033 0559 491 303"
    }
}, {
    "node": {
        "title": "Kayola",
        "latitude": "43.117833",
        "longitude": "-1.238226",
        "nid": "8028",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "beds": "15",
        "bike": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "marker": "Private.png",
        "image": "8028-60993.webp",
        "email": "refuge.orisson@wanadoo.fr",
        "street": "Uhart-Cize",
        "phone": "0033 06 81 49 79 56, 0033 05 59 49 13 03"
    }
}, {
    "node": {
        "title": "Albergue Ferme Ithurburia",
        "latitude": "43.124205",
        "longitude": "-1.244872",
        "nid": "7537",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "14\u20ac",
        "beds": "22",
        "barrest": "Yes",
        "bike": "Yes",
        "cal": "ALL YEAR",
        "open": "15:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/ferme-ithurburia.html?aid=1499405",
        "marker": "Private.png",
        "image": "7537-134824.webp",
        "email": "jeanne.ourtiague@orange.fr",
        "phone": "0033 055 9371 117"
    }
}, {
    "node": {
        "title": "Hotel Le Clementenia",
        "latitude": "43.110135",
        "longitude": "-1.280277",
        "nid": "12121",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/fr\/le-clementenia.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "12121-136462.webp",
        "email": "ehotelleclementenia@sfr.fr",
        "street": "Le bourg D933",
        "phone": "0033 5 24 34 10 06"
    }
}, {
    "node": {
        "title": "Casa Rural Etxezuria",
        "latitude": "43.094314",
        "longitude": "-1.298929",
        "nid": "12124",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "casaetxezuria@yahoo.es",
        "street": "Elizaldea, 60",
        "phone": "948 790 011, 609 436 190"
    }
}, {
    "node": {
        "title": "Albergue municipal de Valcarlos",
        "latitude": "43.093774",
        "longitude": "-1.300709",
        "nid": "11861",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "10*\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "email": "luzaide-valcarlos@wanadoo.es",
        "street": "Elizaldea, 52",
        "phone": "948 790 117, 948 790 199"
    }
}, {
    "node": {
        "title": "Hostal Maitena",
        "latitude": "43.091593",
        "longitude": "-1.302926",
        "nid": "12123",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Elizaldea",
        "phone": "948 790 210"
    }
}, {
    "node": {
        "title": "Casa Rural Erlanio",
        "latitude": "43.095122",
        "longitude": "-1.300153",
        "nid": "12122",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "erlanisa@hotmail.es ",
        "street": "Elizaldea, 58",
        "phone": "948 790 218, 669 651 266"
    }
}, {
    "node": {
        "title": "Casa Sabina",
        "latitude": "43.009354",
        "longitude": "-1.320607",
        "nid": "11654",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Roncesvalles, 18",
        "phone": "948 760 012"
    }
}, {
    "node": {
        "title": "Apartamentos Casa de los Beneficiados",
        "latitude": "43.009838",
        "longitude": "-1.320309",
        "nid": "11656",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-de-los-beneficiados.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11656-135294.webp",
        "email": "info@casadebeneficiados.com",
        "street": "Roncesvalles",
        "phone": "948 760 105"
    }
}, {
    "node": {
        "title": "Hotel Roncesvalles",
        "latitude": "43.009857",
        "longitude": "-1.320087",
        "nid": "10023",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/roncesvallhtml?aid=1499405",
        "marker": "Hotel.png",
        "image": "10023-134830.webp",
        "email": "info@hotelroncesvalles.com",
        "street": "Roncesvalles, 14",
        "phone": "948 760 105"
    }
}, {
    "node": {
        "title": "Hostal Posada de Roncesvalles",
        "latitude": "43.008380",
        "longitude": "-1.319400",
        "nid": "11655",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "posada@laposadaderoncesvalles.com",
        "street": "Roncesvalles",
        "phone": "948 760 225"
    }
}, {
    "node": {
        "title": "Refugio de Peregrinos de Roncesvalles - Itzandegia",
        "latitude": "43.010384",
        "longitude": "-1.319290",
        "nid": "1084",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "12\u20ac",
        "beds": "183",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computers, as well as the nearby bars",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes, by email only and with a week notice at least",
        "stove": "Yes",
        "washer": "Yes, 2.7\u20ac",
        "marker": "Parochial.png",
        "image": "1084-134833.webp",
        "email": "info@alberguederoncesvalles.com",
        "street": "Real Colegiata de Roncesvalles",
        "phone": "948 760 000"
    }
}, {
    "node": {
        "title": "Hotel Rural Loizu",
        "latitude": "42.987552",
        "longitude": "-1.336932",
        "nid": "11658",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/rural-loizu.html?aid=1499405",
        "marker": "HotelRural.png",
        "image": "11658-135295.webp",
        "email": "reservas@loizu.com",
        "street": "San Nicol\u00e1s, 13",
        "phone": "948 760 008"
    }
}, {
    "node": {
        "title": "Casa Rural Don J\u00e1uregui de Burguete",
        "latitude": "42.988215",
        "longitude": "-1.336545",
        "nid": "11660",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "San Nicol\u00e1s, 32",
        "phone": "948 760 031"
    }
}, {
    "node": {
        "title": "Hostal Burguete",
        "latitude": "42.990660",
        "longitude": "-1.334860",
        "nid": "11657",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "info@hotelburguete.com",
        "street": "San Nicol\u00e1s, 71",
        "phone": "948 760 005"
    }
}, {
    "node": {
        "title": "Casa Rural Txiki Polit",
        "latitude": "42.990111",
        "longitude": "-1.335123",
        "nid": "11659",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "meseverril@yahoo.es",
        "street": "Roncesvalles, 42",
        "phone": "948 760 019, 607 815 587"
    }
}, {
    "node": {
        "title": " Casa Rural Roncesvalles ",
        "latitude": "42.978848",
        "longitude": "-1.369429",
        "nid": "12179",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "https:\/\/www.booking.com\/hotel\/es\/casa-rural-roncesvallhtml?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12179-136774.webp",
        "email": "info@apartamentosirati.com",
        "street": "San Bartolome, 86",
        "phone": "628 271 155"
    }
}, {
    "node": {
        "title": "Camping Urrobi",
        "latitude": "42.973095",
        "longitude": "-1.352456",
        "nid": "7560",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "price": "5.15\u20ac",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "reserve": "Yes",
        "marker": "Camping.png",
        "image": "7560-61005.webp",
        "email": "info@campingurrobi.com",
        "street": "Pamplona-Valcarlos, Km. 42",
        "phone": "948 760 200"
    }
}, {
    "node": {
        "title": "Albergue Haizea",
        "latitude": "42.978489",
        "longitude": "-1.366192",
        "nid": "9644",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi (free) and computer",
        "microwave": "Yes",
        "cal": "DEC - ½ NOV",
        "open": "9:00",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-rural-haizea.html?aid=1499405",
        "marker": "Private.png",
        "image": "9644-135296.webp",
        "email": "iralepo@hotmail.com",
        "street": "Saroiberri",
        "phone": "948 760 379"
    }
}, {
    "node": {
        "title": "Casa Rural Gertxada",
        "latitude": "42.981501",
        "longitude": "-1.371389",
        "nid": "11662",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "gertxadabasajaun@yahoo.es",
        "street": "Espinal",
        "phone": "948 760 261, 609 176 321"
    }
}, {
    "node": {
        "title": "Irugoienea",
        "latitude": "42.977111",
        "longitude": "-1.364881",
        "nid": "8244",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "21",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "HOLY - ½ OCT",
        "open": "13:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "8244-63417.webp",
        "email": "info@irugoienea.com ",
        "street": "Oihanilun, 2",
        "phone": "649 412 487",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casa Rural Errebesena",
        "latitude": "42.979854",
        "longitude": "-1.372143",
        "nid": "11661",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "errebesena@yahoo.es",
        "street": "San Bartolom\u00e9, 25",
        "phone": "948 760 141, 690 644 696"
    }
}, {
    "node": {
        "title": "Casa Rural La Posada Nueva",
        "latitude": "42.967954",
        "longitude": "-1.417489",
        "nid": "11664",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "infoposadanueva@laposadanueva.net",
        "street": "San Pedro, 2",
        "phone": "948 760 173, 699 131 433"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Coraz\u00f3n Puro",
        "latitude": "42.968264",
        "longitude": "-1.417046",
        "nid": "11663",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "open": "12:00",
        "marker": "Pension.png",
        "email": "corazonpuro@corazonpuro.es",
        "street": "San Pedro, 19",
        "phone": "948 392 113"
    }
}, {
    "node": {
        "title": "Casa Rural Maitetxu",
        "latitude": "42.966732",
        "longitude": "-1.417097",
        "nid": "12206",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "ruth@casamaitetxu.com",
        "street": "San Pedro, 12",
        "phone": "669 755 563, 948 760 175"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Zubiaren Etxea",
        "latitude": "42.929805",
        "longitude": "-1.503102",
        "nid": "12207",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "info@pensionzubiarenetxea.com",
        "street": "Camino de Santiago, 2",
        "phone": "948 304 293, 618 014 515"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Usoa",
        "latitude": "42.929985",
        "longitude": "-1.504146",
        "nid": "11665",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Puente de la Rabia, 4",
        "phone": "948 304 306"
    }
}, {
    "node": {
        "title": "Albergue Zaldiko",
        "latitude": "42.930094",
        "longitude": "-1.504026",
        "nid": "1223",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "24",
        "bike": "Sometimes",
        "close": "None",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1223-61009.webp",
        "email": "alberguezaldiko@telefonica.net",
        "street": "Puente de la Rabia, 1",
        "phone": "948 304 301, 609 736 420"
    }
}, {
    "node": {
        "title": "Hostal Gau Txori",
        "latitude": "42.931191",
        "longitude": "-1.504188",
        "nid": "11667",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "info@hostalgautxori.com",
        "street": "Ctra. N-135",
        "phone": "948 304 076"
    }
}, {
    "node": {
        "title": "Albergue R\u00edo Arga Ibaia",
        "latitude": "42.929845",
        "longitude": "-1.503437",
        "nid": "9889",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9889-134839.webp",
        "email": "hrioarga@gmail.com",
        "street": "Puente de la Rabia, 7",
        "phone": "948 304 243, 680 104 471"
    }
}, {
    "node": {
        "title": "Albergue Segunda Etapa",
        "latitude": "42.931072",
        "longitude": "-1.504164",
        "nid": "12168",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "info@alberguesegundaetapa.com",
        "street": "Roncesvalles, 22",
        "phone": "948 304 170, 697 186 560"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Amets",
        "latitude": "42.930788",
        "longitude": "-1.503732",
        "nid": "10021",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-amets.html?aid=1499405",
        "marker": "Pension.png",
        "image": "10021-135297.webp",
        "street": "Gerestegi, 25",
        "phone": "618 636 189"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Zubiri",
        "latitude": "42.931652",
        "longitude": "-1.504566",
        "nid": "1222",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "8\u20ac",
        "beds": "52",
        "bike": "Yes, in the fronton court behind the albergue",
        "close": "22:00",
        "wifi": "Yes, 1\u20ac\/20min",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "10:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Municipal.png",
        "image": "1222-134884.webp",
        "email": "concejozubiri@yahoo.es",
        "phone": "628 324 186"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Benta Berri",
        "latitude": "42.929861",
        "longitude": "-1.504688",
        "nid": "11666",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "bentaberri@hotmail.com",
        "street": "Roncesvalles, 10",
        "phone": "636 134 781"
    }
}, {
    "node": {
        "title": "Albergue El Palo de Avellano",
        "latitude": "42.930610",
        "longitude": "-1.504478",
        "nid": "6965",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "16*\u20ac",
        "beds": "60",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "½ MAR - OCT",
        "open": "13:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6965-61008.webp",
        "email": "info@elpalodeavellano.com",
        "street": "Roncesvalles, 16",
        "phone": "666 499 175"
    }
}, {
    "node": {
        "title": "Hoster\u00eda de Zubiri",
        "latitude": "42.929583",
        "longitude": "-1.504952",
        "nid": "11668",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "close": "24:00",
        "cal": "½ APR - ½ OCT",
        "open": "7:30",
        "marker": "HotelRural.png",
        "image": "11668-136843.webp",
        "email": "info@hosteriadezubiri.com",
        "street": "Roncesvalles, 6",
        "phone": "948 304 329",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Suseia",
        "latitude": "42.934700",
        "longitude": "-1.503947",
        "nid": "9890",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15*\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "20:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "13:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9890-134674.webp",
        "email": "info@suseiazubiri.com",
        "street": "Murelu, 12",
        "phone": "948 304 353",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Tau",
        "latitude": "42.902284",
        "longitude": "-1.539885",
        "nid": "11671",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-tau.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11671-135300.webp",
        "email": " pensiontau@live.com",
        "street": "Errotabidea, 18",
        "phone": "948 304 720"
    }
}, {
    "node": {
        "title": "Hostel Bide Ederra",
        "latitude": "42.901459",
        "longitude": "-1.541437",
        "nid": "9855",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "16\u20ac",
        "beds": "6",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "info@hostelbideederra.com",
        "street": "San Nicolas, 27",
        "phone": "948 304 692, 667 406 554"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El Camino - Casa Sangalo",
        "latitude": "42.900423",
        "longitude": "-1.544446",
        "nid": "11670",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-casa-sangalo.ca.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11670-135299.webp",
        "street": "Portalcelay, 12",
        "phone": "948 304 250"
    }
}, {
    "node": {
        "title": "Casa Elita",
        "latitude": "42.902638",
        "longitude": "-1.539092",
        "nid": "11672",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-elita.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11672-135301.webp",
        "email": "pensioncasaelita@gmail.com",
        "street": "Amairu, 7",
        "phone": "948 304 449",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue de Larrasoa\u00f1a",
        "latitude": "42.901182",
        "longitude": "-1.542056",
        "nid": "1225",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "58",
        "bike": "Yes",
        "close": "19:30",
        "wifi": "Yes, 1\u20ac\/20min",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "13:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Municipal.png",
        "image": "1225-61010.webp",
        "street": "San Nicolas",
        "phone": "605 505 489"
    }
}, {
    "node": {
        "title": "Albergue San Nicol\u00e1s",
        "latitude": "42.903508",
        "longitude": "-1.538429",
        "nid": "11635",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "11635-134849.webp",
        "email": "alberguesannicolas@gmail.com",
        "street": "Sorandi, 5-7",
        "phone": "948 304 245, 619 559 225",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel Akerreta",
        "latitude": "42.896623",
        "longitude": "-1.543536",
        "nid": "10020",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/akerreta.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10020-135298.webp",
        "street": "Akerreta",
        "phone": "948 304 572"
    }
}, {
    "node": {
        "title": "Albergue La Parada de Zuriain",
        "latitude": "42.878526",
        "longitude": "-1.566026",
        "nid": "9776",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12*\u20ac",
        "beds": "13",
        "barrest": "Yes",
        "close": "21:30",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "7:30",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9776-134850.webp",
        "email": "laparadadezuriain@yahoo.es",
        "street": "Landa, 8",
        "phone": "699 556 741, 616 038 685"
    }
}, {
    "node": {
        "title": "Albergue Parroquial de Zabaldika",
        "latitude": "42.856038",
        "longitude": "-1.581795",
        "nid": "9361",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "18",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ APR - ½ OCT",
        "open": "14:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Parochial.png",
        "image": "9361-134852.webp",
        "email": "zabaldika@rscj.es",
        "street": "Church of St. Esteban",
        "phone": "948 330 918, 699 617 559",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue de Trinidad de Arre",
        "latitude": "42.836212",
        "longitude": "-1.604288",
        "nid": "1227",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "8\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "20:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - ½ DEC",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Parochial.png",
        "image": "1227-61016.webp",
        "email": "hmtriniarre@maristasiberica.es",
        "street": "Mayor de Villava",
        "phone": "948 332 941"
    }
}, {
    "node": {
        "title": "Hotel Villava Pamplona",
        "latitude": "42.835102",
        "longitude": "-1.610656",
        "nid": "10019",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/villava.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10019-134847.webp",
        "email": "recep@hotelvillava.com",
        "street": "Pamplona",
        "phone": "948 333 676"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Villava",
        "latitude": "42.831919",
        "longitude": "-1.605886",
        "nid": "6967",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "9\u20ac",
        "beds": "54",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - OCT",
        "open": "13:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/ruralgest-albergue-de-villava-atarrabiako-aterpetxea.html?aid=1499405",
        "marker": "Municipal.png",
        "image": "6967-135315.webp",
        "email": "info@alberguedevillava.com",
        "street": "Pedro de Atarrabia, 17",
        "phone": "948 517 731, 649 713 943"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Obel",
        "latitude": "42.831645",
        "longitude": "-1.609895",
        "nid": "11685",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Las Eras, 5",
        "phone": "948 126 056"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Pasadena",
        "latitude": "42.806393",
        "longitude": "-1.661741",
        "nid": "11683",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pensia3n-pasadena.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11683-135308.webp",
        "email": "info@pensionpasadena.com",
        "street": "Pio XII, 32",
        "phone": "948 177 650"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Sarasate",
        "latitude": "42.815286",
        "longitude": "-1.645955",
        "nid": "11676",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-sarasate.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11676-136797.webp",
        "street": "Sarasate, 30",
        "phone": "948 223 084"
    }
}, {
    "node": {
        "title": "Albergue Casa Ibarrola",
        "latitude": "42.820629",
        "longitude": "-1.643280",
        "nid": "8462",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15*\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi (free)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-ibarrola-hostel.html?aid=1499405",
        "marker": "Private.png",
        "image": "8462-135311.webp",
        "email": "info@casaibarrola.com",
        "street": "Carmen, 31",
        "phone": "948 223 332, 692 208 463"
    }
}, {
    "node": {
        "title": "Albergue Plaza Catedral",
        "latitude": "42.819621",
        "longitude": "-1.642034",
        "nid": "11864",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\/18*\u20ac",
        "beds": "45",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-plaza-catedral.html?aid=1499405",
        "marker": "Private.png",
        "image": "11864-135472.webp",
        "email": "reservas@albergueplazacatedral.com",
        "street": "Navarrer\u00eda, 35",
        "phone": "948 591 336"
    }
}, {
    "node": {
        "title": "Pamplona Catedral Hotel",
        "latitude": "42.820198",
        "longitude": "-1.643754",
        "nid": "11680",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pamplona-catedral-hotel.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11680-135305.webp",
        "email": "info@pamplonacatedralhotel.com",
        "street": "Dos de Mayo, 4",
        "phone": "948 226 688"
    }
}, {
    "node": {
        "title": "Albergue Xarma",
        "latitude": "42.813759",
        "longitude": "-1.635080",
        "nid": "9645",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\/16*\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/xarma-hostel.html?aid=1499405",
        "marker": "Private.png",
        "image": "9645-134332.webp",
        "email": "info@xarmahostel.com",
        "street": "Baja Navarra, 23",
        "phone": "948 046 449, 658 843 820"
    }
}, {
    "node": {
        "title": "Albergue Casa Paderborn",
        "latitude": "42.818447",
        "longitude": "-1.637199",
        "nid": "1228",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "6\u20ac",
        "beds": "26",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Association.png",
        "email": "jakobuspilger@paderborn.com",
        "street": "Playa de Caparroso, 6",
        "phone": "948 211 712"
    }
}, {
    "node": {
        "title": "Hostal Navarra B&B",
        "latitude": "42.812927",
        "longitude": "-1.644238",
        "nid": "11682",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-navarra.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11682-135307.webp",
        "email": "info@hostalnavarra.com",
        "street": "Tudela, 9",
        "phone": "948 225 164"
    }
}, {
    "node": {
        "title": "Hotel Eslava",
        "latitude": "42.818135",
        "longitude": "-1.649833",
        "nid": "10018",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/eslava.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10018-135303.webp",
        "email": "correo@hotel-eslava.com",
        "street": "Virgen de la O, 7",
        "phone": "948 222 270, 948 225 157"
    }
}, {
    "node": {
        "title": "Aloha Hostel",
        "latitude": "42.812307",
        "longitude": "-1.642576",
        "nid": "8435",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15*\u20ac",
        "beds": "26",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi (free)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "8:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/aloha-hostel.html?aid=1499405",
        "marker": "Private.png",
        "image": "8435-65609.webp",
        "email": "info@alohahostel.es",
        "street": "Sang\u00fcesa, 2",
        "phone": "948 153 367, 648 289 403"
    }
}, {
    "node": {
        "title": "Albergue de Pamplona-Iru\u00f1ako Aterpea",
        "latitude": "42.819911",
        "longitude": "-1.643070",
        "nid": "11863",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "16\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "20:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "11:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-de-pamplona-irunako.html?aid=1499405",
        "marker": "Private.png",
        "image": "11863-136506.webp",
        "email": "alberguedepamplona@gmail.com",
        "street": "Carmen, 18",
        "phone": "948 044 637, 685 734 207"
    }
}, {
    "node": {
        "title": "Hostal Arriazu",
        "latitude": "42.816316",
        "longitude": "-1.643746",
        "nid": "11678",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostalarriazu.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11678-135304.webp",
        "email": "info@hostalarriazu.com",
        "street": "Las Comedias, 14",
        "phone": "948 210 202"
    }
}, {
    "node": {
        "title": "Hostel Hemingway",
        "latitude": "42.811790",
        "longitude": "-1.636466",
        "nid": "8611",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "13\/20*\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi (free) and computers",
        "kitchen": "Yes, well equipped",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "8:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostel-hemingway.html?aid=1499405",
        "marker": "Private.png",
        "image": "8611-135219.webp",
        "email": "info@hostelhemingway.com",
        "street": "Amaya, 26",
        "phone": "948 983 884"
    }
}, {
    "node": {
        "title": "Gran Hotel La Perla",
        "latitude": "42.817521",
        "longitude": "-1.642682",
        "nid": "11681",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/gran-la-perla.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11681-135306.webp",
        "email": "informacion@granhotellaperla.com, reservas@granhotellaperla.com",
        "street": "Plaza del Castillo, 1",
        "phone": "948 223 000"
    }
}, {
    "node": {
        "title": "Hotel Castillo de Javier",
        "latitude": "42.816207",
        "longitude": "-1.644908",
        "nid": "10017",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/castillo-de-javier.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10017-135302.webp",
        "email": "info@hotelcastillodejavier.com",
        "street": "San Nicol\u00e1s, 50",
        "phone": "948 203 040"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Peregrinos - Iglesia de Jes\u00fas y Maria",
        "latitude": "42.819046",
        "longitude": "-1.641834",
        "nid": "1229",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "8\u20ac",
        "beds": "112",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "1229-61067.webp",
        "email": "jesusymaria@aspacenavarra.org",
        "street": "Compa\u00f1ia, 4",
        "phone": "948 222 644"
    }
}, {
    "node": {
        "title": "AC Hotel Zizur Mayor",
        "latitude": "42.790689",
        "longitude": "-1.693670",
        "nid": "11673",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/ac-zizur-mayor.html?aid=1499405",
        "marker": "Hotel.png",
        "street": "Etxesakan, 3",
        "phone": "948 287 119"
    }
}, {
    "node": {
        "title": "Albergue Familia Roncal",
        "latitude": "42.788260",
        "longitude": "-1.676617",
        "nid": "1231",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "51",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1231-61063.webp",
        "email": "maribelroncal@jacobeo.net",
        "street": "Lurbeltzeta",
        "phone": "948 183 885, 670 323 271"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos de la Orden de Malta",
        "latitude": "42.788535",
        "longitude": "-1.674525",
        "nid": "1230",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "4\u20ac",
        "beds": "27",
        "close": "22:00",
        "kitchen": "Yes",
        "cal": "APR - SEPT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Private.png",
        "image": "1230-61065.webp",
        "phone": "616 651 330"
    }
}, {
    "node": {
        "title": "Albergue San Andr\u00e9s",
        "latitude": "42.748178",
        "longitude": "-1.722867",
        "nid": "9738",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "6:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac including dry",
        "marker": "Private.png",
        "image": "9738-134844.webp",
        "email": "info@alberguezariquiegui.com",
        "street": "Camino de Santiago, 4",
        "phone": "948 353 876"
    }
}, {
    "node": {
        "title": "El Albergue de Zariquiegui",
        "latitude": "42.747946",
        "longitude": "-1.722549",
        "nid": "3123",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11*\u20ac",
        "beds": "22",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT*",
        "open": "14:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3123-61062.webp",
        "street": "San Andr\u00e9s, 16",
        "phone": "948 334 699, 670 360 888"
    }
}, {
    "node": {
        "title": "Albergue Casa Bazt\u00e1n",
        "latitude": "42.708244",
        "longitude": "-1.761302",
        "nid": "11865",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "albergue@casabaztan.com",
        "street": "Mayor, 46",
        "phone": "948 344 528"
    }
}, {
    "node": {
        "title": "Hostal Camino del Perd\u00f3n",
        "latitude": "42.708411",
        "longitude": "-1.760965",
        "nid": "11690",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-rest-camino-del-perdon.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11690-135312.webp",
        "email": "info@caminodelperdon.com",
        "street": "Iruzpeguia, 20",
        "phone": "948 344 598"
    }
}, {
    "node": {
        "title": "Albergue Camino del Perd\u00f3n",
        "latitude": "42.708274",
        "longitude": "-1.761006",
        "nid": "1233",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:30",
        "wifi": "Yes, computer",
        "cal": "MAR - OCT",
        "open": "8:30",
        "reserve": "Yes, for private rooms only",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1233-61059.webp",
        "email": "info@caminodelperdon.es",
        "street": "Mayor, 57",
        "phone": "948 344 598, 948 344 661"
    }
}, {
    "node": {
        "title": "Albergue El Jard\u00edn de Muruz\u00e1bal",
        "latitude": "42.692244",
        "longitude": "-1.767932",
        "nid": "11622",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "23",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - SEPT",
        "open": "12:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-el-jardin-de-muruzabal.html?aid=1499405",
        "marker": "Private.png",
        "image": "11622-135191.webp",
        "email": "eljardindemuruzabal@gmail.com",
        "street": "Monte Viejo, 21",
        "phone": "696 688 399, 619 462 222"
    }
}, {
    "node": {
        "title": "Albergue USDA de Obanos",
        "latitude": "42.679841",
        "longitude": "-1.785572",
        "nid": "1234",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - ½ OCT",
        "open": "13:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1234-134856.webp",
        "phone": "676 560 927"
    }
}, {
    "node": {
        "title": "Casa Rural Raichu",
        "latitude": "42.680958",
        "longitude": "-1.783906",
        "nid": "11689",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "beds": "9",
        "close": "20:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, free",
        "marker": "CasaRural.png",
        "image": "11689-136665.webp",
        "email": "ruralraichu@wanadoo.es",
        "street": "Larrotaga\u00f1a, 2",
        "phone": "948 344 285"
    }
}, {
    "node": {
        "title": "Hostal Rural Mamerto",
        "latitude": "42.680499",
        "longitude": "-1.785611",
        "nid": "11688",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "obanos10@yahoo.com",
        "street": "San Lorenze ,13",
        "phone": "948 344 344"
    }
}, {
    "node": {
        "title": "Hostal Zubi XXI",
        "latitude": "42.675842",
        "longitude": "-1.807044",
        "nid": "11687",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Irunbidea, 28",
        "phone": "948 340 921"
    }
}, {
    "node": {
        "title": "Albergue Jakue",
        "latitude": "42.676383",
        "longitude": "-1.805958",
        "nid": "6968",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "38",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - ½ OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/jakue.html?aid=1499405",
        "marker": "Private.png",
        "image": "6968-61042.webp",
        "email": "hotel@jakue.com",
        "street": "Irunbidea",
        "phone": "948 341 017"
    }
}, {
    "node": {
        "title": "Ganbara ",
        "latitude": "42.671663",
        "longitude": "-1.817043",
        "nid": "12181",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/apartamento-gure-ganbara.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12181-136776.webp",
        "email": "ganbara@artesaniaganbara.com",
        "street": "Mayor, 86",
        "phone": "948 341 186"
    }
}, {
    "node": {
        "title": "Albergue Amalur",
        "latitude": "42.673184",
        "longitude": "-1.813355",
        "nid": "9842",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, WiFi",
        "cal": "½ MAR - OCT",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9842-134253.webp",
        "email": "info@albergueamalur.com",
        "street": "Cerco Viejo, 3",
        "phone": "948 341 090"
    }
}, {
    "node": {
        "title": "Hotel Rural Bidean",
        "latitude": "42.672341",
        "longitude": "-1.813623",
        "nid": "11686",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "marker": "HotelRural.png",
        "email": "info@bidean.com",
        "street": "Mayor, 20",
        "phone": "948 340 457"
    }
}, {
    "node": {
        "title": "Albergue Santiago Ap\u00f3stol (Puenta la Reina)",
        "latitude": "42.672213",
        "longitude": "-1.823902",
        "nid": "1236",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "100",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "cal": "APR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes, but not during peak season",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "image": "1236-61040.webp",
        "email": "alberguesantiagoapostol@hotmail.com",
        "street": "Paraje el Real",
        "phone": "948 340 220"
    }
}, {
    "node": {
        "title": "Albergue Estrella Gu\u00eda",
        "latitude": "42.671283",
        "longitude": "-1.812666",
        "nid": "12131",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12*\u20ac",
        "beds": "6",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "JAN 10 - DEC",
        "open": "13:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "albergueestrellaguia@gmail.com",
        "street": "Los Fueros, 34",
        "phone": "948 340 001, 622 262 431"
    }
}, {
    "node": {
        "title": "Albergue Puente",
        "latitude": "42.671447",
        "longitude": "-1.813434",
        "nid": "9777",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12*\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer free",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - ½ NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "9777-135236.webp",
        "email": "albergue@alberguepuente.com",
        "street": "Los Fueros, 57",
        "phone": "948 341 052, 661 705 642"
    }
}, {
    "node": {
        "title": "Hotel Rural El Cerco",
        "latitude": "42.672907",
        "longitude": "-1.813838",
        "nid": "10016",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "close": "21:00",
        "cal": "½ MAR - ½ DEC",
        "open": "8:00",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-cerco.html?aid=1499405",
        "marker": "HotelRural.png",
        "image": "10016-136845.webp",
        "email": "info@elcerco.es",
        "street": "Rodrigo Ximenez de Rada, 36",
        "phone": "948 341 269, 682 494 332"
    }
}, {
    "node": {
        "title": "Albergue de los Padres Reparadores",
        "latitude": "42.673400",
        "longitude": "-1.810420",
        "nid": "1235",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "100",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "1235-61046.webp",
        "email": "economo.puente@esic.es",
        "street": "Plaza Guillermo Zicke",
        "phone": "948 340 050"
    }
}, {
    "node": {
        "title": "Albergue El Cantero",
        "latitude": "42.668869",
        "longitude": "-1.863219",
        "nid": "12169",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "26",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "12:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "marker": "Private.png",
        "email": "info@albergueelcantero.com",
        "street": "Esperanza, 2",
        "phone": "948 342 142",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos Lurgorri",
        "latitude": "42.668920",
        "longitude": "-1.862746",
        "nid": "5525",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10*\u20ac",
        "beds": "12",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "14:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "marker": "Private.png",
        "image": "5525-61037.webp",
        "email": "subizaalbeniz@telefonica.net",
        "street": "Esperanza, 5",
        "phone": "686 521 174"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Maralotx de Cirauqui",
        "latitude": "42.675802",
        "longitude": "-1.892164",
        "nid": "1237",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes",
        "cal": "½ MAR - ½ OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "1237-61034.webp",
        "street": "San Rom\u00e1n, 30",
        "phone": "678 635 208"
    }
}, {
    "node": {
        "title": "Casa Cirauqui",
        "latitude": "42.676851",
        "longitude": "-1.889091",
        "nid": "10015",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-cirauqui.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "10015-135314.webp",
        "street": "Zelaia, 5"
    }
}, {
    "node": {
        "title": "Albergue de Lorca",
        "latitude": "42.672240",
        "longitude": "-1.945944",
        "nid": "1238",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "14",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - OCT",
        "open": "24h",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1238-61027.webp",
        "email": "txerra26@mixmail.com",
        "street": "Mayor, 40",
        "phone": "948 541 190"
    }
}, {
    "node": {
        "title": "Albergue La Bodega del Camino",
        "latitude": "42.672115",
        "longitude": "-1.945769",
        "nid": "1239",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "40",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT*",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "1239-61028.webp",
        "email": "info@alberguecaminosantiago.es",
        "street": "Placeta, 8",
        "phone": "948 541 162, 948 541 327"
    }
}, {
    "node": {
        "title": "Hostal Arandigoyen",
        "latitude": "42.666739",
        "longitude": "-1.989255",
        "nid": "10014",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-arandigoyen-arandigoyhtml?aid=1499405",
        "marker": "Hostal.png",
        "image": "10014-135316.webp",
        "email": "hostalruralarandigoyen@gmail.com",
        "street": "Nueva, 8",
        "phone": " 948 541 438, 646 392 254"
    }
}, {
    "node": {
        "title": "Albergue La Casa M\u00e1gica",
        "latitude": "42.658891",
        "longitude": "-1.992927",
        "nid": "3128",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "34",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-la-casa-magica.html?aid=1499405",
        "marker": "Private.png",
        "image": "3128-61023.webp",
        "email": "hola@lacasamagica.eu",
        "street": "Rebote, 5",
        "phone": "948 536 095",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Muskildia Etxea ",
        "latitude": "42.659716",
        "longitude": "-1.988346",
        "nid": "12180",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "https:\/\/www.booking.com\/hotel\/es\/muskildia-etxea.html?aid=1499405#map_closed",
        "marker": "CasaRural.png",
        "image": "12180-136775.webp",
        "street": "Musquilda, 2",
        "phone": "626 932 714, 646 392 254"
    }
}, {
    "node": {
        "title": "Hostal Cristina",
        "latitude": "42.671806",
        "longitude": "-2.031928",
        "nid": "11692",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Baja Navarra, 1",
        "phone": "948 550 450"
    }
}, {
    "node": {
        "title": "Camping Lizarra",
        "latitude": "42.657133",
        "longitude": "-2.017311",
        "nid": "7564",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "price": "5.62\u20ac",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "marker": "Camping.png",
        "image": "7564-61020.webp",
        "email": "info@campinglizarra.com",
        "street": "C.P.: 31200 Estella-Navarra",
        "phone": "948 551 733"
    }
}, {
    "node": {
        "title": "Hotel Yerri",
        "latitude": "42.676801",
        "longitude": "-2.035837",
        "nid": "11694",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/yerri.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11694-135318.webp",
        "email": "hotelyerri@hotelyerri.es",
        "street": "Yerri, 35",
        "phone": "948 546 034"
    }
}, {
    "node": {
        "title": "Hotel Chapitel",
        "latitude": "42.670776",
        "longitude": "-2.028902",
        "nid": "10013",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hospederia-chapitel.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10013-134860.webp",
        "email": "contacto@hospederiachapitel.com",
        "street": "Chapitel, 1",
        "phone": "948 551 090"
    }
}, {
    "node": {
        "title": "Hospital de Peregrinos de Estella",
        "latitude": "42.669800",
        "longitude": "-2.027443",
        "nid": "1240",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "96",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1240-61076.webp",
        "email": "caminodesantiagoestella@gmail.com",
        "street": "La R\u00faa, 50",
        "phone": "948 550 200"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Fonda Izarra",
        "latitude": "42.672824",
        "longitude": "-2.032467",
        "nid": "11691",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "barizarra@hotmail.com",
        "street": "Calderer\u00eda, 20",
        "phone": "948 550 678"
    }
}, {
    "node": {
        "title": "Albergue Parroquial San Miguel Archangel",
        "latitude": "42.671991",
        "longitude": "-2.028262",
        "nid": "3162",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - NOV",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Parochial.png",
        "image": "3162-61079.webp",
        "email": "j.miguelarellano@gmail.com",
        "street": "Mercado Viejo, 18",
        "phone": "948 550 431"
    }
}, {
    "node": {
        "title": "B&B Zaldu",
        "latitude": "42.677257",
        "longitude": "-2.034518",
        "nid": "11693",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/b-amp-b-zaldu.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11693-135317.webp",
        "email": " info@pensionzaldu.com",
        "street": "P\u00edo Baroja ,1",
        "phone": "948 552 263"
    }
}, {
    "node": {
        "title": "Hostal El Volante",
        "latitude": "42.663030",
        "longitude": "-2.031870",
        "nid": "10012",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-el-volante-estella-lizarra.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "10012-134862.webp",
        "email": "hostalelvolante@gmail.com",
        "street": "Merkatondoa, 2",
        "phone": "948 553 957, 638 029 005"
    }
}, {
    "node": {
        "title": "Hotel Tximista",
        "latitude": "42.678574",
        "longitude": "-2.037319",
        "nid": "11695",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/tximista.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11695-135319.webp",
        "street": "Zaldu, 15",
        "phone": "948 555 870"
    }
}, {
    "node": {
        "title": "Albergue Capuchinos Rocamador",
        "latitude": "42.665697",
        "longitude": "-2.031355",
        "nid": "11636",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "15\/20\u20ac",
        "beds": "54",
        "bike": "Yes",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Parochial.png",
        "image": "11636-134885.webp",
        "email": "reservas.estella@alberguescapuchinos.org",
        "street": "Rocamador, 6",
        "phone": "948 550 549, 948 550 013"
    }
}, {
    "node": {
        "title": "Albergue de ANFAS de Estella",
        "latitude": "42.671442",
        "longitude": "-2.025716",
        "nid": "1241",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "7\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes (free)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAY - SEPT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Association.png",
        "image": "1241-61082.webp",
        "email": "albergue@anfasnavarra.org",
        "street": "Cordeleros, 7",
        "phone": "639 011 688"
    }
}, {
    "node": {
        "title": "Albergue Juvenil Oncineda",
        "latitude": "42.664787",
        "longitude": "-2.034310",
        "nid": "3160",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "10\u20ac",
        "beds": "150",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes, 1\u20ac per half hour",
        "microwave": "Yes",
        "cal": "ALL YEAR*",
        "open": "11:00",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-oncineda.html?aid=1499405",
        "marker": "Municipal.png",
        "image": "3160-134859.webp",
        "email": "info@albergueestella.com",
        "street": "Monasterio de Irache",
        "phone": "948 555 022"
    }
}, {
    "node": {
        "title": "Camping Iratxe",
        "latitude": "42.646547",
        "longitude": "-2.056289",
        "nid": "7565",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "barrest": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/camping-iratxe-ciudad-de-vacacionhtml?aid=1499405",
        "marker": "Camping.png",
        "image": "7565-136798.webp",
        "email": "info@campingiratxe.com",
        "street": "Prado de Irache, 14",
        "phone": "948 555 555"
    }
}, {
    "node": {
        "title": "Albergue San Cipriano de Ayegui",
        "latitude": "42.658852",
        "longitude": "-2.036633",
        "nid": "3161",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "8\u20ac",
        "beds": "26",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "cal": "ALL YEAR",
        "open": "13:00",
        "fridge": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "3161-134858.webp",
        "email": "albergue_municipal_ayegui@hotmail.com",
        "street": "Polideportivo, 3",
        "phone": "948 554 311"
    }
}, {
    "node": {
        "title": "La Perla Negra",
        "latitude": "42.635133",
        "longitude": "-2.087150",
        "nid": "12170",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "marker": "Private.png",
        "email": "laperlanegra67@gmail.com",
        "street": "Carrera, 18",
        "phone": "627 114 797"
    }
}, {
    "node": {
        "title": "Albergue Villamayor de Monjard\u00edn",
        "latitude": "42.628945",
        "longitude": "-2.104029",
        "nid": "9104",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15*\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "13:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9104-134857.webp",
        "email": "info@alberguevillamayordemonjardin.com",
        "street": "Mayor, 1",
        "phone": "948 537 139"
    }
}, {
    "node": {
        "title": "Albergue Hogar Monjardin Oasis Trails",
        "latitude": "42.629529",
        "longitude": "-2.104018",
        "nid": "1242",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "8\u20ac",
        "beds": "25",
        "bike": "Yes",
        "close": "22:15",
        "wifi": "No, but available at a bar nearby",
        "cal": "APR - NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Association.png",
        "image": "1242-134863.webp",
        "phone": "948 537 136",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casa Rural Montedeio",
        "latitude": "42.629476",
        "longitude": "-2.105644",
        "nid": "11706",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "contacto@casaruralmontedeio.com",
        "street": "Mayor, 17",
        "phone": "676 187 473"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Mavi",
        "latitude": "42.569451",
        "longitude": "-2.192659",
        "nid": "11698",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "info@pensionmavi.es",
        "street": "Medio, 7",
        "phone": "948 640 081"
    }
}, {
    "node": {
        "title": "Casa Alberdi",
        "latitude": "42.568677",
        "longitude": "-2.194598",
        "nid": "1244",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "14:00",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1244-134868.webp",
        "email": "alberdikontxi@gmail.com",
        "street": "Hortal, 3",
        "phone": "948 640 764, 650 965 250"
    }
}, {
    "node": {
        "title": "Hotel Villa de Los Arcos",
        "latitude": "42.555540",
        "longitude": "-2.181011",
        "nid": "11700",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/villa-de-los-arcos.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11700-135320.webp",
        "email": "hotel@villadelosarcos.es",
        "street": "Ctra. NA-129 Km 146",
        "phone": "948 640 002"
    }
}, {
    "node": {
        "title": "Albergue Casa de la Abuela",
        "latitude": "42.569438",
        "longitude": "-2.191751",
        "nid": "5539",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer (free)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "5539-134872.webp",
        "email": "contacto@casadelaabuela.com",
        "street": "Plaza de la Fruta, 8",
        "phone": "948 640 250"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Ostadar",
        "latitude": "42.568227",
        "longitude": "-2.194241",
        "nid": "11702",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-ostadar.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11702-135322.webp",
        "street": "San L\u00e1zaro, 9",
        "phone": "649 961 440"
    }
}, {
    "node": {
        "title": "Hostal Ezequiel",
        "latitude": "42.566949",
        "longitude": "-2.190732",
        "nid": "10011",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "beds": "23",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "9:30",
        "wifi": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "marker": "Hostal.png",
        "image": "10011-134867.webp",
        "email": "info@hostalezequiel.com",
        "street": "La Serna, 14",
        "phone": "948 640 107"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Isaac Santiago",
        "latitude": "42.569059",
        "longitude": "-2.194208",
        "nid": "1243",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "6\u20ac",
        "beds": "70",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Association.png",
        "image": "1243-61090.webp",
        "street": "San L\u00e1zaro",
        "phone": "948 441 091"
    }
}, {
    "node": {
        "title": "Hotel M\u00f3naco",
        "latitude": "42.568460",
        "longitude": "-2.191854",
        "nid": "11699",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "info@hotelmonaco.es",
        "street": "Plaza del Coso, 1",
        "phone": "948 640 000"
    }
}, {
    "node": {
        "title": "Albergue de la Fuente - Casa de Austria",
        "latitude": "42.570963",
        "longitude": "-2.192857",
        "nid": "1246",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "54",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "FEB - ½ DEC",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1246-61096.webp",
        "email": "ignaciovela@yahoo.com",
        "street": "Estanco, 5",
        "phone": "948 640 797, 636 018 348"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Los Arcos",
        "latitude": "42.568173",
        "longitude": "-2.190065",
        "nid": "11701",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-los-arcos-los-arcos.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11701-135321.webp",
        "street": "La Carrera, 8",
        "phone": "608 585 153"
    }
}, {
    "node": {
        "title": "Hostal Suetxe",
        "latitude": "42.575309",
        "longitude": "-2.195882",
        "nid": "10010",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-suetxe.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "10010-134866.webp",
        "email": "reservas@hostalsuetxe.es",
        "street": "Carramendavia",
        "phone": "948 441 175, 618 724 437"
    }
}, {
    "node": {
        "title": "Deshojando el Camino",
        "latitude": "42.553714",
        "longitude": "-2.265513",
        "nid": "9862",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "26",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "HOLY - NOV",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "image": "9862-134878.webp",
        "email": "info@deshojandoelcamino.com",
        "street": "Barrio Nuevo, 4",
        "phone": "609 203 206, 948 648 473"
    }
}, {
    "node": {
        "title": "Casa Rural El Olivo",
        "latitude": "42.553264",
        "longitude": "-2.267282",
        "nid": "11703",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "informacion@elolivodesansol.com",
        "street": "Taconera, 9",
        "phone": "948 648 345"
    }
}, {
    "node": {
        "title": "Casa Rural Martintxo",
        "latitude": "42.559909",
        "longitude": "-2.284710",
        "nid": "11697",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-de-habitaciones-martintxo.html?aid=1499405",
        "marker": "CasaRural.png",
        "email": "info@martintxo.es",
        "street": "la Plaza, 6",
        "phone": "618 629 084"
    }
}, {
    "node": {
        "title": "Hostal San Andr\u00e9s",
        "latitude": "42.551467",
        "longitude": "-2.270768",
        "nid": "11696",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-rural-san-andres.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11696-135323.webp",
        "email": "info@sanandreshostal.com",
        "street": "Jes\u00fas Ordo\u00f1ez, 6",
        "phone": "948 648 472"
    }
}, {
    "node": {
        "title": "Albergue La Pata de Oca",
        "latitude": "42.551875",
        "longitude": "-2.271603",
        "nid": "5511",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "42",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "image": "5511-61101.webp",
        "email": "reservas@alberguelapatadeoca.com",
        "street": "Mayor, 5",
        "phone": "948 378 457, 608 250 121",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Casa Mariela",
        "latitude": "42.551952",
        "longitude": "-2.271034",
        "nid": "3158",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "open": "6:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3158-134870.webp",
        "email": "fernando_berdeja_7@hotmail.com",
        "street": "Plaza Padre Valeriano Ord\u00f3\u00f1ez, 6",
        "phone": "948 648 251"
    }
}, {
    "node": {
        "title": "Hostal Casa Armend\u00e1riz",
        "latitude": "42.514885",
        "longitude": "-2.372934",
        "nid": "11712",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "casa_armendariz@hotmail.com",
        "street": "Navarro Villoslada, 19",
        "phone": "948 645 078"
    }
}, {
    "node": {
        "title": "Albergue Parroquial Santa Mar\u00eda de Viana",
        "latitude": "42.515135",
        "longitude": "-2.371802",
        "nid": "6969",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "15",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "JUN -  SEPT",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "image": "6969-61106.webp",
        "email": "parroquiaviana@gmail.com",
        "street": "Plaza de los Fueros",
        "phone": "948 645 037, 646 666 738"
    }
}, {
    "node": {
        "title": "Hotel Palacio de Pujadas",
        "latitude": "42.514884",
        "longitude": "-2.373326",
        "nid": "10009",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/palacio-de-pujadas.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10009-134873.webp",
        "email": "info@palaciodepujadas.com",
        "street": "Navarro Villoslada, 30",
        "phone": "948 646 464"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n San Pedro",
        "latitude": "42.514651",
        "longitude": "-2.372497",
        "nid": "11714",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "joseigna1969@hotmail.es",
        "street": "Medio de San Pedro, 13",
        "phone": "948 645 927"
    }
}, {
    "node": {
        "title": "Alberguer\u00eda Andr\u00e9s Mu\u00f1oz",
        "latitude": "42.514527",
        "longitude": "-2.373433",
        "nid": "1249",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "54",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.5\u20ac",
        "marker": "Municipal.png",
        "image": "1249-61103.webp",
        "email": "ayuntamiento@viana.es",
        "street": "San Pedro",
        "phone": "948 645 530"
    }
}, {
    "node": {
        "title": "Albergue Izar",
        "latitude": "42.515238",
        "longitude": "-2.368521",
        "nid": "9368",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\/15\u20ac",
        "beds": "44",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9368-134871.webp",
        "email": "albergueizar@gmail.com",
        "street": "El Cristo, 6",
        "phone": "948 090 002, 660 071 349"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Saint Mateo",
        "latitude": "42.464614",
        "longitude": "-2.456816",
        "nid": "11710",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-saint-mateo.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11710-135329.webp",
        "email": "info@pensionsaintmateo.com",
        "street": "Marqu\u00e9s de Murrieta, 35",
        "phone": "620 637 191"
    }
}, {
    "node": {
        "title": "Hotel Murrieta",
        "latitude": "42.465829",
        "longitude": "-2.451802",
        "nid": "10007",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/murrieta.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10007-135331.webp",
        "email": "info@hotelmurrietalogrono.com",
        "street": "Marqu\u00e9s de Murrieta, 1",
        "phone": "941 224 150"
    }
}, {
    "node": {
        "title": "Albergue Logro\u00f1o",
        "latitude": "42.466275",
        "longitude": "-2.447549",
        "nid": "9367",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "48",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "9:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "email": "info@casaconencanto.net",
        "street": "Capit\u00e1n Gallarza, 10",
        "phone": "941 254 226, 608 234 723"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Logro\u00f1o",
        "latitude": "42.468183",
        "longitude": "-2.444683",
        "nid": "1250",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "7\u20ac",
        "beds": "68",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Association.png",
        "image": "1250-61111.webp",
        "email": "info@asantiago.org",
        "street": "R\u00faavieja, 32",
        "phone": "941 248 686"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Logro\u00f1o",
        "latitude": "42.466184",
        "longitude": "-2.453742",
        "nid": "11707",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-logrono.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11707-135326.webp",
        "street": "Canalejas, 7",
        "phone": "941 101 186"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El Camino",
        "latitude": "42.465101",
        "longitude": "-2.456410",
        "nid": "10004",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pensia3n-el-camino.html?aid=1499405",
        "marker": "Pension.png",
        "image": "10004-135368.webp",
        "email": "info@pensionelcamino.com",
        "street": "Industria, 2",
        "phone": "618 655 000"
    }
}, {
    "node": {
        "title": "Albergue Parroquial de Santiago",
        "latitude": "42.467669",
        "longitude": "-2.447773",
        "nid": "3705",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "30",
        "wifi": "Yes",
        "cal": "JUN - SEPT",
        "pilgrim": "Yes",
        "marker": "Parochial.png",
        "email": "info@santiagoelreal.org",
        "street": "Barriocepo, 8",
        "phone": "941 209 501",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel Calle Mayor",
        "latitude": "42.467308",
        "longitude": "-2.446235",
        "nid": "11709",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/calle-mayor.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11709-135328.webp",
        "email": "info@hotelcallemayor.com",
        "street": "Marqu\u00e9s de San Nicol\u00e1s, 71",
        "phone": "941 232 368"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Laurel",
        "latitude": "42.465441",
        "longitude": "-2.454688",
        "nid": "10006",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pensia3n-laurel.html?aid=1499405",
        "marker": "Pension.png",
        "image": "10006-135366.webp",
        "email": "info@pensionlaurel.com",
        "street": "Gonzalo de Berceo, 4",
        "phone": "941 124 506"
    }
}, {
    "node": {
        "title": "Hostel Entresue\u00f1os",
        "latitude": "42.466499",
        "longitude": "-2.444515",
        "nid": "8250",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "96",
        "bike": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "24h",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostel-entresuea-os-logroa-o.html?aid=1499405",
        "marker": "Private.png",
        "image": "8250-63724.jpeg",
        "email": "info@hostellogrono.com",
        "street": "Portales, 12",
        "phone": "941 271 334"
    }
}, {
    "node": {
        "title": "Hotel F&G Logro\u00f1o",
        "latitude": "42.468431",
        "longitude": "-2.443025",
        "nid": "11711",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/f-g-logrono.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11711-135330.webp",
        "email": "reservas.logrono@fghotels.com",
        "street": "Viana, 2-6",
        "phone": "941 008 900"
    }
}, {
    "node": {
        "title": "Hotel Sercotel Portales",
        "latitude": "42.466146",
        "longitude": "-2.450450",
        "nid": "10008",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/portalhtml?aid=1499405",
        "marker": "Hotel.png",
        "image": "10008-135325.webp",
        "email": "reservas@hotelportales.es",
        "street": "Portales, 85",
        "phone": " 902 141 515"
    }
}, {
    "node": {
        "title": "Albergue Santiago Ap\u00f3stol",
        "latitude": "42.468594",
        "longitude": "-2.444003",
        "nid": "9646",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "76",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes, costs 1.5\u20ac",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "6:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-santiago-apostol.html?aid=1499405",
        "marker": "Private.png",
        "image": "9646-135365.webp",
        "email": "ruavieja42@gmail.com",
        "street": "Ruavieja, 42",
        "phone": "941 256 976, 670 993 560"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Albas",
        "latitude": "42.467897",
        "longitude": "-2.436197",
        "nid": "3156",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "22",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-peregrinos-albas.html?aid=1499405",
        "marker": "Private.png",
        "image": "3156-134316.webp",
        "email": "albas@alberguealbas.es",
        "street": "Mart\u00ednez Flamarique, 4",
        "phone": "941 700 832"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Parque del Ebro",
        "latitude": "42.468515",
        "longitude": "-2.438402",
        "nid": "11708",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pensia3n-parque-del-ebro.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11708-135327.webp",
        "email": "reservas@parquedelebro.com",
        "street": "Pl. de la Constituci\u00f3n, 24",
        "phone": "616 840 786"
    }
}, {
    "node": {
        "title": "Hostal La Numantina",
        "latitude": "42.466197",
        "longitude": "-2.446588",
        "nid": "10005",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-la-numantina.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "10005-135367.webp",
        "email": "hostal.numantina@gmail.com ",
        "street": "Sagasta, 4",
        "phone": "941 251 411"
    }
}, {
    "node": {
        "title": "Albergue Check in Rioja",
        "latitude": "42.467388",
        "longitude": "-2.442518",
        "nid": "7869",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - ½ OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/check-in-rioja.html?aid=1499405",
        "marker": "Private.png",
        "image": "7869-135332.webp",
        "email": "checkinrioja@gmail.com",
        "street": "Los Ba\u00f1os, 2",
        "phone": "941 272 329"
    }
}, {
    "node": {
        "title": "Hostal Villa de Navarrete",
        "latitude": "42.429071",
        "longitude": "-2.560872",
        "nid": "11715",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "reservas@hostalvilladenavarrete.com",
        "street": "La Cruz, 2",
        "phone": "941 440 318"
    }
}, {
    "node": {
        "title": "Albergue Pilgrim's",
        "latitude": "42.429219",
        "longitude": "-2.562851",
        "nid": "9370",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "38",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "11:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9370-134888.webp",
        "email": "alberguepilgrims@gmail.com",
        "street": "Abad\u00eda, 1",
        "phone": "941 441 550",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue El camino de las Estrellas",
        "latitude": "42.431874",
        "longitude": "-2.558470",
        "nid": "12200",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "40",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "alberguenavarrete0@gmail.com",
        "street": "Burgos, 9",
        "phone": "618 051 392"
    }
}, {
    "node": {
        "title": "Albergue A la Sombra del Laurel",
        "latitude": "42.429792",
        "longitude": "-2.569063",
        "nid": "9867",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "ALL YEAR*",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-a-la-sombra-del-laurel.html?aid=1499405",
        "marker": "Private.png",
        "image": "9867-134568.webp",
        "email": "info@alasombradellaurel.com",
        "street": "Carretera de Burgos, 52",
        "phone": "639 861 110"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Navarrete",
        "latitude": "42.429219",
        "longitude": "-2.560450",
        "nid": "1251",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "7\u20ac",
        "beds": "48",
        "close": "22:00",
        "wifi": "Yes, in the local library",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1251-134890.webp",
        "email": "info@asantiago.org",
        "street": "San Juan, 2",
        "phone": "941 440 722 "
    }
}, {
    "node": {
        "title": "Hotel Rey Sancho",
        "latitude": "42.429372",
        "longitude": "-2.562003",
        "nid": "10003",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/rey-sancho.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10003-134886.webp",
        "email": "hotelreysancho@gmail.com",
        "street": "Mayor Alta, 5",
        "phone": "941 441 378"
    }
}, {
    "node": {
        "title": "Albergue La Casa del Peregrino",
        "latitude": "42.431221",
        "longitude": "-2.559758",
        "nid": "6970",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - SEPT",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-la-casa-del-peregrino.html?aid=1499405",
        "marker": "Private.png",
        "image": "6970-131744.jpeg",
        "email": "alberguenavarrete@gmail.com",
        "street": "Las Huertas, 3",
        "phone": "630 982 928"
    }
}, {
    "node": {
        "title": "Casa Peregrinando",
        "latitude": "42.430080",
        "longitude": "-2.562565",
        "nid": "11716",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Mayor Alta, 34",
        "phone": "941 441 324"
    }
}, {
    "node": {
        "title": "Albergue Buen Camino",
        "latitude": "42.429073",
        "longitude": "-2.561435",
        "nid": "9647",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "10",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes, basic",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9647-134889.webp",
        "email": "reservas@alberguebuencamino.es",
        "street": "La Cruz, 2",
        "phone": "941 440 318, 681 252 222"
    }
}, {
    "node": {
        "title": "Hotel San Camilo",
        "latitude": "42.433752",
        "longitude": "-2.566810",
        "nid": "10002",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/san-camilo.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10002-135354.webp",
        "email": "sancamilo@hotelsancamilo.es",
        "street": "Fuenmayor, 4",
        "phone": "941 441 111"
    }
}, {
    "node": {
        "title": "Albergue El C\u00e1ntaro",
        "latitude": "42.430431",
        "longitude": "-2.563985",
        "nid": "1252",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "28",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi\/free",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1252-52168.webp",
        "email": "info@albergueelcantaro.com",
        "street": "Herrer\u00edas, 16",
        "phone": "941 441 180, 629 942 691"
    }
}, {
    "node": {
        "title": "Hotel Las \u00c1guedas",
        "latitude": "42.403675",
        "longitude": "-2.626934",
        "nid": "10001",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/las-aguedas.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10001-135353.webp",
        "email": "hotellasaguedas@gmail.com",
        "street": "Plaza de Santa Coloma, 11",
        "phone": "941 441 774, 636 220 629"
    }
}, {
    "node": {
        "title": "Albergue San Saturnino",
        "latitude": "42.403963",
        "longitude": "-2.626821",
        "nid": "1253",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "42",
        "barrest": "No, but small shop.",
        "bike": "Yes",
        "close": "22:00, unlocked",
        "wifi": "Yes, WiFi (free)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1253-52316.webp",
        "email": "ventosa@jacobeos.net",
        "street": "Medio Derecha 9 ",
        "phone": "941 441 899, 657 823 740"
    }
}, {
    "node": {
        "title": "Albergue de Najera",
        "latitude": "42.414966",
        "longitude": "-2.734201",
        "nid": "1254",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "90",
        "close": "22:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "1254-61205.webp",
        "street": "Plaza de Santiago",
        "phone": "941 360 675"
    }
}, {
    "node": {
        "title": "Hotel Duques de N\u00e1jera",
        "latitude": "42.417786",
        "longitude": "-2.733922",
        "nid": "10000",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/duques-de-najera.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "10000-134895.webp",
        "email": "info@hotelduquesdenajera.com",
        "street": "Carmen, 7",
        "phone": "941 410 421"
    }
}, {
    "node": {
        "title": "Albergue Calle Mayor",
        "latitude": "42.417669",
        "longitude": "-2.733825",
        "nid": "6972",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "beds": "16",
        "bike": "Yes",
        "wifi": "Yes",
        "cal": "HOLY - NOV",
        "open": "10:00",
        "reserve": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-calle-mayor.html?aid=1499405",
        "marker": "Private.png",
        "image": "6972-134894.webp",
        "email": "callemayor@grupostar.com",
        "street": "Dicar\u00e1n, 5",
        "phone": "941 360 407"
    }
}, {
    "node": {
        "title": "Albergue El Peregrino",
        "latitude": "42.418233",
        "longitude": "-2.725540",
        "nid": "11886",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "11886-135170.webp",
        "email": "alberguenajera@gmail.com",
        "street": "San Fernando, 90",
        "phone": "941 896 027, 640 072 753"
    }
}, {
    "node": {
        "title": "Hostal Hispano",
        "latitude": "42.379124",
        "longitude": "-2.654926",
        "nid": "9997",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "price": "15\u20ac",
        "cal": "ALL YEAR",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hispano.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9997-136858.webp",
        "email": "hispanonajera@yahoo.es",
        "street": "La Cepa, 2",
        "phone": "941 363 615",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal Ciudad de N\u00e1jera",
        "latitude": "42.418142",
        "longitude": "-2.734770",
        "nid": "9999",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-ciudad-de-na-jera.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9999-135351.webp",
        "email": "ciudaddenajera@gmail.com",
        "street": "Cuarta Calleja San Miguel, 14",
        "phone": "941 360 660"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Sancho III - La Juder\u00eda",
        "latitude": "42.415608",
        "longitude": "-2.735016",
        "nid": "1255",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "10",
        "close": "22:00",
        "cal": "HOLY - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "1255-61116.webp",
        "street": "Constantino Garran, 13",
        "phone": "941 361 138"
    }
}, {
    "node": {
        "title": "Albergue Nido de \u0421ig\u00fce\u00f1a",
        "latitude": "42.418212",
        "longitude": "-2.735007",
        "nid": "11866",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "19",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "email": "alberguenajera@gmail.com",
        "street": "Cuarta Calleja San Miguel, 4",
        "phone": "941 896 027, 640 072 753"
    }
}, {
    "node": {
        "title": "Albergue Puerta de N\u00e1jera",
        "latitude": "42.417638",
        "longitude": "-2.733411",
        "nid": "6973",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "6973-52280.webp",
        "email": "reservas@alberguedenajera.com",
        "street": "Carmen, 4",
        "phone": "941 362 317, 683 616 894"
    }
}, {
    "node": {
        "title": "Pension San Lorenzo",
        "latitude": "42.416687",
        "longitude": "-2.734928",
        "nid": "12368",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-san-lorenzo-canas.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12368-136996.webp",
        "email": "reservas@pensionsanlorenzo.es",
        "street": "Garr\u00e1n, 10",
        "phone": "941 36 37 22, 626 709 783"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Calle Mayor",
        "latitude": "42.417592",
        "longitude": "-2.733638",
        "nid": "9998",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-calle-mayor.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9998-135352.webp",
        "street": "Dicar\u00e1n, 5",
        "phone": "941 360 407"
    }
}, {
    "node": {
        "title": "Hotel Real Casona de las Amas",
        "latitude": "42.424570",
        "longitude": "-2.801750",
        "nid": "9996",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/real-casona-de-las-amas.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9996-135350.webp",
        "email": "mail@realcasonadelasamas.com ",
        "street": "Mayor, 5",
        "phone": "941 416 103"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Peregrinos de Azofra",
        "latitude": "42.424740",
        "longitude": "-2.800634",
        "nid": "1256",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "7\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer 1\u20ac\/20min",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT ",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, price included",
        "marker": "Municipal.png",
        "image": "1256-61208.webp",
        "email": "lomaa@hotmail.es",
        "street": "Las Parras, 7",
        "phone": "941 379 220, 607 383 811"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n La Plaza",
        "latitude": "42.423460",
        "longitude": "-2.800978",
        "nid": "11720",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "beds": "12",
        "barrest": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "marker": "Pension.png",
        "email": "reservaslaplaza@gmail.com",
        "street": "Plaza de Espa\u00f1a, 7",
        "phone": "629 828 702",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Virgen de Guadalupe",
        "latitude": "42.412708",
        "longitude": "-2.894372",
        "nid": "3706",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "35",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "½ MAR - ½ OCT",
        "open": "13:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "image": "3706-61210.webp",
        "email": "virgendeguadalupe1@gmail.com",
        "street": "Barrio Alto, 1",
        "phone": "638 924 069"
    }
}, {
    "node": {
        "title": "Casa Rural Victoria",
        "latitude": "42.410015",
        "longitude": "-2.896035",
        "nid": "9995",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-victoria-cirueaa.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9995-134899.webp",
        "email": "info@casavictoriarural.com",
        "street": "Plaza del Horno, 8",
        "phone": "941 426 105, 628 983 351"
    }
}, {
    "node": {
        "title": "Albergue Victoria",
        "latitude": "42.410277",
        "longitude": "-2.894474",
        "nid": "9739",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "10",
        "bike": "Yes",
        "close": "21:00",
        "family": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9739-134900.webp",
        "email": "info@casavictoriarural.com",
        "street": "Plaza del Horno, 8",
        "phone": "941 426 105",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal Rey Pedro I",
        "latitude": "42.439462",
        "longitude": "-2.952865",
        "nid": "11717",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "info@hostalpedroprimero.es",
        "street": "San Roque, 9",
        "phone": "941 341 160"
    }
}, {
    "node": {
        "title": "Albergue del Abad\u00eda Cisterciense Nuestra Sra. de la Anunciaci\u00f3n",
        "latitude": "42.440510",
        "longitude": "-2.950963",
        "nid": "1257",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "7\u20ac",
        "beds": "33",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAY - SEPT",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "image": "1257-52312.webp",
        "email": "hospederia@cister-lacalzada.com",
        "street": "Mayor, 29",
        "phone": "941 340 700"
    }
}, {
    "node": {
        "title": "Hostal Miguel",
        "latitude": "42.439577",
        "longitude": "-2.955819",
        "nid": "11719",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-miguel.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11719-136799.webp",
        "street": "Juan Carlos I, 23",
        "phone": "600 212 691"
    }
}, {
    "node": {
        "title": "Hotel El Corregidor",
        "latitude": "42.440601",
        "longitude": "-2.950559",
        "nid": "9992",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-corregidor.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9992-134898.webp",
        "email": "contacto@hotelelcorregidor.com",
        "street": "Mayor, 14",
        "phone": "941 342 128, 941 342 552"
    }
}, {
    "node": {
        "title": "Hostal La Catedral",
        "latitude": "42.440335",
        "longitude": "-2.954190",
        "nid": "9994",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-la-catedral.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9994-134896.webp",
        "street": "Isidoro Salas, 49",
        "phone": "651 948 260"
    }
}, {
    "node": {
        "title": "Hospeder\u00eda Cisterciense",
        "latitude": "42.439765",
        "longitude": "-2.950994",
        "nid": "11718",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hospederaa-cisterciense.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11718-135349.webp",
        "email": "hospederia@cister-lacalzada.com",
        "street": "Pinar, 2",
        "phone": "941 340 700"
    }
}, {
    "node": {
        "title": "Albergue Casa del Santo",
        "latitude": "42.440912",
        "longitude": "-2.952530",
        "nid": "1258",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "7\u20ac",
        "beds": "211",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, Wifi and computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Parochial.png",
        "image": "1258-61211.webp",
        "email": "albergue@alberguecofradiadelsanto.com",
        "street": "Mayor, 42",
        "phone": "941 343 390"
    }
}, {
    "node": {
        "title": "Parador de Santo Domingo de la Calzada",
        "latitude": "42.440830",
        "longitude": "-2.954056",
        "nid": "9993",
        "nodetype": "Accommodation",
        "accomtype": "Parador",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/parador-santo-domingo-de-la-calzada.html?aid=1499405",
        "marker": "Parador.png",
        "image": "9993-134897.webp",
        "email": "sto.domingo@parador.es",
        "street": "Plaza del Santo, 3",
        "phone": "941 340 300"
    }
}, {
    "node": {
        "title": "Albergue Municipal Nuestra Se\u00f1ora de Carrasquedo",
        "latitude": "42.437585",
        "longitude": "-3.023214",
        "nid": "9891",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes, repair and cleaning too.",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "open": "8:30",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-nuestra-senora-de-carrasquedo.html?aid=1499405",
        "marker": "Municipal.png",
        "image": "9891-134904.webp",
        "email": "armando@obr.es",
        "street": "Carrasquedo",
        "phone": "627 341 907, 665 284 685"
    }
}, {
    "node": {
        "title": "Casa Rural Cerro de Mirabel",
        "latitude": "42.449920",
        "longitude": "-3.028536",
        "nid": "11727",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "esther_martinez-5@hotmail.com",
        "street": "Mayor, 40",
        "phone": "941 420 798"
    }
}, {
    "node": {
        "title": "La Casa de las Sonrisas",
        "latitude": "42.450410",
        "longitude": "-3.027436",
        "nid": "8544",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "Donativo",
        "beds": "14, 36 in 2013",
        "bike": "Yes",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "image": "8544-134903.webp",
        "email": "kj17_887@hotmail.com",
        "street": "Mayor, 16",
        "phone": "687 877 891",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casa Rural Jacobea",
        "latitude": "42.450482",
        "longitude": "-3.027227",
        "nid": "11726",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "Mayor, 34",
        "phone": "941 420 684"
    }
}, {
    "node": {
        "title": "Albergue Ave de Paso",
        "latitude": "42.450642",
        "longitude": "-3.025333",
        "nid": "12132",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "10",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "avedepasoalbergue@outlook.es",
        "street": "El Ca\u00f1o, 18",
        "phone": "666 801 051",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hospital de peregrinos San Juan Bautista",
        "latitude": "42.450441",
        "longitude": "-3.026905",
        "nid": "1259",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "40",
        "bike": "Yes",
        "close": "None",
        "family": "Yes",
        "kitchen": "Yes, but all meals are communal",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "24h",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "image": "1259-61213.webp",
        "phone": "941 420 818",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel Redecilla del Camino",
        "latitude": "42.438137",
        "longitude": "-3.065166",
        "nid": "9990",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/redecilla-del-camino.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9990-134908.webp",
        "email": "info@casaruraldelcamino.com",
        "street": "Mayor, 12",
        "phone": "697 641 117"
    }
}, {
    "node": {
        "title": "Albergue municipal San L\u00e1zaro",
        "latitude": "42.438279",
        "longitude": "-3.064812",
        "nid": "1261",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "38",
        "bike": "Yes",
        "close": "None",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 1\u20ac",
        "marker": "Municipal.png",
        "image": "1261-61215.webp",
        "street": "Mayor, 24",
        "phone": "947 580 283, 947 588 078"
    }
}, {
    "node": {
        "title": "Albergue Essentia",
        "latitude": "42.438420",
        "longitude": "-3.064435",
        "nid": "12171",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "10",
        "bike": "Yes",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "email": "info@essentiacamino.com",
        "street": "Mayor, 34",
        "phone": "606 046 298"
    }
}, {
    "node": {
        "title": "El Chocolatero",
        "latitude": "42.438186",
        "longitude": "-3.084192",
        "nid": "9991",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "barrest": "Yes",
        "wifi": "Yes, WiFi",
        "washer": "Yes, 5\u20ac including dry",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-chocolatero-redecilla-del-camino.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9991-134909.webp",
        "email": "chococastil@gmail.com",
        "street": "N-120, km. 57,5",
        "phone": "947 588 063"
    }
}, {
    "node": {
        "title": "Albergue Bideluze",
        "latitude": "42.436988",
        "longitude": "-3.082760",
        "nid": "12201",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "email": "info@alberguebideluze.com",
        "street": "Mayor, 8",
        "phone": "616 647 115"
    }
}, {
    "node": {
        "title": "Refugio de Peregrinos Acacio y Orietta",
        "latitude": "42.426379",
        "longitude": "-3.100210",
        "nid": "1260",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "10",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer 2\u20ac",
        "kitchen": "Yes, but private use only.",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "13:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/refugio-de-peregrinos-acacio-amp-orietta.html?aid=1499405",
        "marker": "Private.png",
        "image": "1260-61216.webp",
        "email": "casaperegrina@yahoo.es ",
        "street": "Nueva, 6",
        "phone": "947 585 220, 679 941 123",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel Mihotelito",
        "latitude": "42.426134",
        "longitude": "-3.101642",
        "nid": "9989",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/mihotelito.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9989-134915.webp",
        "email": "mihotelito@mihotelito.es",
        "street": "Plaza Mayor, 16",
        "phone": "947 585 225"
    }
}, {
    "node": {
        "title": "Parada Viloria",
        "latitude": "42.427067",
        "longitude": "-3.099638",
        "nid": "9861",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "20:00",
        "wifi": "Yes, WiFi ",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "10:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9861-134914.webp",
        "email": "paradaviloria@yahoo.es",
        "street": "Bajera, 37",
        "phone": "639 451 660, 610 625 065"
    }
}, {
    "node": {
        "title": "Albergue San Luis de Francia",
        "latitude": "42.431125",
        "longitude": "-3.134112",
        "nid": "1262",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "26",
        "bike": "Yes",
        "close": "23:00",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1262-136936.webp",
        "email": "alberguesanluisdefrancia@hotmail.com",
        "street": "Carretera de Quintanilla",
        "phone": "947 580 566, 659 967 967 "
    }
}, {
    "node": {
        "title": "Hotel Belorado",
        "latitude": "42.417944",
        "longitude": "-3.198189",
        "nid": "11725",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "informacion@hotelbelorado.com",
        "street": "Burgos, 30",
        "phone": "947 580 684"
    }
}, {
    "node": {
        "title": "Albergue Rural El Corro",
        "latitude": "42.419691",
        "longitude": "-3.188326",
        "nid": "9844",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "40",
        "bike": "Yes",
        "wifi": "Yes, WiFi and Computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "9844-134912.webp",
        "email": "albergueelcorro@gmail.com",
        "street": "Mayor, 68",
        "phone": "947 581 419, 629 507 470"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Waslala",
        "latitude": "42.420489",
        "longitude": "-3.188999",
        "nid": "11721",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "beds": "6",
        "bike": "Yes",
        "close": "11:00",
        "wifi": "Yes",
        "cal": "FEB - NOV",
        "open": "11:00",
        "washer": "Yes, 6.50\u20ac",
        "marker": "Pension.png",
        "email": "casawaslala@gmail.com",
        "street": "Mayor, 57",
        "phone": "947 580 726",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Cuatro Cantones",
        "latitude": "42.421133",
        "longitude": "-3.190651",
        "nid": "1264",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\/11\u20ac",
        "beds": "65",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, wifi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-de-peregrinos-cuatro-cantonhtml?aid=1499405",
        "marker": "Private.png",
        "image": "1264-80722.webp",
        "email": "cuatrocantones@hotmail.com",
        "street": "Hipolito L\u00f3pez Bernal, 10",
        "phone": "947 580 591, 686 906 492"
    }
}, {
    "node": {
        "title": "Hotel Rural Verdeancho",
        "latitude": "42.420211",
        "longitude": "-3.188296",
        "nid": "11723",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "marker": "HotelRural.png",
        "email": "info@casaverdeancho.com",
        "street": "El Corro, 11",
        "phone": "659 484 584"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos Caminante",
        "latitude": "42.420607",
        "longitude": "-3.189147",
        "nid": "3154",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "22",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3154-61225.webp",
        "email": "  g.caminante@hotmail.com",
        "street": "Mayor, 36",
        "phone": "947 580 231, 656 873 927"
    }
}, {
    "node": {
        "title": "El Salto",
        "latitude": "42.414158",
        "longitude": "-3.201978",
        "nid": "12069",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "beds": "24",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "marker": "Private.png",
        "image": "12069-136200.webp",
        "email": "saltoenbelorado@gmail.com",
        "street": "Los Cauces",
        "phone": "669 415 639, 670 667 778"
    }
}, {
    "node": {
        "title": "Hotel Jacobeo",
        "latitude": "42.418891",
        "longitude": "-3.191539",
        "nid": "9988",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "image": "9988-134911.webp",
        "email": "contacto@hoteljacobeo.net",
        "street": "Burgos, 3",
        "phone": "947 580 010"
    }
}, {
    "node": {
        "title": "Refugio parroquial de Belorado - Santa Maria y San Pedro",
        "latitude": "42.420619",
        "longitude": "-3.188435",
        "nid": "1263",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAY - NOV",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "image": "1263-61221.webp",
        "street": "Barrio del Corro",
        "phone": "947 580 085"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n To\u00f1i",
        "latitude": "42.419407",
        "longitude": "-3.192745",
        "nid": "11722",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bike": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "reserve": "Yes, and preferred",
        "washer": "Yes, 6\u20ac",
        "marker": "Pension.png",
        "image": "11722-135285.webp",
        "email": "informacion@pensiontoni.com",
        "street": "Redecilla del Campo, 7",
        "phone": "616 010 808, 947 580 525"
    }
}, {
    "node": {
        "title": "Albergue A Santiago",
        "latitude": "42.418049",
        "longitude": "-3.182681",
        "nid": "1265",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\/7\u20ac",
        "beds": "89",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - OCT",
        "open": "7:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-a-santiago-belorado.html?aid=1499405",
        "marker": "Private.png",
        "image": "1265-61220.webp",
        "email": "albergueasantiago@hotmail.com",
        "street": "Camino de los Paules",
        "phone": "947 567 164, 677 811 847"
    }
}, {
    "node": {
        "title": "Albergue Parroquial de Tosantos",
        "latitude": "42.413667",
        "longitude": "-3.242555",
        "nid": "1266",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "30",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "No, but available at the bar across the street",
        "kitchen": "Yes, but all meals are communal",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "image": "1266-61226.webp",
        "street": "Santa Marina",
        "phone": "947 580 371"
    }
}, {
    "node": {
        "title": "Albergue Los Arancones",
        "latitude": "42.414388",
        "longitude": "-3.242496",
        "nid": "11867",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "email": "jessi.chope@hotmail.com",
        "street": "La Iglesia",
        "phone": "947 581 485, 693 299 063"
    }
}, {
    "node": {
        "title": "Albergue de San Roque",
        "latitude": "42.406276",
        "longitude": "-3.261888",
        "nid": "3550",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "open": "8:00",
        "pilgrim": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Municipal.png",
        "image": "3550-61227.webp",
        "email": "alberguesanroque@hotmail.es",
        "street": "Plaza Mayor",
        "phone": "680 501 887, 696 833 888"
    }
}, {
    "node": {
        "title": "Albergue La Campana de Espinosa del Camino",
        "latitude": "42.405442",
        "longitude": "-3.280335",
        "nid": "1268",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "Donativo",
        "beds": "10",
        "bike": "Yes",
        "close": "22:00",
        "cal": "ALL YEAR**",
        "open": "11:00",
        "marker": "Private.png",
        "image": "1268-61228.webp",
        "street": "Espinosa del Camino",
        "phone": "678 479 361"
    }
}, {
    "node": {
        "title": "Albergue Espinosa del Camino",
        "latitude": "42.405512",
        "longitude": "-3.279403",
        "nid": "11868",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "10",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "street": "Barruelo, 23",
        "phone": "630 104 922,  630 104 925"
    }
}, {
    "node": {
        "title": "Casa Rural La Alpargater\u00eda",
        "latitude": "42.390044",
        "longitude": "-3.308313",
        "nid": "11730",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-alpargateria.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11730-136801.webp",
        "street": "Mayor, 2",
        "phone": "686 040 884"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos San Antonio Abad",
        "latitude": "42.388251",
        "longitude": "-3.309743",
        "nid": "1269",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\/10\u20ac",
        "beds": "40",
        "barrest": "Yes, open at 7am",
        "bike": "Yes, washing too.",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "½ MAR - ½ NOV",
        "open": "11:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/san-anta3n-abad.html?aid=1499405",
        "marker": "Private.png",
        "image": "1269-61230.webp",
        "email": "hotelsanantonabad@gmail.com",
        "street": "Hospital, 4 ",
        "phone": "947 582 150",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El P\u00e1jaro",
        "latitude": "42.390250",
        "longitude": "-3.308034",
        "nid": "11729",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-el-pajaro.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11729-136800.webp",
        "street": "la Plaza, 2",
        "phone": "947 582 029"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Jomer",
        "latitude": "42.386375",
        "longitude": "-3.309128",
        "nid": "11731",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Mayor, 52",
        "phone": "947 582 146"
    }
}, {
    "node": {
        "title": "Albergue de Villafranca Montes de Oca",
        "latitude": "42.389170",
        "longitude": "-3.308666",
        "nid": "6975",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "7\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "marker": "Municipal.png",
        "image": "6975-61229.webp",
        "email": "alberguemunicipalvillafrancamo@gmail.com",
        "street": "Mayor",
        "phone": "691 801 211"
    }
}, {
    "node": {
        "title": "Albergue del Monasterio de San Juan de Ortega",
        "latitude": "42.375983",
        "longitude": "-3.437219",
        "nid": "1271",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "7\u20ac",
        "beds": "70",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "1271-61231.webp",
        "email": "monasteriosanjuandeortega@gmail.com",
        "street": "La Iglesia, 1",
        "phone": "947 560 438"
    }
}, {
    "node": {
        "title": "Hotel Rural La Henera",
        "latitude": "42.375717",
        "longitude": "-3.435802",
        "nid": "11728",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "marker": "HotelRural.png",
        "email": "manuel@sanjuandeortega.com",
        "street": "La Iglesia, 4",
        "phone": "606 198 734"
    }
}, {
    "node": {
        "title": "Albergue San Rafael",
        "latitude": "42.369705",
        "longitude": "-3.478318",
        "nid": "1273",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "image": "1273-134926.webp",
        "email": "alberguesanrafael2010@hotmail.com",
        "street": "Medio, 19",
        "phone": "947 430 392",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue El Pajar de Ag\u00e9s",
        "latitude": "42.369848",
        "longitude": "-3.479168",
        "nid": "1272",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes",
        "cal": "MAR - ½ NOV",
        "open": "10:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1272-134917.webp",
        "email": "info@elpajardeages.es",
        "street": "Paralela Medio, 12",
        "phone": "947 400 629, 699 273 856"
    }
}, {
    "node": {
        "title": "Albergue La Taberna de Ag\u00e9s",
        "latitude": "42.369419",
        "longitude": "-3.479197",
        "nid": "5540",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "36",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, computers",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "5540-134918.webp",
        "email": "info@alberguedeages.com",
        "street": "Medio, 21",
        "phone": "947 400 697, 660 044 575"
    }
}, {
    "node": {
        "title": "Albergue el Peregrino de Atapuerca",
        "latitude": "42.376189",
        "longitude": "-3.506935",
        "nid": "1275",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "48",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "13:00",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1275-52841.webp",
        "email": "rocio@albergueatapuerca.com",
        "street": "Carretera, 105",
        "phone": "661 580 882"
    }
}, {
    "node": {
        "title": "Albergue de Olmos de Atapuerca",
        "latitude": "42.392049",
        "longitude": "-3.533145",
        "nid": "6976",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "7\u20ac",
        "beds": "24",
        "bike": "Yes",
        "wifi": "Yes, WiFi nearby",
        "kitchen": "Yes",
        "cal": "MAR - NOV",
        "pilgrim": "Yes",
        "stove": "Yes",
        "marker": "Municipal.png",
        "street": "La Iglesia",
        "phone": "633 586 876"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El Palomar",
        "latitude": "42.374912",
        "longitude": "-3.506697",
        "nid": "11747",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Revilla, 22",
        "phone": "947 400 675"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos La Hutte",
        "latitude": "42.377727",
        "longitude": "-3.507171",
        "nid": "1276",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\u20ac",
        "beds": "18",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "marker": "Private.png",
        "image": "1276-134924.webp",
        "email": "papasol@burgosturismorural.com",
        "phone": "947 430 320"
    }
}, {
    "node": {
        "title": "Casa Rural Papasol",
        "latitude": "42.377669",
        "longitude": "-3.506982",
        "nid": "9987",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/centro-de-turismo-rural-papasol.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9987-134922.webp",
        "email": "papasol@burgosturismorural.com ",
        "street": "Enmedio, 36",
        "phone": "947 430 320"
    }
}, {
    "node": {
        "title": "Albergue V\u00eda Minera",
        "latitude": "42.359923",
        "longitude": "-3.559700",
        "nid": "8148",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\/8\u20ac",
        "beds": "42",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "10:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "8148-133746.webp",
        "email": "albergueviaminera@gmail.com",
        "street": "Medio, 19",
        "phone": "652 941 647"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Carde\u00f1uela Riopico",
        "latitude": "42.359256",
        "longitude": "-3.560079",
        "nid": "9892",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "cal": "ALL YEAR**",
        "pilgrim": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9892-134919.webp",
        "street": "Carde\u00f1uela Riopico",
        "phone": "646 249 597"
    }
}, {
    "node": {
        "title": "Albergue Santa Fe",
        "latitude": "42.358923",
        "longitude": "-3.559379",
        "nid": "9713",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "23",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:55",
        "wifi": "Yes, WiFi and computer free",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9713-134391.webp",
        "email": "alberguesantafe@hotmail.com",
        "street": "Los Huertos, 2",
        "phone": "947 560 722, 626 352 269"
    }
}, {
    "node": {
        "title": "Casa Rural La Carde\u00f1uela",
        "latitude": "42.359088",
        "longitude": "-3.562378",
        "nid": "11748",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "V\u00eda Minera",
        "phone": "947 210 479"
    }
}, {
    "node": {
        "title": "Albergue El Peregrino",
        "latitude": "42.360181",
        "longitude": "-3.584550",
        "nid": "12216",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "albergueorbanejariopico@gmail.com",
        "street": "Principal, 1",
        "phone": "947 430 980, 618 025 044"
    }
}, {
    "node": {
        "title": "Casa Rural Fortaleza",
        "latitude": "42.360352",
        "longitude": "-3.582667",
        "nid": "11749",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "close": "20:00",
        "wifi": "Yes, WiFi",
        "cal": "MAY - SEPT",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "CasaRural.png",
        "image": "11749-136384.webp",
        "email": "correo@casaruralfortaleza.com",
        "street": "Principal, 31",
        "phone": " 678 116 570, 947 225 354"
    }
}, {
    "node": {
        "title": "Hotel Versus",
        "latitude": "42.343586",
        "longitude": "-3.625861",
        "nid": "11732",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/h-m-versus.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11732-135348.webp",
        "email": "info@hotelversus.com",
        "street": "Iglesia, 8",
        "phone": "947 474 977"
    }
}, {
    "node": {
        "title": "Hostal Hilton",
        "latitude": "42.352243",
        "longitude": "-3.671368",
        "nid": "11743",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-hilton.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11743-135344.webp",
        "street": "Vitoria, 165",
        "phone": "947 225 116"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Santiago",
        "latitude": "42.349350",
        "longitude": "-3.674500",
        "nid": "11738",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-santiago.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11738-135364.webp",
        "street": "Santiago Ap\u00f3stol, 8",
        "phone": "947 046 230"
    }
}, {
    "node": {
        "title": "Hostal Manj\u00f3n",
        "latitude": "42.341484",
        "longitude": "-3.696661",
        "nid": "11733",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-manjon.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11733-135342.webp",
        "street": "Gran Teatro, 1",
        "phone": "947 208 689"
    }
}, {
    "node": {
        "title": "Hostel Burgos",
        "latitude": "42.338278",
        "longitude": "-3.701206",
        "nid": "12172",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "16.50\u20ac",
        "beds": "68",
        "barrest": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "24h",
        "reserve": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/abc-conde-de-miranda.html?aid=1499405",
        "marker": "Private.png",
        "image": "12172-136802.webp",
        "email": "info@hostelburgos.com",
        "street": "Miranda, 4",
        "phone": "947 250 801"
    }
}, {
    "node": {
        "title": "Hotel Puerta Romeros",
        "latitude": "42.341598",
        "longitude": "-3.727483",
        "nid": "11740",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/puerta-romeros.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11740-135347.webp",
        "email": "reservas@puertaromeros.com",
        "street": "San Amaro, 2",
        "phone": "947 462 012"
    }
}, {
    "node": {
        "title": "Hostal riMboMbin",
        "latitude": "42.340844",
        "longitude": "-3.702873",
        "nid": "11735",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-rimbombin.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11735-135340.webp",
        "street": "Sombrerer\u00eda, 6",
        "phone": "947 261 200"
    }
}, {
    "node": {
        "title": "Albergue Ema\u00fas",
        "latitude": "42.339088",
        "longitude": "-3.689338",
        "nid": "1279",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "20",
        "bike": "Yes",
        "close": "20:00",
        "family": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - OCT",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "street": "San Pedro de Carde\u00f1a, 31",
        "phone": "947 252 851"
    }
}, {
    "node": {
        "title": "Hostal V\u00eda L\u00e1ctea",
        "latitude": "42.342674",
        "longitude": "-3.733138",
        "nid": "11742",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/via-lactea.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11742-135345.webp",
        "street": "Villadiego, 16",
        "phone": "947 463 211"
    }
}, {
    "node": {
        "title": "Hotel Cord\u00f3n",
        "latitude": "42.341796",
        "longitude": "-3.698600",
        "nid": "11737",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/cordon.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11737-135338.webp",
        "email": "info@hotelcordon.com",
        "street": "La Puebla, 6",
        "phone": "947 265 000"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Bella Vista",
        "latitude": "42.342699",
        "longitude": "-3.736818",
        "nid": "10891",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "info@bella-vista.es",
        "street": "Villadiego, 19",
        "phone": "947 460 222"
    }
}, {
    "node": {
        "title": "Hostal Lar",
        "latitude": "42.342213",
        "longitude": "-3.696436",
        "nid": "11744",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-lar.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11744-135343.webp",
        "email": "reservas@hostallar.es",
        "street": "Cardenal Benlloch, 1",
        "phone": "947 209 655"
    }
}, {
    "node": {
        "title": "Hotel Mes\u00f3n del Cid",
        "latitude": "42.339881",
        "longitude": "-3.705993",
        "nid": "11739",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/mesondelcid.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11739-135363.webp",
        "email": "mesondelcid@mesondelcid.es",
        "street": "Plaza Santa Mar\u00eda, 8",
        "phone": "947 208 715"
    }
}, {
    "node": {
        "title": "Hotel Alda Entrearcos",
        "latitude": "42.341371",
        "longitude": "-3.702841",
        "nid": "11734",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/entrearcos.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11734-135341.webp",
        "email": "entrearcos@aldahostels.es",
        "street": "Paloma, 4",
        "phone": "947 252 911"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Burgos",
        "latitude": "42.342289",
        "longitude": "-3.703927",
        "nid": "1278",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "150",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1278-134950.webp",
        "email": "asociacion@caminosantiagoburgos.com",
        "street": "Fernan Gonz\u00e1lez, 28",
        "phone": "947 460 922"
    }
}, {
    "node": {
        "title": "Hotel Abad\u00eda Camino de Santiago",
        "latitude": "42.342595",
        "longitude": "-3.732076",
        "nid": "11741",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/abadia-camino-santiago.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11741-135346.webp",
        "email": "info@hotelabadiaburgos.com",
        "street": "Villadiego, 10",
        "phone": "947 040 404"
    }
}, {
    "node": {
        "title": "Hotel Abba Burgos",
        "latitude": "42.340991",
        "longitude": "-3.708704",
        "nid": "11736",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/abbaburgos.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11736-135339.webp",
        "email": "abba-burgos@abbahoteles.com",
        "street": "Fern\u00e1n Gonzalez, 72",
        "phone": "947 001 100"
    }
}, {
    "node": {
        "title": "Albergue Divina Pastora",
        "latitude": "42.341744",
        "longitude": "-3.701985",
        "nid": "1280",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "5\u20ac",
        "beds": "16",
        "close": "21:00",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "washer": "Yes",
        "marker": "Association.png",
        "image": "1280-134948.webp",
        "street": "Lain Calvo, 10",
        "phone": "947 207 952"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Mary",
        "latitude": "42.349363",
        "longitude": "-3.815232",
        "nid": "11750",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Pozas",
        "phone": "947 451 125"
    }
}, {
    "node": {
        "title": "Albergue La F\u00e1brica",
        "latitude": "42.347934",
        "longitude": "-3.813590",
        "nid": "9839",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "32, 14 in the albergue.",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:30",
        "reserve": "Yes",
        "washer": "Yes, 2.5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-de-turismo-superior-quot-la-fabrica-quot.html?aid=1499405",
        "marker": "Private.png",
        "image": "9839-135337.webp",
        "email": "cristina@alberguelafabrica.com",
        "street": "Camino de la F\u00e1brica, 27",
        "phone": "646 000 908"
    }
}, {
    "node": {
        "title": "Albergue La Casa de Beli",
        "latitude": "42.349472",
        "longitude": "-3.815853",
        "nid": "12133",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casa-de-beli.html?aid=1499405",
        "marker": "Private.png",
        "image": "12133-136825.webp",
        "email": "lacasadebeli@gmail.com",
        "street": "General Yag\u00fce, 16",
        "phone": "947 451 234, 629 351 675"
    }
}, {
    "node": {
        "title": "Albergue de Tardajos",
        "latitude": "42.348165",
        "longitude": "-3.817002",
        "nid": "1282",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "18",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "MAR - NOV",
        "open": "15:00",
        "pilgrim": "Yes",
        "marker": "Municipal.png",
        "image": "1282-61232.webp",
        "street": "Asunci\u00f3n",
        "phone": "947 451 189"
    }
}, {
    "node": {
        "title": "Albergue Liberanos Domine",
        "latitude": "42.339855",
        "longitude": "-3.835282",
        "nid": "1284",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1284-61233.webp",
        "email": "clementinadelatorre@gmail.com",
        "street": "Francisco Ribera, 6",
        "phone": "695 116 901"
    }
}, {
    "node": {
        "title": "Casa Rural de Sol a Sol",
        "latitude": "42.338669",
        "longitude": "-3.923806",
        "nid": "11752",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "close": "24:00",
        "cal": "½ APR - ½ OCT",
        "open": "10:00",
        "washer": "Yes, 3\u20ac",
        "marker": "CasaRural.png",
        "email": "info@burgoscasarural.com",
        "street": "Cantarranas, 7",
        "phone": "649 876 091"
    }
}, {
    "node": {
        "title": "Albergue El Alfar de Hornillos",
        "latitude": "42.338421",
        "longitude": "-3.924042",
        "nid": "9648",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:30",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9648-134549.webp",
        "email": "elalfardehornillos@gmail.com",
        "street": "Cantarranas, 8",
        "phone": "619 235 930, 654 263 857"
    }
}, {
    "node": {
        "title": "Casa Rural La Casa del Abuelo",
        "latitude": "42.338192",
        "longitude": "-3.924905",
        "nid": "11751",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "Real, 44",
        "phone": "661 869 618"
    }
}, {
    "node": {
        "title": "Albergue de Hornillos del Camino",
        "latitude": "42.338699",
        "longitude": "-3.926032",
        "nid": "1285",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - JAN",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1285-134441.webp",
        "email": "hornillos.alberguemunicipal@gmail.com",
        "street": "Plaza de la Iglesia",
        "phone": "689 784 681"
    }
}, {
    "node": {
        "title": "Hornillos Meeting Point",
        "latitude": "42.338670",
        "longitude": "-3.924226",
        "nid": "9853",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hornillos-meeting-point.html?aid=1499405",
        "marker": "Private.png",
        "image": "9853-136581.webp",
        "email": "info@hornillosmeetingpoint.com",
        "street": "Cantarranas, 3",
        "phone": "608 113 599"
    }
}, {
    "node": {
        "title": "Albergue Arroyo de San Bol",
        "latitude": "42.323560",
        "longitude": "-3.990111",
        "nid": "9649",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "12",
        "close": "19:00",
        "family": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - ½ OCT",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "marker": "Private.png",
        "image": "9649-134550.webp",
        "email": "lilianaborroto@yahoo.es",
        "phone": "606 893 407"
    }
}, {
    "node": {
        "title": "Albergue Juan de Yepes",
        "latitude": "42.312718",
        "longitude": "-4.043151",
        "nid": "11637",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "54",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - ½ OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "11637-134969.webp",
        "email": "juandeyepes@santa-brigida.es",
        "street": "Real, 1",
        "phone": "638 938 546"
    }
}, {
    "node": {
        "title": "Casa Rural El Descanso",
        "latitude": "42.312732",
        "longitude": "-4.044326",
        "nid": "11754",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "info@casaeldescanso.com",
        "street": "Real, 16",
        "phone": "947 377 035"
    }
}, {
    "node": {
        "title": "Mes\u00f3n Albergue El Puntido",
        "latitude": "42.312724",
        "longitude": "-4.044410",
        "nid": "1288",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - DEC",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1288-52313.webp",
        "email": "contacto@puntido.com",
        "street": "La Iglesia, 6",
        "phone": "947 378 597"
    }
}, {
    "node": {
        "title": "Albergue Santa Br\u00edgida",
        "latitude": "42.312624",
        "longitude": "-4.044889",
        "nid": "6981",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "16",
        "barrest": "Yes, only for pilgrims staying at the albergue",
        "bike": "Yes",
        "close": "22:30",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "6:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6981-134965.webp",
        "email": "reservas@alberguesantabrigida.com",
        "street": "Real, 15",
        "phone": "628 927 317, 659 020 226"
    }
}, {
    "node": {
        "title": "Hostal Fuentestrella",
        "latitude": "42.312677",
        "longitude": "-4.043942",
        "nid": "11753",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "fuentestrella@yahoo.es",
        "street": "Iglesia, 6",
        "phone": "947 377 261"
    }
}, {
    "node": {
        "title": "Albergue de Hontanas Hospital de San Juan",
        "latitude": "42.312713",
        "longitude": "-4.044753",
        "nid": "1289",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "55",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "marker": "Municipal.png",
        "image": "1289-134963.webp",
        "email": "hospitalsanjuan.hontanas@gmail.com",
        "street": "Real, 26",
        "phone": "947 677 021, 947 377 436"
    }
}, {
    "node": {
        "title": "Albergue de San Ant\u00f3n",
        "latitude": "42.292575",
        "longitude": "-4.098977",
        "nid": "1290",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "Donativo",
        "beds": "12",
        "bike": "Yes",
        "close": "20:00",
        "family": "Yes",
        "cal": "MAY - SEPT",
        "open": "8:00",
        "pilgrim": "Yes",
        "marker": "Private.png",
        "image": "1290-61251.webp",
        "email": "campoovidio@gmail.com",
        "street": "Carretera de Hontanas-Castrojeriz",
        "phone": "none"
    }
}, {
    "node": {
        "title": "Albergue Ori\u00f3n",
        "latitude": "42.291445",
        "longitude": "-4.130146",
        "nid": "12173",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "22",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "albergueorion2016@hotamil.com",
        "street": "Colegiata, 28",
        "phone": " 649 481 609, 672 580 959"
    }
}, {
    "node": {
        "title": "Hotel Iacobus",
        "latitude": "42.287986",
        "longitude": "-4.140399",
        "nid": "9983",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/iacobus.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9983-134966.webp",
        "email": "hotel@iacobuscastrojeriz.com",
        "street": "Puerta del Monte, 3",
        "phone": "947 378 647, 607 922 127"
    }
}, {
    "node": {
        "title": "Albergue de San Esteban",
        "latitude": "42.288898",
        "longitude": "-4.140558",
        "nid": "1292",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo\/5 (winter)\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, computer",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Municipal.png",
        "image": "1292-61249.webp",
        "email": "sanestebancastrojeriz@gmail.com",
        "street": "Plaza Mayor",
        "phone": "947 377 001"
    }
}, {
    "node": {
        "title": "Hotel La Posada de Castrojeriz",
        "latitude": "42.288179",
        "longitude": "-4.141218",
        "nid": "9985",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-posada-de-castrojeriz.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9985-135336.webp",
        "email": "info@laposadadecastrojeriz.es",
        "street": "Landelino Tardajos, 5",
        "phone": "947 378 610"
    }
}, {
    "node": {
        "title": "Albergue and Camping de Castrojeriz",
        "latitude": "42.291033",
        "longitude": "-4.131811",
        "nid": "6982",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "35",
        "bike": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi and computer",
        "cal": "½ MAR - ½ NOV",
        "open": "24h",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6982-61247.webp",
        "email": "info@campingcamino.com",
        "street": "Virgen del Manzano",
        "phone": "947 377 255, 658 966 743"
    }
}, {
    "node": {
        "title": "Albergue Rosal\u00eda",
        "latitude": "42.288370",
        "longitude": "-4.141886",
        "nid": "12107",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - OCT",
        "open": "11:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-rosalia-47-pilgrim-hostel.html?aid=1499405",
        "marker": "Private.png",
        "image": "12107-136518.jpeg",
        "email": "info@alberguerosalia.com",
        "street": "Cord\u00f3n, 2",
        "phone": "947 373 714",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal El Mes\u00f3n de Castrojeriz",
        "latitude": "42.288259",
        "longitude": "-4.141846",
        "nid": "9982",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-mesa3n-de-castrojeriz.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9982-135334.webp",
        "street": "Cord\u00f3n, 1",
        "phone": "947 377 400"
    }
}, {
    "node": {
        "title": "Albergue Casa Nostra",
        "latitude": "42.288625",
        "longitude": "-4.135250",
        "nid": "1291",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6.50\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "??? - DEC",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes, the day before by phone",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "image": "1291-61248.webp",
        "email": "encastrojeriz@hotmail.com",
        "street": "Real de Oriente, 52",
        "phone": "947 377 493"
    }
}, {
    "node": {
        "title": "Hotel La Cachava",
        "latitude": "42.289572",
        "longitude": "-4.133735",
        "nid": "9984",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-cachava.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9984-135333.webp",
        "street": "Real, 93",
        "phone": "947 378 547"
    }
}, {
    "node": {
        "title": "Refugio Tradicional de San Juan",
        "latitude": "42.289123",
        "longitude": "-4.142599",
        "nid": "1293",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "Donativo",
        "beds": "28",
        "bike": "Yes",
        "close": "22:30",
        "cal": "APR - OCT",
        "open": "15:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Association.png",
        "image": "1293-52171.webp",
        "email": "castrojeriz.refugio@gmail.com",
        "street": "Cord\u00f3n",
        "phone": "947 377 400"
    }
}, {
    "node": {
        "title": "Posada Emebed",
        "latitude": "42.288195",
        "longitude": "-4.139802",
        "nid": "9986",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/emebed-posada.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9986-135335.webp",
        "street": "Plaza Mayor, 5",
        "phone": "947 377 268"
    }
}, {
    "node": {
        "title": "Albergue Ultreia",
        "latitude": "42.289464",
        "longitude": "-4.133989",
        "nid": "9741",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "13:00",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "9741-134967.webp",
        "email": "albergue.ultreia.castrojeriz@gmail.com",
        "street": "Real de Oriente, 77",
        "phone": "947 378 640"
    }
}, {
    "node": {
        "title": "Albergue de San Nicolas",
        "latitude": "42.278896",
        "longitude": "-4.242911",
        "nid": "1295",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "12",
        "close": "Night",
        "family": "Yes",
        "cal": "JUN - SEPT",
        "open": "Morning",
        "pilgrim": "Yes",
        "marker": "Parochial.png",
        "image": "1295-133364.webp",
        "street": "Puente Fitero",
        "phone": "947 377 359"
    }
}, {
    "node": {
        "title": "Albergue Hogar del Peregrino",
        "latitude": "42.288259",
        "longitude": "-4.258020",
        "nid": "12174",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "7",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "email": "alberguehogardelperegrino@hotmail.com",
        "street": "Santa Mar\u00eda, 17",
        "phone": "979 151 866, 616 629 353"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Itero del Castillo",
        "latitude": "42.288771",
        "longitude": "-4.244456",
        "nid": "1294",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "7",
        "bike": "Yes",
        "close": "24h",
        "cal": "ALL YEAR",
        "open": "24h",
        "pilgrim": "Yes",
        "marker": "Municipal.png",
        "email": "albergueiterocastillo@gmail.com",
        "street": "Plaza del Ayuntamiento",
        "phone": "947 377 357"
    }
}, {
    "node": {
        "title": "Albergue Puente Fitero",
        "latitude": "42.286466",
        "longitude": "-4.255723",
        "nid": "1297",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "12",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "10:30",
        "pilgrim": "Yes",
        "stove": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-puente-fitero.html?aid=1499405",
        "marker": "Municipal.png",
        "image": "1297-61243.webp",
        "street": "Plaza Virgen del Pilar",
        "phone": "979 151 826, 605 034 347"
    }
}, {
    "node": {
        "title": "Albergue de Itero La Mochilla",
        "latitude": "42.287382",
        "longitude": "-4.258170",
        "nid": "1296",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "20",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1296-61240.webp",
        "email": "lamochilaitero@gmail.com",
        "street": "Santa Ana, 3",
        "phone": "979 151 781",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue En el Camino",
        "latitude": "42.258421",
        "longitude": "-4.346455",
        "nid": "1299",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "48",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, computers",
        "cal": "MAY - OCT",
        "open": "24h",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1299-61236.webp",
        "email": "boadillaman@hotmail.com",
        "street": "Francos, 3",
        "phone": "979 810 284, 619 105 168"
    }
}, {
    "node": {
        "title": "Albergue Titas",
        "latitude": "42.260235",
        "longitude": "-4.348143",
        "nid": "9650",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "12",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac including dry",
        "marker": "Private.png",
        "image": "9650-134554.webp",
        "email": "alberguetitas@hotmail.com",
        "street": "Mayor, 7 ",
        "phone": "979 810 776"
    }
}, {
    "node": {
        "title": "Albergue de Boadilla del Camino",
        "latitude": "42.259729",
        "longitude": "-4.346159",
        "nid": "1298",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "3\u20ac",
        "beds": "12",
        "close": "22:00",
        "cal": "Varies",
        "open": "12:00",
        "pilgrim": "Yes",
        "marker": "Municipal.png",
        "street": "Escuelas",
        "phone": "979 810 390"
    }
}, {
    "node": {
        "title": "Albergue Putzu",
        "latitude": "42.261029",
        "longitude": "-4.344605",
        "nid": "3548",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "16",
        "bike": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "cal": "ALL YEAR**",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "phone": "none"
    }
}, {
    "node": {
        "title": "Hotel En el Camino",
        "latitude": "42.258274",
        "longitude": "-4.346858",
        "nid": "11638",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/en-el-camino.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11638-134970.webp",
        "email": "boadillaman@hotmail.com",
        "street": "Francos, 3",
        "phone": "979 810 284, 619 105 168"
    }
}, {
    "node": {
        "title": "Hostal Camino de Santiago",
        "latitude": "42.269172",
        "longitude": "-4.404659",
        "nid": "9981",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-camino-de-santiago.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9981-134960.webp",
        "street": "Ej\u00e9rcito Espa\u00f1ol, 26",
        "phone": "979 810 053, 979 810 282"
    }
}, {
    "node": {
        "title": "Albergue Estrella del Camino",
        "latitude": "42.269476",
        "longitude": "-4.404518",
        "nid": "3153",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computers",
        "cal": "MAR - OCT",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3153-52200.webp",
        "email": "alberguestrella@terra.es",
        "street": "Ej\u00e9rcito Espa\u00f1ol",
        "phone": "979 810 399, 653 751 582"
    }
}, {
    "node": {
        "title": "Hotel San Mart\u00edn",
        "latitude": "42.267039",
        "longitude": "-4.406712",
        "nid": "11758",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "info@hotelsanmartin.es",
        "street": "Plaza San Mart\u00edn, 7",
        "phone": "979 810 000"
    }
}, {
    "node": {
        "title": "Albergue Betania",
        "latitude": "42.269447",
        "longitude": "-4.404125",
        "nid": "9651",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "Donativo",
        "beds": "5",
        "cal": "DEC - FEB",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "9651-131600.webp",
        "email": "betaniafromista@gmail.com",
        "street": "Ej\u00e9rcito Espa\u00f1ol, 26",
        "phone": "638 846 043"
    }
}, {
    "node": {
        "title": " Pension La Via Lactea",
        "latitude": "42.266100",
        "longitude": "-4.406502",
        "nid": "12182",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-la-via-lactea.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12182-136777.webp",
        "street": "Julio Senador, 1",
        "phone": "696 009 803"
    }
}, {
    "node": {
        "title": "Hostal San Pedro",
        "latitude": "42.268306",
        "longitude": "-4.404724",
        "nid": "9980",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-san-pedro.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9980-134958.webp",
        "street": "Ej\u00e9rcito Espa\u00f1ol, 8",
        "phone": "979 810 409"
    }
}, {
    "node": {
        "title": "Albergue de Fromista",
        "latitude": "42.267290",
        "longitude": "-4.407148",
        "nid": "1300",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "8\u20ac",
        "beds": "56",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR**",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Municipal.png",
        "image": "1300-134964.webp",
        "email": "carmen-hospitalero@live.com",
        "street": "Hospital",
        "phone": "979 811 089, 686 579 702 - Carmen"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Mabel",
        "latitude": "42.267951",
        "longitude": "-4.405616",
        "nid": "11757",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Plaza de Tuy, 2",
        "phone": "674 495 676"
    }
}, {
    "node": {
        "title": "Albergue Canal de Castilla",
        "latitude": "42.264610",
        "longitude": "-4.402709",
        "nid": "3702",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15*\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "MAR - NOV",
        "open": "11:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "email": "info@albergueperegrinosfromista.com",
        "street": "La Estaci\u00f3n, 2",
        "phone": "979 810 193"
    }
}, {
    "node": {
        "title": "Hostal El Ap\u00f3stol",
        "latitude": "42.269003",
        "longitude": "-4.404887",
        "nid": "11759",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-el-apostol.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11759-135362.webp",
        "street": "Ej\u00e9rcito Espa\u00f1ol, 5",
        "phone": "979 810 255"
    }
}, {
    "node": {
        "title": "Hotel Do\u00f1a Mayor",
        "latitude": "42.268997",
        "longitude": "-4.403229",
        "nid": "9979",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "close": "21:00",
        "cal": "HOLY - NOV",
        "open": "8:00",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/dona-mayor.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9979-136838.webp",
        "email": "reservas@hoteldonamayor.com",
        "street": "Francesa, 31",
        "phone": "979 810 588",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue de Poblaci\u00f3n de Campos",
        "latitude": "42.269077",
        "longitude": "-4.444970",
        "nid": "1301",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "4\u20ac",
        "beds": "18",
        "bike": "Yes",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1301-134978.webp",
        "email": "info@amanecerencampos.net",
        "street": "Paseo del Cementerio",
        "phone": "979 811 099, 685 510 020"
    }
}, {
    "node": {
        "title": "Albergue La Finca",
        "latitude": "42.268025",
        "longitude": "-4.440590",
        "nid": "12134",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "email": "info@alberguelafinca.es",
        "street": "Carretera 980, km. 16",
        "phone": "979 067 028"
    }
}, {
    "node": {
        "title": "Casa Rural Amanecer en Campos",
        "latitude": "42.268641",
        "longitude": "-4.445663",
        "nid": "9978",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/amanecer-en-campos.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9978-134976.webp",
        "email": "info@amanecerencampos.com",
        "street": "Fuente Nueva, 5",
        "phone": "979 811 099, 625 469 326"
    }
}, {
    "node": {
        "title": "Casa Rural La Casona de Do\u00f1a Petra",
        "latitude": "42.297467",
        "longitude": "-4.500558",
        "nid": "9977",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casona-de-dona-petra.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9977-134974.webp",
        "street": "Ram\u00f3n y Cajal",
        "phone": "979 065 978, 639 104 547"
    }
}, {
    "node": {
        "title": "Albergue Amanecer",
        "latitude": "42.297735",
        "longitude": "-4.498880",
        "nid": "3152",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\u20ac",
        "beds": "36",
        "barrest": "Yes",
        "bike": "Yes",
        "family": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "cal": "MAR - OCT",
        "fridge": "Yes",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "3152-52832.webp",
        "email": "albergueamanecer@gmail.com",
        "street": "Jos\u00e9 Antonio, 2",
        "phone": "662 279 102",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": " Casa abuela Gaspara I",
        "latitude": "42.317205",
        "longitude": "-4.543724",
        "nid": "12183",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-abuela-gaspara-i.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12183-136778.webp",
        "street": "Cantarranas, 20",
        "phone": "665 961 850"
    }
}, {
    "node": {
        "title": "Albergue Villalc\u00e1zar de Sirga",
        "latitude": "42.316208",
        "longitude": "-4.542709",
        "nid": "1302",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "20",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "APR - OCT",
        "open": "16:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "marker": "Municipal.png",
        "image": "1302-134972.webp",
        "email": "ayto-villalcazar@dip-palencia.es",
        "street": "Plaza del Peregrino",
        "phone": "979 888 041"
    }
}, {
    "node": {
        "title": "Hostal Infanta Do\u00f1a Leonor",
        "latitude": "42.315695",
        "longitude": "-4.542475",
        "nid": "9975",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "beds": "24",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "APR - ½ OCT",
        "open": "8:00",
        "reserve": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/dona-infanta-leonor.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9975-136925.webp",
        "email": "info@hostal-infantaleonor.com",
        "street": "Condes de Toreno",
        "phone": "979 888 118",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casas Rurales Don Camino",
        "latitude": "42.316184",
        "longitude": "-4.545250",
        "nid": "11756",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "Real, 1",
        "phone": "979 888 163"
    }
}, {
    "node": {
        "title": "Albergue Don Camino",
        "latitude": "42.316097",
        "longitude": "-4.545716",
        "nid": "9872",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "28",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "20:30",
        "wifi": "Yes, WiFi",
        "cal": "APR - OCT",
        "open": "7:00",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9872-134971.webp",
        "email": "aureafederico@hotmail.com",
        "street": "Real, 23",
        "phone": "979 888 163"
    }
}, {
    "node": {
        "title": "Hostal Las Cantigas",
        "latitude": "42.316412",
        "longitude": "-4.542332",
        "nid": "9976",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/las-cantigas.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9976-134973.webp",
        "email": "info@hostallascantigas.com",
        "street": "Doctor Durango, 2",
        "phone": "979 888 027, 636 252 657"
    }
}, {
    "node": {
        "title": "Hostal Santiago",
        "latitude": "42.338132",
        "longitude": "-4.602647",
        "nid": "9971",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "beds": "34",
        "barrest": "Yes",
        "bike": "Yes, washing too",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "½ MAR - OCT",
        "open": "8:30",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-santiago.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9971-134991.webp",
        "email": "caleruco2010@hotmail.com",
        "street": "Plaza de los Regentes, 8",
        "phone": "979 881 052"
    }
}, {
    "node": {
        "title": "Apartamentos Casa T\u00eda Paula",
        "latitude": "42.339653",
        "longitude": "-4.608797",
        "nid": "9973",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-tia-paula-y-casa-de-la-fuente.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9973-134994.webp",
        "street": "Obispo Souto, 8",
        "phone": "979 880 331, 626 443 200"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Parroquia de Santa Maria",
        "latitude": "42.337767",
        "longitude": "-4.601648",
        "nid": "1304",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "54",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "1304-134984.webp",
        "email": "viastellarum@gmail.com",
        "street": "Cl\u00e9rigo Pastor, 6",
        "phone": "979 880 768, 979 880 500"
    }
}, {
    "node": {
        "title": "Hostal Albe",
        "latitude": "42.339715",
        "longitude": "-4.604200",
        "nid": "11755",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Esteban Collantes, 21",
        "phone": "979 880 913"
    }
}, {
    "node": {
        "title": "Monasterio de Santa Clara",
        "latitude": "42.336906",
        "longitude": "-4.598375",
        "nid": "9652",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "23:00",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "11:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "9652-134980.webp",
        "street": "Santa Clara, 1",
        "phone": "979 880 837"
    }
}, {
    "node": {
        "title": "Hostal La Corte",
        "latitude": "42.337149",
        "longitude": "-4.601724",
        "nid": "9972",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "barrest": "Yes",
        "cal": "APR - ½ OCT",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-corte.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9972-134990.webp",
        "email": "contacto@hostallacorte.com",
        "street": "Plaza de los Regentes, 8",
        "phone": "979 880 138",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "El Ed\u00e9n",
        "latitude": "42.334443",
        "longitude": "-4.604259",
        "nid": "1303",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "marker": "Camping.png",
        "email": "administracion@campingeleden.es ",
        "phone": "979 880 200"
    }
}, {
    "node": {
        "title": "Hotel Real Monasterio de San Zoilo",
        "latitude": "42.340291",
        "longitude": "-4.610610",
        "nid": "9974",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/real-monasterio-de-san-zoilo.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9974-134982.webp",
        "email": "hotel@sanzoilo.com",
        "street": "Obispo Souto",
        "phone": "979 880 049"
    }
}, {
    "node": {
        "title": "Albergue Esp\u00edritu Santo",
        "latitude": "42.336277",
        "longitude": "-4.602823",
        "nid": "1305",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "90",
        "close": "22:00",
        "wifi": "Yes, and Wifi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1305-135025.webp",
        "email": "espiritusanto@hijasdelacaridad.org",
        "street": "Plaza de San Juan, 4",
        "phone": "979 880 052"
    }
}, {
    "node": {
        "title": "Hostal Camino Real",
        "latitude": "42.328528",
        "longitude": "-4.804502",
        "nid": "11760",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Trasera Mayor, 8",
        "phone": "979 883 187"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Calzadilla de la Cueza",
        "latitude": "42.329064",
        "longitude": "-4.802402",
        "nid": "9778",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "22:00",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "9778-134992.webp",
        "email": "municipalcalzadilla@outlook.es",
        "street": "Mayor, 1",
        "phone": "670 558 954"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Camino Real",
        "latitude": "42.328923",
        "longitude": "-4.802491",
        "nid": "1307",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "80",
        "barrest": "Yes, around the corner",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1307-134988.webp",
        "email": "cesaracero2004@yahoo.es",
        "street": "Trazera Mayor, 8",
        "phone": "979 883 187",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue La Morena",
        "latitude": "42.354689",
        "longitude": "-4.865602",
        "nid": "11869",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "42",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "8:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-morena-ledigos.html?aid=1499405",
        "marker": "Private.png",
        "image": "11869-135361.webp",
        "email": "info@alberguelamorena.com",
        "street": "Carretera, 3",
        "phone": "626 065 052",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "El Palomar",
        "latitude": "42.353414",
        "longitude": "-4.865755",
        "nid": "1308",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\u20ac",
        "beds": "52",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "FEB - NOV",
        "open": "10:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1308-134986.webp",
        "street": "Ronda de Abajo",
        "phone": "979 883 605, 979 883 614"
    }
}, {
    "node": {
        "title": "Albergue Jacques de Molay",
        "latitude": "42.362505",
        "longitude": "-4.890497",
        "nid": "1310",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\/10\u20ac",
        "beds": "49",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "cal": "FEB - DEC",
        "open": "7:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1310-135001.webp",
        "email": "jacquesdemolay@hotmail.com",
        "street": "La Iglesia, 18",
        "phone": "979 883 679"
    }
}, {
    "node": {
        "title": "Albergue Los Templarios",
        "latitude": "42.364754",
        "longitude": "-4.884866",
        "nid": "1309",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "51",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "MAR - SEPT",
        "open": "7:00",
        "pilgrim": "Yes",
        "reserve": "Yes, held until 15:00",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1309-134399.webp",
        "email": "alberguelostemplarios@hotmail.com",
        "street": "La Carretera",
        "phone": "667 252 279, 979 065 968"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos San Bruno",
        "latitude": "42.360986",
        "longitude": "-4.927794",
        "nid": "6983",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\/9\u20ac",
        "beds": "32",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "cal": "APR - JAN",
        "open": "6:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes, but they are only held until 17:00",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "6983-135006.webp",
        "email": "brunobernoni@gmail.com",
        "street": "Ontan\u00f3n, 9",
        "phone": "979 061 465, 672 629 658",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal Moratinos",
        "latitude": "42.360479",
        "longitude": "-4.925004",
        "nid": "9970",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "price": "10\u20ac",
        "beds": "25",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "8:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-moratinos.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9970-134678.webp",
        "email": "info@hostalmoratinos.es",
        "street": "Real, 12",
        "phone": "638 222 720"
    }
}, {
    "node": {
        "title": "Alberguer\u00eda Laganares",
        "latitude": "42.363514",
        "longitude": "-4.951874",
        "nid": "1311",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "22",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "½ MAR - NOV",
        "open": "7:30",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1311-135005.webp",
        "email": "laganares@yahoo.es",
        "street": "Plaza de la Iglesia",
        "phone": "979 188 142, 629 181 536"
    }
}, {
    "node": {
        "title": "Albergue Jacobeo Juli\u00e1n Campo y Jos\u00e9 Santino Manzano",
        "latitude": "42.255134",
        "longitude": "-4.965288",
        "nid": "5526",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "5\u20ac",
        "beds": "20",
        "bike": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Association.png",
        "email": "info@acaminosantiagovillada.com",
        "street": "Ferial Nuevo",
        "phone": "979 844 005, 610 216 222 - If Closed"
    }
}, {
    "node": {
        "title": "Albergue El Labriego",
        "latitude": "42.370423",
        "longitude": "-5.034007",
        "nid": "12175",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "juangarciacc10@gmail.com",
        "street": "Doctores Bermejo y Calder\u00f3n, 9",
        "phone": "722 115 161, 622 646 136"
    }
}, {
    "node": {
        "title": "Hotel Puerta de Sahag\u00fan",
        "latitude": "42.369595",
        "longitude": "-5.020494",
        "nid": "11647",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/puerta-de-sahagun.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11647-135097.webp",
        "email": "info@hotelpuertadesahagun.com",
        "street": "Ctra. de Burgos",
        "phone": "987 781 880"
    }
}, {
    "node": {
        "title": "Hospederia Santa Cruz",
        "latitude": "42.370494",
        "longitude": "-5.032726",
        "nid": "3147",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "8\u20ac",
        "beds": "12",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "cal": "MAY - OCT",
        "open": "11:30",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Parochial.png",
        "image": "3147-136091.webp",
        "email": "hospederiasantacruz@hotmail.es",
        "street": "Antonio Nicolas, 40",
        "phone": "987 781 139",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casa Rural Los Balcones del Camino",
        "latitude": "42.371929",
        "longitude": "-5.029705",
        "nid": "11764",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/los-balcones-del-camino.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11764-135356.webp",
        "email": "losbalconesdelcamino@gmail.com",
        "street": "Constituci\u00f3n, 53",
        "phone": "676 838 242"
    }
}, {
    "node": {
        "title": "Hostal Domus Viatoris",
        "latitude": "42.371945",
        "longitude": "-5.024475",
        "nid": "9967",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/domus-viatoris.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9967-135360.webp",
        "street": "Sahag\u00fan-Arriondas",
        "phone": "987 782 141, 679 977 928"
    }
}, {
    "node": {
        "title": "Hostal La Bastide du Chemin",
        "latitude": "42.370805",
        "longitude": "-5.027167",
        "nid": "11766",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "wifi": "Yes, WiFi",
        "cal": "½ MAR - ½ NOV",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-la-bastide-du-chemin.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11766-135355.webp",
        "email": "info@labastideduchemin.es",
        "street": "Arco, 66",
        "phone": "987 781 183, 617 042 531"
    }
}, {
    "node": {
        "title": "Hostal La Codorniz",
        "latitude": "42.370831",
        "longitude": "-5.026868",
        "nid": "9969",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-la-codorniz.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9969-134998.webp",
        "email": "info@hostallacodorniz.com",
        "street": "Constituci\u00f3n, 97",
        "phone": "987 780 276"
    }
}, {
    "node": {
        "title": "Albergue Viatoris",
        "latitude": "42.371960",
        "longitude": "-5.024812",
        "nid": "1313",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "8:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1313-135002.webp",
        "email": "reservas@viatoris.es",
        "street": "Arco Traves\u00eda, 25",
        "phone": "987 780 975, 679 977 828 - Emergencies only"
    }
}, {
    "node": {
        "title": "Casa Rural Arturo I",
        "latitude": "42.371203",
        "longitude": "-5.027654",
        "nid": "11763",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-arturo.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11763-135357.webp",
        "street": "Arco, 72",
        "phone": "987 780 912"
    }
}, {
    "node": {
        "title": "Pedro Ponce",
        "latitude": "42.371800",
        "longitude": "-5.043134",
        "nid": "7601",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "price": "3.35\u20ac",
        "cal": "MAR - OCT",
        "marker": "Camping.png",
        "email": "ayuntamiento23@sahagun.org",
        "street": "Tineo",
        "phone": "987 780 415"
    }
}, {
    "node": {
        "title": "Hostal Escarcha",
        "latitude": "42.371357",
        "longitude": "-5.030053",
        "nid": "11765",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "escarcha_17@hotmail.com",
        "street": "Regina Franco, 12",
        "phone": "987 781 856"
    }
}, {
    "node": {
        "title": "Hostal Alfonso VI",
        "latitude": "42.370724",
        "longitude": "-5.028466",
        "nid": "9968",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-alfonso-vi.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9968-135359.webp",
        "email": "hostalalfonsovi@hotmail.com",
        "street": "Antonio Nicol\u00e1s, 4",
        "phone": "987 781 144"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Cluny",
        "latitude": "42.371026",
        "longitude": "-5.026948",
        "nid": "1312",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "64",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "10:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1312-135003.webp",
        "email": "otsahagun@hotmail.com",
        "street": "Arco, 2 Antiguo Iglesia de la Trinidad",
        "phone": "987 780 001, 987 781 015"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos San Roque",
        "latitude": "42.386607",
        "longitude": "-5.078609",
        "nid": "1314",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "36",
        "bike": "Yes",
        "close": "None",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "24h",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "1314-134999.webp",
        "email": "roblemirador@hotmail.com",
        "street": "Real",
        "phone": "650 979 425, 674 587 001"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Bercianos",
        "latitude": "42.386298",
        "longitude": "-5.146512",
        "nid": "1315",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "46",
        "bike": "Yes",
        "close": "None",
        "family": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "24h",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Parochial.png",
        "image": "1315-135027.webp",
        "email": "none",
        "street": "Santa Rita, 11",
        "phone": "987 784 008"
    }
}, {
    "node": {
        "title": "Hostal Rivero",
        "latitude": "42.388074",
        "longitude": "-5.142398",
        "nid": "11762",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Mayor, 12",
        "phone": "987 784 287"
    }
}, {
    "node": {
        "title": "Albergue Santa Clara",
        "latitude": "42.388019",
        "longitude": "-5.145455",
        "nid": "9654",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10*\u20ac",
        "beds": "19",
        "barrest": "Coming, please comment if you find it open",
        "bike": "Yes",
        "close": "None",
        "wifi": "Yes, Wifi and computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ AUG - ½ JUL",
        "open": "Varies",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9654-135028.webp",
        "email": "alberguesantaclara@hotmail.com",
        "street": "Iglesia, 3",
        "phone": "987 784 314"
    }
}, {
    "node": {
        "title": "Albergue de Calzadilla de los Hermanillos",
        "latitude": "42.433767",
        "longitude": "-5.156493",
        "nid": "1316",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "22",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "24h",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1316-135026.webp",
        "email": "elburgoranero@gmail.com",
        "street": "Mayor, 28",
        "phone": "987 330 023, 987 330 013 - Ayuntamiento del Burgo Ranero"
    }
}, {
    "node": {
        "title": "Casa Rural Casa El Cura",
        "latitude": "42.434706",
        "longitude": "-5.157333",
        "nid": "11773",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "beds": "17",
        "barrest": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "washer": "Yes, 5\u20ac",
        "marker": "CasaRural.png",
        "email": "lacasaelcura@yahoo.es",
        "street": "La Carretera, 13",
        "phone": "987 337 647"
    }
}, {
    "node": {
        "title": "Albergue V\u00eda Trajana",
        "latitude": "42.433073",
        "longitude": "-5.153271",
        "nid": "6984",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computers",
        "cal": "APR - OCT",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "6984-135099.webp",
        "email": "info@albergueviatrajana.com",
        "street": "Mayor, 57",
        "phone": "987 337 610"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Domenico Laffi",
        "latitude": "42.423437",
        "longitude": "-5.218626",
        "nid": "1317",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "Donativo",
        "beds": "28",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1317-135030.webp",
        "email": "elburgoranero@gmail.com",
        "street": "Mayor, 3",
        "phone": "987 330 023, 987 330 047"
    }
}, {
    "node": {
        "title": "Albergue La Laguna",
        "latitude": "42.424072",
        "longitude": "-5.220665",
        "nid": "9655",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, but in bar",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "street": "La Laguna, 12",
        "phone": "987 330 094"
    }
}, {
    "node": {
        "title": "Casa Rural Piedras Blancas",
        "latitude": "42.423073",
        "longitude": "-5.218455",
        "nid": "11771",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "image": "11771-136750.webp",
        "street": "Fray Pedro del Burgo, 32",
        "phone": "987 330 094"
    }
}, {
    "node": {
        "title": "Hospeder\u00eda Jacobea El Nogal",
        "latitude": "42.422713",
        "longitude": "-5.217703",
        "nid": "1318",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "8\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - NOV",
        "open": "10:30",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Association.png",
        "image": "1318-135031.webp",
        "email": "jelnogal@hotmail.com ",
        "street": "Fray Pedro, 44",
        "phone": "627 229 331"
    }
}, {
    "node": {
        "title": "Hostal El Peregrino",
        "latitude": "42.422929",
        "longitude": "-5.218396",
        "nid": "11770",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "image": "11770-136749.webp",
        "street": "Fray Pedro del Burgo, 36",
        "phone": "987 330 069"
    }
}, {
    "node": {
        "title": "Hotel Castillo El Burgo",
        "latitude": "42.417648",
        "longitude": "-5.215933",
        "nid": "11772",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/castillo-el-burgo.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11772-135371.webp",
        "street": "Autov\u00eda A231 km 34",
        "phone": "987 330 403"
    }
}, {
    "node": {
        "title": "Albergue La Parada",
        "latitude": "42.475081",
        "longitude": "-5.353906",
        "nid": "6985",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, computer",
        "cal": "APR - NOV",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "6985-135034.webp",
        "email": "alberguelaparada@gmail.com",
        "street": "Escuela, 7",
        "phone": "987 317 880"
    }
}, {
    "node": {
        "title": "Albergue Gil",
        "latitude": "42.473885",
        "longitude": "-5.353737",
        "nid": "11639",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "14",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "HOLY - NOV",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "11639-135044.webp",
        "email": "alberguegil@outlook.es",
        "street": "Cantas, 30",
        "phone": "987 317 804, 620 424 271"
    }
}, {
    "node": {
        "title": "Albergue Vive tu Camino",
        "latitude": "42.472502",
        "longitude": "-5.353013",
        "nid": "12153",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "12153-136747.webp",
        "email": "alberguevivetucamino@gmail.com",
        "street": "Real, 56",
        "phone": "987 317 837, 610 293 986"
    }
}, {
    "node": {
        "title": "Albergue de Reliegos D Gaiferos",
        "latitude": "42.475127",
        "longitude": "-5.354025",
        "nid": "1319",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "45",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Municipal.png",
        "image": "1319-135032.webp",
        "email": "none",
        "street": "Escuela",
        "phone": "987 317 801"
    }
}, {
    "node": {
        "title": "Albergue Piedras Blancas II",
        "latitude": "42.471670",
        "longitude": "-5.350524",
        "nid": "9369",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "10",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFo",
        "cal": "MAR - OCT",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9369-135036.webp",
        "street": "Cantas",
        "phone": "987 190 627, 607 163 982"
    }
}, {
    "node": {
        "title": "Albergue de Ada",
        "latitude": "42.473140",
        "longitude": "-5.354145",
        "nid": "11870",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "10",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "image": "11870-136748.webp",
        "email": "alberguedeada@gmail.com",
        "street": "Real, 42",
        "phone": "691 153 010",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal Alberguer\u00eda del Camino",
        "latitude": "42.499284",
        "longitude": "-5.416660",
        "nid": "11769",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "albergueria@albergueriadelcamino.com",
        "street": "Concepci\u00f3n, 12",
        "phone": "987 311 193"
    }
}, {
    "node": {
        "title": "Albergue de Mansilla de las Mulas",
        "latitude": "42.499789",
        "longitude": "-5.418534",
        "nid": "1320",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "76",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "cal": "MAR - ½ DEC",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1320-135037.webp",
        "email": "alberguemansilla@gmail.com",
        "street": "Puente, 5",
        "phone": "987 311 250, 661 977 305"
    }
}, {
    "node": {
        "title": " Casa Rural Las Singer",
        "latitude": "42.499371",
        "longitude": "-5.417145",
        "nid": "12184",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-las-singer.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12184-136779.webp",
        "email": "reservas@casarurallassinger.es",
        "street": "Jos\u00e9 \u00c1lvarez, 6",
        "phone": "987 310 454"
    }
}, {
    "node": {
        "title": "Camping Esla",
        "latitude": "42.504620",
        "longitude": "-5.411650",
        "nid": "7603",
        "nodetype": "Accommodation",
        "accomtype": "Camping",
        "marker": "Camping.png",
        "street": "Fuente de los Prados",
        "phone": "987 310 089"
    }
}, {
    "node": {
        "title": "La Casa de los Soportales",
        "latitude": "42.499401",
        "longitude": "-5.414275",
        "nid": "11768",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "marker": "HotelRural.png",
        "street": "Arrabal, 9",
        "phone": "987 310 232, 600 471 597"
    }
}, {
    "node": {
        "title": "Albergue Gaia",
        "latitude": "42.497328",
        "longitude": "-5.415646",
        "nid": "11871",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "18",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - JAN",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "11871-135187.webp",
        "email": "alberguedegaia@hotmail.com",
        "street": "Constituci\u00f3n, 28",
        "phone": "987 310 308, 699 911 311"
    }
}, {
    "node": {
        "title": "Albergue El Jard\u00edn del Camino",
        "latitude": "42.497676",
        "longitude": "-5.414688",
        "nid": "3703",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\/10\u20ac",
        "beds": "32",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "3703-135040.webp",
        "email": "olgabrez@yahoo.es",
        "street": "Camino de Santiago, 1",
        "phone": "987 310 232, 600 471 597"
    }
}, {
    "node": {
        "title": "La Pensi\u00f3n de Blanca",
        "latitude": "42.498076",
        "longitude": "-5.414441",
        "nid": "11767",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "beds": "14",
        "close": "23:00",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Pension.png",
        "image": "11767-136523.webp",
        "email": "info@lapensiondeblanca.com",
        "street": "Picos de Europa, 4",
        "phone": "626 003 177, 676 191 829"
    }
}, {
    "node": {
        "title": "Hostal La Monta\u00f1a",
        "latitude": "42.545433",
        "longitude": "-5.459577",
        "nid": "11784",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Camino de Santiago, 17",
        "phone": "987 312 161"
    }
}, {
    "node": {
        "title": "Albergue El Delfin Verde",
        "latitude": "42.544041",
        "longitude": "-5.454326",
        "nid": "7604",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "24",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computers",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "image": "7604-135042.webp",
        "email": "hostaleldelfinverde@gmail.com ",
        "street": "carretera general 601, 15 B",
        "phone": "987 312 065"
    }
}, {
    "node": {
        "title": "Albergue San Pelayo",
        "latitude": "42.545397",
        "longitude": "-5.456837",
        "nid": "1321",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "56",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-san-pelayo.html?aid=1499405",
        "marker": "Private.png",
        "image": "1321-135039.webp",
        "email": "alberguesanpelayo@hotmail.com",
        "street": "El Romero, 9",
        "phone": "987 312 677, 650 918 281"
    }
}, {
    "node": {
        "title": "Hotel Camino Real",
        "latitude": "42.566547",
        "longitude": "-5.502970",
        "nid": "9966",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/camino-real-leon.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9966-135033.webp",
        "email": "reservas@hotelcaminoreal.com",
        "street": "Carretera N-601, Km 320",
        "phone": "987 218 134"
    }
}, {
    "node": {
        "title": "Albergue La Torre",
        "latitude": "42.566400",
        "longitude": "-5.497334",
        "nid": "1322",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "38",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "20:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR*",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-turistico-la-torre.html?aid=1499405",
        "marker": "Private.png",
        "image": "1322-135043.webp",
        "email": "info@alberguetorre.es",
        "street": "La Torre, 1",
        "phone": "987 205 896, 669 660 914"
    }
}, {
    "node": {
        "title": "Albergue Santo Tom\u00e1s de Canterbury",
        "latitude": "42.578544",
        "longitude": "-5.550181",
        "nid": "9772",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "59",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer ",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "7:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9772-134240.webp",
        "email": "alberguesantotomas@terra.com",
        "street": "La Lastra, 53",
        "phone": "987 392 626"
    }
}, {
    "node": {
        "title": "Hotel NH Plaza Mayor",
        "latitude": "42.597371",
        "longitude": "-5.566206",
        "nid": "9964",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/nhplazamayor.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9964-135046.webp",
        "email": "nhcollectionplazamayor@nh-hotels.com",
        "street": "Plaza Mayor, 7-11",
        "phone": "987 344 357"
    }
}, {
    "node": {
        "title": "Albergue San Francisco de Asis",
        "latitude": "42.593746",
        "longitude": "-5.570124",
        "nid": "8490",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "10\u20ac",
        "beds": "90",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, Wifi",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "8:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, free",
        "marker": "Parochial.png",
        "image": "8490-136569.webp",
        "email": "reservas.leon@alberguescapuchinos.org",
        "street": "Alcalde Miguel Casta\u00f1os, 4",
        "phone": "987 215 060"
    }
}, {
    "node": {
        "title": "Hotel Real Colegiata de San Isidoro",
        "latitude": "42.601165",
        "longitude": "-5.571253",
        "nid": "11780",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/monumento-real-colegiata-san-isidoro.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11780-135376.webp",
        "email": "reservas@hotelrealcolegiata.es",
        "street": "Plaza Santo Martino, 5",
        "phone": "987 875 088"
    }
}, {
    "node": {
        "title": "Albergue Check in Le\u00f3n",
        "latitude": "42.586712",
        "longitude": "-5.560313",
        "nid": "11621",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/check-in-leon.html?aid=1499405",
        "marker": "Private.png",
        "image": "11621-136804.webp",
        "email": "info@checkinleon.es",
        "street": "Alcalde Miguel Casta\u00f1o, 88",
        "phone": "987 498 793, 686 956 896, 609 556 278"
    }
}, {
    "node": {
        "title": "Albergue Leon Hostel",
        "latitude": "42.598644",
        "longitude": "-5.568309",
        "nid": "9868",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\/14\u20ac",
        "beds": "20",
        "close": "20:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/leon-hostel.html?aid=1499405",
        "marker": "Private.png",
        "image": "9868-134559.webp",
        "email": "info@leonhostel.es",
        "street": "Ancha, 8",
        "phone": "987 079 907"
    }
}, {
    "node": {
        "title": "Hostal Londres",
        "latitude": "42.598708",
        "longitude": "-5.576271",
        "nid": "11782",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "price": "15\u20ac",
        "beds": "25",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "FEB - DEC",
        "open": "10:00",
        "reserve": "Yes",
        "washer": "Yes, 1\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-londres.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11782-135175.webp",
        "email": "reservas@hostallondres.com",
        "street": "Roma, 1",
        "phone": "987 222 274, 633 208 824"
    }
}, {
    "node": {
        "title": "Hostal Alda Casco Antiguo",
        "latitude": "42.600664",
        "longitude": "-5.567332",
        "nid": "11776",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-casco-antiguo.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11776-135373.webp",
        "email": "cascoantiguo@aldahostels.es",
        "street": "Cardenal Land\u00e1zuri, 11",
        "phone": "987 074 000"
    }
}, {
    "node": {
        "title": "Albergue Muralla Leonesa",
        "latitude": "42.596566",
        "longitude": "-5.566142",
        "nid": "9871",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "80",
        "bike": "Yes",
        "close": "2:00",
        "wifi": "Yes, WiFi ",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/abergue-muralla-leonesa.html?aid=1499405",
        "marker": "Private.png",
        "image": "9871-135370.webp",
        "email": "info@alberguemurallaleonesa.com",
        "street": "Tarifa, 5",
        "phone": "987 177 873"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de las Benedictinas - Sta. Mar\u00eda de Carbajal",
        "latitude": "42.595142",
        "longitude": "-5.568218",
        "nid": "1324",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "132",
        "bike": "Yes",
        "close": "21:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "FEB - ½ DEC",
        "open": "11:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Parochial.png",
        "email": "sorperegrina@hotmail.com",
        "street": "Pl. Santa Maria del Camino, 11",
        "phone": "987 252 866, 680 649 289"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n La Torre",
        "latitude": "42.600393",
        "longitude": "-5.573014",
        "nid": "11779",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/alda-centro-lea3n.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11779-135385.webp",
        "street": "La Torre, 3",
        "phone": "987 225 594"
    }
}, {
    "node": {
        "title": "Parador San Marcos",
        "latitude": "42.601747",
        "longitude": "-5.582085",
        "nid": "9965",
        "nodetype": "Accommodation",
        "accomtype": "Parador",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/parador-de-leon.html?aid=1499405",
        "marker": "Parador.png",
        "image": "9965-135142.webp",
        "email": "leon@parador.es",
        "street": "Plaza San Marcos, 7",
        "phone": "987 237 300"
    }
}, {
    "node": {
        "title": "Albergue Unamuno",
        "latitude": "42.600110",
        "longitude": "-5.568806",
        "nid": "9656",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "85",
        "bike": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "JUL - ½ SEPT",
        "open": "24h",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "info@residenciaunamuno.com",
        "street": "San Pelayo, 15",
        "phone": "987 233 010, 601 377 423"
    }
}, {
    "node": {
        "title": "Hostal Quevedo",
        "latitude": "42.600360",
        "longitude": "-5.585126",
        "nid": "11781",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-quevedo.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11781-135375.webp",
        "email": "recepcion@hostalquevedoleon.com",
        "street": "Quevedo, 13",
        "phone": "987 242 975"
    }
}, {
    "node": {
        "title": "Hostal Albany",
        "latitude": "42.598490",
        "longitude": "-5.567497",
        "nid": "11775",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albany.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11775-135374.webp",
        "email": "info@albanyleon.net",
        "street": "Paloma, 13",
        "phone": "987 264 600"
    }
}, {
    "node": {
        "title": "Hostel Urban R\u00edo Cea",
        "latitude": "42.597522",
        "longitude": "-5.571774",
        "nid": "9869",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "18\/20*\u20ac",
        "beds": "20",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9869-135049.webp",
        "email": "info@hostelurbanriocea.com",
        "street": " Legi\u00f3n VII, 6",
        "phone": "636 946 294, 639 179 386"
    }
}, {
    "node": {
        "title": "Hostal Misericordia",
        "latitude": "42.596138",
        "longitude": "-5.567000",
        "nid": "11783",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-misericordia-leon2.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11783-135380.webp",
        "street": "Misericordia, 15",
        "phone": "987 264 600"
    }
}, {
    "node": {
        "title": "Hotel Le Petit Le\u00f3n",
        "latitude": "42.597688",
        "longitude": "-5.567780",
        "nid": "11777",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/le-petit-palace.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11777-135372.webp",
        "email": "recepcion@lepetitleonhotel.com",
        "street": "Pozo, 2",
        "phone": "987 075 508"
    }
}, {
    "node": {
        "title": "Hotel Alfageme",
        "latitude": "42.593569",
        "longitude": "-5.606155",
        "nid": "11789",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/alfageme.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11789-135383.webp",
        "email": "reservas@hotelalfageme.es",
        "street": "Las Lagunas, 1",
        "phone": "987 840 490"
    }
}, {
    "node": {
        "title": "Hostal El Abuelo",
        "latitude": "42.595380",
        "longitude": "-5.609282",
        "nid": "11788",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-restaurante-el-abuelo.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11788-135384.webp",
        "email": "hostalelabuelo@hotmail.com",
        "street": "Los Mesones, 6",
        "phone": "987 801 044"
    }
}, {
    "node": {
        "title": "Hostal Plaza",
        "latitude": "42.579427",
        "longitude": "-5.642034",
        "nid": "10810",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-plaza-leon.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "10810-135381.webp",
        "email": "caminoHP@hotmail.com",
        "street": "Astorga, 96",
        "phone": "987 302 019"
    }
}, {
    "node": {
        "title": "Hotel Villapaloma",
        "latitude": "42.581510",
        "longitude": "-5.638347",
        "nid": "9961",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/villapaloma-sl.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9961-135382.webp",
        "email": "villapaloma@argored.com",
        "street": "Astorga, 47",
        "phone": "987 300 990"
    }
}, {
    "node": {
        "title": "Hostal Central",
        "latitude": "42.580528",
        "longitude": "-5.640038",
        "nid": "10729",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "info@hotelrestaurantecentral.com",
        "street": "Astorga, 85",
        "phone": "987 302 041"
    }
}, {
    "node": {
        "title": "Hostal Julio C\u00e9sar",
        "latitude": "42.581685",
        "longitude": "-5.639621",
        "nid": "11786",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Cervantes, 6",
        "phone": "987 302 044"
    }
}, {
    "node": {
        "title": "Albergue D.Antonino y D\u00f1a. Cinia",
        "latitude": "42.578486",
        "longitude": "-5.639411",
        "nid": "1325",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "7\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "cal": "APR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "email": "alberguevirgen@gmail.com",
        "street": "Padre Eustoquio, 16",
        "phone": "987 302 800, 615 217 335"
    }
}, {
    "node": {
        "title": "Hostales Plaza y San Froil\u00e1n",
        "latitude": "42.579456",
        "longitude": "-5.642166",
        "nid": "9962",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-san-froilan.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9962-135051.webp",
        "email": "caminoHP@hotmail.com",
        "street": "Astorga, 98",
        "phone": "987 302 019, 666 042 560"
    }
}, {
    "node": {
        "title": "La Casa del Camino",
        "latitude": "42.570834",
        "longitude": "-5.677958",
        "nid": "12148",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "32",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casa-del-camino.html?aid=1499405",
        "marker": "Private.png",
        "image": "12148-136796.webp",
        "email": "lacasadelcamin@gmail.com",
        "street": "Camino El Jano, 2",
        "phone": "987 303 455, 669 874 750"
    }
}, {
    "node": {
        "title": "Albergue Casa de Jes\u00fas",
        "latitude": "42.483675",
        "longitude": "-5.726023",
        "nid": "3145",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3145-135052.webp",
        "email": "refugiojesus@hotmail.com",
        "street": "Coruso, 11",
        "phone": "686 053 390"
    }
}, {
    "node": {
        "title": "Albergue San Antonio de Padua",
        "latitude": "42.484605",
        "longitude": "-5.725669",
        "nid": "1330",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computers",
        "cal": "ALL YEAR",
        "open": "11:30",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1330-135059.webp",
        "email": "alberguesanantoniodepadua@hotmail.com",
        "street": "Le\u00f3n, 33",
        "phone": "987 390 192, 687 300 666",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Mes\u00f3n Albergue Tio Pepe",
        "latitude": "42.483068",
        "longitude": "-5.726657",
        "nid": "1329",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "26",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "MAR - ½ DEC",
        "open": "6:30",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1329-135053.webp",
        "email": "reservas@alberguetiopepe.es",
        "street": "El Teso de la Iglesia, 2",
        "phone": "987 390 517",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal Libertad",
        "latitude": "42.516978",
        "longitude": "-5.766493",
        "nid": "11787",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Angel Mart\u00ednez Fuertes, 25",
        "phone": "987 390 123"
    }
}, {
    "node": {
        "title": "Albergue de Villadangos del P\u00e1ramo",
        "latitude": "42.519583",
        "longitude": "-5.765258",
        "nid": "1326",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "54",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1326-61256.webp",
        "email": "albergue@proyectojovenleon.org",
        "street": "Carretera a Villadangos del P\u00e1ramo",
        "phone": "671 010 786"
    }
}, {
    "node": {
        "title": "Albergue de San Mart\u00edn del Camino",
        "latitude": "42.495235",
        "longitude": "-5.808694",
        "nid": "1327",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "68",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "1327-61258.webp",
        "email": "martinez_sonia@hotmail.com",
        "street": "Carretere de Le\u00f3n - Astorga",
        "phone": "676 020 388"
    }
}, {
    "node": {
        "title": "Albergue Vieira",
        "latitude": "42.497999",
        "longitude": "-5.803092",
        "nid": "3547",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "image": "3547-135087.webp",
        "email": "amelianievesalbergue@gmail.com",
        "street": "Peregrinos",
        "phone": "987 378 565, 620 671 864",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Santa Ana",
        "latitude": "42.496145",
        "longitude": "-5.806443",
        "nid": "1328",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "4\/6\u20ac",
        "beds": "96",
        "bike": "Yes",
        "family": "Yes",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-peregrinos-santa-ana.html?aid=1499405",
        "marker": "Private.png",
        "image": "1328-61259.webp",
        "email": "martinez_sonia@hotmail.com",
        "street": "N-120, 3",
        "phone": "987 378 653"
    }
}, {
    "node": {
        "title": "Albergue La Casa Verde",
        "latitude": "42.493157",
        "longitude": "-5.809946",
        "nid": "12135",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "8",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 1\u20ac",
        "marker": "Private.png",
        "email": "alberguelacasaverde@gmail.com",
        "street": "Traves\u00eda de La Estaci\u00f3n, 8",
        "phone": "646 879 437"
    }
}, {
    "node": {
        "title": "Albergue Santa Luc\u00eda",
        "latitude": "42.461952",
        "longitude": "-5.835215",
        "nid": "8287",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "29",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "8:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "8287-135058.webp",
        "email": "alberguesantalucia@hotmail.com",
        "street": "Doctor V\u00e9lez, 17",
        "phone": "987 389 105"
    }
}, {
    "node": {
        "title": "Bed & Breakfast El Caminero",
        "latitude": "42.460072",
        "longitude": "-5.881138",
        "nid": "11793",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "info@elcaminero.es",
        "street": "Sierra Pambley, 56",
        "phone": "987 389 020"
    }
}, {
    "node": {
        "title": "Albergue San Miguel",
        "latitude": "42.463474",
        "longitude": "-5.883971",
        "nid": "1333",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, Wifi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1333-135065.webp",
        "email": "alberguesanmiguel@gmail.com",
        "street": "Alvarez Vega, 35",
        "phone": "987 388 285"
    }
}, {
    "node": {
        "title": "Albergue La Encina",
        "latitude": "42.465682",
        "longitude": "-5.881496",
        "nid": "9657",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9.5\u20ac",
        "beds": "22",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi and paid computer",
        "cal": "ALL YEAR",
        "open": "24h",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-la-encina.html?aid=1499405",
        "marker": "Private.png",
        "image": "9657-133203.webp",
        "email": "segunramos@hotmail.com",
        "street": "Suero de Qui\u00f1ones",
        "phone": "987 361 087, 606 306 836"
    }
}, {
    "node": {
        "title": "Hostal Don Suero de Qui\u00f1ones",
        "latitude": "42.463534",
        "longitude": "-5.881179",
        "nid": "11792",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-don-suero-de-quinones.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11792-135387.webp",
        "email": "info@donsuero.es",
        "street": "\u00c1lvarez Vega, 1",
        "phone": "987 388 238"
    }
}, {
    "node": {
        "title": "Albergue Parroquial Karl Leisner",
        "latitude": "42.463573",
        "longitude": "-5.883533",
        "nid": "1332",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "90",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "image": "1332-135064.webp",
        "email": "info@alberguekarlleisner.com",
        "street": "\u00c1lvarez Vega, 32",
        "phone": "987 388 444, 661 994 238"
    }
}, {
    "node": {
        "title": "Casa Rural Nuestra Se\u00f1ora de Lourdes",
        "latitude": "42.460939",
        "longitude": "-5.881211",
        "nid": "11794",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "nuestrasenoradelourdes@hotmail.com",
        "street": "Sierra Pambley, 40",
        "phone": "987 388 253"
    }
}, {
    "node": {
        "title": "Albergue Verde",
        "latitude": "42.459412",
        "longitude": "-5.884938",
        "nid": "6986",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "26",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "HOLY - NOV",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6986-61260.webp",
        "email": "oasis@albergueverde.es",
        "street": "Fueros de Le\u00f3n, 76",
        "phone": "689 927 926, 607 671 670",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel El Paso Honroso",
        "latitude": "42.458435",
        "longitude": "-5.882739",
        "nid": "9960",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-paso-honroso.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9960-135386.webp",
        "email": "hotel@elpasohonroso.com",
        "street": "Carraterra N-120",
        "phone": "987 361 010"
    }
}, {
    "node": {
        "title": "Albergue Villares de \u00d3rbigo",
        "latitude": "42.469580",
        "longitude": "-5.910172",
        "nid": "5528",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "FEB - ½ DEC",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "5528-135248.webp",
        "email": "info@alberguevillaresdeorbigo.com",
        "street": "Arnal, 21",
        "phone": "987 132 935",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Camino Franc\u00e9s",
        "latitude": "42.458416",
        "longitude": "-5.930232",
        "nid": "9775",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "14",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "APR - OCT",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "9775-136754.webp",
        "email": "alberguecaminofrances@gmail.com",
        "street": "Real, 68",
        "phone": "987 361 014"
    }
}, {
    "node": {
        "title": "Albergue Parroquial Santib\u00e1nez Valdeiglsias",
        "latitude": "42.458418",
        "longitude": "-5.930726",
        "nid": "1334",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "None",
        "family": "Yes",
        "cal": "MAR - NOV",
        "open": "13:30",
        "pilgrim": "Yes",
        "washer": "Yes",
        "marker": "Parochial.png",
        "image": "1334-61265.webp",
        "street": "Caromonte",
        "phone": "626 362 159"
    }
}, {
    "node": {
        "title": "Hostal Juli",
        "latitude": "42.454274",
        "longitude": "-6.016104",
        "nid": "12185",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "price": "Donativo",
        "beds": "10",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-juli-san-justo-de-la-vega.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "12185-136780.webp",
        "street": "Real, 56",
        "phone": "987 617 632"
    }
}, {
    "node": {
        "title": "Hostal La Peseta",
        "latitude": "42.454246",
        "longitude": "-6.052367",
        "nid": "9958",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/restaurante-la-peseta.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9958-135391.webp",
        "email": "info@restaurantelapeseta.com",
        "street": "Plaza de San Bartolom\u00e9, 3",
        "phone": "987 617 275"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Siervas de Mar\u00eda",
        "latitude": "42.452865",
        "longitude": "-6.051355",
        "nid": "1336",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "5\u20ac",
        "beds": "165",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer (free)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Association.png",
        "email": "asociacion@caminodesantiagoastorga.com",
        "street": "Plaza San Francisco, 3",
        "phone": "987 616 034, 618 271 773"
    }
}, {
    "node": {
        "title": "Hotel SPA Ciudad de Astorga",
        "latitude": "42.457115",
        "longitude": "-6.054990",
        "nid": "11643",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/spa-ciudad-de-astorga.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11643-135417.webp",
        "street": "Sitios, 7",
        "phone": "987 603 001"
    }
}, {
    "node": {
        "title": "Hotel Gaud\u00ed",
        "latitude": "42.457162",
        "longitude": "-6.056170",
        "nid": "9955",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/gaudi-astorga.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9955-135388.webp",
        "email": "reservas@hotelgaudi.es",
        "street": "Ingeniero Eduardo de Castro, 6",
        "phone": "987 615 654, 987 602 380"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Garc\u00eda",
        "latitude": "42.453157",
        "longitude": "-6.053333",
        "nid": "11791",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Bajada de Postigo, 3",
        "phone": "987 616 046"
    }
}, {
    "node": {
        "title": "Hostal Coru\u00f1a",
        "latitude": "42.460099",
        "longitude": "-6.063123",
        "nid": "9957",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-corua-a.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9957-135390.webp",
        "email": "reservas@reservascoruna.e.telefonica.net",
        "street": "Ponferrada, 72",
        "phone": "987 615 009"
    }
}, {
    "node": {
        "title": "Albergue San Javier",
        "latitude": "42.456853",
        "longitude": "-6.057791",
        "nid": "1335",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "95",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - DEC",
        "open": "10:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "image": "1335-61270.webp",
        "email": "alberguesanjavier@hotmail.com",
        "street": "Porter\u00eda, 6",
        "phone": "987 618 532, 609 420 931"
    }
}, {
    "node": {
        "title": "Hotel SPA V\u00eda de la Plata",
        "latitude": "42.453819",
        "longitude": "-6.051713",
        "nid": "9959",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/via-de-la-plata-spa.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9959-135418.webp",
        "email": "info@hotelviadelaplata.es",
        "street": "Padres Redentoristas, 5",
        "phone": "987 619 000"
    }
}, {
    "node": {
        "title": "Hostal Delf\u00edn",
        "latitude": "42.464131",
        "longitude": "-6.073046",
        "nid": "11790",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-residencia-delfin.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11790-135416.webp",
        "email": "hostal@hostaldelfin.com",
        "street": "Carretera N-VI, 417",
        "phone": "987 602 414"
    }
}, {
    "node": {
        "title": "Hotel Astur Plaza",
        "latitude": "42.454463",
        "longitude": "-6.053145",
        "nid": "9956",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/astur-plaza.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9956-135389.webp",
        "email": "info@hotelasturplaza.es",
        "street": "Plaza Espa\u00f1a",
        "phone": " 987 617 665"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Ecce Homo",
        "latitude": "42.460441",
        "longitude": "-6.080606",
        "nid": "8151",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "10",
        "bike": "Yes",
        "close": "22:00",
        "cal": "MAR - OCT",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "8151-61273.webp",
        "email": "morrolas@hotmail.es",
        "phone": "620 960 060"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Murias de Rechivaldo",
        "latitude": "42.460334",
        "longitude": "-6.107145",
        "nid": "1337",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:30",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1337-61281.webp",
        "street": "Santa Colomba, 52",
        "phone": "987 691 150"
    }
}, {
    "node": {
        "title": "Albergue Casa Flor",
        "latitude": "42.460627",
        "longitude": "-6.107464",
        "nid": "6987",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "15",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "Varies",
        "open": "10:00",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "6987-61283.webp",
        "email": "rodkiko@gmail.com",
        "street": "Carretera Santa Colomba, 54",
        "phone": "609 478 323"
    }
}, {
    "node": {
        "title": "Hoster\u00eda Casa Flor",
        "latitude": "42.460566",
        "longitude": "-6.107272",
        "nid": "9954",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hosteria-casaflor.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9954-135143.webp",
        "email": "info@hosteriacasaflor.com",
        "street": "Carretera Santa Colomba, 52",
        "phone": "987 603 148, 609 478 323"
    }
}, {
    "node": {
        "title": "Albergue las \u00c1guedas",
        "latitude": "42.459331",
        "longitude": "-6.107733",
        "nid": "1338",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "40",
        "barrest": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, Wifi and computer",
        "kitchen": "Yes, with permission and 2\u20ac",
        "microwave": "Yes",
        "cal": "MAR - NOV*",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-las-aguedas.html?aid=1499405",
        "marker": "Private.png",
        "image": "1338-61284.webp",
        "email": "lasaguedas@yahoo.es",
        "street": "Camino de Santiago, 52",
        "phone": "987 691 234, 636 067 840",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casa Rural La Veleta",
        "latitude": "42.460403",
        "longitude": "-6.105080",
        "nid": "9953",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/centro-de-turismo-rural-la-veleta.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9953-135140.webp",
        "street": "Plaza Mayor, 1",
        "phone": "616 598 133"
    }
}, {
    "node": {
        "title": "Casa Rural Casa Coscolo",
        "latitude": "42.464349",
        "longitude": "-6.127748",
        "nid": "9951",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-coscolo.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9951-135415.webp",
        "street": "El Rinc\u00f3n, 1",
        "phone": "987 691 984"
    }
}, {
    "node": {
        "title": "Hotel Cuca la Vaina",
        "latitude": "42.464352",
        "longitude": "-6.128662",
        "nid": "11798",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "informacion@cucalavaina.es",
        "street": "Jard\u00edn",
        "phone": "987 691 034"
    }
}, {
    "node": {
        "title": "Albergue municipal de Castrillo de los Polvazares",
        "latitude": "42.464561",
        "longitude": "-6.129209",
        "nid": "6989",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "8",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "No, but available at the bar",
        "kitchen": "Yes",
        "cal": "APR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "marker": "Municipal.png",
        "image": "6989-134321.webp",
        "email": "nuber79@hotmail.com",
        "street": "Jard\u00edn",
        "phone": "655 803 706"
    }
}, {
    "node": {
        "title": "Los Polvazares",
        "latitude": "42.465097",
        "longitude": "-6.127480",
        "nid": "9952",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/los-polvazarhtml?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9952-135414.webp",
        "street": "Real, 18"
    }
}, {
    "node": {
        "title": "Hospeder\u00eda San Blas",
        "latitude": "42.455203",
        "longitude": "-6.158894",
        "nid": "1340",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "24",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, computers",
        "cal": "ALL YEAR",
        "open": "10:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1340-61277.webp",
        "email": "info@alberguesanblas.com",
        "street": "Real, 11",
        "phone": "987 691 411, 637 464 833"
    }
}, {
    "node": {
        "title": "Albergue y Centro de turismo rural El Caminante",
        "latitude": "42.455223",
        "longitude": "-6.158266",
        "nid": "1341",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1341-61279.webp",
        "email": "elcaminante.ctr@gmail.com",
        "street": "Real, 2",
        "phone": "987 691 098"
    }
}, {
    "node": {
        "title": "Albergue Gabino",
        "latitude": "42.462726",
        "longitude": "-6.209119",
        "nid": "1343",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8*\u20ac",
        "beds": "30",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - NOV",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1343-61286.webp",
        "email": "alberguegabino@hotmail.es",
        "street": "Real, 9",
        "phone": "660 912 823"
    }
}, {
    "node": {
        "title": "Albergue Municipal de Rabanal del Camino",
        "latitude": "42.480796",
        "longitude": "-6.284099",
        "nid": "1346",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "4\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAY - ½ SEPT",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Municipal.png",
        "image": "1346-61289.webp",
        "email": "rabanalbergue@hotmail.com",
        "street": "Plaza de Jer\u00f3nimo Mor\u00e1n Alonso",
        "phone": "987 631 687"
    }
}, {
    "node": {
        "title": " La Candela",
        "latitude": "42.479652",
        "longitude": "-6.276893",
        "nid": "12187",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-candela.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12187-136782.webp",
        "street": "Carretera de Rabanal",
        "phone": "626 385 952"
    }
}, {
    "node": {
        "title": "Apartamentos Rurales Las Carballedas",
        "latitude": "42.479902",
        "longitude": "-6.267093",
        "nid": "11644",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/apartamentos-rurales-las-carballedas.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11644-135413.webp",
        "email": "reservas@lascarballedas.com",
        "street": "Ganso, 1 ",
        "phone": "686 705 595, 690 332 155"
    }
}, {
    "node": {
        "title": "La Posada de Gaspar",
        "latitude": "42.482413",
        "longitude": "-6.285810",
        "nid": "11646",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "image": "11646-135093.webp",
        "street": "Real, 27",
        "phone": "987 631 629"
    }
}, {
    "node": {
        "title": "Albergue Nuestra Se\u00f1ora del Pilar",
        "latitude": "42.480648",
        "longitude": "-6.284488",
        "nid": "1345",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "72",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "24h",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes, for large groups only",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "image": "1345-61292.webp",
        "email": "rabanalelpilar@hotmail.com",
        "street": "Plaza de Jer\u00f3nimo Mor\u00e1n Alonso",
        "phone": "987 631 621, 616 089 942"
    }
}, {
    "node": {
        "title": "Casa Indie",
        "latitude": "42.481137",
        "longitude": "-6.284240",
        "nid": "12186",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "barrest": "Yes",
        "close": "22:00",
        "cal": "HOLY - OCT",
        "open": "12:00",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/rural-casa-indie.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12186-136870.webp",
        "email": "casaindie@hotmail.com ",
        "street": "Calle del Medio",
        "phone": "625 470 392",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue La Senda",
        "latitude": "42.481335",
        "longitude": "-6.280888",
        "nid": "1347",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "34",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1347-61287.webp",
        "email": "alberguelasenda@hotmail.com",
        "street": "Real",
        "phone": "696 819 060"
    }
}, {
    "node": {
        "title": "Hostal El Refugio",
        "latitude": "42.482447",
        "longitude": "-6.285054",
        "nid": "11645",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "barrest": "Yes",
        "marker": "Hostal.png",
        "image": "11645-135090.webp",
        "email": "info@hostalelrefugio.es",
        "street": "Real, 74",
        "phone": "987 631 592"
    }
}, {
    "node": {
        "title": "Refugio Guacelmo",
        "latitude": "42.482042",
        "longitude": "-6.284819",
        "nid": "1344",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "Donativo",
        "beds": "44",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Association.png",
        "image": "1344-61293.webp",
        "email": "r-wardens@csj.org.uk",
        "street": "Plaza del Peregrino",
        "phone": "987 631 647"
    }
}, {
    "node": {
        "title": "Posada El Tes\u00edn",
        "latitude": "42.481205",
        "longitude": "-6.280645",
        "nid": "11795",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "barrest": "Yes",
        "close": "21:00",
        "cal": "HOLY - OCT",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/posada-el-tesin.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11795-136805.webp",
        "street": "Real",
        "phone": "625 470 392",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue La Posada del Druida",
        "latitude": "42.491109",
        "longitude": "-6.342287",
        "nid": "9854",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9854-135068.webp",
        "phone": "696 820 136"
    }
}, {
    "node": {
        "title": "Albergue Monte Irago",
        "latitude": "42.491087",
        "longitude": "-6.342673",
        "nid": "1349",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "35",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes, for elderly pilgrims",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1349-61295.webp",
        "email": "alberguemonteirago@hotmail.es",
        "street": "Real",
        "phone": "695 452 950",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue La Cruz de Fierro",
        "latitude": "42.492044",
        "longitude": "-6.342681",
        "nid": "6992",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "open": "10:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6992-135144.webp",
        "phone": "987 691 093, 665 258 169"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El Trasgu de Foncebad\u00f3n",
        "latitude": "42.491306",
        "longitude": "-6.342902",
        "nid": "11804",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "beds": "9",
        "barrest": "Yes",
        "close": "20:00",
        "cal": "FEB - ½ Dec",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-trasgu-de-foncebadon.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11804-136841.jpeg",
        "email": "info@eltrasgudefoncebadon.es",
        "street": "Real",
        "phone": "987 053 877",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Parroquial Domus Dei",
        "latitude": "42.491910",
        "longitude": "-6.343660",
        "nid": "1348",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "18 + 12",
        "bike": "Yes",
        "close": "22:30",
        "family": "Yes",
        "wifi": "No, but available at the bar",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "1348-133731.webp",
        "email": "peregrinosflue@terra.com",
        "street": "Real",
        "phone": "none"
    }
}, {
    "node": {
        "title": "Albergue Roger de Lauria",
        "latitude": "42.491215",
        "longitude": "-6.341448",
        "nid": "3144",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "FEB - NOV",
        "open": "12:00",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "3144-135069.webp",
        "email": "alberguerogerdelauria@gmail.com",
        "phone": "625 313 425"
    }
}, {
    "node": {
        "title": "Refugio de Manjar\u00edn - Albergue Templario de Tom\u00e1s",
        "latitude": "42.490438",
        "longitude": "-6.386833",
        "nid": "1350",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "Donativo",
        "beds": "25",
        "bike": "Yes",
        "family": "Yes",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "marker": "Private.png",
        "image": "1350-61296.webp",
        "email": "manjarin@wanadoo.es",
        "phone": "none",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue La Casa del Peregrino",
        "latitude": "42.500874",
        "longitude": "-6.460169",
        "nid": "9852",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "103",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "cal": "½ JAN - ½ DEC",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-la-casa-del-peregrino-el-acebo-de-san-miguel.html?aid=1499405",
        "marker": "Private.png",
        "image": "9852-135070.webp",
        "email": "alberguelacasadelperegrino@gmail.com",
        "phone": "987 057 793"
    }
}, {
    "node": {
        "title": "Casa Rural La Rosa del Agua",
        "latitude": "42.497773",
        "longitude": "-6.456026",
        "nid": "11803",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-rosa-del-agua.html?aid=1499405",
        "marker": "CasaRural.png",
        "email": "reservas@larosadelagua.com",
        "street": "Real, 52",
        "phone": "616 849 738"
    }
}, {
    "node": {
        "title": "Albergue Mes\u00f3n El Acebo",
        "latitude": "42.499025",
        "longitude": "-6.457266",
        "nid": "1352",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "23",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, WiFi",
        "cal": "½ FEB - ½ DEC",
        "open": "13:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "email": "mesonelacebo@hotmail.com",
        "street": "Real, 16",
        "phone": "987 695 074, 615 500 408"
    }
}, {
    "node": {
        "title": "Casa Rural La Trucha del Arco Iris",
        "latitude": "42.498691",
        "longitude": "-6.457459",
        "nid": "11802",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-trucha-del-arco-iris.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11802-135411.webp",
        "email": "latruchaacebo@gmail.com",
        "street": "La Cruz, 10",
        "phone": "987 695 548",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Parroquial Ap\u00f3stol Santiago",
        "latitude": "42.498839",
        "longitude": "-6.456785",
        "nid": "1351",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "23",
        "bike": "Yes",
        "close": "22:30",
        "family": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "No, except for the handicapped",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "1351-133728.webp",
        "email": "peregrinosflue@terra.com",
        "street": "La Iglesia",
        "phone": "none"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Riego de Ambr\u00f3s",
        "latitude": "42.522091",
        "longitude": "-6.478643",
        "nid": "9950",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-casa-riego-de-ambros.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9950-135410.webp",
        "street": "Carretera de Astorga",
        "phone": "987 695 188"
    }
}, {
    "node": {
        "title": "Albergue de Riego de Ambros",
        "latitude": "42.520694",
        "longitude": "-6.478937",
        "nid": "1354",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "22:30",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Municipal.png",
        "image": "1354-61297.webp",
        "street": "Plaza San Sebasti\u00e1n",
        "phone": "987 695 190"
    }
}, {
    "node": {
        "title": "Hotel de Floriana",
        "latitude": "42.539278",
        "longitude": "-6.523853",
        "nid": "9949",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/de-floriana-molinaseca.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9949-136806.webp",
        "email": "info@defloriana.com",
        "street": "Manuel Fraga Iribarne",
        "phone": "987 453 146"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Molinaseca San Roque",
        "latitude": "42.541931",
        "longitude": "-6.528137",
        "nid": "1355",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "28",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1355-61299.webp",
        "email": "anam75513@gmail.com",
        "street": "Manual Fraga Iribarne",
        "phone": "987 453 077"
    }
}, {
    "node": {
        "title": "Casa Rural La Torre De Babel",
        "latitude": "42.537881",
        "longitude": "-6.520018",
        "nid": "11800",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-babel.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11800-135407.webp",
        "street": "Real, 42",
        "phone": "679 856 478"
    }
}, {
    "node": {
        "title": "Casa Rural Pajarapinta",
        "latitude": "42.537975",
        "longitude": "-6.520573",
        "nid": "9946",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-pajarapinta.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9946-135409.webp",
        "email": "E-mail.casaruralpajarapinta@hotmail.es",
        "street": "Real, 30",
        "phone": "987 453 040, 657 915 858"
    }
}, {
    "node": {
        "title": "Hostal Casa San Nicolas",
        "latitude": "42.537258",
        "longitude": "-6.519653",
        "nid": "12151",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bike": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-casa-san-nicolas.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "12151-136866.webp",
        "email": "casasannicolas@outlook.com",
        "street": "La Iglesia, 43",
        "phone": "645 562 008, 630 111 846"
    }
}, {
    "node": {
        "title": "The Way Hostel Molinaseca",
        "latitude": "42.537930",
        "longitude": "-6.519168",
        "nid": "9948",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/the-way-hostel-molinaseca.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9948-135066.webp",
        "email": "info@thewayhostel.com",
        "street": "El Palacio, 10",
        "phone": "637 941 017"
    }
}, {
    "node": {
        "title": "Casa Rural Mar\u00eda",
        "latitude": "42.537652",
        "longitude": "-6.520669",
        "nid": "11799",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-maria-molinaseca1.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11799-135408.webp",
        "email": "reservas@molinasecamaria.com",
        "street": "La Iglesia, 25",
        "phone": "987 453 160"
    }
}, {
    "node": {
        "title": "Albergue Molinaseca - Santa Marina",
        "latitude": "42.540836",
        "longitude": "-6.526174",
        "nid": "1356",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "59",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1356-61298.webp",
        "email": "alfredomolinaseca@hotmail.com",
        "street": "Manual Fraga Iribarne",
        "phone": "615 302 390, 653 375 727"
    }
}, {
    "node": {
        "title": "Hostal El Palacio",
        "latitude": "42.538168",
        "longitude": "-6.519709",
        "nid": "11801",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-el-palacio.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11801-135406.webp",
        "email": "info@casaelpalacio.com",
        "street": "El Palacio, 19",
        "phone": "987 453 094"
    }
}, {
    "node": {
        "title": "Posada de Muriel",
        "latitude": "42.538570",
        "longitude": "-6.522348",
        "nid": "9947",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-posada-de-muriel-molinaseca1.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9947-135075.webp",
        "email": "reservas@laposadademuriel.com",
        "street": "Plaza de Cristo",
        "phone": "987 453 201, 627 272 035"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos San Nicol\u00e1s de Fl\u00fce",
        "latitude": "42.543390",
        "longitude": "-6.586304",
        "nid": "1357",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "175",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "1357-61300.webp",
        "email": "peregrinosflue@terra.es",
        "street": "Loma",
        "phone": "987 413 381"
    }
}, {
    "node": {
        "title": "Hostal R\u00edo Selmo",
        "latitude": "42.548010",
        "longitude": "-6.596665",
        "nid": "11812",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-rio-selmo.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11812-135400.webp",
        "email": "hostalrioselmo@gmail.com",
        "street": "R\u00edo Selmo, 22",
        "phone": "987 402 665"
    }
}, {
    "node": {
        "title": "Hostal Nirvana",
        "latitude": "42.551595",
        "longitude": "-6.598470",
        "nid": "9943",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-nirvana.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9943-135405.webp",
        "email": "info-reservas@hostalnirvana.com",
        "street": "Lago de Carucedo, 12",
        "phone": " 987 410 761"
    }
}, {
    "node": {
        "title": "Hostal San Miguel",
        "latitude": "42.546371",
        "longitude": "-6.596919",
        "nid": "11809",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "info@hostalsanmiguelponferrada.es",
        "street": "Juan de Lama, 14",
        "phone": "987 426 700"
    }
}, {
    "node": {
        "title": "Hotel Aroi Ponferrada",
        "latitude": "42.547103",
        "longitude": "-6.601034",
        "nid": "11811",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/aroi-ponferrada.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11811-135401.webp",
        "email": "bierzoplaza@aroihoteles.com",
        "street": "Marcelo Mac\u00edas, 4",
        "phone": "987 409 427"
    }
}, {
    "node": {
        "title": "Albergue Alea",
        "latitude": "42.546733",
        "longitude": "-6.582296",
        "nid": "9857",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9857-136852.webp",
        "email": "info@alberguealea.com",
        "street": "Teleno, 33",
        "phone": "987 404 133, 660 416 251",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Guiana",
        "latitude": "42.542895",
        "longitude": "-6.591136",
        "nid": "11873",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "90",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "6:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-guiana.html?aid=1499405",
        "marker": "Private.png",
        "image": "11873-136807.webp",
        "email": "info@albergueguiana.com",
        "street": "Castillo, 112",
        "phone": "987 409 327"
    }
}, {
    "node": {
        "title": "Hotel El Castillo",
        "latitude": "42.542750",
        "longitude": "-6.592044",
        "nid": "9945",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-castillo.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9945-135403.webp",
        "email": "elcastillo@picos.com",
        "street": "El Castillo, 115",
        "phone": "987 456 227"
    }
}, {
    "node": {
        "title": "Hotel Los Templarios",
        "latitude": "42.545315",
        "longitude": "-6.591717",
        "nid": "11810",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/los-templarios.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11810-135402.webp",
        "email": "info@hotellostemplarios.info",
        "street": "Fl\u00f3rez Osorio, 3",
        "phone": "987 411 484"
    }
}, {
    "node": {
        "title": "Casa Rural El Almendro de Mar\u00eda",
        "latitude": "42.573629",
        "longitude": "-6.611205",
        "nid": "11808",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, included in price",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-almendro-de-maraa.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11808-135256.webp",
        "email": "crelalmendro@gmail.com",
        "street": "Real, 56",
        "phone": "633 481 100"
    }
}, {
    "node": {
        "title": "Hostal Monteclaro",
        "latitude": "42.567834",
        "longitude": "-6.639310",
        "nid": "11874",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-monteclaro.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11874-134569.jpeg",
        "street": "Antonio Cort\u00e9s, 24",
        "phone": "987 455 982"
    }
}, {
    "node": {
        "title": "La Casita",
        "latitude": "42.579827",
        "longitude": "-6.671957",
        "nid": "11284",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "beds": "7",
        "bike": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "24h",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casita-bed-amp-breakfast.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11284-134702.webp",
        "email": "lacasitacamponaraya@gmail.com",
        "street": "Plaza Constitucion, 35",
        "phone": "617 791 122"
    }
}, {
    "node": {
        "title": "Albergue Naraya",
        "latitude": "42.576354",
        "longitude": "-6.661270",
        "nid": "9856",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "26",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "alberguenaraya@gmail.com",
        "street": "Galicia, 506",
        "phone": "987 459 159"
    }
}, {
    "node": {
        "title": "Albergue La Medina",
        "latitude": "42.579630",
        "longitude": "-6.668841",
        "nid": "12136",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-la-medina.html?aid=1499405",
        "marker": "Private.png",
        "image": "12136-136808.webp",
        "email": "alberguelamedina@gmail.com",
        "street": "Camino de Santiago, 87",
        "phone": "987 463 962, 667 348 551"
    }
}, {
    "node": {
        "title": "Hostal Santa Mar\u00eda",
        "latitude": "42.599789",
        "longitude": "-6.724379",
        "nid": "10968",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "santamaria20sl@hotmail.com",
        "street": "Santa Mar\u00eda, 20",
        "phone": "987 549 588"
    }
}, {
    "node": {
        "title": "Hostal La Gallega",
        "latitude": "42.599895",
        "longitude": "-6.724802",
        "nid": "11806",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "email": "hostalgallega@gmail.com",
        "street": "Santa Maria, 23",
        "phone": "987 549 476"
    }
}, {
    "node": {
        "title": "Albergue de Cacabelos",
        "latitude": "42.600205",
        "longitude": "-6.731248",
        "nid": "1358",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "70",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "MAY - OCT",
        "open": "12:00",
        "pilgrim": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1358-61302.webp",
        "email": "turismo@cacabelos.org",
        "street": "Plaza del Santuario",
        "phone": "987 546 151"
    }
}, {
    "node": {
        "title": "Hotel Moncloa de San L\u00e1zaro",
        "latitude": "42.599845",
        "longitude": "-6.717947",
        "nid": "9942",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-moncloa-de-san-lazaro.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9942-135399.webp",
        "email": "info@moncloadesanlazaro.com",
        "street": "Cimadevilla, 97",
        "phone": "987 546 101"
    }
}, {
    "node": {
        "title": "Hotel Villa de Cacabelos",
        "latitude": "42.597971",
        "longitude": "-6.725092",
        "nid": "11805",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "reservas@hotelvilladecacabelos.es",
        "street": "Constituci\u00f3n, 12",
        "phone": "987 548 148"
    }
}, {
    "node": {
        "title": "Hostal Siglo XIX",
        "latitude": "42.599596",
        "longitude": "-6.725700",
        "nid": "12188",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-siglo-xix.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "12188-136783.webp",
        "email": "info@hostalsigloxix.com",
        "street": "Santa Maria, 2",
        "phone": "633 422 661"
    }
}, {
    "node": {
        "title": "Albergue La Gallega",
        "latitude": "42.599901",
        "longitude": "-6.724675",
        "nid": "9658",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "hostalgallega@gmail.com",
        "street": "Santa Mar\u00eda, 23",
        "phone": "987 549 476, 680 917 109"
    }
}, {
    "node": {
        "title": "Albergue El Serbal y la Luna",
        "latitude": "42.605850",
        "longitude": "-6.748839",
        "nid": "6411",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:00",
        "family": "Yes",
        "wifi": "Yes",
        "cal": "MAR - NOV",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6411-61305.webp",
        "email": "alberguedepieros@gmail.com",
        "street": "El Pozo, 15",
        "phone": "639 888 924",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal El Cruce",
        "latitude": "42.607827",
        "longitude": "-6.816228",
        "nid": "11640",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "El Salvador, 41",
        "phone": "987 540 185"
    }
}, {
    "node": {
        "title": "Hostal Burbia",
        "latitude": "42.610679",
        "longitude": "-6.808677",
        "nid": "9937",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bike": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-burbia-villafranca-del-bierzo.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9937-135094.webp",
        "email": "reservas@hostalburbia.com",
        "street": "Fuente Cubero, 13",
        "phone": "987 542 667"
    }
}, {
    "node": {
        "title": "Refugio Ave Fenix de familia Jato",
        "latitude": "42.604045",
        "longitude": "-6.808206",
        "nid": "1359",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "80",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, computer",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1359-61303.webp",
        "email": "albergueavefenix@gmail.com",
        "street": "Santiago, 10",
        "phone": "987 540 229"
    }
}, {
    "node": {
        "title": "Hotel San Francisco",
        "latitude": "42.606137",
        "longitude": "-6.809303",
        "nid": "11642",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/san-francisco-villafranca-del-bierzo.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11642-135079.webp",
        "email": "reservas@hotelsanfrancisco.org",
        "street": "Plaza Mayor, 6",
        "phone": "987 540 465"
    }
}, {
    "node": {
        "title": "Hostal La Puerta del Perd\u00f3n",
        "latitude": "42.604590",
        "longitude": "-6.809324",
        "nid": "9939",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "image": "9939-135080.webp",
        "email": "info@lapuertadelperdon.com",
        "street": "Plaza de Prim, 4",
        "phone": "987 540 614, 605 785 053"
    }
}, {
    "node": {
        "title": "Albergue de la Piedra",
        "latitude": "42.610215",
        "longitude": "-6.813578",
        "nid": "3143",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "38",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, Wifi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3143-136679.webp",
        "email": "amigos@alberguedelapiedra.com",
        "street": "Esp\u00edritu Santo, 14",
        "phone": "987 540 260"
    }
}, {
    "node": {
        "title": "Albergue El Castillo",
        "latitude": "42.604560",
        "longitude": "-6.811073",
        "nid": "12210",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "marker": "Private.png",
        "email": "albergueelcastillo@gmail.com",
        "street": "Castillo, 8",
        "phone": "987 540 344"
    }
}, {
    "node": {
        "title": "Hotel Las Do\u00f1as del Portazgo",
        "latitude": "42.608727",
        "longitude": "-6.810880",
        "nid": "9941",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/las-donas-del-portazgo.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9941-135096.webp",
        "street": "Ribadeo, 2",
        "phone": "987 542 742"
    }
}, {
    "node": {
        "title": "Albergue Leo",
        "latitude": "42.608348",
        "longitude": "-6.810939",
        "nid": "9894",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "24",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9894-134631.webp",
        "email": "info@albergueleo.com",
        "street": "Ribadeo (del Agua), 10",
        "phone": "987 542 658, 658 049 244"
    }
}, {
    "node": {
        "title": "Posada Plaza Mayor",
        "latitude": "42.606202",
        "longitude": "-6.809531",
        "nid": "11641",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/posada-plaza-mayor.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11641-135078.webp",
        "email": "info@villafrancaplaza.com",
        "street": "Plaza Mayor, 4",
        "phone": "987 540 620"
    }
}, {
    "node": {
        "title": "Hostal M\u00e9ndez",
        "latitude": "42.609825",
        "longitude": "-6.813551",
        "nid": "9938",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-casa-mendez.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9938-135398.webp",
        "email": "info@restaurantemendez.com",
        "street": "Espiritu Santo, 1",
        "phone": "987 540 055"
    }
}, {
    "node": {
        "title": "Albergue Municipal Villafranca del Bierzo",
        "latitude": "42.604576",
        "longitude": "-6.806513",
        "nid": "1360",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "62",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1360-61304.webp",
        "email": "turismo@villafrancadelbierzo.org",
        "street": "Campo de la Gallina",
        "phone": "987 542 680, 987 542 356"
    }
}, {
    "node": {
        "title": "B&B La Casa de Leo",
        "latitude": "42.603211",
        "longitude": "-6.814858",
        "nid": "11818",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casa-de-leo-bed-amp-breakfast.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11818-135396.webp",
        "street": "Pena Pic\u00f3n",
        "phone": "629 206 074"
    }
}, {
    "node": {
        "title": "Parador Villafranca del Bierzo",
        "latitude": "42.603571",
        "longitude": "-6.813337",
        "nid": "9940",
        "nodetype": "Accommodation",
        "accomtype": "Parador",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/parador-de-villafranca-del-bierzo.html?aid=1499405",
        "marker": "Parador.png",
        "image": "9940-135397.webp",
        "email": "villafranca@parador.es",
        "street": "Calvo Sotelo, 28",
        "phone": "987 540 175"
    }
}, {
    "node": {
        "title": "Albergue Hospeder\u00eda San Nicol\u00e1s El Real",
        "latitude": "42.607676",
        "longitude": "-6.808478",
        "nid": "9715",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "150",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "email": "info@sannicolaselreal.com",
        "street": "Traves\u00eda de san Nicol\u00e1s, 4",
        "phone": "696 978 653 , 620 329 386"
    }
}, {
    "node": {
        "title": "Albergue de Pereje",
        "latitude": "42.626194",
        "longitude": "-6.844324",
        "nid": "1361",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "55",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1361-61306.webp",
        "street": "Camino de Santiago",
        "phone": "987 540 138"
    }
}, {
    "node": {
        "title": "Albergue Lamas",
        "latitude": "42.660944",
        "longitude": "-6.864309",
        "nid": "12137",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "10",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "12137-136704.webp",
        "email": "alberguelamaspradela@outlook.com",
        "street": "Calella",
        "phone": "677 569 764, 674 157 620"
    }
}, {
    "node": {
        "title": "Casa Rural Os Arroxos",
        "latitude": "42.649706",
        "longitude": "-6.881814",
        "nid": "11816",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "Camino de Santiago",
        "phone": "987 566 529"
    }
}, {
    "node": {
        "title": "Albergue Crispeta",
        "latitude": "42.647968",
        "longitude": "-6.875947",
        "nid": "3142",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "48",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "3142-61307.webp",
        "email": "osarroxos@gmail.com",
        "street": "Camino Santiago, 162",
        "phone": "620 329 386, 696 978 653"
    }
}, {
    "node": {
        "title": "Albergue Parroquial de Trabadelo",
        "latitude": "42.649623",
        "longitude": "-6.880464",
        "nid": "9895",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "22",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Parochial.png",
        "image": "9895-135147.webp",
        "email": "arrifersa@hotmail.com",
        "street": "La Iglesia",
        "phone": "630 628 130"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El Puente Peregrino",
        "latitude": "42.648052",
        "longitude": "-6.877368",
        "nid": "9936",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-puente-peregrino.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9936-135114.webp",
        "street": "Camino de Santiago, 153",
        "phone": "987 566 500",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Municipal La Casa del Peregrino",
        "latitude": "42.649257",
        "longitude": "-6.882212",
        "nid": "1362",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "36",
        "bike": "Yes",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-municipal-de-trabadelo.html?aid=1499405",
        "marker": "Municipal.png",
        "image": "1362-61308.webp",
        "email": "ayto@trabadelo.org",
        "phone": "987 566 448, 687 827 987, 647 635 831"
    }
}, {
    "node": {
        "title": "Casa Susi",
        "latitude": "42.648006",
        "longitude": "-6.877553",
        "nid": "12369",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "12",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "12369-136993.jpeg",
        "email": "casa.susi@hotmail.com",
        "street": "Camino de Santiago",
        "phone": "675 242 114"
    }
}, {
    "node": {
        "title": "Albergue Camino y Leyenda ",
        "latitude": "42.649432",
        "longitude": "-6.881817",
        "nid": "9740",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-camino-y-leyenda.html?aid=1499405",
        "marker": "Private.png",
        "image": "9740-134943.jpeg",
        "email": "alberguecaminoyleyenda@gmail.com",
        "street": "Camino de Santiago",
        "phone": "987 566 446, 628 921 776"
    }
}, {
    "node": {
        "title": "Hostal Nova Ruta",
        "latitude": "42.647454",
        "longitude": "-6.877006",
        "nid": "9935",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-nova-ruta.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9935-135115.webp",
        "email": " info@hostalnovaruta.com",
        "street": "Carretera N-VI",
        "phone": " 696 978 652,  608 612 665"
    }
}, {
    "node": {
        "title": "Albergue El Peregrino",
        "latitude": "42.660143",
        "longitude": "-6.917835",
        "nid": "1363",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "25",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "MAR - NOV",
        "open": "8:00",
        "reserve": "Yes",
        "washer": "Yes, 2.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-el-peregrino.html?aid=1499405",
        "marker": "Private.png",
        "image": "1363-61309.webp",
        "email": "reservas@laportela.com",
        "street": "Camino de Santiago,  5",
        "phone": "987 543 197"
    }
}, {
    "node": {
        "title": "Hotel Valcarce",
        "latitude": "42.659406",
        "longitude": "-6.915073",
        "nid": "9934",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/valcarce.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9934-135108.webp",
        "street": "Carretera N-VI",
        "phone": "987 543 180"
    }
}, {
    "node": {
        "title": "Albergue Camynos",
        "latitude": "42.663655",
        "longitude": "-6.926613",
        "nid": "9659",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "10",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "20:00",
        "wifi": "Yes, WiFi",
        "cal": "½ APR - OCT",
        "open": "9:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-camynos.html?aid=1499405",
        "marker": "Private.png",
        "image": "9659-131923.webp",
        "email": "camynos@gmail.com",
        "street": "Carretera N-VI, 43",
        "phone": "629 743 124",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostel El Rinc\u00f3n del Ap\u00f3stol",
        "latitude": "42.665304",
        "longitude": "-6.930801",
        "nid": "11815",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-rincon-del-apostol.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11815-135394.webp",
        "street": "Carretera N-VI, 1",
        "phone": "987 543 099"
    }
}, {
    "node": {
        "title": "Albergue Das Animas",
        "latitude": "42.665335",
        "longitude": "-6.928609",
        "nid": "1364",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "17",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-das-animas-ambasmestas.html?aid=1499405",
        "marker": "Private.png",
        "image": "1364-136848.webp",
        "email": "info@das-animas.com",
        "street": "Campo Bajo, 3",
        "phone": "987 543 077, 618 568 845",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Casa Rural Ambasmestas",
        "latitude": "42.665840",
        "longitude": "-6.928425",
        "nid": "9933",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/ctr-ambasmestas.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9933-135395.webp",
        "street": "Carretera N-VI",
        "phone": "987 543 247 "
    }
}, {
    "node": {
        "title": "Albergue Casa del Pescador",
        "latitude": "42.664300",
        "longitude": "-6.927068",
        "nid": "11876",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/15\u20ac",
        "beds": "12",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - ½ NOV",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-casa-del-pescador.html?aid=1499405",
        "marker": "Private.png",
        "image": "11876-136809.webp",
        "email": "casitadelpescador@gmail.com",
        "street": "Carretera N-VI",
        "phone": "603 515 868, 987 684 955",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Santa Mar\u00eda Magdalena",
        "latitude": "42.665308",
        "longitude": "-6.946986",
        "nid": "8152",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "16",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "13:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "8152-61312.webp",
        "email": "alberguelamagdalena@yahoo.es",
        "street": "Carretera N-VI, 57",
        "phone": "987 543 230, 628 736 186"
    }
}, {
    "node": {
        "title": "Casa Rural El Recanto",
        "latitude": "42.665044",
        "longitude": "-6.940330",
        "nid": "11814",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-rural-el-recanto.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11814-135393.webp",
        "email": "elrecanto@hotmail.com",
        "street": "Camino de Santiago, 38",
        "phone": "987 543 202"
    }
}, {
    "node": {
        "title": "Albergue El Paso",
        "latitude": "42.665215",
        "longitude": "-6.947627",
        "nid": "11878",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "28",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "info@albergueelpaso.es",
        "street": "Carretera N-VI, 6",
        "phone": "628 104 309"
    }
}, {
    "node": {
        "title": "Albergue de Vega de Valcarce",
        "latitude": "42.665478",
        "longitude": "-6.945346",
        "nid": "1366",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "5\u20ac",
        "beds": "92",
        "bike": "Yes",
        "close": "None",
        "wifi": "Yes, computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Municipal.png",
        "image": "1366-135129.webp",
        "email": "alberguemunicipal@vegadevalcarce.net",
        "street": "Pandedelo",
        "phone": "987 543 006,  657 097 954"
    }
}, {
    "node": {
        "title": "Albergue Virgen de la Encina",
        "latitude": "42.663146",
        "longitude": "-6.947048",
        "nid": "9661",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "26\u20ac",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "JUN - AUG",
        "open": "6:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Parochial.png",
        "email": "pablosanagustinvalladolid@gmail.com",
        "street": "Juan Pablo II",
        "phone": "649 133 272"
    }
}, {
    "node": {
        "title": "Refugio Vegetariano ISANA",
        "latitude": "42.665118",
        "longitude": "-6.946090",
        "nid": "11877",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "12",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "8:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "isana@escuelamacrobiotica.es",
        "street": "Carretera N-VI, 23",
        "phone": "617 056 179",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Refugio Pequeno Potala",
        "latitude": "42.673029",
        "longitude": "-6.967245",
        "nid": "1367",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "34",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1367-131591.webp",
        "email": "pequepotala@hotmail.com",
        "street": "Carretera N-VI, 20",
        "phone": "987 561 322"
    }
}, {
    "node": {
        "title": "Albergue and Pensi\u00f3n Casa Lixa",
        "latitude": "42.670663",
        "longitude": "-6.981254",
        "nid": "12164",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "cal": "MAR - SEPT",
        "open": "10:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-lixa.html?aid=1499405",
        "marker": "Private.png",
        "image": "12164-136769.webp",
        "email": "info@casalixa.com",
        "street": "Camino de Santiago, 35",
        "phone": "987 134 915",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Pol\u00edn",
        "latitude": "42.671564",
        "longitude": "-6.984788",
        "nid": "9930",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-polan.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9930-135392.webp",
        "street": "Camino de Santiago, 6",
        "phone": "987 543 039"
    }
}, {
    "node": {
        "title": "Casa Rural Para\u00edso del Bierzo",
        "latitude": "42.672324",
        "longitude": "-6.975427",
        "nid": "9932",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/c-t-r-paraiso-del-bierzo.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9932-135100.webp",
        "street": "Carretera N-VI",
        "phone": "987 684 138,  629 928 260"
    }
}, {
    "node": {
        "title": "Casa Rural do Ferreiro",
        "latitude": "42.670714",
        "longitude": "-6.982772",
        "nid": "12165",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "barrest": "Yes",
        "wifi": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-do-ferreiro-la-fragua.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12165-136771.webp",
        "street": "Camino de Santiago, 41",
        "phone": "987 684 903"
    }
}, {
    "node": {
        "title": "Albergue Las Herrer\u00edas",
        "latitude": "42.670628",
        "longitude": "-6.980889",
        "nid": "3141",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "7\u20ac",
        "beds": "20",
        "wifi": "No, but available at the bar",
        "cal": "APR - NOV",
        "open": "14:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "3141-136990.jpeg",
        "email": "myriamcm@yahoo.com",
        "phone": "654 353 940",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel Rural El Capricho de Josana",
        "latitude": "42.672340",
        "longitude": "-6.978410",
        "nid": "9931",
        "nodetype": "Accommodation",
        "accomtype": "Hotel Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-capricho-de-josana.html?aid=1499405",
        "marker": "HotelRural.png",
        "image": "9931-135131.webp",
        "email": "reservas@elcaprichodejosana.com",
        "street": "Camino de Santiago",
        "phone": "987 119 300, 644 448 204"
    }
}, {
    "node": {
        "title": "Albergue Ultreia Antigua Casa Parroquial",
        "latitude": "42.683792",
        "longitude": "-7.006760",
        "nid": "1368",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "5\u20ac",
        "beds": "66",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "kitchen": "Yes",
        "cal": "APR - OCT",
        "open": "14:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Parochial.png",
        "image": "1368-61315.webp",
        "email": "sabinethomsen@gmx.de",
        "phone": "630 836 865",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue El Refugio",
        "latitude": "42.685176",
        "longitude": "-7.010146",
        "nid": "12176",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "5\u20ac",
        "beds": "8",
        "barrest": "Yes",
        "bike": "Yes",
        "family": "Yes",
        "kitchen": "Yes",
        "cal": "MAR - NOV",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "email": "caminarte.asociacion@gmail.com",
        "street": "Camino de Santiago, 9",
        "phone": "654 911 223",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue La Escuela de La Laguna ",
        "latitude": "42.701487",
        "longitude": "-7.022167",
        "nid": "1369",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "49",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - NOV",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1369-61316.webp",
        "email": " baralbergueescuela@hotmail.es",
        "phone": "987 684 786, 619 479 238"
    }
}, {
    "node": {
        "title": "Casa Rural Vali\u00f1a",
        "latitude": "42.707742",
        "longitude": "-7.044073",
        "nid": "11826",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "O Cebreiro",
        "phone": "982 367 125"
    }
}, {
    "node": {
        "title": "Casa Rural Navarro",
        "latitude": "42.708069",
        "longitude": "-7.043344",
        "nid": "12208",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "email": "info@casaturismoruralnavarro.com",
        "street": "O Cebreiro",
        "phone": "982 367 007"
    }
}, {
    "node": {
        "title": "Casa Rural Venta Celta",
        "latitude": "42.707638",
        "longitude": "-7.044211",
        "nid": "11825",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "O Cebreiro",
        "phone": "667 553 006"
    }
}, {
    "node": {
        "title": "Hotel O Cebreiro",
        "latitude": "42.708035",
        "longitude": "-7.044205",
        "nid": "11827",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "informacion@hotelcebreiro.com",
        "street": "O Cebreiro",
        "phone": "982 367 182"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos O Cebreiro",
        "latitude": "42.707744",
        "longitude": "-7.045798",
        "nid": "1370",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "104",
        "close": "22:00",
        "wifi": "No, but available at the bar",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Xunta.png",
        "image": "1370-135118.webp",
        "phone": "660 396 809"
    }
}, {
    "node": {
        "title": "Albergue Linar do Rei",
        "latitude": "42.699154",
        "longitude": "-7.073924",
        "nid": "12177",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "linardorei@gmail.com",
        "street": "Li\u00f1ares",
        "phone": " 616 464 831"
    }
}, {
    "node": {
        "title": "Casa Rural Jaime",
        "latitude": "42.699435",
        "longitude": "-7.072829",
        "nid": "11828",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "Li\u00f1ares",
        "phone": "982 367 166"
    }
}, {
    "node": {
        "title": "Albergue de Hospital da Condesa",
        "latitude": "42.704728",
        "longitude": "-7.098756",
        "nid": "1371",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "20",
        "close": "23:00",
        "kitchen": "Yes, but no kitchenware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1371-61317.webp",
        "phone": "982 161 336, 660 396 810"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n O Tear",
        "latitude": "42.705099",
        "longitude": "-7.101524",
        "nid": "12211",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "image": "1371-61317.webp",
        "phone": "982 367 183"
    }
}, {
    "node": {
        "title": "Albergue Bar Puerto",
        "latitude": "42.712588",
        "longitude": "-7.126114",
        "nid": "8154",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "close": "23:00",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "8154-61318.webp",
        "phone": "982 367 172"
    }
}, {
    "node": {
        "title": "Albergue A Reboleira",
        "latitude": "42.731268",
        "longitude": "-7.157142",
        "nid": "1372",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "80",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "½ MAR - OCT",
        "open": "7:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-a-reboleira.html?aid=1499405",
        "marker": "Private.png",
        "image": "1372-136536.webp",
        "email": "alberguefonfria@yahoo.es",
        "street": "Camino de Santiago",
        "phone": "982 181 271",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Lucas",
        "latitude": "42.732029",
        "longitude": "-7.158590",
        "nid": "11819",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-de-lucas.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11819-135379.webp",
        "email": "fonfria25@gmail.com",
        "street": "Fonfr\u00eda, 25",
        "phone": "690 346 740"
    }
}, {
    "node": {
        "title": "Casa Rural Xato",
        "latitude": "42.744164",
        "longitude": "-7.178115",
        "nid": "11821",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "O Biduedo",
        "phone": "982 187 301"
    }
}, {
    "node": {
        "title": "Casa Rural Quiroga",
        "latitude": "42.743073",
        "longitude": "-7.178104",
        "nid": "11820",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "O Biduedo",
        "phone": "982 187 299"
    }
}, {
    "node": {
        "title": "Albergue Fillobal",
        "latitude": "42.744194",
        "longitude": "-7.204944",
        "nid": "8612",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "22",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "8612-70227.webp",
        "email": "alberguefillobal@yahoo.es",
        "street": "Fillobal, 2",
        "phone": "666 826 414"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Xacobeo",
        "latitude": "42.755857",
        "longitude": "-7.241358",
        "nid": "11823",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Leoncio Cadorniga, 12",
        "phone": "982 548 037"
    }
}, {
    "node": {
        "title": "Albergue Berce do Caminho",
        "latitude": "42.755677",
        "longitude": "-7.242538",
        "nid": "1378",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "27",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1378-135122.webp",
        "street": "Camilo Jos\u00e9 Cela, 11",
        "phone": "982 548 127"
    }
}, {
    "node": {
        "title": "Albergue Lemos",
        "latitude": "42.756469",
        "longitude": "-7.237194",
        "nid": "12146",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "32",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-albergue-lemos.xu.html?aid=1499405",
        "marker": "Private.png",
        "image": "12146-136810.webp",
        "email": "pensionalberguelemos@outlook.com",
        "street": "Castilla, 24",
        "phone": "677 117 238"
    }
}, {
    "node": {
        "title": "Albergue A Horta de Abel",
        "latitude": "42.756113",
        "longitude": "-7.239541",
        "nid": "9662",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9662-135124.webp",
        "email": "casapacios@hotmail.com",
        "street": "Peregrino, 5",
        "phone": "608 080 556"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos de la Xunta",
        "latitude": "42.755236",
        "longitude": "-7.235385",
        "nid": "1374",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "56",
        "close": "22:00",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1374-135121.webp",
        "phone": "982 548 087, 660 396 811"
    }
}, {
    "node": {
        "title": "Hostal Vilasante",
        "latitude": "42.755468",
        "longitude": "-7.242944",
        "nid": "11822",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Camilo Jos\u00e9 Cela, 7",
        "phone": "982 548 116"
    }
}, {
    "node": {
        "title": "Albergue de Oribio",
        "latitude": "42.756461",
        "longitude": "-7.239038",
        "nid": "1377",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "27",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "albergueoribio@gmail.com",
        "street": "Castilla, 20",
        "phone": "982 548 085"
    }
}, {
    "node": {
        "title": "Casa Olga",
        "latitude": "42.757654",
        "longitude": "-7.239906",
        "nid": "11824",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-olga-triacastela.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11824-136811.webp",
        "email": "vanfervass@hotmail.com",
        "street": "Castro, 2",
        "phone": "982 548 134"
    }
}, {
    "node": {
        "title": "Complexo Xacobeo",
        "latitude": "42.755867",
        "longitude": "-7.240668",
        "nid": "4017",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "48",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/complexo-xacobeo-triacastela.xu.html?aid=1499405",
        "marker": "Private.png",
        "image": "4017-135120.webp",
        "email": "info@complexoxacobeo.com",
        "street": "Santiago, 8",
        "phone": "982 548 037, 690 613 388"
    }
}, {
    "node": {
        "title": "Albergue Atrio",
        "latitude": "42.756028",
        "longitude": "-7.239701",
        "nid": "12158",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "25",
        "barrest": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "FEB - NOV",
        "open": "6:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-albergue-lemos.html?aid=1499405",
        "marker": "Private.png",
        "email": "xoan65@gmail.com",
        "street": "Peregrino, 1",
        "phone": "982 548 488, 699 504 958"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Fern\u00e1ndez",
        "latitude": "42.756345",
        "longitude": "-7.240697",
        "nid": "11813",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Plaza da Iglesia, 3",
        "phone": "982 548 024"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Aitzenea",
        "latitude": "42.756154",
        "longitude": "-7.241830",
        "nid": "1375",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "38",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1375-52271.webp",
        "email": "info@aitzenea.com",
        "street": "Plaza Vista Alegre, 1",
        "phone": "982 548 076"
    }
}, {
    "node": {
        "title": "Albergue El Beso",
        "latitude": "42.766083",
        "longitude": "-7.253603",
        "nid": "9366",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "cal": "APR - OCT",
        "open": "6:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9366-135247.webp",
        "email": "alberguelbeso@gmail.com",
        "phone": "633 550 558",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Val de Samos",
        "latitude": "42.730949",
        "longitude": "-7.326976",
        "nid": "4586",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "48",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ APR - NOV",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "4586-133393.webp",
        "email": "info@valdesamos.com",
        "street": "Compostela, 16",
        "phone": "982 546 163, 609 638 801"
    }
}, {
    "node": {
        "title": "Casa Forte de Lus\u00edo",
        "latitude": "42.739223",
        "longitude": "-7.280127",
        "nid": "6409",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "marker": "Xunta.png",
        "street": "Lus\u00edo, 5",
        "phone": "659 721 324, 682 157 378"
    }
}, {
    "node": {
        "title": "Hotel A Veiga",
        "latitude": "42.729740",
        "longitude": "-7.330949",
        "nid": "9929",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/a-veiga.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9929-135378.webp",
        "street": "Compostela, 61",
        "phone": "982 546 052"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Monasterio de Samos",
        "latitude": "42.732450",
        "longitude": "-7.325649",
        "nid": "1379",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "Donativo",
        "beds": "70",
        "bike": "Yes",
        "close": "22:30",
        "cal": "ALL YEAR",
        "open": "15:30",
        "pilgrim": "Yes",
        "marker": "Parochial.png",
        "image": "1379-135125.webp",
        "email": "info@abadiadesamos.com",
        "street": "Monasterio de Samos, 1",
        "phone": "982 546 046"
    }
}, {
    "node": {
        "title": "Casa Rural Licerio",
        "latitude": "42.729345",
        "longitude": "-7.327808",
        "nid": "11831",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "price": "15\u20ac",
        "beds": "3",
        "cal": "MAY - OCT",
        "reserve": "Yes",
        "washer": "Yes, Donativo",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-licerio.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11831-136757.webp",
        "email": "casalicerio@gmail.com",
        "street": "Compostela, 44",
        "phone": "653 593 814"
    }
}, {
    "node": {
        "title": "Albergue A Cova do Frade",
        "latitude": "42.732461",
        "longitude": "-7.325385",
        "nid": "6408",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "14",
        "bike": "1",
        "close": "23:00",
        "wifi": "1",
        "cal": "Closed December and January",
        "open": "11:00",
        "reserve": "1",
        "marker": "Private.png",
        "email": "dvitorafels@hotmail.com",
        "street": "Savador, 1",
        "phone": "982 546 087, 616 664 968"
    }
}, {
    "node": {
        "title": "Hotel Domus Itineres",
        "latitude": "42.732571",
        "longitude": "-7.325176",
        "nid": "9928",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/domus-itineris.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9928-135149.webp",
        "street": "Peregrino, 29",
        "phone": "982 546 088"
    }
}, {
    "node": {
        "title": "Guest House Victoria",
        "latitude": "42.732474",
        "longitude": "-7.325135",
        "nid": "11830",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/guest-house-victoria.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11830-135377.webp",
        "email": "hostalvictoriasamos@yahoo.es",
        "street": "El Salvador, 4",
        "phone": "982 546 022"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Cines",
        "latitude": "42.773931",
        "longitude": "-7.339604",
        "nid": "11833",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "barrest": "Yes",
        "close": "24:00",
        "family": "Yes",
        "cal": "MAR - OCT",
        "open": "7:00",
        "washer": "Yes, 4\/8\u20ac",
        "marker": "Pension.png",
        "email": "hotelcasacines@hotmail.com",
        "street": "Pint\u00edn, 5",
        "phone": "982 167 939, 685 140 635",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue de Hospital de Calvor",
        "latitude": "42.773699",
        "longitude": "-7.355376",
        "nid": "1376",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "22",
        "close": "23:00",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1376-135130.webp",
        "phone": "660 396 812"
    }
}, {
    "node": {
        "title": "Paloma y Le\u00f1a",
        "latitude": "42.773412",
        "longitude": "-7.370249",
        "nid": "1395",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "32",
        "bike": "Yes",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "cal": "MAR - ½ NOV",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "palomaylena@gmail.com",
        "street": "San Mamed, 4",
        "phone": "982 533 248, 658 906 816",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hotel Alfonso IX",
        "latitude": "42.776449",
        "longitude": "-7.410375",
        "nid": "9927",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/carris-alfonso-ix.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9927-135438.webp",
        "email": "info@alfonsoix.com",
        "street": "Peregrino, 29",
        "phone": "982 530 005"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n La Estaci\u00f3n",
        "latitude": "42.782787",
        "longitude": "-7.414876",
        "nid": "9922",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-gata-roja.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9922-135434.webp",
        "street": "Mat\u00edas L\u00f3pez, 106",
        "phone": "616 989 703"
    }
}, {
    "node": {
        "title": "Albergue Credencial",
        "latitude": "42.774853",
        "longitude": "-7.408977",
        "nid": "9843",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "28",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-credencial.html?aid=1499405",
        "marker": "Private.png",
        "image": "9843-134427.webp",
        "email": "alberguecredencial@gmail.com",
        "street": "Peregrino, 50",
        "phone": "982 876 455"
    }
}, {
    "node": {
        "title": "Albergue Monasterio de la Magdalena",
        "latitude": "42.779181",
        "longitude": "-7.421780",
        "nid": "6996",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "102",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, paid Wifi and computers",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac (+ 0.60cent soap)",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-monasterio-de-la-magdalena.html?aid=1499405",
        "marker": "Private.png",
        "image": "6996-131782.webp",
        "email": "sarria@alberguesdelcamino.com",
        "street": "Merced, 60",
        "phone": "982 533 568"
    }
}, {
    "node": {
        "title": "Albergue Los Blasones",
        "latitude": "42.777218",
        "longitude": "-7.416073",
        "nid": "1385",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "42",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-los-blasonhtml?aid=1499405",
        "marker": "Private.png",
        "image": "1385-61596.webp",
        "email": "info@alberguelosblasones.com",
        "street": "Mayor, 31",
        "phone": "600 512 565"
    }
}, {
    "node": {
        "title": "Albergue La Casona de Sarria",
        "latitude": "42.781094",
        "longitude": "-7.420785",
        "nid": "11879",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "15",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casona-de-sarria.html?aid=1499405",
        "marker": "Private.png",
        "image": "11879-135436.webp",
        "email": "info@lacasonadesarria.es",
        "street": "San L\u00e1zaro, 24",
        "phone": "982 535 556,  670 036 444"
    }
}, {
    "node": {
        "title": "La Pensi\u00f3n de Ana",
        "latitude": "42.776110",
        "longitude": "-7.395988",
        "nid": "9924",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-pension-de-ana.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9924-135432.webp",
        "street": "Cimo da Agra, 11",
        "phone": "982 531 458, 699 029 556"
    }
}, {
    "node": {
        "title": "Albergue Mat\u00edas",
        "latitude": "42.776624",
        "longitude": "-7.416922",
        "nid": "9866",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "40",
        "barrest": "Yes, with pizza!",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - NOV",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-matias.html?aid=1499405",
        "marker": "Private.png",
        "image": "9866-134430.webp",
        "email": "anuman43@hotmail.com",
        "street": "Mayor, 4",
        "phone": "982 534 285"
    }
}, {
    "node": {
        "title": "Albergue Barbacoa del Camino",
        "latitude": "42.774328",
        "longitude": "-7.419552",
        "nid": "8104",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-barbacoa-del-camino-c-b.html?aid=1499405",
        "marker": "Private.png",
        "image": "8104-134431.webp",
        "email": "info@aquarooms.es",
        "street": "Esqueiredos, 1",
        "phone": "619 879 476, 603 412 052"
    }
}, {
    "node": {
        "title": "Albergue Internacional Sarria",
        "latitude": "42.777486",
        "longitude": "-7.414976",
        "nid": "3138",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "44",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computer",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "image": "3138-61595.webp",
        "email": " info@albergueinternacionalsarria.es",
        "street": "Mayor, 57",
        "phone": "982 535 109"
    }
}, {
    "node": {
        "title": "Albergue A Pedra",
        "latitude": "42.775817",
        "longitude": "-7.404457",
        "nid": "1382",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "15",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - OCT",
        "open": "11:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-a-pedra.html?aid=1499405",
        "marker": "Private.png",
        "image": "1382-61599.webp",
        "email": "info@albergueapedra.com",
        "street": "Vigo de Sarria, 19",
        "phone": "982 530 130",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Hostal dpCristal",
        "latitude": "42.775677",
        "longitude": "-7.406491",
        "nid": "9926",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/dpcristal.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9926-135439.webp",
        "email": "cristalsarria@gmail.com",
        "street": "Calvo Sotelo, 198 ",
        "phone": "669 799 512"
    }
}, {
    "node": {
        "title": "Albergue Oasis",
        "latitude": "42.775736",
        "longitude": "-7.405048",
        "nid": "9874",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "27",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes, well equipped",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-oasis.html?aid=1499405",
        "marker": "Private.png",
        "image": "9874-134428.webp",
        "email": "reservas@albergueoasis.com",
        "street": "Vigo de Sarria, 12",
        "phone": "605 948 644"
    }
}, {
    "node": {
        "title": "Albergue Obradoiro",
        "latitude": "42.777434",
        "longitude": "-7.415542",
        "nid": "9663",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "38",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - NOV",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/obradoiro-sarria.html?aid=1499405",
        "marker": "Private.png",
        "image": "9663-132139.webp",
        "email": "arte-san@hotmail.es ",
        "street": "Mayor, 49",
        "phone": "982 532 442, 647 209 267"
    }
}, {
    "node": {
        "title": "Albergue Casa Peltre",
        "latitude": "42.776891",
        "longitude": "-7.413251",
        "nid": "6995",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "22",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "6995-61597.webp",
        "email": "hola@alberguecasapeltre.es",
        "street": "Escalinata da Fonte, 10",
        "phone": "606 226 067"
    }
}, {
    "node": {
        "title": "Albergue Dos Oito Marabed\u00eds",
        "latitude": "42.776269",
        "longitude": "-7.417516",
        "nid": "1384",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "23:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAY - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-dos-oito-marabedis.html?aid=1499405",
        "marker": "Private.png",
        "image": "1384-61601.webp",
        "street": "Conde de Lemos, 23",
        "phone": "629 461 770"
    }
}, {
    "node": {
        "title": "Hotel Mar de Plata",
        "latitude": "42.783182",
        "longitude": "-7.416844",
        "nid": "11832",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/mar-de-plata.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11832-135437.webp",
        "email": "reservas@hotelmardeplata.com",
        "street": "Formigueiros, 5",
        "phone": "982 530 724"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Mat\u00edas",
        "latitude": "42.782524",
        "longitude": "-7.416367",
        "nid": "9923",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-matias.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9923-135433.webp",
        "street": "Calvo Sotelo, 39",
        "phone": "659 160 498"
    }
}, {
    "node": {
        "title": "Albergue Puente Ribeira",
        "latitude": "42.775773",
        "longitude": "-7.411185",
        "nid": "9865",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "beds": "50",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "6:30",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-puente-ribeira.html?aid=1499405",
        "marker": "Private.png",
        "image": "9865-134429.webp",
        "email": "info@alberguepuenteribeira.com",
        "street": "Peregrino, 23",
        "phone": "982 876 789, 698 175 619"
    }
}, {
    "node": {
        "title": "Albergue Mayor",
        "latitude": "42.777049",
        "longitude": "-7.413967",
        "nid": "6997",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes, fully equipped",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "10:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-mayor.html?aid=1499405",
        "marker": "Private.png",
        "image": "6997-61593.webp",
        "email": "alberguemayor@gmail.com",
        "street": "Mayor, 64",
        "phone": "685 148 474, 646 427 734"
    }
}, {
    "node": {
        "title": "Albergue O Durmi\u00f1ento",
        "latitude": "42.777380",
        "longitude": "-7.415162",
        "nid": "1386",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "41",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "APR - NOV",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-o-durminento.html?aid=1499405",
        "marker": "Private.png",
        "image": "1386-61594.webp",
        "email": "durmiento_sarria@hotmail.com ",
        "street": "Mayor, 44",
        "phone": "982 531 099"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos de Sarria",
        "latitude": "42.777426",
        "longitude": "-7.413567",
        "nid": "1381",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "40",
        "close": "23:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1381-61592.webp",
        "street": "Mayor, 31",
        "phone": "660 396 813"
    }
}, {
    "node": {
        "title": "Albergue Casino",
        "latitude": "42.777460",
        "longitude": "-7.414786",
        "nid": "11880",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "28",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "6:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-cafeteria-casino.html?aid=1499405",
        "marker": "Private.png",
        "image": "11880-135435.webp",
        "email": "alberguecafeteriacasino@gmail.com",
        "street": "Mayor, 65",
        "phone": "982 886 785"
    }
}, {
    "node": {
        "title": "Matias Rooms",
        "latitude": "42.778118",
        "longitude": "-7.416093",
        "nid": "9925",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/apartment-matias-rooms.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9925-135440.webp",
        "email": "anuman43@hotmail.com",
        "street": "Rosal\u00eda de Castro, 19"
    }
}, {
    "node": {
        "title": "Albergue O Antoxo",
        "latitude": "42.775795",
        "longitude": "-7.406341",
        "nid": "9873",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "marker": "Private.png",
        "image": "9873-134426.webp",
        "email": "lataperiavinoteca@gmail.com",
        "street": "Calvo Sotelo, 211",
        "phone": "982 535 421"
    }
}, {
    "node": {
        "title": "Alma do Cami\u00f1o",
        "latitude": "42.776475",
        "longitude": "-7.407279",
        "nid": "9362",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "100",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - ½ DEC",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-alma-do-camino.html?aid=1499405",
        "marker": "Private.png",
        "image": "9362-134354.webp",
        "email": "sarria@almadocamino.com",
        "street": "Calvo Sotelo, 199",
        "phone": "982 876 768"
    }
}, {
    "node": {
        "title": "Albergue San L\u00e1zaro",
        "latitude": "42.780811",
        "longitude": "-7.420063",
        "nid": "5529",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-san-lazaro.html?aid=1499405",
        "marker": "Private.png",
        "image": "5529-61600.webp",
        "email": "alberguesanlazaro@hotmail.com",
        "street": "San L\u00e1zaro, 7",
        "phone": "982 530 626, 659 185 482"
    }
}, {
    "node": {
        "title": "Albergue Don \u00c1lvaro",
        "latitude": "42.776957",
        "longitude": "-7.416739",
        "nid": "1383",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "48",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ JAN - ½ DEC",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-casa-don-alvaro-sarria.html?aid=1499405",
        "marker": "Private.png",
        "image": "1383-52807.webp",
        "email": "info@alberguedonalvaro.com",
        "street": "Mayor, 10",
        "phone": "686 468 803"
    }
}, {
    "node": {
        "title": "Albergue Casa Barbadelo",
        "latitude": "42.769126",
        "longitude": "-7.444868",
        "nid": "6998",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "9\u20ac",
        "beds": "70",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, computer",
        "cal": "HOLY - NOV",
        "open": "6:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-barbadelo.html?aid=1499405",
        "marker": "Private.png",
        "image": "6998-52307.webp",
        "email": "info@barbadelo.com",
        "phone": "982 531 934, 638 674 607"
    }
}, {
    "node": {
        "title": "Albergue 108 to Santiago",
        "latitude": "42.768748",
        "longitude": "-7.444193",
        "nid": "9664",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "8\u20ac",
        "beds": "12",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/108-to-santiago.html?aid=1499405",
        "marker": "Private.png",
        "image": "9664-134278_1.webp",
        "email": "info@aquarooms.es",
        "phone": "634 894 524"
    }
}, {
    "node": {
        "title": "A Casa de Carmen",
        "latitude": "42.768029",
        "longitude": "-7.451434",
        "nid": "5512",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "32",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "open": "13:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "marker": "Private.png",
        "image": "5512-61602.webp",
        "email": "albergueacasadecarmen@gmail.com",
        "phone": "982 532 294, 606 156 705"
    }
}, {
    "node": {
        "title": "Albergue de Barbadelo",
        "latitude": "42.766293",
        "longitude": "-7.450823",
        "nid": "1387",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "18",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1387-61604.webp",
        "phone": "660 396 814"
    }
}, {
    "node": {
        "title": "Albergue O Pombal",
        "latitude": "42.764682",
        "longitude": "-7.448924",
        "nid": "5537",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "8",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "13:00",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "5537-61603.webp",
        "email": "albergueopombal@gmail.com",
        "phone": "686 718 732"
    }
}, {
    "node": {
        "title": "Albergue Molino de Marz\u00e1n",
        "latitude": "42.772591",
        "longitude": "-7.480016",
        "nid": "9847",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "bike": "Yes",
        "close": "21:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "12:30",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9847-134279.webp",
        "email": "adm@molinomarzan.com",
        "phone": "679 438 077",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Casa Morgade",
        "latitude": "42.782110",
        "longitude": "-7.521172",
        "nid": "5538",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "cal": "??? - OCT",
        "open": "8:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "street": "Morgade - A Pinza",
        "phone": "982 531 250"
    }
}, {
    "node": {
        "title": "Albergue Casa Cruceiro",
        "latitude": "42.783821",
        "longitude": "-7.532913",
        "nid": "8251",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "MAR - NOV",
        "open": "11:00",
        "washer": "Yes",
        "marker": "Private.png",
        "image": "8251-66713.webp",
        "email": "casacruceirodeferreiros@gmail.com",
        "street": "Ferreiros, 2",
        "phone": "982 541 240, 639 020 064"
    }
}, {
    "node": {
        "title": "Albergue de Ferreiro",
        "latitude": "42.783159",
        "longitude": "-7.532757",
        "nid": "1405",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "22",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1405-134280.webp",
        "phone": "982 15 74 96, 660 396 815"
    }
}, {
    "node": {
        "title": "Albergue O Mirallos",
        "latitude": "42.783754",
        "longitude": "-7.536313",
        "nid": "11881",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "Donativo",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "pilgrim": "Yes",
        "marker": "Private.png",
        "email": "omirallosmanuel@gmail.com",
        "street": "Mirallos, 2",
        "phone": "982 157 162, 639 010 696"
    }
}, {
    "node": {
        "title": "Albergue Casa do Rego",
        "latitude": "42.785117",
        "longitude": "-7.542254",
        "nid": "11616",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "6",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "HOLY - OCT",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "image": "11616-134719.webp",
        "email": "casadoregopena@gmail.com",
        "street": "Aldea A Pena, 4",
        "phone": "982 167 812"
    }
}, {
    "node": {
        "title": "Albergue Mercadoiro",
        "latitude": "42.788634",
        "longitude": "-7.569000",
        "nid": "3137",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "36",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, computers",
        "cal": "½ MAR - ½ NOV",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "email": "mercadoiro@mercadoiro.com",
        "street": "Aldea de Mercadoiro, 2",
        "phone": "982 545 359"
    }
}, {
    "node": {
        "title": "Albergue Casa Banderas",
        "latitude": "42.795647",
        "longitude": "-7.602820",
        "nid": "9714",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "10",
        "bike": "Yes",
        "close": "20:00",
        "family": "Yes",
        "wifi": "Yes, WiFi and computer",
        "cal": "APR - OCT",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9714-134729.webp",
        "email": "info@casabanderas.com",
        "phone": "982 545 391, 607 431 277",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue Casa do Marabillas",
        "latitude": "42.809884",
        "longitude": "-7.615955",
        "nid": "11882",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15*\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes. 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-do-marabillas.html?aid=1499405",
        "marker": "Private.png",
        "email": "casadomarabillas@gmail.com",
        "street": "Cami\u00f1o do Monte, 3",
        "phone": "982 189 086, 744 450 425"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Portomi\u00f1o",
        "latitude": "42.809500",
        "longitude": "-7.615714",
        "nid": "9921",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/portomiao.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9921-135431.webp",
        "email": "info@portomino.com",
        "street": "Sarria, 2",
        "phone": "982 547 575, 982 545 196"
    }
}, {
    "node": {
        "title": "Albergue Manuel",
        "latitude": "42.808651",
        "longitude": "-7.614328",
        "nid": "6999",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "bike": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "6999-134725.webp",
        "street": "Mi\u00f1o, 1",
        "phone": "982 545 385"
    }
}, {
    "node": {
        "title": "Albergue de Portomar\u00edn",
        "latitude": "42.808293",
        "longitude": "-7.614942",
        "nid": "1406",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "110",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1406-61605.webp",
        "street": "Carretera de Lugo",
        "phone": "982 545 143, 660 396 816"
    }
}, {
    "node": {
        "title": "Albergue Pons Minea",
        "latitude": "42.805919",
        "longitude": "-7.617224",
        "nid": "12157",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "36",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "cal": "APR - OCT",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 6\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pons-minea.xu.html?aid=1499405",
        "marker": "Private.png",
        "image": "12157-136751.webp",
        "email": "info@ponsminea.es",
        "street": "Sarria, 11",
        "phone": "610 737 995, 686 456 931"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Posada del Camino",
        "latitude": "42.807802",
        "longitude": "-7.615898",
        "nid": "11834",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Lugo, 1",
        "phone": "982 545 081"
    }
}, {
    "node": {
        "title": "Novo Porto",
        "latitude": "42.807233",
        "longitude": "-7.616890",
        "nid": "9700",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "22",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - NOV",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "novoportoalbergue@gmail.com",
        "street": "Benigno Quiroga, 12",
        "phone": "982 545 277, 610 436 736"
    }
}, {
    "node": {
        "title": "Albergue El Caminante",
        "latitude": "42.807348",
        "longitude": "-7.616623",
        "nid": "1408",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "46",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "cal": "APR - OCT",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 6\u20ac",
        "marker": "Private.png",
        "email": "pension_elcaminante@hotmail.com",
        "street": "Benigno Quiroga, 6",
        "phone": "982 545 176"
    }
}, {
    "node": {
        "title": "Albergue Pasi\u00f1o a Pasi\u00f1o",
        "latitude": "42.806511",
        "longitude": "-7.616801",
        "nid": "12217",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "All Year",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "email": "alberguepasoapaso@gmail.com",
        "street": "Compostela, 25",
        "phone": "665 667 243"
    }
}, {
    "node": {
        "title": "Hotel Ferramenteiro",
        "latitude": "42.806323",
        "longitude": "-7.618784",
        "nid": "11836",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/ferramenteiro-de-portomarin.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11836-135430.webp",
        "email": "info@hotelferramenteiro.com",
        "street": "Chantada, 3",
        "phone": "982 545 361"
    }
}, {
    "node": {
        "title": "Albergue Folgueira",
        "latitude": "42.806605",
        "longitude": "-7.619729",
        "nid": "9848",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-folgueira.html?aid=1499405",
        "marker": "Private.png",
        "image": "9848-134723.webp",
        "email": "info@alberguefolgueira.com",
        "street": "Chantada, 18",
        "phone": "982 545 166, 659 445 651"
    }
}, {
    "node": {
        "title": "Albergue O Mirador",
        "latitude": "42.806123",
        "longitude": "-7.617930",
        "nid": "3136",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "29",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "image": "3136-61607.webp",
        "email": "rubensoengasla@yahoo.es",
        "street": "Peregrino, 27",
        "phone": "982 545 323, 667 544 545"
    }
}, {
    "node": {
        "title": "Albergue Aqua Portomar\u00edn",
        "latitude": "42.807393",
        "longitude": "-7.618174",
        "nid": "11883",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes. 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-aqua-portomarin.html?aid=1499405",
        "marker": "Private.png",
        "image": "11883-136812.webp",
        "email": "albergueaquaportomarin@hotmail.com",
        "street": "Barreiros, 2",
        "phone": "608 921 372"
    }
}, {
    "node": {
        "title": "Albergue Casa Cruz",
        "latitude": "42.807122",
        "longitude": "-7.617012",
        "nid": "11617",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "16",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "11617-134726.webp",
        "email": "info@casacruzportomarin.com",
        "street": "Benigno Quiroga, 16",
        "phone": "982 545 140, 652 204 548"
    }
}, {
    "node": {
        "title": "Albergue Ultreia Portomarin",
        "latitude": "42.808011",
        "longitude": "-7.616744",
        "nid": "7000",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "26",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "7000-136880.webp",
        "email": "info@ultreiaportomarin.com",
        "street": "Diputaci\u00f3n, 9",
        "phone": "982 545 067, 676 607 292"
    }
}, {
    "node": {
        "title": "Albergue Ferramenteiro",
        "latitude": "42.806095",
        "longitude": "-7.618318",
        "nid": "1407",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "130",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-ferramenteiro.html?aid=1499405",
        "marker": "Private.png",
        "image": "1407-136856.webp",
        "email": "info@albergueferramenteiro.com",
        "street": "Chantada, 3",
        "phone": "982 545 362, 630 958 866"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Mar",
        "latitude": "42.808029",
        "longitude": "-7.614964",
        "nid": "12190",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-mar-portomarin22.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12190-136785.webp",
        "email": "info@pensionmar.com",
        "street": "Fraga Iribarne, 5",
        "phone": "622 611 211, 630 040 194"
    }
}, {
    "node": {
        "title": "Hotel Villajard\u00edn",
        "latitude": "42.807388",
        "longitude": "-7.615034",
        "nid": "11835",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "close": "24:00",
        "cal": "½ MAR - OCT",
        "open": "7:00",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Hotel.png",
        "email": "reservas@hotelvillajardin.com",
        "street": "Mi\u00f1o, 14",
        "phone": "982 545 054"
    }
}, {
    "node": {
        "title": "Albergue Villamart\u00edn",
        "latitude": "42.807046",
        "longitude": "-7.615095",
        "nid": "9838",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAY - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": " info@alberguevillamartin.com",
        "street": "Mi\u00f1o, 14",
        "phone": " 982 545 054"
    }
}, {
    "node": {
        "title": "Albergue PortoSantiago",
        "latitude": "42.808069",
        "longitude": "-7.616794",
        "nid": "3135",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "14",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "FEB - NOV",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3135-134290.webp",
        "email": "info@albergueportosantiago.com",
        "street": "Diputaci\u00f3n, 8",
        "phone": "618 826 515"
    }
}, {
    "node": {
        "title": "Albergue de Gonzar",
        "latitude": "42.826540",
        "longitude": "-7.696524",
        "nid": "1410",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "28",
        "close": "22:00",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1410-61606.webp",
        "street": "Carretera",
        "phone": "982 157 840"
    }
}, {
    "node": {
        "title": "Albergue Privado de Gonzar - Casa Garcia",
        "latitude": "42.825103",
        "longitude": "-7.696085",
        "nid": "1409",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "cal": "ALL YEAR",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "street": "Gonzar, 8",
        "phone": "982 157 842, 670 862 386"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Maruja",
        "latitude": "42.831701",
        "longitude": "-7.708991",
        "nid": "11841",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-casa-maruja.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11841-136813.webp",
        "street": "Castromaior",
        "phone": "982 189 054"
    }
}, {
    "node": {
        "title": "Albergue Ortiz",
        "latitude": "42.830542",
        "longitude": "-7.704248",
        "nid": "12178",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "10:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "info@albergueortiz.com",
        "street": "Castromaior, 2",
        "phone": "982 099 416, 625 668 991"
    }
}, {
    "node": {
        "title": "Albergue de Hospital da Cruz",
        "latitude": "42.841330",
        "longitude": "-7.735979",
        "nid": "1411",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "32",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1411-64042.webp",
        "street": "Antiguas Escuelas",
        "phone": "982 545 232, 660 396 818"
    }
}, {
    "node": {
        "title": "Hostal El Labrador",
        "latitude": "42.840857",
        "longitude": "-7.735185",
        "nid": "11842",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "marker": "Hostal.png",
        "street": "Alto Hospital, 2",
        "phone": "982 545 303"
    }
}, {
    "node": {
        "title": "Albergue O Cruceiro",
        "latitude": "42.844015",
        "longitude": "-7.749003",
        "nid": "1413",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computers",
        "cal": "MAR - OCT",
        "open": "9:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1413-64039.webp",
        "email": "delinavazquez@hotmail.es",
        "street": "Ventas de Nar\u00f3n, 6",
        "phone": "658 064 917"
    }
}, {
    "node": {
        "title": "Albergue Casa Molar",
        "latitude": "42.844396",
        "longitude": "-7.747743",
        "nid": "1412",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "cal": "MAR - NOV",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1412-64040.webp",
        "email": "casamolar_ventas@yahoo.es",
        "street": "Ventas de Nar\u00f3n, 8",
        "phone": "697 794 507"
    }
}, {
    "node": {
        "title": "Albergue Fuente del Peregrino",
        "latitude": "42.860119",
        "longitude": "-7.781571",
        "nid": "1416",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "Donativo",
        "beds": "9",
        "bike": "Yes",
        "close": "20:00",
        "family": "Yes",
        "cal": "½ APR - SEPT",
        "open": "10:00",
        "pilgrim": "Yes",
        "marker": "Association.png",
        "image": "1416-64036.webp",
        "email": "lafuentedelperegrino@agape.org",
        "street": "Ligonde, 4",
        "phone": "687 550 527"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Escuela de Ligonde",
        "latitude": "42.861412",
        "longitude": "-7.782515",
        "nid": "1415",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "8\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - NOV",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Xunta.png",
        "image": "1415-64035.webp",
        "phone": "679 816 061"
    }
}, {
    "node": {
        "title": "Pension Eirexe",
        "latitude": "42.865715",
        "longitude": "-7.787179",
        "nid": "12167",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "price": "10\u20ac",
        "beds": "4",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes, 6\u20ac with dry",
        "marker": "Pension.png",
        "phone": "982 153 475, 650 965 873"
    }
}, {
    "node": {
        "title": "Albergue de Eirexe",
        "latitude": "42.865432",
        "longitude": "-7.787090",
        "nid": "1414",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "20",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1414-64018.webp",
        "street": "Eirexe, 17",
        "phone": "982 153 483, 660 396 819"
    }
}, {
    "node": {
        "title": "Albergue A Paso de Formiga",
        "latitude": "42.873490",
        "longitude": "-7.807169",
        "nid": "9751",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "13",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi",
        "cal": "HOLY - OCT",
        "open": "7:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9751-134353.webp",
        "email": "apasodeformiga@hotmail.com",
        "street": "Portos , 4",
        "phone": "618 984 605"
    }
}, {
    "node": {
        "title": "Casa Rectoral de Lestedo",
        "latitude": "42.873074",
        "longitude": "-7.815453",
        "nid": "11840",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "marker": "CasaRural.png",
        "street": "Lestedo",
        "phone": "982 153 435"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Curro",
        "latitude": "42.873053",
        "longitude": "-7.868306",
        "nid": "11839",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-casa-curro.html?aid=1499405",
        "marker": "Pension.png",
        "street": "Ourense, 15",
        "phone": "982 380 044"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Palas",
        "latitude": "42.875441",
        "longitude": "-7.864376",
        "nid": "9919",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-palas.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9919-135428.webp",
        "email": "info@pensionpalas.es",
        "street": "San Tirso",
        "phone": "982 380 065"
    }
}, {
    "node": {
        "title": "Albergue Mes\u00f3n de Benito",
        "latitude": "42.872433",
        "longitude": "-7.867194",
        "nid": "7003",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "80",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "APR - ½ OCT",
        "open": "6:30",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "7003-64046.webp",
        "email": "alberguemesondebenito@gmail.com",
        "street": "Paz",
        "phone": "982 103 356, 636 834 065"
    }
}, {
    "node": {
        "title": "Casa Carla",
        "latitude": "42.872131",
        "longitude": "-7.870497",
        "nid": "12192",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-carla-palas-de-rei.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "12192-136787.webp",
        "email": "rivasroucomariadelcarmen@yahoo.es",
        "street": "La Paz, 3",
        "phone": " 626 518 388"
    }
}, {
    "node": {
        "title": "Albergue A Casi\u00f1a di Marcello",
        "latitude": "42.873183",
        "longitude": "-7.872603",
        "nid": "11291",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "14",
        "bike": "Yes",
        "close": "22:02",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR*",
        "open": "13:28",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-a-casina-di-marcello.html?aid=1499405",
        "marker": "Private.png",
        "image": "11291-136102.webp",
        "email": "albergueacasina@gmail.com",
        "street": "Cami\u00f1o da Aldeia de Abaixo",
        "phone": "640 723 903"
    }
}, {
    "node": {
        "title": "Albergue Castro",
        "latitude": "42.872975",
        "longitude": "-7.868513",
        "nid": "9666",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "56",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9666-134432.webp",
        "email": "alberguecastro@yahoo.es",
        "street": "Ourense, 24",
        "phone": "609 080 655"
    }
}, {
    "node": {
        "title": "Albergue de Palas de Rei",
        "latitude": "42.873234",
        "longitude": "-7.869043",
        "nid": "1418",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "23:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Xunta.png",
        "image": "1418-61615.webp",
        "street": "Carretera de Compostela, 19",
        "phone": "660 396 820"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Bar Plaza",
        "latitude": "42.873016",
        "longitude": "-7.869084",
        "nid": "11838",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes",
        "cal": "MAR - SEPT",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-plaza-san-ignacio.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11838-135242.jpeg",
        "email": "info@pensionplazapalas.es",
        "street": "Compostela, 21",
        "phone": "982 380 109"
    }
}, {
    "node": {
        "title": "Hostal Ponterrox\u00e1n",
        "latitude": "42.872245",
        "longitude": "-7.881339",
        "nid": "9918",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/ponterroxan.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9918-135429.webp",
        "email": "hostalponteroxan@hostalponteroxan.es",
        "street": "Compostela, 109",
        "phone": "982 380 132"
    }
}, {
    "node": {
        "title": "Albergue Os Chacotes",
        "latitude": "42.873962",
        "longitude": "-7.857205",
        "nid": "7002",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "112",
        "close": "22:00",
        "wifi": "No, but wifi nearby",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "7002-64030.webp",
        "street": "Carretera de Compostela, 19",
        "phone": "607 481 536"
    }
}, {
    "node": {
        "title": "Pension Casa Cami\u00f1o",
        "latitude": "42.873311",
        "longitude": "-7.869900",
        "nid": "12191",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-restaurante-casa-camino.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12191-136786.webp",
        "email": "reservas@pulperiacasacamino.es",
        "street": "Peregrino, 10",
        "phone": "982 374 066"
    }
}, {
    "node": {
        "title": "Complejo la Caba\u00f1a",
        "latitude": "42.873944",
        "longitude": "-7.859430",
        "nid": "9920",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/complejo-la-cabana.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "9920-135427.webp",
        "email": "complejolacabana@complejolacabana.com",
        "street": "Doctor Pardo Ouro, 4",
        "phone": "982 380 750, 982 380 751 "
    }
}, {
    "node": {
        "title": "Albergue San Marcos",
        "latitude": "42.872625",
        "longitude": "-7.867994",
        "nid": "9665",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "71",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "open": "6:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9665-134289.webp",
        "email": "info@alberguesanmarcos.es",
        "street": "Next to church",
        "phone": "982 380 711, 606 726 356"
    }
}, {
    "node": {
        "title": "Albergue Zendoira",
        "latitude": "42.869049",
        "longitude": "-7.868201",
        "nid": "12212",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/zendoira.html?aid=1499405&no_rooms=1&group_adults=1",
        "marker": "Private.png",
        "email": "info@zendoira.com",
        "street": "Amado Losada, 10",
        "phone": "629 727 605, 608 490 075"
    }
}, {
    "node": {
        "title": "Hostel O Castelo",
        "latitude": "42.872814",
        "longitude": "-7.867015",
        "nid": "11837",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/o-castelo-palas-de-rei.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11837-135425.webp",
        "email": "hola@hostelocastelo.com",
        "street": "Cruceiro, 14",
        "phone": "618 401 130"
    }
}, {
    "node": {
        "title": "Albergue Outeiro",
        "latitude": "42.873763",
        "longitude": "-7.867370",
        "nid": "9896",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "50",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes, well equipped",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-outeiro.html?aid=1499405",
        "marker": "Private.png",
        "image": "9896-136814.webp",
        "email": "info@albergueouteiro.com",
        "street": "Plaza de Galicia, 25",
        "phone": "982 380 242, 630 134 357"
    }
}, {
    "node": {
        "title": "Albergue Buen Camino",
        "latitude": "42.873191",
        "longitude": "-7.869844",
        "nid": "1420",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "41",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "HOLY - OCT",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1420-64044.webp",
        "email": "alberguebuencamino@yahoo.es",
        "street": "Peregrino, 3",
        "phone": "982 380 233, 639 882 229"
    }
}, {
    "node": {
        "title": "Albergue O Abrigadoiro",
        "latitude": "42.874436",
        "longitude": "-7.903214",
        "nid": "1419",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "HOLY - OCT",
        "open": "7:00",
        "reserve": "Yes",
        "washer": "Yes, 5\u20ac",
        "marker": "Private.png",
        "email": " medeagomez@yahoo.es",
        "street": "San Xulian do Cami\u00f1o",
        "phone": "982 374 117"
    }
}, {
    "node": {
        "title": "Albergue Casa Domingo",
        "latitude": "42.878182",
        "longitude": "-7.914577",
        "nid": "1421",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "18",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "family": "Yes",
        "wifi": "Yes, computer",
        "cal": "MAY - OCT",
        "open": "12:00",
        "pilgrim": "Pilgrims preferred",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "casa_domingo@hotmail.com",
        "street": "Aldea de Pontecampa\u00f1a - Mato",
        "phone": "982 163 226, 630 728 864"
    }
}, {
    "node": {
        "title": "Albergue Tur\u00edstico A Bolboreta",
        "latitude": "42.869847",
        "longitude": "-7.941356",
        "nid": "1423",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "13*\u20ac",
        "beds": "8",
        "bike": "Yes",
        "close": "21:00",
        "family": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - ½ OCT",
        "open": "13:00",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/a-bolboreta.html?aid=1499405",
        "marker": "Private.png",
        "image": "1423-136839.webp",
        "email": " montse@abolboreta.com",
        "street": "Vilar de Remonde",
        "phone": "609 124 717"
    }
}, {
    "node": {
        "title": "Albergue de Mato",
        "latitude": "42.878840",
        "longitude": "-7.928925",
        "nid": "1422",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "phone": "982 173 483, 660 396 821"
    }
}, {
    "node": {
        "title": "Hostal Rural Casa de los Somoza",
        "latitude": "42.884885",
        "longitude": "-7.958525",
        "nid": "11851",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/casa-de-los-somoza.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11851-136816.webp",
        "street": "O Coto",
        "phone": "981 507 372"
    }
}, {
    "node": {
        "title": "Hotel Los Dos Alemanes",
        "latitude": "42.884978",
        "longitude": "-7.957084",
        "nid": "9917",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/die-zwei-deutsch.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9917-135424.webp",
        "street": "Coto Melide",
        "phone": "981 507 337"
    }
}, {
    "node": {
        "title": "Albergue Montoto",
        "latitude": "42.912341",
        "longitude": "-8.019341",
        "nid": "12193",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "42",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - OCT",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-montoto.html?aid=1499405",
        "marker": "Private.png",
        "image": "12193-136788.webp",
        "email": "alberguemontoto@gmail.com",
        "street": "Codeseira, 31",
        "phone": "981 507 337, 646 941 887"
    }
}, {
    "node": {
        "title": "B&B A L\u00faa do Cami\u00f1o",
        "latitude": "42.911410",
        "longitude": "-8.007861",
        "nid": "11848",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/a-lua-do-camino.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11848-135420.webp",
        "email": "luadocamino@gmail.com",
        "street": "Circunvalaci\u00f3n - Campo da Feira",
        "phone": "620 958 331"
    }
}, {
    "node": {
        "title": "Albergue San Ant\u00f3n",
        "latitude": "42.914767",
        "longitude": "-8.017334",
        "nid": "9717",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "36",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-san-anton.html?aid=1499405",
        "marker": "Private.png",
        "image": "9717-134375.webp",
        "email": "alberguesananton@gmail.com",
        "street": "San Antonio, 6",
        "phone": "981 506 427, 698 153 672"
    }
}, {
    "node": {
        "title": "Albergue Pereiro",
        "latitude": "42.913248",
        "longitude": "-8.017118",
        "nid": "9363",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "6\/10\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostel-pereiro.html?aid=1499405",
        "marker": "Private.png",
        "image": "9363-136850.webp",
        "email": "info@alberguepereiro.com",
        "street": "Progreso, 43",
        "phone": "981 506 314"
    }
}, {
    "node": {
        "title": "Hotel Sony",
        "latitude": "42.911885",
        "longitude": "-8.020282",
        "nid": "11850",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pensia3n-restaurante-sony.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11850-136818.webp",
        "street": "Santiago",
        "phone": "981 505 473"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Berenguela",
        "latitude": "42.913906",
        "longitude": "-8.014292",
        "nid": "10025",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "close": "23:30",
        "cal": "ALL YEAR",
        "open": "12:30",
        "washer": "Yes",
        "marker": "Pension.png",
        "image": "10025-136857.webp",
        "email": "reservas@pensionberenguela.com",
        "street": "San Roque, 2",
        "phone": "981 505 417"
    }
}, {
    "node": {
        "title": "Albergue Melide",
        "latitude": "42.912359",
        "longitude": "-8.006600",
        "nid": "9667",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "57",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "APR - OCT",
        "open": "11:00",
        "reserve": "Yes, online",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-melide-melide.html?aid=1499405",
        "marker": "Private.png",
        "image": "9667-134349.webp",
        "email": "info@alberguemelide.com",
        "street": "Lugo, 92",
        "phone": "981 507 491, 627 901 552"
    }
}, {
    "node": {
        "title": "Albergue Arraigos",
        "latitude": "42.913884",
        "longitude": "-8.013918",
        "nid": "12145",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "20",
        "barrest": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-arraigos.html?aid=1499405",
        "marker": "Private.png",
        "street": "Cant\u00f3n San Roque, 9",
        "phone": "646 343 370"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n El Molino",
        "latitude": "42.915046",
        "longitude": "-8.011507",
        "nid": "11807",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/el-molino-melide.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11807-135421.webp",
        "street": "Castro, 23",
        "phone": "981 506 048"
    }
}, {
    "node": {
        "title": "Albergue Vilela",
        "latitude": "42.914795",
        "longitude": "-8.017130",
        "nid": "9669",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "28",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR*",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-melide.html?aid=1499405",
        "marker": "Private.png",
        "image": "9669-135423.webp",
        "email": "alberguevilela@gmail.com",
        "street": "San Antonio, 2",
        "phone": "616 011 375"
    }
}, {
    "node": {
        "title": "Albergue O Apalpador",
        "latitude": "42.914449",
        "longitude": "-8.017894",
        "nid": "5530",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "5530-61626.webp",
        "email": "cochemelide@hotmail.com",
        "street": "San Antonio, 23",
        "phone": "679 837 969"
    }
}, {
    "node": {
        "title": "Hotel Carlos 96",
        "latitude": "42.912324",
        "longitude": "-8.004990",
        "nid": "11849",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/carlos-96-sl.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "11849-136817.webp",
        "email": "gestion@hc96.com",
        "street": "Lugo, 119",
        "phone": "981 507 633"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Xaneiro",
        "latitude": "42.914635",
        "longitude": "-8.011678",
        "nid": "10024",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/restaurante-xaneiro.html?aid=1499405",
        "marker": "Pension.png",
        "image": "10024-135422.webp",
        "email": "hotel@xaneiro.com",
        "street": "La Habana, 43",
        "phone": "981 506 140"
    }
}, {
    "node": {
        "title": "Albergue O Cruceiro",
        "latitude": "42.913938",
        "longitude": "-8.014497",
        "nid": "9364",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "72",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer (pay)",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9364-96795.webp",
        "email": "albergueocruceiro@yahoo.es",
        "street": "Ronda de A Coru\u00f1a, 2",
        "phone": "616 764 896"
    }
}, {
    "node": {
        "title": "Albergue Alfonso II",
        "latitude": "42.918350",
        "longitude": "-8.014685",
        "nid": "11884",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "34",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - SEPT",
        "open": "11:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-alfonso-ii.html?aid=1499405",
        "marker": "Private.png",
        "image": "11884-135419.webp",
        "email": "info@alberguealfonsoelcasto.com",
        "street": "Toques y Friol, 52-54",
        "phone": "981 506 454, 608 604 850"
    }
}, {
    "node": {
        "title": "Hotel Pousada Chiquit\u00edn",
        "latitude": "42.914663",
        "longitude": "-8.017799",
        "nid": "10026",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "price": "30-70\u20ac",
        "beds": "16",
        "marker": "Hotel.png",
        "image": "10026-136742.webp",
        "email": "info@chiquitinmelide.com",
        "street": "San Ant\u00f3n, 18",
        "phone": "981 815 333"
    }
}, {
    "node": {
        "title": "Albergue O Apalpador II",
        "latitude": "42.913815",
        "longitude": "-8.013943",
        "nid": "9668",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "image": "9668-131918.webp",
        "email": "cochemelide@hotmail.com",
        "street": "Cant\u00f3n de San Roque, 9",
        "phone": "981 506 266, 679 837 969"
    }
}, {
    "node": {
        "title": "Albergue de Melide",
        "latitude": "42.914451",
        "longitude": "-8.018160",
        "nid": "1425",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "156",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "street": "San Antonio, 25",
        "phone": "660 396 822"
    }
}, {
    "node": {
        "title": "Albergue Boente",
        "latitude": "42.916098",
        "longitude": "-8.077810",
        "nid": "8095",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "22",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR*",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "8095-61631.webp",
        "email": "albergueboente@hotmail.es",
        "street": "En frente de la Iglesia",
        "phone": "981 501 974, 629 321 707"
    }
}, {
    "node": {
        "title": "Albergue Os Albergues",
        "latitude": "42.915999",
        "longitude": "-8.078272",
        "nid": "7005",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - OCT",
        "open": "8:00",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "7005-61633.webp",
        "email": "os_albergues@hotmail.es",
        "phone": "981 501 853, 629 146 826"
    }
}, {
    "node": {
        "title": "Albergue Santiago",
        "latitude": "42.924688",
        "longitude": "-8.097885",
        "nid": "3134",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "1\u20ac",
        "beds": "6",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, computer",
        "kitchen": "No, bar attached",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3134-61639.webp",
        "email": "albergue.santiago.castaneda@gmail.com",
        "phone": "981 501 711, 699 761 698"
    }
}, {
    "node": {
        "title": "Albergue Los Caminantes",
        "latitude": "42.930151",
        "longitude": "-8.131415",
        "nid": "7006",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "56",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "7006-61636.webp",
        "email": "info@albergueloscaminantes.com",
        "street": "Ribadiso de Baixo",
        "phone": "981 500 295, 647 020 600"
    }
}, {
    "node": {
        "title": "Albergue de Ribadiso da Baixo",
        "latitude": "42.930798",
        "longitude": "-8.130446",
        "nid": "1426",
        "nodetype": "Accommodation",
        "accomtype": "Municipal",
        "price": "6\u20ac",
        "beds": "70",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "No, but available at a bar nearby",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Municipal.png",
        "image": "1426-61634.webp",
        "street": "Ribadiso de Baixo",
        "phone": "981 501 185, 660 396 823"
    }
}, {
    "node": {
        "title": "Albergue Milpes",
        "latitude": "42.930484",
        "longitude": "-8.136095",
        "nid": "9849",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "24",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "MAY - OCT",
        "open": "11:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-milphtml?aid=1499405",
        "marker": "Private.png",
        "image": "9849-134291.webp",
        "email": "info@alberguemilpes.com",
        "street": "Ribadiso, 7",
        "phone": "981 500 425"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Cima do Lugar",
        "latitude": "42.926957",
        "longitude": "-8.160743",
        "nid": "12195",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-pension-cima-do-lugar.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12195-136790.webp",
        "street": "Cima do Lugar, 22"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Mes\u00f3n do Peregrino",
        "latitude": "42.927332",
        "longitude": "-8.163994",
        "nid": "11844",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "arzua@mesondoperegrino.com",
        "street": "Ram\u00f3n Franco, 7",
        "phone": "981 500 145"
    }
}, {
    "node": {
        "title": "Albergue Santiago Ap\u00f3stol (Arz\u00faa)",
        "latitude": "42.928912",
        "longitude": "-8.156205",
        "nid": "3133",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "72",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR*",
        "open": "7:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3133-61640.webp",
        "email": "santiagoapostolalbergue@hotmail.com",
        "street": "Lugo, 107",
        "phone": "981 508 132, 981 500 004"
    }
}, {
    "node": {
        "title": "Albergue de Arz\u00faa",
        "latitude": "42.926278",
        "longitude": "-8.162588",
        "nid": "1427",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "46",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1427-61643.webp",
        "street": "Cima de Lugar, 6",
        "phone": "660 396 824"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casa Teodora",
        "latitude": "42.927100",
        "longitude": "-8.160891",
        "nid": "11846",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "reservas@casateodora.com",
        "street": "Lugo, 38",
        "phone": "981 500 083"
    }
}, {
    "node": {
        "title": "De Camino Albergue",
        "latitude": "42.928629",
        "longitude": "-8.156396",
        "nid": "9850",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "46",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "MAR - NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/de-camino.html?aid=1499405",
        "marker": "Private.png",
        "image": "9850-136736.webp",
        "email": "info@decaminoalbergue.com",
        "street": "Lugo, 118",
        "phone": "981 500 415, 678 758 296"
    }
}, {
    "node": {
        "title": "Albergue Via Lactea",
        "latitude": "42.925425",
        "longitude": "-8.164085",
        "nid": "1429",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "120",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes, 2 of them",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "1429-135206.webp",
        "email": "vialacteaalbergue@hotmail.com",
        "street": "Jos\u00e9 Antonio, 26",
        "phone": "981 500 581, 616 759 447"
    }
}, {
    "node": {
        "title": "La Casona de Nen\u00e9 ",
        "latitude": "42.926390",
        "longitude": "-8.162600",
        "nid": "12194",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/la-casona-de-nene.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "12194-136789.webp",
        "street": "Padre Pardo, 24"
    }
}, {
    "node": {
        "title": "Hotel Suiza",
        "latitude": "42.929984",
        "longitude": "-8.171907",
        "nid": "11843",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "marker": "Hotel.png",
        "email": "informacion@hsuiza.com",
        "street": "Vello",
        "phone": "981 500 908"
    }
}, {
    "node": {
        "title": "Albergue da Fonte",
        "latitude": "42.926282",
        "longitude": "-8.164284",
        "nid": "3132",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3132-61645.webp",
        "email": "alberguedafonte@hotmail.com",
        "street": "Carme, 18",
        "phone": "981 501 118, 659 999 496"
    }
}, {
    "node": {
        "title": "The Way Hostel Arz\u00faa ",
        "latitude": "42.926858",
        "longitude": "-8.161144",
        "nid": "12556",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/17\u20ac",
        "beds": "40",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "kitchen": "No",
        "microwave": "Yes",
        "cal": "APR - ½ NOV",
        "pilgrim": "No",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "No",
        "washer": "Yes, 3",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/the-way-hostel-arzua.html?aid=1499405",
        "marker": "Private.png",
        "image": "12556-137376.webp",
        "email": "thewayarzua@gmail.com",
        "street": "Cima do Lugar, 28",
        "phone": "680 969 697"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n R\u00faa",
        "latitude": "42.929041",
        "longitude": "-8.155338",
        "nid": "11845",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "pensionrua@hotmail.com",
        "street": "Lugo, 130",
        "phone": "981 500 139"
    }
}, {
    "node": {
        "title": "Albergue Los Caminantes",
        "latitude": "42.927111",
        "longitude": "-8.165210",
        "nid": "5541",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "28",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "HOLY - NOV",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "5541-64027.webp",
        "email": "info@albergueloscaminantes.com",
        "street": "Santiago, 14",
        "phone": "981 508 127, 647 020 600"
    }
}, {
    "node": {
        "title": "Albergue Ultreia",
        "latitude": "42.928767",
        "longitude": "-8.156020",
        "nid": "1428",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10*\u20ac",
        "beds": "38",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ FEB - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1428-61642.webp",
        "email": "info@albergueultreia.com",
        "street": "Lugo, 126",
        "phone": "981 500 471, 626 639 450",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "O Albergue de Selmo",
        "latitude": "42.929762",
        "longitude": "-8.153969",
        "nid": "11885",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "50",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - ½ OCT",
        "open": "12:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "marker": "Private.png",
        "email": "info@oalberguedeselmo.com",
        "street": "Lugo, 133",
        "phone": "981 939 018"
    }
}, {
    "node": {
        "title": "Pazo Santa Mar\u00eda",
        "latitude": "42.938706",
        "longitude": "-8.158336",
        "nid": "9916",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pazo-de-santa-maria.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9916-135441.webp",
        "street": "O Pazo",
        "phone": "981 500 702"
    }
}, {
    "node": {
        "title": "Albergue Don Quijote",
        "latitude": "42.928996",
        "longitude": "-8.155366",
        "nid": "1430",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "50",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "1430-61641.webp",
        "email": "alberguedonquijote@hotmail.com",
        "street": "Lugo, 130",
        "phone": "981 500 139, 696 162 695"
    }
}, {
    "node": {
        "title": "Albergue Cami\u00f1o das Ocas",
        "latitude": "42.921644",
        "longitude": "-8.206894",
        "nid": "9897",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "email": "contacto@caminodasocas.com",
        "phone": "648 404 780"
    }
}, {
    "node": {
        "title": "Albergue Alborada",
        "latitude": "42.926171",
        "longitude": "-8.280628",
        "nid": "12138",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "10",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "HOLY - OCT",
        "open": "14:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "pensionalberguealborada@gmail.com",
        "street": "N-547",
        "phone": "620 151 209"
    }
}, {
    "node": {
        "title": "El Albergue de Boni",
        "latitude": "42.926724",
        "longitude": "-8.280196",
        "nid": "9670",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "30",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "open": "14:00",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "email": "elalberguedeboni@gmail.com",
        "street": "N-547, km 75.5",
        "phone": "618 965 907"
    }
}, {
    "node": {
        "title": "Albergue Turistico de Salceda",
        "latitude": "42.922170",
        "longitude": "-8.275456",
        "nid": "7007",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "8 in albergue 28 in pension",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/complejo-turistico-salceda.html?aid=1499405",
        "marker": "Private.png",
        "image": "7007-135443.webp",
        "email": "pousadadesalceda@gmail.com",
        "street": "Calzada",
        "phone": "981 502 767"
    }
}, {
    "node": {
        "title": "Albergue El Chalet",
        "latitude": "42.916594",
        "longitude": "-8.309736",
        "nid": "12129",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "14",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "APR - OCT",
        "open": "12:00",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "elchaletalbergue@gmail.com",
        "street": "A Brea, 5",
        "phone": "659 380 723"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n The Way",
        "latitude": "42.920326",
        "longitude": "-8.304677",
        "nid": "9915",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-the-way.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9915-135444.webp",
        "street": "A Brea, 36 ",
        "phone": "628 120 202"
    }
}, {
    "node": {
        "title": "Albergue Andaina",
        "latitude": "42.915494",
        "longitude": "-8.323259",
        "nid": "12218",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "14",
        "barrest": "Yes",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "6:30",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "parrilladaandaina@gmail.com",
        "phone": "981 502 925"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Santa Irene (Municipal)",
        "latitude": "42.918397",
        "longitude": "-8.335662",
        "nid": "1432",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1432-134297.webp",
        "phone": "660 396 825"
    }
}, {
    "node": {
        "title": "Albergue de Peregrinos Santa Irene (Privado)",
        "latitude": "42.917062",
        "longitude": "-8.332164",
        "nid": "1431",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "13\u20ac",
        "beds": "15",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes, WiFi",
        "cal": "HOLY - NOV",
        "open": "12:00",
        "reserve": "Yes",
        "washer": "Yes, 4.50\u20ac",
        "marker": "Private.png",
        "image": "1431-134746.webp",
        "street": "Concejo do Pino",
        "phone": "981 511 000"
    }
}, {
    "node": {
        "title": "Albergue Rural Astrar",
        "latitude": "42.911767",
        "longitude": "-8.337726",
        "nid": "9898",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9898-134742.webp",
        "email": "albergueruralastrar@gmail.com",
        "street": "Astrar, 18",
        "phone": "981 511 463, 608 092 820"
    }
}, {
    "node": {
        "title": "Albergue Edreira",
        "latitude": "42.903681",
        "longitude": "-8.360606",
        "nid": "5532",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computers",
        "microwave": "Yes",
        "cal": "MAR - OCT*",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "5532-134748.webp",
        "email": "info@albergue-edreira.com",
        "street": "Fonte, 19",
        "phone": "981 511 365, 660 234 995"
    }
}, {
    "node": {
        "title": "Bule Bic",
        "latitude": "42.905637",
        "longitude": "-8.360438",
        "nid": "12221",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Lugo, 18",
        "phone": "981 511 222"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Comp\u00e1s",
        "latitude": "42.909083",
        "longitude": "-8.358820",
        "nid": "11857",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "pensioncompas@gmail.com",
        "street": "Lugo, 47",
        "phone": "981 511 309"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n A Solaina",
        "latitude": "42.905192",
        "longitude": "-8.360319",
        "nid": "11852",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-a-solaina.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11852-135449.webp",
        "email": "pensionasolaina@hotmail.com",
        "street": "Pic\u00f3n, 3",
        "phone": "633 530 918"
    }
}, {
    "node": {
        "title": "Albergue Cruceiro de Pedrouzo",
        "latitude": "42.902969",
        "longitude": "-8.362829",
        "nid": "9671",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "94",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9671-134744.webp",
        "email": "reservas@alberguecruceirodepedrouzo.com",
        "street": "Iglesia, 7",
        "phone": "981 511 371, 629 518 204"
    }
}, {
    "node": {
        "title": "Albergue de Arca do Pino",
        "latitude": "42.907131",
        "longitude": "-8.358718",
        "nid": "1433",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "120",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes, but no cookware",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "image": "1433-62401.webp",
        "street": "Pedrouzo",
        "phone": "660 396 826"
    }
}, {
    "node": {
        "title": "LO Pedrouzo",
        "latitude": "42.906430",
        "longitude": "-8.365202",
        "nid": "12223",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-lo.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12223-136930.jpeg",
        "street": "Mollados, 43",
        "phone": "608 989 100"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Codesal",
        "latitude": "42.902144",
        "longitude": "-8.366900",
        "nid": "12196",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-codesal.html?aid=1499405",
        "marker": "Pension.png",
        "image": "12196-136791.webp",
        "street": "Codesal, 17",
        "phone": "981 511 061"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n 9 de Abril",
        "latitude": "42.904231",
        "longitude": "-8.363561",
        "nid": "11854",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-9-de-abril.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11854-135447.webp",
        "email": "infopension9deabril@gmail.com",
        "street": "Santiago, 7",
        "phone": "606 764 762"
    }
}, {
    "node": {
        "title": "Hotel O Pino",
        "latitude": "42.916254",
        "longitude": "-8.350983",
        "nid": "9912",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/o-pino.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9912-135445.webp",
        "email": "info@hotelopino.com",
        "street": "A R\u00faa, 9",
        "phone": "981 511 035"
    }
}, {
    "node": {
        "title": "Albergue O Burgo",
        "latitude": "42.909166",
        "longitude": "-8.358707",
        "nid": "5531",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "14",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "MAY - OCT",
        "open": "12:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "5531-64026.webp",
        "email": "albergueoburgo@gmail.com",
        "street": "Lugo, 47",
        "phone": "630 404 138"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n O Mui\u00f1o ",
        "latitude": "42.905372",
        "longitude": "-8.362988",
        "nid": "12220",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Mui\u00f1o, 1",
        "phone": "981 511 144"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Arca",
        "latitude": "42.905964",
        "longitude": "-8.365867",
        "nid": "11856",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "email": "pensionarca@gmail.com",
        "street": "Mollados, 25",
        "phone": "981 511 437"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Maribel",
        "latitude": "42.905575",
        "longitude": "-8.365864",
        "nid": "9914",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-maribel.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9914-135153.webp",
        "email": "info@pensionmaribel.com",
        "street": "Mollados, 23",
        "phone": "981 511 404, 609 459 966"
    }
}, {
    "node": {
        "title": "Albergue Otero",
        "latitude": "42.905001",
        "longitude": "-8.363460",
        "nid": "7009",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, computers",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "7009-64025.webp",
        "email": "info@albergueotero.com",
        "street": "Forcarei, 2",
        "phone": "671 663 374"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Casal de Calma",
        "latitude": "42.901559",
        "longitude": "-8.362954",
        "nid": "12222",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Igrexa, 10",
        "phone": "680 910 676"
    }
}, {
    "node": {
        "title": "Albergue O Trisquel",
        "latitude": "42.905040",
        "longitude": "-8.360969",
        "nid": "12130",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "68",
        "bike": "Yes",
        "close": "22:00",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "MAR - OCT",
        "open": "11:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-o-trisquel.html?aid=1499405",
        "marker": "Private.png",
        "image": "12130-136819.webp",
        "email": "informatrisquel@gmail.com",
        "street": "Pic\u00f3n, 1",
        "phone": "616 644 740, 607 024 304"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Platas",
        "latitude": "42.906735",
        "longitude": "-8.359349",
        "nid": "11853",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bike": "Yes",
        "wifi": "Yes, WiFi and computer",
        "cal": "½ MAR - NOV",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-platas.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11853-136878.webp",
        "email": "hostalplatas@hotmail.com",
        "street": "Lugo, 26",
        "phone": "981 511 378",
        "veg": "Yes"
    }
}, {
    "node": {
        "title": "Albergue REM",
        "latitude": "42.903058",
        "longitude": "-8.362913",
        "nid": "9851",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "40",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi",
        "cal": "ALL YEAR",
        "open": "10:00",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/rem-hostel.html?aid=1499405",
        "marker": "Private.png",
        "image": "9851-134745.webp",
        "email": "info@hostelrem.com",
        "street": "Iglesia, 7",
        "phone": "981 510 407, 618 533 515"
    }
}, {
    "node": {
        "title": "Albergue Porta de Santiago",
        "latitude": "42.905231",
        "longitude": "-8.361512",
        "nid": "3131",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "23:00",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "MAR - NOV",
        "open": "12:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "3131-134747.webp",
        "email": "portadesantiago@hotmail.com",
        "street": "Lugo, 11",
        "phone": "981 511 103, 607 835 354"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Maruja",
        "latitude": "42.903339",
        "longitude": "-8.364680",
        "nid": "12219",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "marker": "Pension.png",
        "street": "Nova, 9",
        "phone": "981 511 406"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n En Ruta SCQ",
        "latitude": "42.903832",
        "longitude": "-8.366161",
        "nid": "11855",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/en-ruta-scq.html?aid=1499405",
        "marker": "Pension.png",
        "image": "11855-135446.webp",
        "email": "info@enrutascq.es",
        "street": "Santiago, 23",
        "phone": "981 511 471"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Una Estrella Dorada",
        "latitude": "42.905080",
        "longitude": "-8.361325",
        "nid": "9913",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pr-una-estrella-dorada.html?aid=1499405",
        "marker": "Pension.png",
        "image": "9913-135461.webp",
        "street": "Lugo, 10",
        "phone": "630 018 363"
    }
}, {
    "node": {
        "title": "Hotel Amenal",
        "latitude": "42.905579",
        "longitude": "-8.394482",
        "nid": "9911",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/amenal-camino-de-santiago.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9911-135460.webp",
        "email": "reservas@hotelamenal.com",
        "street": "Amenal, 12",
        "phone": "981 510 431"
    }
}, {
    "node": {
        "title": "Hotel Garcas",
        "latitude": "42.899799",
        "longitude": "-8.436792",
        "nid": "9910",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/garcas.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9910-135458.webp",
        "email": "reservas@hotelgarcas.com",
        "street": "Naval, 2",
        "phone": "981 888 225"
    }
}, {
    "node": {
        "title": "Pazo Xan Xordo",
        "latitude": "42.901715",
        "longitude": "-8.455428",
        "nid": "11860",
        "nodetype": "Accommodation",
        "accomtype": "Casa Rural",
        "cal": "MAR - DEC",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pazo-xan-xordo.html?aid=1499405",
        "marker": "CasaRural.png",
        "image": "11860-135462.webp",
        "email": "info@pazoxanxordo.com",
        "street": "Xan Xordo, 6",
        "phone": "981 888 259"
    }
}, {
    "node": {
        "title": "Hotel Ruta Jacobea",
        "latitude": "42.899601",
        "longitude": "-8.440869",
        "nid": "9909",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/ruta-jacobea.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9909-135459.webp",
        "street": "Lavacolla, 41",
        "phone": "981 888 211"
    }
}, {
    "node": {
        "title": "Hostal San Paio",
        "latitude": "42.900256",
        "longitude": "-8.447525",
        "nid": "11859",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-san-paio-lavacolla.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "11859-136820.webp",
        "email": "contacto@hostalsanpaio.com",
        "street": "Lavacolla",
        "phone": "981 888 205"
    }
}, {
    "node": {
        "title": "Albergue Lavacolla",
        "latitude": "42.899260",
        "longitude": "-8.443954",
        "nid": "12204",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "32",
        "bike": "Yes",
        "close": "22:30",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "12:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes",
        "marker": "Private.png",
        "email": "reservas@alberguelavacolla.com",
        "street": "Lavacolla, 35",
        "phone": "981 897 274, 653 630 300"
    }
}, {
    "node": {
        "title": "Hotel Akelarre",
        "latitude": "42.892648",
        "longitude": "-8.488966",
        "nid": "9908",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/akelarre.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9908-135457.webp",
        "street": "San Marcos, 37",
        "phone": "981 552 689"
    }
}, {
    "node": {
        "title": "Albergue de Monte do Gozo",
        "latitude": "42.887464",
        "longitude": "-8.498075",
        "nid": "1434",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "6\u20ac",
        "beds": "400",
        "bike": "Yes, but not always secure",
        "close": "22:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "13:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Xunta.png",
        "email": "kuba@mypielgrzymi.com",
        "street": "Estrellas, 80",
        "phone": "660 396 827"
    }
}, {
    "node": {
        "title": "Albergue de peregrinos Jaime Garc\u00eda Rodr\u00edguez",
        "latitude": "42.886102",
        "longitude": "-8.522934",
        "nid": "5533",
        "nodetype": "Accommodation",
        "accomtype": "Association",
        "price": "8\u20ac",
        "beds": "150",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "11:30",
        "pilgrim": "Yes",
        "reserve": "Yes",
        "washer": "Yes",
        "marker": "Association.png",
        "street": "Estocolmo",
        "phone": "981 587 324"
    }
}, {
    "node": {
        "title": "Albergue Basquinos 45",
        "latitude": "42.888074",
        "longitude": "-8.537270",
        "nid": "12198",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-basquinos-45.html?aid=1499405",
        "marker": "Private.png",
        "image": "12198-136794.webp",
        "street": "Pedro de Mezonzo, 28",
        "phone": "661 894 536"
    }
}, {
    "node": {
        "title": "Hostal Mapoula",
        "latitude": "42.877002",
        "longitude": "-8.544230",
        "nid": "9904",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-mapoula.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9904-135456.webp",
        "email": "mapoula@mapoula.com",
        "street": "Entremurallas, 10-3",
        "phone": "981 580 124"
    }
}, {
    "node": {
        "title": "Hostal Reis Cat\u00f3licos (Parador de Santiago)",
        "latitude": "42.880984",
        "longitude": "-8.545867",
        "nid": "9899",
        "nodetype": "Accommodation",
        "accomtype": "Parador",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hostal-reis-catolicos.html?aid=1499405",
        "marker": "Parador.png",
        "image": "9899-135454.webp",
        "street": "Obradoiro, 1 ",
        "phone": "981 582 200"
    }
}, {
    "node": {
        "title": "Albergue Meiga Backpackers",
        "latitude": "42.887390",
        "longitude": "-8.538836",
        "nid": "9675",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "11\/13*\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "20:00",
        "wifi": "Yes, WiFi ",
        "kitchen": "Yes",
        "cal": "½ MAR - OCT",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.5\u20ac",
        "marker": "Private.png",
        "email": "info_meiga@yahoo.es",
        "street": "Basqui\u00f1os, 67",
        "phone": "981 570 846"
    }
}, {
    "node": {
        "title": "Albergue Santo Santiago",
        "latitude": "42.887254",
        "longitude": "-8.525412",
        "nid": "7010",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\u20ac",
        "beds": "40",
        "bike": "Yes",
        "close": "23:30",
        "wifi": "Yes, WiFi and computer",
        "cal": "ALL YEAR",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "email": "elsantosantiago@gmail.com",
        "street": "Vali\u00f1o, 3",
        "phone": "657 402 403"
    }
}, {
    "node": {
        "title": "Residencia de Peregrinos San L\u00e1zaro",
        "latitude": "42.887374",
        "longitude": "-8.513173",
        "nid": "1436",
        "nodetype": "Accommodation",
        "accomtype": "Xunta",
        "price": "10\u20ac",
        "beds": "80",
        "bike": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4.40\u20ac",
        "marker": "Xunta.png",
        "street": "San L\u00e1zaro",
        "phone": "981 571 488, 618 266 894"
    }
}, {
    "node": {
        "title": "Blanco Albergue",
        "latitude": "42.881877",
        "longitude": "-8.548862",
        "nid": "12224",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "20",
        "bike": "Yes",
        "wifi": "Yes, WiFi",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "reserve": "Yes",
        "stove": "Yes",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/blanco-albergue.html?aid=1499405",
        "marker": "Private.png",
        "image": "12224-137031.jpeg",
        "email": "blancoalbergue@gmail.com",
        "street": "Galeras, 30",
        "phone": "881 976 850, 699 591 238"
    }
}, {
    "node": {
        "title": "Albergue Monterrey",
        "latitude": "42.886075",
        "longitude": "-8.529218",
        "nid": "12149",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "8:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-monterrey.html?aid=1499405",
        "marker": "Private.png",
        "email": "rstmonterrey@gmailcom",
        "street": "Fonti\u00f1as, 65a",
        "phone": "981 587 557, 655 484 299"
    }
}, {
    "node": {
        "title": "Hotel Nest Style Santiago",
        "latitude": "42.875159",
        "longitude": "-8.545040",
        "nid": "9901",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/nest-style-santiago.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9901-135452.webp",
        "email": "booking@neststylesantiago.com",
        "street": "Doutor Teixeiro, 15",
        "phone": "981 563 444"
    }
}, {
    "node": {
        "title": "Albergue La Estrella de Santiago",
        "latitude": "42.883272",
        "longitude": "-8.534170",
        "nid": "9677",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/14\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "22:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "marker": "Private.png",
        "image": "9677-137396.png",
        "email": "info@laestrelladesantiago.es",
        "street": "Concheiros, 36-38",
        "phone": "881 973 926"
    }
}, {
    "node": {
        "title": "Albergue Fin del Camino",
        "latitude": "42.885499",
        "longitude": "-8.523785",
        "nid": "9672",
        "nodetype": "Accommodation",
        "accomtype": "Parochial",
        "price": "8*\u20ac",
        "beds": "110",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi and computers",
        "kitchen": "Yes",
        "cal": "HOLY - ½ OCT",
        "open": "11:30",
        "pilgrim": "Yes",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Parochial.png",
        "image": "9672-133744.webp",
        "email": "albergue@fundacionperegrinacionasantiago.com",
        "street": "Moscova",
        "phone": "981 587 324"
    }
}, {
    "node": {
        "title": "Albergue O Fogar de Teodomiro",
        "latitude": "42.882644",
        "longitude": "-8.542131",
        "nid": "3707",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\u20ac",
        "beds": "20",
        "bike": "Yes",
        "close": "20:00",
        "wifi": "Yes",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 5\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/o-fogar-de-teodomiro.html?aid=1499405",
        "marker": "Private.png",
        "image": "3707-64024.webp",
        "email": "info@fogarteodomiro.com",
        "street": "Algalia de Arriba, 3",
        "phone": "981 582 920, 699 631 592"
    }
}, {
    "node": {
        "title": "Albergue Compostela",
        "latitude": "42.872170",
        "longitude": "-8.547900",
        "nid": "12197",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-compostela.html?aid=1499405",
        "marker": "Private.png",
        "image": "12197-136793.webp",
        "email": "contacto@alberguecompostela.es",
        "street": "Pedro de Mezonzo, 28",
        "phone": "881 017 840"
    }
}, {
    "node": {
        "title": "Hostal Alameda",
        "latitude": "42.878222",
        "longitude": "-8.547825",
        "nid": "9903",
        "nodetype": "Accommodation",
        "accomtype": "Hostal",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-alameda-santiago-de-compostela.html?aid=1499405",
        "marker": "Hostal.png",
        "image": "9903-135450.webp",
        "street": "San Clemente, 32",
        "phone": "981 588 100, 981 588 689"
    }
}, {
    "node": {
        "title": "Albergue La Estaci\u00f3n",
        "latitude": "42.868615",
        "longitude": "-8.545536",
        "nid": "9859",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "7:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "email": "info@alberguelaestacion.com",
        "street": "Xoana Nogueira, 14",
        "phone": "981 594 624, 639 228 617"
    }
}, {
    "node": {
        "title": "Albergue Azabache",
        "latitude": "42.881214",
        "longitude": "-8.543563",
        "nid": "9674",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "14\/18\u20ac",
        "beds": "22",
        "close": "16:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "cal": "ALL YEAR",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.5\u20ac",
        "marker": "Private.png",
        "image": "9674-134277.webp",
        "email": "azabachehostel@yahoo.es",
        "street": "Azabacher\u00eda, 15",
        "phone": "981 071 254"
    }
}, {
    "node": {
        "title": "Albergue Mundoalbergue",
        "latitude": "42.878585",
        "longitude": "-8.547567",
        "nid": "6410",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\/18\u20ac",
        "beds": "30",
        "barrest": "Yes",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR**",
        "open": "10:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3.5\u20ac",
        "marker": "Private.png",
        "image": "6410-52824.webp",
        "email": " info@mundoalbergue.es",
        "street": "San Clemente, 26",
        "phone": "981 588 625, 674 415 600"
    }
}, {
    "node": {
        "title": "Albergue Acuario de Santiago de Compostela",
        "latitude": "42.886594",
        "longitude": "-8.525720",
        "nid": "1435",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "60",
        "bike": "Yes",
        "close": "24:00",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "APR - NOV",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-acuario.html?aid=1499405",
        "marker": "Private.png",
        "image": "1435-134392.webp",
        "email": "reservas@acuariosantiago.com",
        "street": "Estocolmo, 2",
        "phone": "981 575 438"
    }
}, {
    "node": {
        "title": "Albergue Linares",
        "latitude": "42.882844",
        "longitude": "-8.541600",
        "nid": "12199",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-linarhtml?aid=1499405",
        "marker": "Private.png",
        "image": "12199-136795.webp",
        "street": "Algalia de Abaixo, 34",
        "phone": "981 580 443"
    }
}, {
    "node": {
        "title": "Pensi\u00f3n Pazo de Argra",
        "latitude": "42.878590",
        "longitude": "-8.542637",
        "nid": "9905",
        "nodetype": "Accommodation",
        "accomtype": "Pension",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/pension-pazo-de-agra.html?aid=1499405",
        "marker": "Pension.png",
        "street": "Calderer\u00eda, 37",
        "phone": "981 583 517"
    }
}, {
    "node": {
        "title": "Hospeder\u00eda San Mart\u00edn Pinario",
        "latitude": "42.881397",
        "longitude": "-8.544767",
        "nid": "9900",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/hospederia-san-martin-pinario-seminario-mayor.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9900-135453.webp",
        "street": "Pl. de la Inmaculada, 3 ",
        "phone": "981 560 282"
    }
}, {
    "node": {
        "title": "Albergue Roots & Boots",
        "latitude": "42.879670",
        "longitude": "-8.550122",
        "nid": "9676",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "12\/18\u20ac",
        "beds": "48",
        "bike": "Yes",
        "close": "24",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/roots.html?aid=1499405",
        "marker": "Private.png",
        "image": "9676-135455.webp",
        "email": "info@rootsandboots.es",
        "street": "Cruceiro do Gaio, 7",
        "phone": "699 631 594"
    }
}, {
    "node": {
        "title": "Albergue Tur\u00edstico La Salle",
        "latitude": "42.884801",
        "longitude": "-8.540475",
        "nid": "7011",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "17\u20ac",
        "beds": "84",
        "bike": "Yes",
        "wifi": "Yes, WiFi and computer",
        "microwave": "Yes",
        "cal": "FEB - NOV",
        "open": "15:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 2\u20ac",
        "marker": "Private.png",
        "email": "info@alberguelasalle.com",
        "street": "Tras Santa Clara",
        "phone": "981 585 667, 981 584 611"
    }
}, {
    "node": {
        "title": "Albergue Seminario Menor en Santiago de Compostela",
        "latitude": "42.876648",
        "longitude": "-8.537332",
        "nid": "1437",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/12\u20ac",
        "beds": "199",
        "bike": "Yes, but not always secure",
        "close": "24:00",
        "wifi": "Yes, computer.  Not free.",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ MAR - OCT",
        "open": "8:30",
        "fridge": "Yes",
        "reserve": "Yes, online or by phone",
        "stove": "Yes",
        "washer": "Yes, 3.50\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-seminario-menor.html?aid=1499405",
        "marker": "Private.png",
        "image": "1437-131888.webp",
        "email": "santiago@alberguesdelcamino.com",
        "street": "Quiroga Palacios",
        "phone": "881 031 768"
    }
}, {
    "node": {
        "title": "Albergue La Credencial",
        "latitude": "42.885130",
        "longitude": "-8.532056",
        "nid": "12150",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/14\u20ac",
        "beds": "36",
        "bike": "Yes",
        "close": "21:00",
        "wifi": "Yes, WiFi",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "9:00",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/albergue-turistico-la-credencial.html?aid=1499405",
        "marker": "Private.png",
        "image": "12150-136821.webp",
        "email": "reservaslacredencial@gmail.com",
        "street": "Fonte dos Concheiros, 13",
        "phone": "981 068 083, 639 966 704"
    }
}, {
    "node": {
        "title": "Hotel Avenida",
        "latitude": "42.877434",
        "longitude": "-8.542782",
        "nid": "9902",
        "nodetype": "Accommodation",
        "accomtype": "Hotel",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/sino-avenida-santiago.html?aid=1499405",
        "marker": "Hotel.png",
        "image": "9902-135451.webp",
        "street": "Fuente de San Antonio, 5",
        "phone": "981 568 426"
    }
}, {
    "node": {
        "title": "Albergue Porta Real",
        "latitude": "42.882142",
        "longitude": "-8.534625",
        "nid": "9840",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "10\/15\u20ac",
        "beds": "24",
        "bike": "Yes",
        "close": "21:30",
        "wifi": "Yes, WiFi and computers",
        "microwave": "Yes",
        "cal": "ALL YEAR",
        "open": "8:30",
        "fridge": "Yes",
        "reserve": "Yes",
        "washer": "Yes, 4\u20ac",
        "marker": "Private.png",
        "image": "9840-134247.webp",
        "email": "reservas@albergueportareal.es",
        "street": "dos Concheiros, 10",
        "phone": "633 610 114"
    }
}, {
    "node": {
        "title": "Albergue The Last Stamp",
        "latitude": "42.880873",
        "longitude": "-8.542499",
        "nid": "9673",
        "nodetype": "Accommodation",
        "accomtype": "Private",
        "price": "15\/25\u20ac",
        "beds": "62",
        "bike": "Yes",
        "close": "24h",
        "wifi": "Yes, WiFi and computer",
        "kitchen": "Yes",
        "microwave": "Yes",
        "cal": "½ FEB - ½ DEC",
        "open": "24h",
        "fridge": "Yes",
        "reserve": "Yes",
        "stove": "Yes",
        "washer": "Yes, 3\u20ac",
        "bookingURL": "http:\/\/www.booking.com\/hotel\/es\/the-last-stamp.html?aid=1499405",
        "marker": "Private.png",
        "image": "9673-131601.webp",
        "email": "reservas@thelaststamp.es",
        "street": "Preguntoiro, 10",
        "phone": "981 563 525"
    }
}]
export default nodes