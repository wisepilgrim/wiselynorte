const fs = require("fs");
// FROM THERE
const files = fs.readdirSync("./Images/images").filter(x => x.includes("webp"));
const ex =
  "{\n" +
  files.map(x => {
    //let names = name.split("-");
    /*if(names.length > 1){
      filename = names[0] + names[1].trim();
    } else {
      filename = names[0];
    }*/
    return (`"${x}": require("../Images/images/${x}"),`);
  }).join("\n") +
  "}";
const res = "export default " + ex;
// TO HERE
fs.writeFileSync("./Images/index.js", res);
