import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Linking,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Communications from 'react-native-communications';
import HTMLView from 'react-native-htmlview';

import { strings } from '../../locales/i18n';
import { Images, Colors } from '../Themes';

const phoneicon = "\u0410";
const emailicon = "\u0411";
const webicon = "\u0412";
const facebookicon = "\u0409";

export default class Popup extends Component<{}> {
  constructor(props) {
    super(props);
    console.log(this.props.marker.image);
  }


  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    });
  }

  render() {
    const { marker } = this.props;

    const showserviceview = (marker.accomtype === 'Private' || marker.accomtype === 'Municipal' || marker.accomtype === 'Association' || marker.accomtype === 'Parochial') ? true : false;
    const bookingsize = (marker.accomtype === 'Private' || marker.accomtype === 'Municipal' || marker.accomtype === 'Association' || marker.accomtype === 'Parochial' || marker.accomtype === 'Camping') ? 'small' : 'big';

    //console.log(Images.images[marker.image]);
    return (
        <View style={styles.modalContent}>
          { Platform.OS === 'android' ?
            <TouchableOpacity
              onPress={() => this.props.navigator.dismissLightBox()}
            >
              <Image
                style={{ width: 24, height: 24, marginLeft: 5, marginTop: 15 }}
                source={Images.popup.close}
              />
            </TouchableOpacity>
              : null
          }


          <View style={styles.modalHeader}>
              <Text style={styles.headerTitle}>{marker.title}</Text>
              <Text style={styles.headerSubtitle}>{marker.accomtype}</Text>
          </View>

          <TouchableOpacity
            onPress={() => this.props.navigator.dismissLightBox()}
          >
            <Image
              onPress={() => this.props.navigator.dismissLightBox()}
              style={{ width: 24, height: 24, position: 'absolute', right: 6, top: -32}}
              source={Images.popup.close}
              zIndex={100}
            />
          </TouchableOpacity>

          { marker.image === undefined || marker.image === null ?
            <View style={styles.imgWrapperblank}>
              <Image
              style={styles.imgblank}
              />
            </View> :
            <View style={styles.imgWrapper}>
              <Image
              style={styles.img}
              source={Images.images[marker.image]}
              />
            </View>
          }

          <View style={showserviceview ? styles.serviceView : { display: 'none' }}>
              <View style={styles.serviceIconGroup}>
                <Image
                  style={marker.pilgrim ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.pilgrimBT}
                />
                <Image
                  style={marker.hasprivate ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hotelBT}
                />
                <Image
                  style={marker.reserve ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.reserveBT}
                />
                <Image
                  style={marker.barrest ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.barrestBT}
                />
                <Image
                  style={marker.kitchen ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.kitchenBT}
                />
                <Image
                  style={marker.family ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.familyBT}
                />
                <Image
                  style={marker.veg ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.vegBT}
                />
                <Image
                  style={marker.washer ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.washerBT}
                />
                <Image
                  style={marker.wifi ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.wifiBT}
                />
                <Image
                  style={marker.bike ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.bikeBT}
                />
              </View>
          </View>
          <View style={styles.practicalView}>
            <View style={marker.price ? styles.practicalIconGroup : { display: 'none' }}>
              <Image
                style={styles.practicalIcon}
                source={Images.popup.hostelBigBT}
              />
                <Text>{marker.beds}:{marker.price}</Text>
            </View>
            <View style={marker.open ? styles.practicalIconGroup : { display: 'none' }}>
              <Image
                style={styles.practicalIcon}
                source={Images.popup.openBT}
              />
              <Text>{marker.open}</Text>
            </View>
            <View style={marker.close ? styles.practicalIconGroup : { display: 'none' }}>
              <Image
                style={styles.practicalIcon}
                source={Images.popup.closeBT}
              />
              <Text>{marker.close}</Text>
            </View>
            <View style={marker.cal ? styles.practicalIconGroup : { display: 'none' }}>
              <Image
                style={styles.practicalIcon}
                source={Images.popup.calBT}
              />
              <Text>{marker.cal}</Text>

            </View>
            <TouchableOpacity
              style={((bookingsize === 'small') && marker.bookingURL) ? styles.practicalIconGroup : { display: 'none' }}
              onPress={() => this.openURL(marker.bookingURL)}
            >
              <Image
                style={styles.bookingButtonSmall}
                source={Images.popup.bookingSmallBT}
              />
            </TouchableOpacity>

          </View>




          <ScrollView contentContainerStyle={styles.description}>
            { marker.street === undefined || marker.street === null ?
              null :
              <View >
                <Text style={styles.contactText}>{marker.street}</Text>
              </View>
            }

            { marker.phone === undefined || marker.phone === null ?
              null :
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => Communications.phonecall(marker.phone, true)}
                >
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.buttonicon}>{phoneicon}</Text>
                    <Text style={styles.buttontext}>{marker.phone}</Text>
                  </View>
                </TouchableOpacity>
            }

            { marker.email === undefined || marker.email === null ?
              null :
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => Communications.email([marker.email],null,null,null,null)}
                >
                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.buttonicon}>{emailicon}</Text>
                    <Text style={styles.buttontext}>{marker.email}</Text>
                  </View>
                </TouchableOpacity>
            }

            { marker.facebook === undefined || marker.facebook === null ?
              null :
              <TouchableOpacity
                style={styles.button}
                onPress={() => Communications.web(marker.facebook)}
              >
                  <Text style={styles.buttontext}>FACEBOOK</Text>
              </TouchableOpacity>
            }

            { marker.website === undefined || marker.website === null ?
              null :
              <TouchableOpacity
                style={styles.button}
                onPress={() => Communications.web(marker.website)}
              >
                  <Text style={styles.buttontext}>{strings('popup.officialwebsite')}</Text>
              </TouchableOpacity>
            }

            <TouchableOpacity
              style={((bookingsize === 'big') && marker.bookingURL) ? styles.bookingmapbar : { display: 'none' }}
              onPress={() => this.openURL(marker.bookingURL)}
            >
              <Image
                style={styles.bookingButton}
                source={Images.popup.bookingBT}
              />
            </TouchableOpacity>

            { marker.body === undefined || marker.body === null ?
              null :
              <HTMLView
               value={marker.body}
               stylesheet={htmlstyles}
              />
            }
            { marker.description === undefined || marker.description === null ?
              null :
              <View style={{ paddingTop: 30 }}>
                <HTMLView
                 value={marker.description}
                 stylesheet={htmlstyles}
                />
              </View>
            }
          </ScrollView>
        </View>
    );
  }
}

const baseStyle = {
  width: '100%',
  height: Platform.isPad ? 700 : 250,
};
const fontSize = 16;
const htmlstyles = StyleSheet.create({
  a: {
    fontWeight: '300',
    fontSize: fontSize,
    textAlign: 'justify',
  },
  em: {
    fontStyle: 'italic',
    fontSize: fontSize,
    textAlign: 'justify',
  },
	p: {
		fontSize: fontSize,
    textAlign: 'justify',
    marginBottom: -20,
	},
	strong: {
		fontWeight:'bold',
		fontSize: fontSize,
    textAlign: 'justify',
	},
	li: {
		fontSize: fontSize,
	}
})


const styles = StyleSheet.create({
  modalContent: {
    width: Dimensions.get('window').width,// * 0.98,
    height: Dimensions.get('window').height,// * 0.75,
    backgroundColor: '#ffffff',
    borderRadius: 0,
    paddingTop: Platform.OS === 'android' ? 0 : 40,
    //padding: 0,
    //paddingBottom: 0,
    justifyContent: 'flex-start'
  },
  modalHeader: {
    //paddingTop: 0,
    //paddingBottom: 5,
    marginTop: 12,
    // marginRight: 30,
    flexDirection: 'column',
    alignItems: 'center',
  },
  headerTitle: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 18,
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 5
  },
  buttonicon: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    color: Colors.snow,
    fontSize: 22,
    paddingLeft: 5,
    paddingRight: 10,
  },
  headerSubtitle: {
    fontWeight: 'bold',
  },
  img: baseStyle,
  imgblank: {
    height: 60
  },
  imgWrapper: {
    overflow: 'hidden',
    ...baseStyle,
  },
  imgWrapperblank: {
    overflow: 'hidden',
    height: 60
  },
  serviceView: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
  },
  serviceIconGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  serviceIcon: {
    height: 30,
    width: 30,
    marginRight: 0
  },
  serviceIconDisable: {
    height: 30,
    width: 30,
    marginRight: 0,
    opacity: 0.1
  },
  practicalView: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
    paddingBottom: 8,
    justifyContent: 'space-between',
  },
  practicalIconGroup: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  practicalIcon: {
    height: 48,
    width: 48,
    marginRight: 0
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20
  },
  bookingButton: {
    height: Platform.isPad ? 100 : 60,
    width: 60,
    //marginRight: 5,
    // marginLeft: 5,
    flex: 1,
    borderRadius: 2,

  },
  bookingButtonSmall: {
      height: 54,
      width: 54,
      marginRight: 5,
      marginLeft: 5,
  },
  description: {
    padding: 5,
    //flexDirection: 'column',
    justifyContent: 'space-between',
    //alignItems: 'center',
  },
  descriptionText: {
  },
  contactText: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingLeft: 5,
    paddingBottom: 3,
  },
  button: {
    //alignItems: 'center',
    backgroundColor: Colors.charcoal,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 4,
    borderRadius: 2,
  },
  buttontext: {
      fontSize: 18,
      //textAlign: 'left',
      fontWeight: 'bold',
      color: 'white'
  },


});
