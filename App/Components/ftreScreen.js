import React, { Component } from 'react';
import {
  AsyncStorage,
  Modal,
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  Image,
  Platform,
  Dimensions
} from 'react-native';

import { Images } from '../Themes';

export default class FtueScreen extends Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      modalVisible: false
    };
  }
  componentDidMount() {
    AsyncStorage.getItem(this.props.pagekey, (err, result) => {
      if (err) {
      } else {
        if (result == null) {
          console.log("null value recieved", result);
          this.setModalVisible(true);
        } else {
          // DELETE ME BEFORE BUILDING!!!
          // this.setModalVisible(true);
          console.log("result", result);
        }
      }
    });
    AsyncStorage.setItem(this.props.pagekey, JSON.stringify({ "value":"true" }), (err,result) => {
            console.log("error",err,"result",result);
            });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    return (
      <View>
        <Modal
          animationType={'slide'}
          transparent={true}
          style={styles.ftreContainer}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}
        >
          <View style={styles.ftreContainer}>
            <View style={styles.ftreTitleContainer}>
              <Text style={styles.ftreTitle}>{this.props.title}</Text>
            </View>
            <Image
              style={styles.image}
              source={Images.markers.maptiles}
            />

            <View style={styles.ftreDescriptionContainer}>
              <Text style={styles.ftreDescription}>
                {this.props.description}
              </Text>
            </View>
            <View style={styles.ftreExitContainer}>
              <TouchableHighlight
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
              >
                <View style={styles.ftreExitButtonContainer}>
                  <Text style={styles.ftreExitButtonText}>OK</Text>
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
ftreContainer: {
		backgroundColor: 'white',
		flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width,
    paddingTop: 10,
    borderWidth: 20,
		borderColor: 'black'
	},
	ftreTitle: {
    fontSize: 28,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 10,
    marginBottom: 30,
    marginLeft: 10,
    textAlign: 'left',
	},
	ftreDescription: {
		color: 'black',
    fontSize: 16,
		marginRight: 10,
		marginLeft: 10
	},
	ftreCloseIcon: {
		alignSelf: 'flex-end',
		flex: 0.5,
		marginRight: 10
	},
	ftreTitleContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	ftreDescriptionContainer: {
		flex: 4,
    paddingTop: 10,
	},
	ftreExitContainer: {
		flex: 2,
		justifyContent: 'flex-start',
		alignItems: 'center',
	},
	ftreExitButtonContainer: {
		width: 200,
		height: 40,
		backgroundColor: 'black',
		borderRadius: 10,
		justifyContent: 'center',
	},
	ftreExitButtonText: {
		color: 'white',
		fontSize: 20,
		fontWeight: 'bold',
		textAlign: 'center'
	},
  image: {
    flex: 1,
    resizeMode: 'cover',
    alignSelf: 'center',
    paddingBottom: 30,
    marginTop: 5,
    paddingTop: 5,
    height: 300,
    width: Dimensions.get('window').width * 0.85,
  }
});
