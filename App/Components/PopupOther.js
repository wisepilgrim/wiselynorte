import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Linking,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import HTMLView from 'react-native-htmlview';
import { Images } from '../Themes';
import { strings } from '../../locales/i18n';



export default class PopupOther extends Component<{}> {
  constructor(props){
    super(props);
    console.log('Img' + JSON.stringify(this.props.marker));
  }

  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\t know how to open URI: ' + url);
      }
    });
  }

  render() {
    const { marker } = this.props;
    // const tosantiago = (marker.tosantiago) ? (marker.tosantiago + 'km → ' + strings('popup.destination')) : '';
    // const altitude = (marker.altitude) ? (marker.altitude + 'm.') : null;
    const tohere = (marker.tohere) ? (marker.tohere + 'km → ') : null;


    const sharedCheck = (marker.hasshared.indexOf('yes') >= 0) ? 'true' : 'false';
    const privateCheck = (marker.hasprivate.indexOf('yes') >= 0) ? 'true' : 'false';
    const campingCheck = (marker.hascamping.indexOf('yes') >= 0) ? 'true' : 'false';

    const showserviceview = ((sharedCheck === 'true') || (privateCheck === 'true') || (campingCheck === 'true') || marker.hospital || marker.pharmacy || marker.barrest || marker.supermarket || marker.atm || marker.correos || marker.busterminal || marker.bus || marker.train) ? true : false;
    return (
        <View style={styles.modalContent}>



          { Platform.OS === 'android' ?
            <TouchableOpacity
              onPress={() => this.props.navigator.dismissLightBox()}
            >
              <Image
                style={{ width: 24, height: 24, marginLeft: 5, marginTop: 15 }}
                source={Images.popup.close}
              />
            </TouchableOpacity>
              : null
          }



          <View style={styles.modalHeader}>
            <Text style={styles.headerTitle}>{tohere}{marker.title}</Text>

            { marker.tosantiago === undefined || marker.tosantiago === null ?
              null :
              <Text style={styles.headerSubtitle}>{marker.tosantiago}km → {strings('popup.destination')}</Text>
            }

            { marker.altitude === undefined || marker.altitude === null ?
              null :
              <Text style={styles.headerSubtitle}>{marker.altitude}</Text>
            }
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigator.dismissLightBox()}
          >
            <Image
              style={{ width: 24, height: 24, position: 'absolute', right: 6, top: -24}}
              source={Images.popup.close}
              zIndex={100}
            />
          </TouchableOpacity>

          <View style={showserviceview ? styles.serviceView : { display: 'none' }}>
              <View style={styles.serviceIconGroup}>
                <Image
                  style={(sharedCheck === 'true') ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hostelBT}
                />
                <Image
                  style={(privateCheck == 'true') ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hotelBT}
                />
                <Image
                  style={(campingCheck == 'true') ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.campingBT}
                />
              </View>

              <View style={styles.serviceIconGroup}>
                <Image
                  style={marker.hospital ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.hospitalBT}
                />
                <Image
                  style={marker.pharmacy ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.pharmacyBT}
                />
              </View>

              <View style={styles.serviceIconGroup}>
                <Image
                  style={marker.barrest ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.barrestBT}
                />
                <Image
                  style={marker.supermarket ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.supermarketBT}
                />
                <Image
                  style={marker.atm ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.atmBT}
                />
                <Image
                  style={marker.correos ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.correosBT}
                />
              </View>

              <View style={styles.serviceIconGroup}>
                <Image
                  style={marker.busterminal ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.busterminalBT}
                />
                <Image
                  style={marker.bus ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.busBT}
                />
                <Image
                  style={marker.train ? styles.serviceIcon : styles.serviceIconDisable}
                  source={Images.popup.trainBT}
                />
              </View>

          </View>

          <TouchableOpacity
            style={marker.bookingURL ? styles.bookingmapbar : { display: 'none' }}
            onPress={() => this.openURL(marker.bookingURL)}
          >
            <Image
              style={styles.bookingButton}
              source={Images.popup.bookingBT}
            />
          </TouchableOpacity>


          <ScrollView contentContainerStyle={styles.scrollview}>

          { marker.image === undefined || marker.image === null ?
            null :
            <View style={styles.imgWrapper}>
              <Image
                style={styles.img}
                source={Images.images[marker.image]}
              />
            </View>
          }


          { marker.body === undefined || marker.body === null ?
            null :
            <HTMLView
             value={marker.body}
             stylesheet={htmlstyles}
            />
          }
          { marker.road === undefined || marker.road === null ?
            null :
            <View style={{ marginTop: 30, padding: 5, paddingBottom: 30, backgroundColor: '#eeeeee' }}>
              <HTMLView
               value={marker.road}
               stylesheet={htmlstyles}
              />
            </View>
          }


          </ScrollView>
        </View>
    );
  }
}

const baseStyle = {
  width: '100%',
  height: Platform.isPad ? 700 : 250,
  marginBottom: 5,
};

const fontSize = 16;
const htmlstyles = StyleSheet.create({
  a: {
    fontWeight: '300',
    fontSize: fontSize,
    textAlign: 'justify',
  },
  em: {
    fontStyle: 'italic',
    fontSize: fontSize,
    textAlign: 'justify',
  },
	p: {
		fontSize: fontSize,
    textAlign: 'justify',
    marginBottom: -20,
	},
	strong: {
		fontWeight:'bold',
		fontSize: fontSize,
    textAlign: 'justify',
	},
	li: {
		fontSize: fontSize,
	}
})


const styles = StyleSheet.create({
  modalContent: {
    width: Dimensions.get('window').width,// * 0.98,
    height: Dimensions.get('window').height,// * 0.95,
    backgroundColor: '#ffffff',
    borderRadius: 0,
    paddingTop: Platform.OS === 'android' ? 0 : 40,
    //padding: 0,
    //paddingBottom: 0,
    justifyContent: 'flex-start'
  },
  modalHeader: {
    //paddingTop: 0,
    //paddingBottom: 5,
    marginTop: 12,
    //marginRight: 30,
    flexDirection: 'column',
    alignItems: 'center',
  },
  headerTitle: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 18,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 5,
    //marginTop: 5
  },
  headerSubtitle: {
    fontWeight: 'bold',
  },
  img: baseStyle,
  imgWrapper: {
    overflow: 'hidden',
    ...baseStyle,
  },
  scrollview: {
    padding: 5,
    paddingBottom: 30
  },
  serviceView: {
    flexDirection: 'row',
    padding: 4,
    justifyContent: 'space-between',
  },
  serviceIconGroup: {
    flexDirection: 'row',
  },
  serviceIcon: {
    height: 28,
    width: 28,
    marginRight: 0
  },
  serviceIconDisable: {
    height: 28,
    width: 28,
    marginRight: 0,
    opacity: 0.1
  },
  practicalView: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
    justifyContent: 'space-between',
  },
  practicalIconGroup: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  practicalIcon: {
    height: 48,
    width: 48,
    marginRight: 0
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10
  },
  bookingButton: {
    height: Platform.isPad ? 100 : 60,
    width: 60,
    marginRight: 5,
    marginLeft: 5,
    flex: 1
  },
  description: {
    margin: 5,
    paddingBottom: 30,
    flexDirection: 'column',
    justifyContent: 'space-between',
    //alignItems: 'center',
  },
  descriptionText: {
    fontSize: 16,
    marginTop: 10,
  }
});
