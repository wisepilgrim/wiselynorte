import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity,
  Linking
} from 'react-native';
import { Colors, Images } from '../Themes';
import { strings } from '../../locales/i18n';

export default class Casaivar extends Component<{}> {
  constructor(props) {
    super(props);
    this.props.navigator.setTitle({
      title: strings('thebigmap.title')

    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    });
  }


  onNavigatorEvent = (event) => {
    switch (event.id) {
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={true}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          bounces={true}
          horizontal={false}
          overScrollMode='always'
          showsHorizontalScrollIndicator={false}
        >
          <View style={styles.innercontainer}>
          <Image style={styles.logo} source={Images.thebigmapmockup} />
          <Text style={styles.header}>{strings('thebigmap.header')}</Text>

          <Text style={styles.text}>{strings('thebigmap.text1')}</Text>
          <Text style={styles.text}>{strings('thebigmap.text2')}</Text>
          <Text style={styles.text}>{strings('thebigmap.text3')}</Text>
          <Text style={styles.text}>{strings('thebigmap.text4')}</Text>
          <Image style={styles.logosquare} source={Images.thebigmapcloseup} />
          <Text style={styles.subheader}>{strings('thebigmap.shippingdetails')}</Text>
          <Text style={styles.text}>{strings('thebigmap.text5')}</Text>

          <TouchableOpacity
            style={styles.button}
            onPress={() => this.openURL(strings('thebigmap.url'))}
          >
            <Text style={styles.buttontext}>{strings('thebigmap.button')}</Text>
          </TouchableOpacity>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  innercontainer: {
    width: Dimensions.get('window').width,
    alignSelf: 'flex-start'
  },
  button: {
    alignItems: 'center',
    backgroundColor: strings('global.routecolor'),
    padding: 10,
    margin: 10,
    borderRadius: 5,
  },
  addressbox: {
    borderWidth: 1,
    borderColor: Colors.hamburger,
    marginLeft: 30,
    marginRight: 30,
    padding: 5,
    marginBottom: 5
  },
  iconset: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  iconsetvert: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  text: {
    fontSize: 16,
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    textAlign: 'justify',

  },
  buttontext: {
    fontSize: 18,
    flex: 1,
    textAlign: 'justify',
    fontWeight: 'bold',
    color: Colors.snow,

  },
  textitalic: {
    fontSize: 16,
    fontStyle: 'italic',
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',

  },
  bullet: {
    fontSize: 16,
    flex: 1,
    marginLeft: 20,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',

  },
  bulletbold: {
    fontSize: 16,
    flex: 1,
    marginLeft: 20,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',
    fontWeight: 'bold',


  },
  icon: {
    height: 50,
    width: 50,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    resizeMode: 'contain'
  },
  iconwide: {
    width: 300,
    height: 50,
    resizeMode: 'contain',
    marginLeft: 10,
    marginBottom: 10

  },
  header: {
    fontSize: 28,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 10,
    marginLeft: 10,
    paddingTop: 5,
    textAlign: 'left',
  },
  subheader: {
    fontSize: 22,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 10,
    marginLeft: 10,
    paddingTop: 5,    
    textAlign: 'left',
  },
  scrollview: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    // justifyContent: 'flex-start'

  },
  logo: {
    width: Dimensions.get('window').width,
    top: 0,
    resizeMode: 'contain',
    height: 300
  },
  logosquare: {
    width: Dimensions.get('window').width,
    top: 0,
    resizeMode: 'contain',
    height: 400,
  },
});
