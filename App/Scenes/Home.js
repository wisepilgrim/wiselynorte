/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import MapView, { LocalTile, MAP_TYPES } from 'react-native-maps';
import RNFS from 'react-native-fs';
import I18n from 'react-native-i18n';
import ployline from '../Constants/polyline';
import MARKERSIOS from '../Constants/markers-ios';
import MARKERSANDROID from '../Constants/markers-android';
import MARKERSIOS_DE from '../Constants/markers-ios-DE';
import MARKERSANDROID_DE from '../Constants/markers-android-DE';
import { Colors, Images } from '../Themes';
import FtreScreen from '../Components/ftreScreen';
import { strings } from '../../locales/i18n';
//alert(I18n.currentLocale());

var _ = require('lodash');

// TODO: CORRECT INITIAL REGION
const INIT_LAT = Number(strings('home.INIT_LAT'));
const INIT_LON = Number(strings('home.INIT_LON'));
const INIT_DELTA = 0.0006;
const ZOOM_MIN = 1;
const ZOOM_MAX_IOS = 16.4;
const ZOOM_MAX = 18;
// TODO: CORRECT BOUNDARIES
const BOUND_SW_LAT = strings('home.BOUND_SW_LAT');
const BOUND_SW_LON = strings('home.BOUND_SW_LON');
const BOUND_NE_LAT = strings('home.BOUND_NE_LAT');
const BOUND_NE_LON = strings('home.BOUND_NE_LON');
const ASSET_URL = RNFS.ExternalDirectoryPath + '/Tiles/{z}/{x}/{y}.webp';
const ASSET_URL_IOS = RNFS.DocumentDirectoryPath + '/Tiles/{z}/{x}/{y}.jpg';
var i18nMARKERSIOS = MARKERSIOS;
var i18nMARKERSANDROID = MARKERSANDROID;

const navigatorStyle = {
  drawUnderNavBar: false,
  navBarTranslucent: false,
  navBarHidden: false,
  navBarTextFontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
  topBarElevationShadowEnabled: true,
  navBarLeftButtonColor: Colors.hamburger,
  navBarTextColor: Colors.hamburger,
  navBarBackgroundColor: Colors.snow,
  navBarButtonColor: Colors.hamburger,
  statusBarTextColorScheme: 'light',
  statusBarTextColorSchemeSingleScreen: 'dark'
};

export default class Home extends Component<{}> {


  constructor(props) {

    if (I18n.currentLocale().includes('de')) {
      i18nMARKERSIOS = MARKERSIOS_DE;
      i18nMARKERSANDROID = MARKERSANDROID_DE;
    }

    super(props);
    this.state = {
      region: {
        latitude: INIT_LAT,
        longitude: INIT_LON,
        latitudeDelta: INIT_DELTA,
        longitudeDelta: INIT_DELTA
      },
      zoomLevel: {
        min: ZOOM_MIN,
        max: (Platform.OS === 'android') ? ZOOM_MAX : ZOOM_MAX_IOS,
      },
      download: {
        status: false
      },
      showUserLocation: false,
      markers: (Platform.OS === 'android') ? i18nMARKERSANDROID : i18nMARKERSIOS,
    };

    i = 0;
    index = 0;
    ploylineKey = Object.keys(ployline);

    this.props.navigator.setTitle({
      title: strings('home.title')
    });

    this.onRegionChangeDebounced = _.debounce(this.onRegionChange, 300);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }


  componentWillMount() {
    if (Platform.OS === 'android') {
      this.readDirectory(RNFS.ExternalDirectoryPath);
    } else {
      this.readDirectory(RNFS.CachesDirectoryPath);
    }
    this.watchId = navigator.geolocation.getCurrentPosition(
       (position) => {
         const { latitude, longitude } = position.coords;
         if (latitude > BOUND_SW_LAT && latitude < BOUND_NE_LAT) {
           if (longitude > BOUND_SW_LON && longitude < BOUND_NE_LON) {
             this.setState({
               showUserLocation: true
             });
           }
         }
      }, (error) => {
        console.log(`error${JSON.stringify(error)}`);
      },
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    );
  }


  componentDidMount() {
    // TODO DO NOT CHANGE THIS FUNCTION.  IT IS THE ONLY THING MAKING IOS ZOOM LEVELS WORK
    // fix for iOS, which fails to load initialRegion correctly at fixed zoom levels
    setTimeout(() => {
      this.mapView && this.mapView.animateToRegion(this.state.region, 500);
      this.state.zoomLevel.min = 8;
    }, 500);
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    this.onRegionChangeDebounced.cancel();
  }

  onNavigatorEvent = (event) => {
    if (event.type === 'DeepLink') {
      if (event.link === 'stuff.casa') {
        this.props.navigator.push({
          screen: 'stuff.casa',
          title: 'Casa',
          navigatorStyle: navigatorStyle,
          navigatorButtons: {
            leftButtons: [
              {
                id: 'back',
                buttonColor: Colors.hamburger
              }
            ]
          },
          animationType: 'none'
        });
      } else {
          this.navigate(event.link, event.payload);
      }
    }

    switch (event.id) {
        case 'location':
          this.showMyLocation();
        break;
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  onRegionChange = (region) => {
      this.setState({
        region: region,
      });
  }

  readDirectory = (path) => {
    RNFS.readDir(path)
      .then((result) => {
        let count = 0;
        for (i = 0; i < result.length; i++) {
            if (result[i].name === 'Tiles') {
              count++;
              break;
            }
        }
        if (count > 0) {
          this.setState({
            download: {
              status: true
            }
          });
        }
      });
  }

  navigate = (screen, title) => {
    this.props.navigator.pop({
      animated: false
    });

    this.props.navigator.push({
      screen: screen,
      title: title,
      navigatorStyle: navigatorStyle,
      navigatorButtons: {
        leftButtons: [
          {
            id: 'sideMenu',
            buttonColor: Colors.hamburger,
            icon: Images.menu
          }
        ]
      },
      animationType: 'none'
    });

    this.props.navigator.toggleDrawer({
      side: 'left',
      animated: false
    });
  }

  showMyLocation = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const userregion = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
        };
        this.map.animateToRegion(userregion, 500);
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.00922,
            longitudeDelta: 0.00421
          }

        });
     }, (error) => {
       console.log(`error${JSON.stringify(error)}`);
     },
     { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
   );
  }

  popupMarker = (event) => {
    console.log(event.nativeEvent);
  }

  openURL = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  }

  fetchMarkers = () => {
    const that = this;
    const { markers, region } = this.state;
    return markers.map(function (item) {
          if (item.node.latitude >= region.latitude - region.latitudeDelta
            && item.node.latitude <= region.latitude + region.latitudeDelta
            && item.node.longitude >= region.longitude - region.longitudeDelta
            && item.node.longitude <= region.longitude + region.longitudeDelta) {

              if (region.latitudeDelta > .5) {
                // SHOW NOTHING
                return null;
              } else if ((region.latitudeDelta < .5) && (region.latitudeDelta > .3)) {
                // SHOW ONLY CITIES AND ALTOS && BORDERS
                if (item.node.marker === 'Alto.png') png = Images.markers.alto;
                else if (item.node.marker === 'ProvinceBorder.png') png = Images.markers.provinceBorder;
                else console.log(item.node.title);

                // THERE ARE THREE MAJOR CATEGORIES OF MAP MARKERS
                // 1: ACCOMMODATION - STRAIGHTFORWARD, USE PNG FROM ABOVE
                if (item.node.nodetype === 'City') {
                  // THIS IS THE VIEW BOX
                  if (item.node.tohere) {
                    return (
                            <MapView.Marker
                              key={i++}
                              coordinate= {{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              style={styles.specialMarker}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            >
                              <View style={styles.bubblecontainer}>
                                <View style={styles.bubble}>
                                  <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.citytitle}>{item.node.title}</Text>
                                    { item.node.hasshared.indexOf('yes') >= 0 ?
                                      <Image
                                        style={styles.cityserviceicon}
                                        source={Images.home.hostelWB}
                                      />
                                      : null
                                    }
                                    { item.node.barrest === undefined || item.node.barrest === null ?
                                      null :
                                      <Image
                                        style={styles.cityserviceicon}
                                        source={Images.home.barrestWB}
                                      />
                                    }
                                  </View>

                                  <Text style={styles.toheretext}>{item.node.tohere} [{item.node.tosantiago}]</Text>
                                </View>
                                <View style={styles.arrowBorder} />
                                <View style={styles.arrow} />
                              </View>
                            </MapView.Marker>
                    );
                  } else {
                    // THIS IS THE GREEN DOT
                    return (
                            <MapView.Marker
                              key={i++}
                              image={png}
                              coordinate={{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            >
                            </MapView.Marker>
                    );
                  }
                } else if ((item.node.nodetype === 'POI') && ((item.node.marker === 'Alto.png') || (item.node.marker === 'ProvinceBorder.png'))) {
                  // 3. POIs - THESE ALL HAVE THEIR OWN COLLECTION OF PNG
                  if (item.node.tohere) {
                    return (
                            <MapView.Marker
                              key={i++}
                              coordinate= {{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              style={styles.specialMarker}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            >
                              <View style={styles.bubblecontainer}>
                                <Image source={png} />
                                <View style={styles.bubble}>
                                  <Text style={styles.toheretext}>{item.node.tohere}km to {item.node.title}</Text>
                                </View>
                              </View>
                            </MapView.Marker>
                    );
                  } else {
                    // THIS IS THE PLAIN MARKER
                    return (
                            <MapView.Marker
                              key={i++}
                              image={png}
                              coordinate={{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            />
                    );
                  }
                }

              } else {
                // SHOW EVERYTHING AS ORIGINALLY SHOWN
                if (item.node.marker === 'Alto.png') png = Images.markers.alto;
                else if (item.node.marker === 'ProvinceBorder.png') png = Images.markers.provinceBorder;
                else if (item.node.marker === 'Apartment.png') png = Images.markers.apartment;
                else if (item.node.marker === 'Association.png') png = Images.markers.association;
                else if (item.node.marker === 'Bombeiro.png') png = Images.markers.bombeiro;
                else if (item.node.marker === 'Camping.png') png = Images.markers.camping;
                else if (item.node.marker === 'CasaRural.png') png = Images.markers.casaRural;
                else if (item.node.marker === 'City.png') png = Images.markers.city;
                else if (item.node.marker === 'Closed.png') png = Images.markers.closed;
                else if (item.node.marker === 'Hostal.png') png = Images.markers.hostal;
                else if (item.node.marker === 'HotelRural.png') png = Images.markers.hotelRural;
                else if (item.node.marker === 'Hotel.png' || item.node.marker === 'Pazo.png') png = Images.markers.hotel;
                else if (item.node.marker === 'Municipal.png') png = Images.markers.municipal;
                else if (item.node.marker === 'Parador.png') png = Images.markers.parador;
                else if (item.node.marker === 'Parochial.png') png = Images.markers.parochial;
                else if (item.node.marker === 'Pension.png') png = Images.markers.pension;
                else if (item.node.marker === 'Polideportivo.png') png = Images.markers.polideportivo;
                else if (item.node.marker === 'Private.png') png = Images.markers.private;
                else if (item.node.marker === 'Xunta.png') png = Images.markers.xunta;
                // Snuck in a few more for additional marker logic below.
                else if (item.node.poimarker === 'POI.png') png = Images.markers.poi;
                else if (item.node.poimarker === 'Castle.png') png = Images.markers.castle;
                else if (item.node.poimarker === 'Ermita.png') png = Images.markers.ermita;
                else if (item.node.poimarker === 'Church.png') png = Images.markers.church;
                else if (item.node.poimarker === 'Cathedral.png') png = Images.markers.cathedral;
                else if (item.node.poimarker === 'Monument.png') png = Images.markers.monument;
                else if (item.node.poimarker === 'Museum.png') png = Images.markers.museum;
                else if (item.node.poimarker === 'Bridge.png') png = Images.markers.bridge;
                else if (item.node.poimarker === 'Castle.png') png = Images.markers.castle;
                else if (item.node.poimarker === 'Cruceiro.png') png = Images.markers.cruceiro;
                else if (item.node.poimarker === 'Fountain.png') png = Images.markers.fountain;
                else if (item.node.poimarker === 'Ruin.png') png = Images.markers.ruin;
                else if (item.node.poimarker === 'Notice.png') png = Images.markers.notice;
                else console.log(item.node.title);

                // THERE ARE THREE MAJOR CATEGORIES OF MAP MARKERS
                // 1: ACCOMMODATION - STRAIGHTFORWARD, USE PNG FROM ABOVE
                if (item.node.nodetype === 'Accommodation') {
                  return (
                          <MapView.Marker
                            anchor={{ x: 0.5, y: 1 }}
                            centerOffset={{ x: 0, y: -25 }}
                            key={i++}
                            image={png}
                            coordinate={{
                              latitude: Number(item.node.latitude),
                              longitude: Number(item.node.longitude)
                            }}
                            zIndex={0}
                            onPress={() =>
                              that.props.navigator.showLightBox({
                                 screen: 'popup',
                                 passProps: {
                                   marker: item.node
                                 },
                                 style: {
                                   backgroundBlur: 'dark',
                                   tapBackgroundToDismiss: true
                                 }
                               })
                            }
                          />
                  );
                // 2. CITY - EITHER GREEN DOT (NO DISTANCE INFORMATION) OR VIEW BOX WITH DISTANCE
              } else if (item.node.nodetype === 'City') {
                  // THIS IS THE VIEW BOX
                  if (item.node.tohere) {
                    return (
                            <MapView.Marker
                              key={i++}
                              coordinate= {{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              style={styles.specialMarker}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            >
                              <View style={styles.bubblecontainer}>
                                <View style={styles.bubble}>
                                  <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.citytitle}>{item.node.title}</Text>
                                    { item.node.hasshared.indexOf('yes') >= 0 ?
                                      <Image
                                        style={styles.cityserviceicon}
                                        source={Images.home.hostelWB}
                                      />
                                      : null
                                    }
                                    { item.node.barrest === undefined || item.node.barrest === null ?
                                      null :
                                      <Image
                                        style={styles.cityserviceicon}
                                        source={Images.home.barrestWB}
                                      />
                                    }
                                  </View>

                                  <Text style={styles.toheretext}>{item.node.tohere} [{item.node.tosantiago}]</Text>
                                </View>
                                <View style={styles.arrowBorder} />
                                <View style={styles.arrow} />
                              </View>
                            </MapView.Marker>
                    );
                  } else {
                    // THIS IS THE GREEN DOT
                    return (
                            <MapView.Marker
                              key={i++}
                              image={png}
                              coordinate={{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            >
                            </MapView.Marker>
                    );
                  }
                } else {
                  // 3. POIs - THESE ALL HAVE THEIR OWN COLLECTION OF PNG
                  if (item.node.tohere) {
                    return (
                            <MapView.Marker
                              key={i++}
                              coordinate= {{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              style={styles.specialMarker}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            >
                              <View style={styles.bubblecontainer}>
                                <Image source={png} />
                                <View style={styles.bubble}>
                                  <Text style={styles.toheretext}>{item.node.tohere}km to {item.node.title}</Text>
                                </View>
                              </View>
                            </MapView.Marker>
                    );
                  } else {
                    // THIS IS THE PLAIN MARKER
                    return (
                            <MapView.Marker
                              key={i++}
                              image={png}
                              coordinate={{
                                latitude: Number(item.node.latitude),
                                longitude: Number(item.node.longitude)
                              }}
                              zIndex={1}
                              onPress={() =>
                                that.props.navigator.showLightBox({
                                   screen: 'popupother',
                                   passProps: {
                                     marker: item.node
                                   },
                                   style: {
                                     backgroundBlur: 'dark',
                                     tapBackgroundToDismiss: true
                                   }
                                 })
                              }
                            />
                    );
                  }
                }

              }



          } else {
            return null;
          }
      });
  }

  render() {
    const { zoomLevel, showUserLocation } = this.state;
    const jsonData = this.fetchMarkers();
    let strokeColor = null;
    const road = ploylineKey.map(function (item) {
          index++;
          if (item === 'MainRoute') strokeColor = strings('global.routecolor');
          else strokeColor = strings('global.secondarycolor');


          // if (item === 'Central') {
          //   strokeColor = strings('global.routecolor');
          // } else if (item === 'Coastal') {
          //   strokeColor = strings('global.secondarycolor');
          // } else if (item === 'caminhatotui' || item === 'ratestofao') {
          //   strokeColor = 'magenta';
          // } else strokeColor = 'red';


          return (
                  <MapView.Polyline
                    key={i++}
                    coordinates={ployline[item]}
                    strokeColor={strokeColor}
                    strokeWidth={8}
                    lineDashPattern={[1, 12]}
                    lineCap="round"
                    lineJoin="round"
                  />
              );
    });

    return (
      <View style={styles.container}>
        <FtreScreen pagekey={'home'} title={strings('ftrescreen.title')} description={strings('ftrescreen.text')} />
        <MapView
          style={styles.map}
          ref={(map) => { this.map = map; }}
          mapType={Platform.OS === 'android' ? 'none' : MAP_TYPES.STANDARD}
          initialRegion={this.state.region}
          onRegionChange={this.onRegionChangeDebounced}
          minZoomLevel={zoomLevel.min}
          maxZoomLevel={zoomLevel.max}
          showsMyLocationButton={false}
          showsUserLocation={showUserLocation}
          rotateEnabled={false}
          loadingEnabled={true}
        >
          {road}
          {jsonData}
            <LocalTile
             pathTemplate={Platform.OS === 'android' ? ASSET_URL : ASSET_URL_IOS}
             tileSize={256}
             zIndex={-1}
            />
        </MapView>
        {
          showUserLocation ?
          <TouchableOpacity
            style={{ position: 'absolute', right: 10, bottom: 10 }}
            onPress={this.showMyLocation}
          >
          <Image
            source={Images.location}
          />
          </TouchableOpacity>
          : null
        }
      </View>
    );
  }
}

const baseStyle = {
  width: '100%',
  height: 250,
  borderTopLeftRadius: 0,
  borderTopRightRadius: 0,
  borderBottomLeftRadius: 0,
  borderBottomRightRadius: 0,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: 500
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    zIndex: -1
  },
  buttonWrapper: {
    position: 'absolute',
    top: 10,
    right: 10
  },
  mapButton: {
     position: 'absolute',
     top: 10,
     right: 10,
     width: 32,
     height: 32,
     borderRadius: 25,
     backgroundColor: 'rgba(255, 255, 255, 0.9)',
     justifyContent: 'center',
     alignItems: 'center',
     zIndex: 1,
  },
  image: {
    width: 32,
    height: 32
  },
  instructions: {
    zIndex: 1
  },
  customView: {
    width: 250,
    height: 250,
    backgroundColor: Colors.snow
  },
  bookingmapbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20
  },
  bookingButton: {
    height: 60,
    width: 60,
    marginRight: 5,
    marginLeft: 5,
    flex: 1
  },
  modalContent: {
    marginTop: 0,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 0,
    backgroundColor: Colors.snow,
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  modalHeader: {
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'column',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  headerSubtitle: {

  },
  img: baseStyle,
  imgWrapper: {
    overflow: 'hidden',
    ...baseStyle,
  },
  serviceView: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
  },
  serviceIconGroup: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  serviceIcon: {
    height: 30,
    width: 30,
    marginRight: 0
  },
  serviceIconDisable: {
    height: 30,
    width: 30,
    marginRight: 0,
    opacity: 0.1
  },
  practicalView: {
    flex: 0,
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 4,
    justifyContent: 'space-between',
  },
  practicalIconGroup: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  practicalIcon: {
    height: 48,
    width: 48,
    marginRight: 0
  },
  description: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //marginBottom: 60
  },
  descriptionText: {
    //flex: 1
  },
  bubblecontainer: {
  flexDirection: 'column',
  alignSelf: 'flex-start',
   alignItems: 'center',
},
  bubble: {
  flex: 0,
  flexDirection: 'column',
  alignSelf: 'flex-start',
  alignItems: 'center',
  backgroundColor: Colors.hamburger,
  padding: 2,
  borderRadius: 2,
  borderColor: Colors.hamburger,
  borderWidth: 0.5,
},
toheretext: {
  color: Colors.snow,
  fontSize: 12,
},
citytitle: {
  color: Colors.snow,
  fontSize: 14,
  marginRight: 5
},
arrow: {
  backgroundColor: 'transparent',
  borderWidth: 4,
  borderColor: 'transparent',
  borderTopColor: Colors.hamburger,
  alignSelf: 'center',
  marginTop: -9,
},
arrowBorder: {
  backgroundColor: 'transparent',
  borderWidth: 4,
  borderColor: 'transparent',
  borderTopColor: Colors.hamburger,
  alignSelf: 'center',
  marginTop: -0.5,
},
specialMarker: {
    zIndex: 1,
},
cityserviceicon: {
  width: 16,
  height: 16
}
});
