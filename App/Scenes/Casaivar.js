import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  Platform,
  Image,
  TouchableOpacity
} from 'react-native';
import { Colors, Images } from '../Themes';
import Communications from 'react-native-communications';
import { strings } from '../../locales/i18n';

export default class Casaivar extends Component<{}> {
  constructor(props) {
    super(props);
    this.props.navigator.setTitle({
      title: strings('casaivar.title')

    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch (event.id) {
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={true}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          bounces={true}
          horizontal={false}
          overScrollMode='always'
          showsHorizontalScrollIndicator={false}
        >
          <View style={styles.innercontainer}>
          <Text style={styles.header}>{strings('casaivar.header')}</Text>
          <Image style={styles.logo} source={Images.casaivarlogo} />

          <Text style={styles.text}>{strings('casaivar.text')}</Text>

          <Text style={styles.subheader}>{strings('casaivar.aboutheader')}</Text>
          <Text style={styles.text}>{strings('casaivar.abouttext1')}</Text>
          <Text style={styles.text}>{strings('casaivar.abouttext2')}</Text>

          <Text style={styles.bullet}>{strings('casaivar.aboutbullet1')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.aboutbullet2')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.aboutbullet3')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.aboutbullet4')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.aboutbullet5')}</Text>

          <Text style={styles.subheader}>{strings('casaivar.openingheader')}</Text>
          <Text style={styles.text}>{strings('casaivar.openingtext1')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.openingbullet1')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.openingbullet2')}</Text>
          <Text style={styles.bullet}>{strings('casaivar.openingbullet3')}</Text>
          <Text style={styles.textitalic}>{strings('casaivar.openingtext2')}</Text>

          <Text style={styles.subheader}>{strings('casaivar.shippingheader')}</Text>
          <Text style={styles.text}>{strings('casaivar.shippingtext1')}</Text>
          <Text style={styles.text}>{strings('casaivar.shippingtext2')}</Text>
          <View style={styles.addressbox}>
            <Text style={styles.bulletbold}>{strings('casaivar.shippingaddress1')}</Text>
            <Text style={styles.bulletbold}>{strings('casaivar.shippingaddress2')}</Text>
            <Text style={styles.bulletbold}>{strings('casaivar.shippingaddress3')}</Text>
            <Text style={styles.bulletbold}>{strings('casaivar.shippingaddress4')}</Text>
            <Text style={styles.bulletbold}>{strings('casaivar.shippingaddress5')}</Text>
            <Text style={styles.bulletbold}>{strings('casaivar.shippingaddress6')}</Text>
          </View>
          <Text style={styles.textitalic}>{strings('casaivar.shippingtext3')}</Text>
          <Text style={styles.textitalic}>{strings('casaivar.shippingtext4')}</Text>



          <Text style={styles.subheader}>{strings('casaivar.pickupheader')}</Text>
          <Text style={styles.text}>{strings('casaivar.pickuptext')}</Text>

          <TouchableOpacity
            style={styles.button}
            onPress={() => Communications.email(['ivar@casaivar.com'],null,null,'Regarding Luggage Storage',null)}
          >
          <Text style={styles.buttontext}>EMAIL IVAR@CASAIVAR.COM</Text>
          </TouchableOpacity>


          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  innercontainer: {
    width: Dimensions.get('window').width,
  },
  addressbox: {
    borderWidth: 1,
    borderColor: Colors.hamburger,
    marginLeft: 30,
    marginRight: 30,
    padding: 5,
    marginBottom: 5
  },
  button: {
    alignItems: 'center',
    backgroundColor: Colors.casaivar,
    padding: 10,
    margin: 10,
    borderRadius: 5,
  },
  iconset: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  iconsetvert: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  text: {
    fontSize: 16,
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',

  },
  textitalic: {
    fontSize: 16,
    fontStyle: 'italic',
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',

  },
  bullet: {
    fontSize: 16,
    flex: 1,
    marginLeft: 20,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',

  },
  bulletbold: {
    fontSize: 16,
    flex: 1,
    marginLeft: 20,
    marginRight: 10,
    marginBottom: 5,
    textAlign: 'justify',
    fontWeight: 'bold',


  },
  icon: {
    height: 50,
    width: 50,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    resizeMode: 'contain'
  },
  iconwide: {
    width: 300,
    height: 50,
    resizeMode: 'contain',
    marginLeft: 10,
    marginBottom: 10

  },
  header: {
    fontSize: 28,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 10,
    marginLeft: 10,
    paddingTop: 10,
    textAlign: 'left',
  },
  subheader: {
    fontSize: 22,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 10,
    marginLeft: 10,
    paddingTop: 10,
    textAlign: 'left',
  },
  scrollview: {
    flexDirection: 'column',
    alignSelf: 'flex-start',

  },
  logo: {
    width: Dimensions.get('window').width,

   resizeMode: 'contain',
    height: 400,
  },
  buttontext: {
      fontSize: 18,
      flex: 1,
      textAlign: 'justify',
      fontWeight: 'bold',
      color: 'white'
  },

});
