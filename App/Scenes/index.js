import { Navigation } from 'react-native-navigation';
import Home from './Home';
import Drawer from './Drawer';
import Offline from './Offline';
import Online from './Online';
import Elevation from './Elevation';
import About from './About';
import Legend from './Legend';
import Casaivar from './Casaivar';
import Stoneboat from './Stoneboat';
import Thebigmap from './Thebigmap';
import Thebook from './Thebook';
import Popup from '../Components/Popup';
import PopupOther from '../Components/PopupOther';
import Test from './Test';
import Other from './Other';

function registerScreens() {
    Navigation.registerComponent('home', () => Home);
    Navigation.registerComponent('drawer', () => Drawer);
    Navigation.registerComponent('offline', () => Offline);
    Navigation.registerComponent('online', () => Online);
    Navigation.registerComponent('elevation', () => Elevation);
    Navigation.registerComponent('about', () => About);
    Navigation.registerComponent('legend', () => Legend);
    Navigation.registerComponent('popup', () => Popup);
    Navigation.registerComponent('popupother', () => PopupOther);
    Navigation.registerComponent('other', () => Other);
    Navigation.registerComponent('casaivar', () => Casaivar);
    Navigation.registerComponent('stoneboat', () => Stoneboat);
    Navigation.registerComponent('thebigmap', () => Thebigmap);
    Navigation.registerComponent('thebook', () => Thebook);
    Navigation.registerComponent('test', () => Test);
}

export {
  registerScreens
};
