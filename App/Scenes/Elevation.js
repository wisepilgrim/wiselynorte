import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Dimensions,
  AsyncStorage
} from 'react-native';
import { Images } from '../Themes';
import { strings } from '../../locales/i18n';


const profileheight = Number(strings('elevation.profileheight'));
const profilewidth = Number(strings('elevation.profilewidth'));
const window = Dimensions.get('window');
const viewheight = window.height - 100;

const scaleRatio = viewheight / profileheight;

const newprofilewidth = profilewidth * scaleRatio;
const newprofileheight = viewheight;

export default class About extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
        myKey: null
    };
    this.props.navigator.setTitle({
      title: strings('elevation.title')
    });

    this.handleScroll = this.handleScroll.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  // onChangeText={(value) => this.saveKey(value)}

  componentDidMount() {
    this.getKey();
    this._scrollView.getScrollResponder().scrollTo({ x: this.state.scroll, y: 0, animated: true });
  }

  onNavigatorEvent = (event) => {
    switch (event.id) {
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }
    async getKey() {
      try {
        const value = await AsyncStorage.getItem('@MySuperStore:key');
        this.setState({ myKey: JSON.parse(value) });
        const scrollpos = JSON.parse(value);
        this._scrollView.getScrollResponder().scrollTo({ x: scrollpos, y: 0, animated: true });
      } catch (error) {
        console.log('Error retrieving data' + error);
      }
    }

    async saveKey(value) {
      try {
        await AsyncStorage.setItem('@MySuperStore:key', JSON.stringify(value));
      } catch (error) {
        console.log('Error saving data' + error);
      }
    }

  handleScroll = function (event: Object) {
     this.saveKey(event.nativeEvent.contentOffset.x);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={false}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          horizontal={true}
          bounces={false}
          overScrollMode='always'
          onScroll={this.handleScroll}
        >
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile1}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile2}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile3}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile4}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile5}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile6}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile7}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile8}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile9}
          />
          <Image
            style={styles.profileimage}
            source={Images.profiles.profile10}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'darkgray',
  },
  scrollview: {
    flexDirection: 'row',
    height: viewheight,
    alignSelf: 'flex-end'
  },
  profileimage: {
    width: newprofilewidth,
    height: newprofileheight,
    left: 0,
    flex: 1,
  }
});
