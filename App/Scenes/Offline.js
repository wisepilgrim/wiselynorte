import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Button,
  ProgressBarAndroid,
  ProgressViewIOS,
} from 'react-native';
import { unzip, subscribe } from 'react-native-zip-archive';
import RNFS from 'react-native-fs';
import { Colors } from '../Themes';
import { strings } from '../../locales/i18n';


const ZIP_FILE = 'Tiles.zip';
const TILE_URL_IOS = strings('offline.TILE_URL_IOS');
const TILE_URL = strings('offline.TILE_URL');

export default class Offline extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      download: {
        status: false,
        progress: -1,
        message: strings('offline.iagree')
      }
    };
    this.props.navigator.setTitle({
      title: strings('offline.title')
    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }


  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }
  onNavigatorEvent = (event) => {
    switch (event.id) {
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }


  download = () => {
      this.setState({
        download: {
          status: true,
          message: strings('offline.downloading'),
          progress: 0
        }
      });
     if(Platform.OS === 'android') {
       this.copyFromURL(RNFS.ExternalDirectoryPath);
     } else {
       this.copyFromURL(RNFS.DocumentDirectoryPath);
     }
  }

  readDirectory = (path) => {
    RNFS.readDir(path)
      .then((result) => {
        let count = 0;
        for (i = 0; i < result.length; i++) {
            if (result[i].name === 'Tiles') {
              count++;
              break;
            }
        }
        if (count > 0) {
          this.setState({
            download: {
              status: true,
              progress: 100,
              message: strings('offline.done')
            }
          });
        }
      });
  }

  copyFromAsset = () => {
      RNFS.getAllExternalFilesDirs()
        .then((dirs) => {
          var path = dirs[0] + '/myfile.zip';
          RNFS.copyFileAssets('mytiles.zip', path)
            .then((result) => {
                unzip(path, dirs[0]).then((done) => {
                  if(RNFS.exists(path)){
                    RNFS.unlink(path);
                  }
                });
            });
      });
  }

  copyFromURL = (path) => {
      RNFS.downloadFile({
          fromUrl: (Platform.OS == 'android') ? TILE_URL : TILE_URL_IOS,
          //fromUrl: TILE_URL,
          toFile: path + "/" + ZIP_FILE,
          progressDivider: 0,
          background: true,
          begin: (res) => {
            this.setState({
              download: {
                status: true,
                message: strings('offline.downloading'),
                progress: 0
              }
            })
          },
          progress: (res) => {
            var progress = Math.round(res.bytesWritten / res.contentLength * 100);
            this.setState({
              download: {
                status: true,
                progress: progress,
                message: strings('offline.downloading')
              }
            })
          }
        }).promise.then((result) => {
          if(result.statusCode === 200){
            this.setState({
              download: {
                status: true,
                progress: 100,
                message: strings('offline.unpacking')
              }
            });

            let filePath = path + "/"+ ZIP_FILE;
            this.unZipProgress = subscribe(({ progress, filePath }) => {
              this.setState({
                download: {
                  status: true,
                  progress: Math.round(progress * 100),
                  message: strings('offline.unpacking')
                }
              });
            });

            unzip(filePath, path)
              .then((done) => {
                RNFS.unlink(path + "/"+ ZIP_FILE);
                this.setState({
                  download: {
                    status: true,
                    progress: 100,
                    message: strings('offline.done')
                  }
                });
                this.unZipProgress.remove();
            });
          }
        }).catch((error) => {
          this.setState({
            download: {
              status: false,
              progress: -1,
              message: strings('offline.failedretry')
            }
          });
          if (RNFS.isResumable(1)) {
              RNFS.resumeDownload(1)
          }
        });
  }

  render() {
    const { download } = this.state;
    const buttonTitle = download.message;
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollview}>
        <Text style={styles.title}>
          {strings('offline.downloading_title1')}
        </Text>
        <Text style={styles.text}>
          {strings('offline.downloading_text1')}
        </Text>
        <Text style={styles.title}>
          {strings('offline.downloading_title2')}
        </Text>
        <Text style={styles.text}>
          {strings('offline.downloading_text2')}
        </Text>
        <Button
          style={styles.button}
          onPress={this.download}
          title={buttonTitle}
          color={Colors.fire}
          disabled={download.status}
        />
        {
          download.progress >= 0 && download.progress < 100 ?
            <View style={styles.progressView}>
              <Text style={styles.progressText}>
                {download.message + " - " +
                 download.progress + "%"}
              </Text>
              {
                Platform.OS === "android" ?
                  <ProgressBarAndroid
                    style={styles.progress}
                    color={Colors.black}
                    styleAttr="Horizontal"
                    progress={download.progress / 100}
                  /> :
                  <ProgressViewIOS
                    style={styles.progress}
                    progress={download.progress / 100}
                    progressTintColor={Colors.black}
                    progressViewStyle="bar"
                    trackTintColor={Colors.whitish}
                  />
              }
            </View>
            : null
        }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    alignSelf: 'baseline',
    backgroundColor: Colors.snow
  },
  title: {
    fontSize: 28,
    paddingTop: 10,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
  },
  scrollview: {
    paddingBottom: 100
  },
  text: {
    fontSize: 16,
    textAlign: 'justify',
    justifyContent: 'center',
    marginBottom: 20
  },
  header: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center'
  },
  progressView: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20
  },
  progressText: {
    color: Colors.black,
    fontSize: 22
  },
  progress: {
    width: '100%',
    alignItems: 'center',
    marginTop: 10
  },
  button: {

    backgroundColor: Colors.snow
  }
});
