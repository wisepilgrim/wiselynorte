import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
import { Colors, Images } from '../Themes';
import { strings } from '../../locales/i18n';

export default class About extends Component<{}> {
  constructor(props) {
    super(props);
    this.props.navigator.setTitle({
      title: strings('legend.title')
    });
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch (event.id) {
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          ref={(view) => this._scrollView = view}
          contentContainerStyle={styles.scrollview}
          showsVerticalScrollIndicator={true}
          alwaysBounceVertical={false}
          scrollEnabled={true}
          bounces={true}
          horizontal={false}
          overScrollMode='always'
          showsHorizontalScrollIndicator={false}
        >
          <View style={styles.innercontainer}>
          <Text style={styles.header}>{strings('legend.mappins')}</Text>

            <View style={styles.iconsetvert}>
              <Image style={styles.iconwide} source={Images.markers.citydistance} />
              <Text style={styles.text}>{strings('legend.distance')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.city} />
              <Text style={styles.text}>{strings('legend.city')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.alto} />
              <Text style={styles.text}>{strings('legend.alto')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.provinceBorder} />
              <Text style={styles.text}>{strings('legend.provinceBorder')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.fountain} />
              <Text style={styles.text}>{strings('legend.fountain')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.ermita} />
              <Text style={styles.text}>{strings('legend.ermita')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.church} />
              <Text style={styles.text}>{strings('legend.church')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.cathedral} />
              <Text style={styles.text}>{strings('legend.cathedral')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.monument} />
              <Text style={styles.text}>{strings('legend.monument')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.museum} />
              <Text style={styles.text}>{strings('legend.museum')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.bridge} />
              <Text style={styles.text}>{strings('legend.bridge')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.castle} />
              <Text style={styles.text}>{strings('legend.castle')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.cruceiro} />
              <Text style={styles.text}>{strings('legend.cruceiro')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.ruin} />
              <Text style={styles.text}>{strings('legend.ruin')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.poi} />
              <Text style={styles.text}>{strings('legend.poi')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.notice} />
              <Text style={styles.text}>{strings('legend.notice')}</Text>
            </View>
            <Text style={styles.header}>{strings('legend.alberguetypes')}</Text>

            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.municipal} />
              <Text style={styles.text}>{strings('legend.municipal')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.xunta} />
              <Text style={styles.text}>{strings('legend.xunta')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.private} />
              <Text style={styles.text}>{strings('legend.private')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.association} />
              <Text style={styles.text}>{strings('legend.association')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.parochial} />
              <Text style={styles.text}>{strings('legend.parochial')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.camping} />
              <Text style={styles.text}>{strings('legend.camping')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.polideportivo} />
              <Text style={styles.text}>{strings('legend.polideportivo')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.bombeiro} />
              <Text style={styles.text}>{strings('legend.bombeiro')}</Text>
            </View>
            <Text style={styles.header}>{strings('legend.privateaccommodation')}</Text>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.parador} />
              <Text style={styles.text}>{strings('legend.parador')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.hotel} />
              <Text style={styles.text}>{strings('legend.hotel')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.hostal} />
              <Text style={styles.text}>{strings('legend.hostal')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.pension} />
              <Text style={styles.text}>{strings('legend.pension')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.hotelRural} />
              <Text style={styles.text}>{strings('legend.hotelRural')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.casaRural} />
              <Text style={styles.text}>{strings('legend.casaRural')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.markers.apartment} />
              <Text style={styles.text}>{strings('legend.apartment')}</Text>
            </View>
            <Text style={styles.header}>{strings('legend.albergueservices')}</Text>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.hostelBT} />
              <Text style={styles.text}>{strings('legend.bedprice')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.openBT} />
              <Text style={styles.text}>{strings('legend.open')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.closeBT} />
              <Text style={styles.text}>{strings('legend.close')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.icon} source={Images.popup.calBT} />
              <Text style={styles.text}>{strings('legend.cal')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.pilgrimBT} />
              <Text style={styles.text}>{strings('legend.pilgrim')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.hotelBT} />
              <Text style={styles.text}>{strings('legend.privateroom')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.reserveBT} />
              <Text style={styles.text}>{strings('legend.reserve')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.barrestBT} />
              <Text style={styles.text}>{strings('legend.barrestalbergue')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.kitchenBT} />
              <Text style={styles.text}>{strings('legend.kitchen')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.familyBT} />
              <Text style={styles.text}>{strings('legend.family')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.vegBT} />
              <Text style={styles.text}>{strings('legend.veg')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.washerBT} />
              <Text style={styles.text}>{strings('legend.washer')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.wifiBT} />
              <Text style={styles.text}>{strings('legend.wifi')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.bikeBT} />
              <Text style={styles.text}>{strings('legend.bike')}</Text>
            </View>
            <Text style={styles.header}>{strings('legend.cityservices')}</Text>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.hostelBT} />
              <Text style={styles.text}>{strings('legend.hasshared')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.hotelBT} />
              <Text style={styles.text}>{strings('legend.hasprivate')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.campingBT} />
              <Text style={styles.text}>{strings('legend.hascamping')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.hospitalBT} />
              <Text style={styles.text}>{strings('legend.hospital')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.pharmacyBT} />
              <Text style={styles.text}>{strings('legend.pharmacy')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.barrestBT} />
              <Text style={styles.text}>{strings('legend.barrest')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.supermarketBT} />
              <Text style={styles.text}>{strings('legend.supermarket')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.atmBT} />
              <Text style={styles.text}>{strings('legend.atm')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.correosBT} />
              <Text style={styles.text}>{strings('legend.correos')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.busterminalBT} />
              <Text style={styles.text}>{strings('legend.busterminal')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.busBT} />
              <Text style={styles.text}>{strings('legend.bus')}</Text>
            </View>
            <View style={styles.iconset}>
              <Image style={styles.iconBT} source={Images.popup.trainBT} />
              <Text style={styles.text}>{strings('legend.train')}</Text>
            </View>
            <View style={styles.iconset}>
              <Text style={styles.text}>{strings('legend.credit')}</Text>
            </View>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  innercontainer: {
    width: Dimensions.get('window').width,
  },
  iconset: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  iconsetvert: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginBottom: 16,
    paddingRight: 10
  },
  text: {
    fontSize: 16,
    flex: 1,
    marginLeft: 10,
    textAlign: 'justify',
  },
  icon: {
    height: 50,
    width: 50,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    resizeMode: 'contain'
  },
  iconwide: {
    width: 180,
    height: 25,
    resizeMode: 'contain',
    marginLeft: 10,
    marginBottom: 10,
  },
  iconBT: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
    marginRight: 19,
    marginLeft: 19,
    marginBottom: 10
  },
  header: {
    fontSize: 28,
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 10,
    marginBottom: 30,
    marginLeft: 10,
    textAlign: 'left',

  },
  scrollview: {
    flexDirection: 'column',
    //height: viewheight,
    alignSelf: 'flex-start',

  },
});
