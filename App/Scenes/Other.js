import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image
} from 'react-native';
import { Images } from '../Themes';


export default class Other extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  onNavigatorEvent = (event) => {
    switch (event.id) {
        case 'sideMenu':
        this.props.navigator.toggleDrawer({
          side: 'left',
          animated: true
        });
        break;
    }
  }

  casaivar = () => {
    this.props.navigator.handleDeepLink({
      link: 'stuff.casaivar',
      payload: 'Casa Ivar',

    });
  }

  stoneboat = () => {
    this.props.navigator.handleDeepLink({
      link: 'stuff.stoneboat',
      payload: 'The Stone Boat',

    });
  }


  thebigmap = () => {
    this.props.navigator.handleDeepLink({
      link: 'stuff.thebigmap',
      payload: 'The Big Map',

    });
  }


  thebook = () => {
    this.props.navigator.handleDeepLink({
      link: 'stuff.thebook',
      payload: 'The Camino Francés Book',

    });
  }


  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.casaivar}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.casaivar}
            />
        </TouchableOpacity>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.stoneboat}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.stoneboat}
            />
        </TouchableOpacity>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.thebigmap}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.thebigmap}
            />
        </TouchableOpacity>
        <TouchableOpacity style={styles.TouchableOpacity} onPress={this.thebook}>
            <Image
              style={styles.opacityImage}
              source={Images.drawer.thebook}
            />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
  },
  group1: {
    flex: 1/6,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  group2: {
    flex: 2/7,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  group3: {
    flex: 2/6,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },

  headerImage: {
    resizeMode: 'contain',
    flex: 1,
    height: null,
    width: 180,
    marginTop: 30
  },
  TouchableOpacity: {
    flex: 1
  },

  opacityImage: {
    resizeMode: 'contain',
    height: null,
    flex: 1,
    width: 180
  },

});
