import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { Colors } from '../Themes';
import { strings } from '../../locales/i18n';

export default class Drawer extends Component<{}> {
  constructor(props) {
    Text.defaultProps.allowFontScaling = false;
    super(props);
  }

  offline = () => {
    this.props.navigator.handleDeepLink({
      link: 'offline',
      payload: ''
    });
  }

  home = () => {
    this.props.navigator.handleDeepLink({
      link: 'home',
      payload: ''
    });
  }

  about = () => {
    this.props.navigator.handleDeepLink({
      link: 'about',
      payload: ''
    });
  }

  legend = () => {
    this.props.navigator.handleDeepLink({
      link: 'legend',
      payload: ''
    });
  }

  elevation = () => {
    this.props.navigator.handleDeepLink({
      link: 'elevation',
      payload: ''
    });
  }

  casaivar = () => {
    this.props.navigator.handleDeepLink({
      link: 'casaivar',
      payload: ''
    });
  }

  thebigmap = () => {
    this.props.navigator.handleDeepLink({
      link: 'thebigmap',
      payload: ''
    });
  }

  thebook = () => {
    this.props.navigator.handleDeepLink({
      link: 'thebook',
      payload: ''
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.group1}>
          <Text style={styles.header}>WISELY:</Text>
          <Text style={styles.headersub}>{strings('drawer.header')}</Text>
        </View>


        <View style={styles.group2}>
          <Text style={styles.menuItem} onPress={this.home}>{strings('drawer.offlinemap')}</Text>
          <Text style={styles.menuItem} onPress={this.elevation}>{strings('drawer.elevation')}</Text>
          <Text style={styles.menuItem} onPress={this.legend}>{strings('drawer.legend')}</Text>
          <Text style={styles.menuItem} onPress={this.offline}>{strings('drawer.downloadmap')}</Text>
        </View>

        <View style={styles.group3}>
          <Text style={styles.menuitemOtherHeader}>{strings('drawer.otherstuff')}</Text>
          <Text style={styles.menuitemOtherHeader2}>{strings('drawer.worthsharing')}</Text>
          <Text style={styles.menuitemOther} onPress={this.casaivar}>{strings('drawer.casaivar')}</Text>
          <Text style={styles.menuitemOther} onPress={this.thebook}>{strings('drawer.thebook')}</Text>
          <Text style={styles.menuitemOther} onPress={this.thebigmap}>{strings('drawer.thebigmap')}</Text>
        </View>


        {/*
        <View style={styles.group3} >
          <TouchableOpacity style={styles.TouchableOpacity} onPress={this.other}  >
            <Text style={styles.menuitemOther} >OTHER STUFF</Text>
            <Text style={styles.menuitemOther} >WORTH SHARING</Text>
          </TouchableOpacity>
        </View>
        */}

    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    paddingLeft: 20,
    paddingRight: 5
  },
  group1: {
    flex: 4/16,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  group2: {
    flex: 7/16,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  group3: {
    flex: 5/16,
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  TouchableOpacity: {
    flex: 1
  },
  header: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 60,
    color: strings('global.routecolor'),
    textAlign: 'left',
    marginTop: 40,
  },
  headersub: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 24,
    marginBottom: 30,
    paddingTop: 5,
    color: strings('global.routecolor'),
    textAlign: 'left',
  },
  menuItem: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 32,
    marginBottom: 0,
    paddingTop: 8
  },
  menuitemOtherHeader: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 24,
    color: '#808080',
  },
  menuitemOtherHeader2: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    fontSize: 24,
    color: '#808080',
    marginBottom: 5
  },
  menuitemOther: {
    fontFamily: (Platform.OS === 'android') ? 'wisepilgrim_regular' : 'wisepilgrim',
    marginTop: 0,
    fontSize: 26,
  },

});
